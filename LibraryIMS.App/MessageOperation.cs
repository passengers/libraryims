﻿using LibraryIMS.App.Utilities;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class MessageOperation : Form
    {
        private readonly IContactService _contactService;
        private readonly IUserService _userService;
        private int _userId;

        public MessageOperation(IContactService contactService, int userId, IUserService userService)
        {
            _userId = userId;
            _contactService = contactService;
            _userService = userService;
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtContent.Text))
            {
                Alert.Show("Mesajınızı girin!", ResultStatus.Error);
            }
            else
            {
                var entity = new ContactAddDto
                {
                    Content = txtContent.Text,
                    UserId = _userId,
                    GeneralStatus = GeneralStatus.Active,
                };
                var result = _contactService.AddContact(entity, _userService.GetUser(_userId).Data.User.UserName);
                if (result.ResultStatus == ResultStatus.Success)
                    Alert.Show(result.Message, ResultStatus.Success);
                else
                    Alert.Show(result.Message, ResultStatus.Error);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
