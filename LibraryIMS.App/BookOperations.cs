﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class BookOperations : Form
    {

        #region Field

        private readonly IBookService _bookService;
        private readonly IImageHelper _imageHelper;
        private readonly IWriterService _writerService;
        private readonly IPublisherService _publisherService;
        private readonly IBookCategoryService _bookCategoryService;
        private readonly ICategoryService _categoryService;


        // Gridde seçilen kullanıcının Id bilgisi
        private int _bookId = -1;
        private int _writerId = -1;
        private int _publisherId = -1;
        private string _fileName = string.Empty;

        #endregion Field

        #region Constructor
        public BookOperations(IBookService bookService, IImageHelper imageHelper, IWriterService writerService, IPublisherService publisherService, IBookCategoryService bookCategoryService, ICategoryService categoryService)
        {
            _bookService = bookService;
            _imageHelper = imageHelper;
            _writerService = writerService;
            _publisherService = publisherService;
            _categoryService = categoryService;
            _bookCategoryService = bookCategoryService;
            InitializeComponent();
            BindComboBox();
            BindBookList();

        }

        #endregion Constructor

        #region Methods
        private void BindBookList(IList<Book> listBook = null)
        {
            if (listBook == null) listBook = GetAllBookByNonDeleted();

            gcBook.DataSource = listBook;
            gvBook.Columns[2].Visible = false;
            gvBook.Columns[7].Visible = false;
            gvBook.Columns[8].Visible = false;
            gvBook.Columns[9].Visible = false;
            gvBook.Columns[10].Visible = false;
            gvBook.Columns[11].Visible = false;
            gvBook.Columns[12].Visible = false;
            gvBook.Columns[13].Visible = false;
            gvBook.Columns[14].Visible = false;
            gvBook.Columns[15].Visible = false;
            gvBook.Columns[16].Visible = false;
            gvBook.Columns[17].Visible = false;
            gvBook.Columns[18].Visible = false;
            gvBook.Columns[19].Visible = false;
            gvBook.Columns[20].Visible = false;
            gvBook.Columns[21].Visible = false;
            gvBook.Columns[22].Visible = false;
            gvBook.Columns[23].Visible = false;

            panelGrid.Visible = listBook.Count > 0;
            lblMessage.Text = $"{listBook.Count} adet ziyaret listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            var book = _bookService.GetAllBookByNonDeleted();
            var writers = _writerService.GetAllWriterByNonDeleted();
            var publishers = _publisherService.GetAllPublisher();
            var categories = _categoryService.GetAllCategoryByNonDeleted();
            cbStatus.Properties.Items.AddRange(Enum.GetValues(typeof(GeneralStatus)));

            if (book.ResultStatus == ResultStatus.Success)
            {
                foreach (var writer in writers.Data.Writers)
                {
                    cbWriters.Properties.Items.Add(writer.Name);
                    cbWriters1.Properties.Items.Add(writer.Name);
                }
                foreach (var publisher in publishers.Data.Publishers)
                {
                    cbPublisher.Properties.Items.Add(publisher.Name);
                }
                foreach (var category in categories.Data.Categories)
                {
                    cbCategory.Properties.Items.Add(category.Name);
                }
            }
            else Alert.Show(book.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Tüm aktif kitapların listesini getir.
        /// </summary>
        private IList<Book> GetAllBookByNonDeleted()
        {
            var books = _bookService.GetAllBookByNonDeleted();
            if (books.ResultStatus == ResultStatus.Success)
                return books.Data.Books;
            return null;
        }

    
        /// <summary>
        /// Gridde seçilen kitapların bilgilerini getir.
        /// </summary>
        private void GetBookObject()
        {
            if (gvBook.SelectedRowsCount > 0)
            {
                int.TryParse(gvBook.GetRowCellValue(gvBook.FocusedRowHandle, "Id").ToString(), out _bookId);
                var book = _bookService.GetBook(_bookId);

                if (book.ResultStatus == ResultStatus.Success)
                {
                    _writerId = Convert.ToInt32(gvBook.GetRowCellValue(gvBook.FocusedRowHandle, "WriterId"));
                    _publisherId = Convert.ToInt32(gvBook.GetRowCellValue(gvBook.FocusedRowHandle, "PublisherId"));
                    var writer = _writerService.GetWriter(_writerId);
                    var publisher = _publisherService.GetPublisher(_publisherId);


                    txtName.Text = book.Data.Book.Name;
                    cbWriters.SelectedItem = writer.Data.Writer.Name.ToString();
                    cbPublisher.SelectedItem = publisher.Data.Publisher.Name.ToString();
                    txtPageNum.Text = Convert.ToString(book.Data.Book.NumberOfPages);
                    txtShelfNum.Text = Convert.ToString(book.Data.Book.Place);
                    txtStock.Text = Convert.ToString(book.Data.Book.Stock);
                    txtReleaseDate.DateTime = Convert.ToDateTime(book.Data.Book.ReleaseDate);
                    cbStatus.SelectedItem = Enum.Parse(typeof(GeneralStatus), book.Data.Book.GeneralStatus.ToString());
                    txtAbout.Text = book.Data.Book.Description;
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{book.Data.Book.Thumbnail}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(book.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Sisteme yeni kitap ekler.
        /// </summary>
        private void AddBook()
        {
            if (!FormControls.CheckFormControls(gbBook))
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            IAppResult<ImageUploadedDto> result = new AppResult<ImageUploadedDto>().Fail("Resim yok");

            if (_fileName != "")
                result = _imageHelper.Upload(txtName.Text, _fileName, PictureType.Book);

            var book = new BookAddDto
            {
                Name = txtName.Text,
                NumberOfPages = Convert.ToInt32(txtPageNum.Text),
                WriterId = cbWriters.SelectedIndex + 1,
                PublisherId = cbPublisher.SelectedIndex + 1,
                Stock = Convert.ToInt32(txtStock.Text),
                ReleaseDate = txtReleaseDate.DateTime,
                Description = txtAbout.Text,
                Place = txtShelfNum.Text,
                Thumbnail = result.ResultStatus == ResultStatus.Success
                ? result.Data.FullName
                : "bookImages/defaultBook.png",
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem,

            };
            var added = _bookService.AddBook(book, "Admin");
            if (added.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(added.Message, ResultStatus.Success);
                BindBookList();
                ClearItems();
            }
            else Alert.Show(added.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Sistemdeki bir kitabı günceller.
        /// </summary>
        private void UpdateBook()
        {
            if (!FormControls.CheckFormControls(gbBook))
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }

            var book = new BookUpdateDto
            {
                Id = _bookId,
                Name = txtName.Text,
                WriterId = cbWriters.SelectedIndex + 1,
                PublisherId = cbPublisher.SelectedIndex + 1,
                NumberOfPages = Convert.ToInt32(txtPageNum.Text),
                Stock = Convert.ToInt32(txtStock.Text),
                ReleaseDate = txtReleaseDate.DateTime,
                Description = txtAbout.Text,
                Place = txtShelfNum.Text,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            bool isNewPictureUploaded = false;
            var oldBookPicture = _bookService.GetBook(_bookId).Data.Book.Thumbnail;
            if (_fileName != string.Empty)
            {
                var uploadedImageDtoResult = _imageHelper.Upload(txtName.Text, _fileName, PictureType.Book);
                book.Thumbnail = uploadedImageDtoResult.ResultStatus == ResultStatus.Success
                ? uploadedImageDtoResult.Data.FullName
                : "bookImages/defaultBook.png";
                if (oldBookPicture != "bookImages/defaultBook.png")
                {
                    isNewPictureUploaded = true;
                }
            }
            else
                book.Thumbnail = oldBookPicture;
            var updatedBook = _bookService.UpdateBook(book, "Admin");
            if (updatedBook.ResultStatus == ResultStatus.Success)
            {
                if (isNewPictureUploaded)
                {
                    _imageHelper.Delete(oldBookPicture);
                }
                Alert.Show(updatedBook.Message, ResultStatus.Success);
                BindBookList();
                ClearItems();
            }
            else Alert.Show(updatedBook.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Seçilen kitabı kaldırır.
        /// </summary>
        /// 
        private void DeleteBook(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Kitap kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _bookService.DeleteBook(_bookId, "Admin");
                Alert.Show(result.Message, ResultStatus.Success);
                ClearItems();
                BindBookList();
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _bookId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Kitap adı veya yazarına göre kitap ara.
        /// </summary>
        private void SearchBook()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindBookList();

            var books = _bookService.FindBooksByText(searchText);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Kategori adına göre kitap ara.
        /// </summary>
        private void SearchBooksByCategory()
        {
            var books = _bookCategoryService.FindBooksByCategoryName(cbCategory.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Yazar adına göre kitap ara.
        /// </summary>
        private void SearchBooksByWriter()
        {
            var books = _bookService.FindBooksByWriterName(cbWriters1.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Stokta olmayan kitapları getir.
        /// </summary>
        private void GetAllBookByNonStock()
        {
            var books = _bookService.GetAllBookByNonStock();
            if (books.ResultStatus == ResultStatus.Success)
                BindBookList(books.Data.Books);
            else BindBookList();
        }

        #endregion Methods

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_bookId > -1) UpdateBook();
            else AddBook();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            if (_bookId > -1) DeleteBook(sender, e);
            else Alert.Show(Messages.User.NotFound(isPlural: false), ResultStatus.Error);
        }

        private void btnImage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "JPEG|*.jpg", ValidateNames = true, Multiselect = false })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    _fileName = ofd.FileName;
                    var stream = new FileStream(_fileName, FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
            }
        }

        #endregion Buttons

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "member.xls";
            if (sfd.ShowDialog() == DialogResult.OK) gcBook.ExportToXls(sfd.FileName);
        }

        #endregion LinkLabel

        #region GridControl
        /// <summary>
        /// Grid üzerindeki kitaplara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcBook_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var contextMenu = new ContextMenu();
                contextMenu.MenuItems.Add(new MenuItem("Kitabı Kaldır", DeleteBook));

                int rowIndex = gvBook.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvBook.ClearSelection();
                    gvBook.SelectRow(rowIndex);
                    _bookId = Convert.ToInt32(gvBook.GetRowCellValue(rowIndex, "Id"));
                    contextMenu.Show(gcBook, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        private void gvBook_RowCellClick_1(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetBookObject();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick_1(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        #region Events

        private void txtSearch_TextChanged_1(object sender, EventArgs e)
        {
            this.SearchBook();
        }

        private void ceAllBook_CheckedChanged(object sender, EventArgs e)
        {
            if (ceAllBook.Checked) GetAllBookByNonStock();
            else BindBookList();
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCategory.SelectedIndex != -1)
            {
                SearchBooksByCategory();
            }
        }

        private void cbWriters1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWriters1.SelectedIndex != -1)
            {
                SearchBooksByWriter();
            }
        }

        #endregion
    }
}
