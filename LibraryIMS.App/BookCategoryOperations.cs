﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using static System.String;

namespace LibraryIMS.App
{
    public partial class BookCategoryOperations : Form
    {
        #region Field

        private readonly ICategoryService _categoryService;
        private readonly IBookService _bookService;
        private readonly IBookCategoryService _bookCategoryService;
        private int _bookId = -1;
        private int _categoryId = -1;

        #endregion Field

        #region Constructor

        public BookCategoryOperations(ICategoryService categoryService, IBookService bookService, IBookCategoryService bookCategoryService)
        {
            _categoryService = categoryService;
            _bookService = bookService;
            _bookCategoryService = bookCategoryService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Form Load

        private void BookCategoryOperations_Load(object sender, EventArgs e)
        {
            BindBookList();
            BindComboBox();
        }

        #endregion Form Load

        #region Methods

        private void BindBookList(IList<Book> listBook = null)
        {
            if (listBook == null) listBook = GetAllBookByNonDeleted();

            gcBook.DataSource = listBook.OrderBy(b=>b.Name);
            gvBook.Columns[2].Visible = false;
            gvBook.Columns[7].Visible = false;
            gvBook.Columns[8].Visible = false;
            gvBook.Columns[9].Visible = false;
            gvBook.Columns[10].Visible = false;
            gvBook.Columns[11].Visible = false;
            gvBook.Columns[12].Visible = false;
            gvBook.Columns[13].Visible = false;
            gvBook.Columns[14].Visible = false;
            gvBook.Columns[15].Visible = false;
            gvBook.Columns[16].Visible = false;
            gvBook.Columns[17].Visible = false;
            gvBook.Columns[18].Visible = false;
            gvBook.Columns[19].Visible = false;
            gvBook.Columns[20].Visible = false;
            gvBook.Columns[21].Visible = false;
            gvBook.Columns[22].Visible = false;
            gvBook.Columns[23].Visible = false;
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            cbAllCategory.Properties.Items.Clear(); cbSearchBookByCategory.Properties.Items.Clear();
            var categories = _categoryService.GetAllCategoryByNonDeleted();
            if (categories.ResultStatus == ResultStatus.Success)
                foreach (var category in categories.Data.Categories)
                {
                    cbAllCategory.Properties.Items.Add(category.Name);
                    cbSearchBookByCategory.Properties.Items.Add(category.Name);
                }
            else Alert.Show(categories.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Kategorisi olmayan kitapların listesini getir.
        /// </summary>
        private void GetAllBookByNonTagged()
        {
            var booksNonTagged = _bookCategoryService.GetAllBookByNonTagged();
            if (booksNonTagged.ResultStatus == ResultStatus.Success)
                 BindBookList(booksNonTagged.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Tüm kitapların listesini getir.
        /// </summary>
        private IList<Book> GetAllBookByNonDeleted()
        {
            var books = _bookService.GetAllBookByNonDeleted();
            if (books.ResultStatus == ResultStatus.Success)
                return books.Data.Books;
            return null;
        }

        /// <summary>
        /// Silinecek kategorinin bilgisini alır ve comboboxa doldurur.
        /// </summary>
        private void GetDeletingCategory()
        {
            if (gvCategory.SelectedRowsCount > 0)
            {
                lblDeleted.Text = "Silinecek Kategori";
                lblDeleted.Font=new Font("Microsoft Sans Serif", 12,FontStyle.Bold);
                lblDeleted.ForeColor = Color.DarkRed;
                int.TryParse(gvCategory.GetRowCellValue(gvCategory.FocusedRowHandle, "Id").ToString(), out _categoryId);
                var category = _categoryService.GetCategory(_categoryId);
                if (category.ResultStatus==ResultStatus.Success)
                {
                    cbAllCategory.Enabled = false;
                    cbAllCategory.SelectedItem = category.Data.Category.Name;
                }
                else Alert.Show(category.Message,ResultStatus.Error);
            }
        }

        /// <summary>
        /// Kitaba ait kategorileri gride doldurur.
        /// </summary>
        private void FillCategoryList()
        {
            if (gvBook.SelectedRowsCount <= 0) return;
            int.TryParse(gvBook.GetRowCellValue(gvBook.FocusedRowHandle, "Id").ToString(), out _bookId);
            var categories = _bookCategoryService.GetCategoriesByBookId(_bookId);
            if (categories.ResultStatus == ResultStatus.Success)
            {
                if (categories.Data.Categories.Count > 0)
                {
                    gcCategory.DataSource = categories.Data.Categories.OrderBy(c => c.ParentId);
                    gvCategory.Columns[0].Visible = false;
                    gvCategory.Columns[1].Visible = false;
                    gvCategory.Columns[3].Visible = false;
                    gvCategory.Columns[4].Visible = false;
                }
                else
                {
                    gcCategory.DataSource = null;
                    Alert.Show("İlgili kitaba ait kategori(ler) bulunamadı", ResultStatus.Info);
                }

            }
            else Alert.Show(categories.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Sisteme yeni kitap kategorisi ekler.
        /// </summary>
        private void AddBookCategory()
        {
            var category = _categoryService.GetCategoryByName(cbAllCategory.SelectedItem.ToString());
            if (category.ResultStatus==ResultStatus.Success)
            {
                var bookCategory = new BookCategoryAddDto
                {
                    BookId = _bookId,
                    CategoryId = category.Data.Category.Id
                };
                var added = _bookCategoryService.AddBookCategory(bookCategory);
                if (added.ResultStatus == ResultStatus.Success)
                {
                    Alert.Show(added.Message, ResultStatus.Success);
                    BindBookList();
                    ClearItems();
                }
                else Alert.Show(added.Message, ResultStatus.Error);
            }
            else Alert.Show(category.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Seçilen kitabın kategorisini veritabanından kaldırır.
        /// </summary>
        private void HardDeleteBookCategory()
        {
            var category = _categoryService.GetCategory(_categoryId);
            var book = _bookService.GetBook(_bookId);
            if (category.ResultStatus==ResultStatus.Success)
            {
                if (book.ResultStatus==ResultStatus.Success)
                {
                    if (XtraMessageBox.Show(
                        $"{book.Data.Book.Name} adlı kitaba ait {category.Data.Category.Name} kategorisi veritabanından kaldırılacaktır. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
                    if (XtraMessageBox.Show(
                        "Bağlı olduğu alt kategoriler varsa onlar da silinecektir. Devam etmek istiyor musunuz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
                    var result = _bookCategoryService.HardDeleteBookCategory(_categoryId, _bookId);
                    var parents = _categoryService.GetParentCategories(_categoryId);
                    if (result.ResultStatus == ResultStatus.Success)
                    {
                        if (parents.ResultStatus == ResultStatus.Success)
                        {
                            var count = parents.Data.Categories.Count;
                            for (var i = 0; i < count; i++)
                            {
                                _bookCategoryService.HardDeleteBookCategory(parents.Data.Categories[i].Id,_bookId);
                            }
                            Alert.Show(result.Message, ResultStatus.Success);
                            BindBookList();
                            ClearItems();
                        }
                        else
                        {
                            Alert.Show(result.Message, ResultStatus.Info);
                        }
                    }
                    else
                        Alert.Show(result.Message, ResultStatus.Info);
                }
                else
                    Alert.Show(book.Message, ResultStatus.Error);
            }
            else
                Alert.Show(category.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _categoryId = -1;
            _bookId = -1;
            lblDeleted.Text = "Tüm Kategoriler:";
            lblDeleted.Font=new Font("Microsoft Sans Serif",11,FontStyle.Bold);
            lblDeleted.ForeColor=Color.Black;
            cbAllCategory.SelectedIndex = -1;
            cbSearchBookByCategory.SelectedIndex = -1;
            ceAllBook.Checked = false;
            gcCategory.DataSource = null;
            cbAllCategory.Enabled = true;
        }

        /// <summary>
        /// Kategori adına göre kitap ara.
        /// </summary>
        private void SearchBooksByCategory()
        {
            var books = _bookCategoryService.FindBooksByCategoryName(cbSearchBookByCategory.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Kitap adına göre ara.
        /// </summary>
        private void SearchBooksByBookName()
        {
            string searchText = txtBookName.Text;
            if (IsNullOrEmpty(searchText)) {BindBookList(); return;}

            var books = _bookService.FindBooksByText(searchText);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        #endregion Methods

        #region Buttons

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_categoryId > -1 && _bookId > -1) HardDeleteBookCategory();
            else Alert.Show(Messages.BookCategories.NotClicked(), ResultStatus.Error);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cbAllCategory.SelectedIndex!=-1 && _bookId>-1) AddBookCategory();
            else Alert.Show(Messages.BookCategories.NotClicked(), ResultStatus.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        #endregion Buttons

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "booksUnTagged.xls";
            if (sfd.ShowDialog() == DialogResult.OK) gcBook.ExportToXls(sfd.FileName);
        }

        #endregion LinkLabel

        #region Events

        private void cbSearchBookByCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSearchBookByCategory.SelectedIndex != -1)
            {
                SearchBooksByCategory();
            }
        }

        private void txtBookName_TextChanged(object sender, EventArgs e)
        {
            SearchBooksByBookName();
        }

        private void ceAllBook_CheckedChanged(object sender, EventArgs e)
        {
            if (ceAllBook.Checked) GetAllBookByNonTagged();
            else BindBookList();
        }

        #endregion Events

        #region GridControl

        private void gvBook_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            FillCategoryList();
        }
        private void gvCategory_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetDeletingCategory();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer
    }
}
