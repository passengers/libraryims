﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;

namespace LibraryIMS.App
{
    public partial class UserRoleOperations : Form
    {
        #region Fields

        private readonly IUserService _userService;
        private int _userId = -1;

        #endregion Fields

        #region Constructor

        public UserRoleOperations(IUserService userService)
        {
            _userService = userService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Form Load

        private void UserRoleOperations_Load(object sender, EventArgs e)
        {
            BindUserList();
            BindComboBox();
        }

        #endregion Form Load

        #region Methods

        private void BindUserList(IList<User> listUser = null)
        {
            if (listUser == null) listUser = GetAllUserByNonDeleted();

            gcUser.DataSource = listUser.OrderBy(u=>u.AccessStatus);
            gvUser.Columns[3].Visible = false;
            gvUser.Columns[4].Visible = false;
            gvUser.Columns[5].Visible = false;
            gvUser.Columns[7].Visible = false;
            gvUser.Columns[8].Visible = false;
            gvUser.Columns[10].Visible = false;
            gvUser.Columns[11].Visible = false;
            gvUser.Columns[12].Visible = false;
            gvUser.Columns[13].Visible = false;
            gvUser.Columns[14].Visible = false;
            gvUser.Columns[15].Visible = false;
            gvUser.Columns[16].Visible = false;
            gvUser.Columns[18].Visible = false;
            gvUser.Columns[20].Visible = false;
            lblMessage.Text = $"{listUser.Count} adet kullanıcı listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            cbRole.Properties.Items.Clear(); cbStatus.Properties.Items.Clear();
            cbRole.Properties.Items.AddRange(Enum.GetValues(typeof(AccessStatus)));
            cbStatus.Properties.Items.AddRange(Enum.GetValues(typeof(AccessStatus)));
        }

        /// <summary>
        /// Tüm aktif kullanıcıların listesini getir.
        /// </summary>
        private IList<User> GetAllUserByNonDeleted()
        {
            var users = _userService.GetAllUserByNonDeleted();
            if (users.ResultStatus == ResultStatus.Success)
                return users.Data.Users;
            return null;
        }

        /// <summary>
        /// Gridde seçilen kullanıcı bilgilerini getir.
        /// </summary>
        private void GetUserObject()
        {
            if (gvUser.SelectedRowsCount > 0)
            {
                int.TryParse(gvUser.GetRowCellValue(gvUser.FocusedRowHandle, "Id").ToString(), out _userId);
                var user = _userService.GetUser(_userId);
                if (user.ResultStatus == ResultStatus.Success)
                {
                    cbRole.SelectedItem = Enum.Parse(typeof(AccessStatus), user.Data.User.AccessStatus.ToString());
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{user.Data.User.Picture}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(user.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Sistemdeki kullanıcı rolünü günceller.
        /// </summary>
        private void UpdateUserRole()
        {
            if (cbRole.SelectedIndex==-1)
            {
                Alert.Show("Lütfen rol seçimi yapınız..", ResultStatus.Warning);
                return;
            }

            var user = _userService.GetUser(_userId);
            if (user.ResultStatus==ResultStatus.Success)
                user.Data.User.AccessStatus = (AccessStatus) cbRole.SelectedItem;
            else Alert.Show(user.Message,ResultStatus.Error);
            var updatedUserRole = _userService.UpdateUserRole(new UserDto{User = user.Data.User}, "Admin");
            if (updatedUserRole.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(updatedUserRole.Message, ResultStatus.Success);
                ClearItems();
            }
            else Alert.Show(updatedUserRole.Message, ResultStatus.Error);
        }
        
        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _userId = -1;
            cbStatus.SelectedIndex = -1;
            pictureBox1.Image = null;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Role göre kullanıcı ara.
        /// </summary>
        private void SearchUser()
        {
            var users = _userService.FindUsersByRole((AccessStatus)cbStatus.SelectedItem);
            if (users.ResultStatus == ResultStatus.Success) BindUserList(users.Data.Users);
            else BindUserList();
        }

        #endregion Methods

        #region Buttons

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateUserRole();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Buttons

        #region GridControl

        private void gvUser_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetUserObject();
        }

        #endregion GridControl

        #region Events

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStatus.SelectedIndex!=-1)
            {
                SearchUser();
            }
            else BindUserList();
        }

        #endregion Events
    }
}
