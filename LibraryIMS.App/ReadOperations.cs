﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class ReadOperations : Form
    {
        #region Field
        private readonly IUserBookService _userbookService;
        private readonly IImageHelper _imageHelper;
        private readonly IUserService _userService;
        private readonly IWriterService _writerService;
        private readonly ICategoryService _categoryService;
        private readonly IBookService _bookService;
        private readonly IPublisherService _publisherService;
        private readonly IBookCategoryService _bookCategoryService;

        private int _publisherId = -1;
        private int _userId = -1;
        private int _bookId = -1;
        private int _writerId = -1;
        private string _fileName = string.Empty;

        #endregion Field

        #region Constructor
        public ReadOperations(IUserBookService userbookService, IUserService userService, IWriterService writerService, IBookService bookService, IPublisherService publisherService, IImageHelper imageHelper, int userId, ICategoryService categoryService, IBookCategoryService bookCategoryService)
        {
            _categoryService = categoryService;
            _bookCategoryService = bookCategoryService;
            _userId = userId;
            _bookService = bookService;
            _imageHelper = imageHelper;
            _writerService = writerService;
            _publisherService = publisherService;
            _userbookService = userbookService;
            _userService = userService;
            InitializeComponent();
            BindBookList();
            BindComboBox();
        }

        #endregion Constructor

        #region Methods
        private void BindBookList(IList<Book> listBook = null)
        {
            if (listBook == null) listBook = GetAllReadBooksByNonDeleted();

            gcArchive.DataSource = listBook;
            gvArchive.Columns[2].Visible = false;
            gvArchive.Columns[7].Visible = false;
            gvArchive.Columns[8].Visible = false;
            gvArchive.Columns[9].Visible = false;
            gvArchive.Columns[10].Visible = false;
            gvArchive.Columns[11].Visible = false;
            gvArchive.Columns[12].Visible = false;
            gvArchive.Columns[13].Visible = false;
            gvArchive.Columns[14].Visible = false;
            gvArchive.Columns[15].Visible = false;
            gvArchive.Columns[16].Visible = false;
            gvArchive.Columns[17].Visible = false;
            gvArchive.Columns[18].Visible = false;
            gvArchive.Columns[19].Visible = false;
            gvArchive.Columns[20].Visible = false;
            gvArchive.Columns[21].Visible = false;
            gvArchive.Columns[22].Visible = false;
            gvArchive.Columns[23].Visible = false;

            //panelGrid.Visible = listBook.Count > 0;
           // lblMessage.Text = $"{listBook.Count} adet kitap listeleniyor.  ";
        }
        private void GetBookObject()
        {
            if (gvArchive.SelectedRowsCount > 0)
            {
                lblControl.Visible = true;
                lblControl2.Visible = true;
                lblControl3.Visible = true;
                int.TryParse(gvArchive.GetRowCellValue(gvArchive.FocusedRowHandle, "Id").ToString(), out _bookId);
                var book = _bookService.GetBook(_bookId);

                if (book.ResultStatus == ResultStatus.Success)
                {
                    _writerId = Convert.ToInt32(gvArchive.GetRowCellValue(gvArchive.FocusedRowHandle, "WriterId"));
                    _publisherId = Convert.ToInt32(gvArchive.GetRowCellValue(gvArchive.FocusedRowHandle, "PublisherId"));
                    var writer = _writerService.GetWriter(_writerId);
                    var publisher = _publisherService.GetPublisher(_publisherId);

                    lblNumberOfReads.Text = Convert.ToString(book.Data.Book.NumberOfReads);
                    lblNumberOfComment.Text = Convert.ToString(book.Data.Book.NumberOfComments);
                    lblNumberOfFavorite.Text = Convert.ToString(book.Data.Book.NumberOfFavorites);
                    txtName.Text = book.Data.Book.Name;
                    cbWriters.SelectedItem = writer.Data.Writer.Name.ToString();
                    cbPublisher.SelectedItem = publisher.Data.Publisher.Name.ToString();
                    txtPageNum.Text = Convert.ToString(book.Data.Book.NumberOfPages);
                    txtShelfNum.Text = Convert.ToString(book.Data.Book.Place);
                    txtStock.Text = Convert.ToString(book.Data.Book.Stock);
                    txtReleaseDate.DateTime = Convert.ToDateTime(book.Data.Book.ReleaseDate);

                    txtAbout.Text = book.Data.Book.Description;
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{book.Data.Book.Thumbnail}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(book.Message, ResultStatus.Error);
            }
        }

        private void SearchBook()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindBookList();

            var books = _bookService.FindBooksByText(searchText);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        private void SearchBooksByCategory()
        {
            var books = _bookCategoryService.FindBooksByCategoryName(cbCategories.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        private void SearchBooksByWriter()
        {
            var books = _bookService.FindBooksByWriterName(cbWriters1.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        private void GetAllBookByNonStock()
        {
            var books = _bookService.GetAllBookByNonStock();
            if (books.ResultStatus == ResultStatus.Success)
                BindBookList(books.Data.Books);
            else BindBookList();
        }

        private void ClearItems()
        {
            _bookId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            FormControls.ClearFormControls(this);
        }

        private void HardDeleteUserBook()
        {
            if (XtraMessageBox.Show("Okuduklarımdan kaldırılacak. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _userbookService.HardDeleteUserBook(_userId,_bookId);
                Alert.Show(result.Message, ResultStatus.Success);
                ClearItems();
            }
        }

        private void BindComboBox()
        {
            var book = _bookService.GetAllBookByNonDeleted();
            var writers = _writerService.GetAllWriterByNonDeleted();
            var publishers = _publisherService.GetAllPublisherByNonDeleted();
            var categories = _categoryService.GetAllCategoryByNonDeleted();

            if (book.ResultStatus == ResultStatus.Success)
            {
                foreach (var writer in writers.Data.Writers)
                {
                    cbWriters.Properties.Items.Add(writer.Name);
                }
                foreach (var publisher in publishers.Data.Publishers)
                {
                    cbPublisher.Properties.Items.Add(publisher.Name);
                }
                foreach (var writer in writers.Data.Writers)
                {
                    cbWriters1.Properties.Items.Add(writer.Name);
                }
                foreach (var category in categories.Data.Categories)
                {
                    cbCategories.Properties.Items.Add(category.Name);
                }
            }
            else Alert.Show(book.Message, ResultStatus.Error);
        }


        private IList<Book> GetAllReadBooksByNonDeleted()
        {
            var books = _userbookService.GetAllReadBooksByUser(_userId);
            if (books.ResultStatus == ResultStatus.Success)
                return books.Data.Books;
            return null;
        }


        #endregion Methods

        #region Button

        private void btnRead_Click(object sender, EventArgs e)
        {
            HardDeleteUserBook();

        }

        private void ReadOperations_Load(object sender, EventArgs e)
        {

        }
        

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }
        #endregion Button

        #region LinkLabel

        private void lnkExcel_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "member.xls";
            if (sfd.ShowDialog() == DialogResult.OK) gcArchive.ExportToXls(sfd.FileName);
        }

        #endregion LinkLabel

        #region GridControl

        private void gvArchive_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetBookObject();
        }
        #endregion GridControl

        #region Timer
        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }
        #endregion Timer 

        #region Events

        private void cbWriters1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbWriters1.SelectedIndex != -1)
            {
                SearchBooksByWriter();
            }
        }

        private void cbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCategories.SelectedIndex != -1)
            {
                SearchBooksByCategory();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchBook();
        }
        #endregion Events
    }

}
