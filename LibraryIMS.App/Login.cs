﻿using LibraryIMS.App.Utilities;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Services.Abstract;
using System;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class Login : Form
    {
        #region Fields

        private readonly IUserService _userService;
        public string UserName { get; set; }
        public bool IsConnected { get; set; }

        #endregion Fields

        #region Constructor

        public Login(IUserService userService)
        {
            _userService = userService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Buttons

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var userName = txtUsername.Text;
            var password = txtPassword.Text != "" ? PasswordHelper.Encrypt(txtPassword.Text) : null;
            var user = _userService.GetUserByUserName(userName);

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                Alert.Show("Kullanıcı adı ya da şifre boş bırakılamaz.", ResultStatus.Warning);
                return;
            }

            if (user.ResultStatus == ResultStatus.Success)
            {
                if (user.Data.User.Password != password)
                {
                    Alert.Show("Kullanıcı adı ya da şifre hatalı.", ResultStatus.Error);
                    return;
                }

                if (user.Data.User.GeneralStatus != GeneralStatus.Active)
                {
                    Alert.Show("Sisteme giriş yapmanız yasaklanmıştır.", ResultStatus.Error);
                    return;
                }
                DialogResult = DialogResult.OK;
                IsConnected = true;
                UserName = userName;
            }
            else
                Alert.Show(user.Message, ResultStatus.Error);
        }

        #endregion Buttons

        #region Events

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnLogin.PerformClick();
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnLogin.PerformClick();
        }

        #endregion Events
    }
}