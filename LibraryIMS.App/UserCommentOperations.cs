﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class UserCommentOperations : Form
    {
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private int _commentId = -1;
        private int _userId;

        public UserCommentOperations(ICommentService commentService, IUserService userService, int userId)
        {
            _commentService = commentService;
            _userService = userService;
            _userId = userId;
            InitializeComponent();
        }

        private void txtBookName_TextChanged(object sender, EventArgs e)
        {
            SearchComment();
        }

        private void SearchComment()
        {
            string searchText = txtBookName.Text;
            if (String.IsNullOrEmpty(searchText)) { BindCommentList(); return; }

            var comments = _commentService.FindCommentsByUserNameAndBookName(_userService.GetUser(_userId).Data.User.UserName, searchText);
            if (comments.ResultStatus == ResultStatus.Success) BindCommentList(comments.Data.Comments);
            else BindCommentList();
        }

        private void BindCommentList(IList<Comment> listComment = null)
        {
            if (listComment == null) listComment = GetAllCommentsByNonDeleted();
            var _comment = from a in listComment
                         select new
                       {
                           Id = a.Id,
                           UserId = a.UserId,
                           BookId = a.BookId,
                           BookName = a.Book.Name,
                           Yorum = a.CommentText,
                           OlusturmaTarihi = a.CreatedDate
                       };
            gcBooks.DataSource = _comment;
            gvBooks.Columns[0].Visible = false;
            gvBooks.Columns[1].Visible = false;
            gvBooks.Columns[2].Visible = false;
        }

        private IList<Comment> GetAllCommentsByNonDeleted()
        {
            var comments = _commentService.FindCommentsByUserName(_userService.GetUser(_userId).Data.User.UserName);
            if (comments.ResultStatus == ResultStatus.Success)
                return comments.Data.Comments;
            return null;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_commentId > -1) DeleteComment(sender, e);
            else Alert.Show(Messages.Comment.NotFound(isPlural: false), ResultStatus.Error);
        }
        private void DeleteComment(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Yorumunuz kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _commentService.DeleteComment(_commentId, _userService.GetUser(_userId).Data.User.UserName);
                Alert.Show(result.Message, ResultStatus.Success);
                ClearItems();
                BindCommentList();
            }
        }
        private void ClearItems()
        {
            _commentId = -1;
            FormControls.ClearFormControls(this);
        }

        private void gvBooks_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetCommentObject();
        }
        private void GetCommentObject()
        {
            if (gvBooks.SelectedRowsCount > 0)
            {
                int.TryParse(gvBooks.GetRowCellValue(gvBooks.FocusedRowHandle, "Id").ToString(), out _commentId);
                var comment = _commentService.GetComment(_commentId);

                if (comment.ResultStatus == ResultStatus.Success)
                {
                    textComment.Text = comment.Data.Comment.CommentText;
                    textRating.Text = "NULL";
                }
                else
                    Alert.Show(comment.Message, ResultStatus.Error);
            }
        }

        private void UserCommentOperations_Load(object sender, EventArgs e)
        {
            BindCommentList();
        }
    }
}
