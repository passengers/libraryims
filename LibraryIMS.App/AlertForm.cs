﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class AlertForm : Form
    {
        public enum Action
        {
            Wait,
            Start,
            Close
        }
        public AlertForm()
        {
            InitializeComponent();
        }

        private int x, y;
        private Action _action;

        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (_action)
            {
                case Action.Wait:
                    timer1.Interval = 5000;
                    _action = Action.Close;
                    break;
                case Action.Start:
                    timer1.Interval = 1;
                    Opacity += 0.1;
                    if (x < Location.X) Left--;
                    else if (Opacity == 1.0) _action = Action.Wait;
                    break;
                case Action.Close:
                    timer1.Interval = 1;
                    Opacity -= 0.1;
                    Left -= 3;
                    if (Opacity == 0.0) Close();
                    break;
            }
        }
        public void ShowAlert(string message, ResultStatus resultStatus)
        {
            Opacity = 0.0;
            StartPosition = FormStartPosition.Manual;
            for (int i = 1; i < 10; i++)
            {
                var alertName = "Uyarı " + i.ToString();
                AlertForm alertForm = (AlertForm)Application.OpenForms[alertName];
                if (alertForm == null)
                {
                    Name = alertName;
                    x = Screen.PrimaryScreen.WorkingArea.Width - Width + 15;
                    y = Screen.PrimaryScreen.WorkingArea.Height - (Height * i) - (5 * i);
                    Location = new Point(x, y);
                    break;
                }
            }
            x = Screen.PrimaryScreen.WorkingArea.Width - Width - 5;

            switch (resultStatus)
            {
                case ResultStatus.Success:
                    btnSuccess.Visible = true;
                    BackColor = Color.SeaGreen;
                    break;
                case ResultStatus.Error:
                    btnError.Visible = true;
                    BackColor = Color.Brown;
                    break;
                case ResultStatus.Warning:
                    btnWarning.Visible = true;
                    BackColor = Color.Peru;
                    break;
                case ResultStatus.Info:
                    btnInfo.Visible = true;
                    BackColor = Color.SteelBlue;
                    break;
            }

            lblMessage.Text = message;
            Show();
            _action = Action.Start;
            timer1.Interval = 1;
            timer1.Start();
        }
    }
}
