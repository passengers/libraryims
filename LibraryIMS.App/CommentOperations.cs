﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using static System.String;

namespace LibraryIMS.App
{
    public partial class CommentOperations : Form
    {
        #region Fields

        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly IBookService _bookService;
        private int _commentId;

        #endregion Fields

        #region FormLoad

        private void CommentOperations_Load(object sender, EventArgs e)
        {
            BindCommentList();
        }

        #endregion FormLoad

        #region Constructor

        public CommentOperations(ICommentService commentService, IBookService bookService, IUserService userService)
        {
            _commentService = commentService;
            _bookService = bookService;
            _userService = userService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        private void BindCommentList(IList<Comment> listComment = null)
        {
            if (listComment == null) listComment = GetAllCommentByNonDeleted();

            gcComment.DataSource = listComment.OrderBy(c => c.CreatedDate);
            gvComment.Columns[0].Visible = false;
            gvComment.Columns[1].Visible = false;
            gvComment.Columns[2].Visible = false;
            gvComment.Columns[3].Visible = false;
            gvComment.Columns[5].Visible = false;
            gvComment.Columns[6].Visible = false;
            gvComment.Columns[8].Visible = false;
            gvComment.Columns[10].Visible = false;
            panelGrid.Visible = listComment.Count > 0;
            lblMessage.Text = $"{listComment.Count} adet yorum listeleniyor.  ";
        }

        /// <summary>
        /// Tüm aktif yorumların listesini getir.
        /// </summary>
        private void GetAllCommentByNonApproved()
        {
            var comments = _commentService.GetAllCommentByNonApproved();
            if (comments.ResultStatus == ResultStatus.Success)
            {
                if (comments.Data.Comments.Count > 0)
                {
                    BindCommentList(comments.Data.Comments); return;
                }
            }
            Alert.Show("Onay bekleyen mesaj bulunamadı.", ResultStatus.Info);
        }

        /// <summary>
        /// Tüm yorumların listesini getir.
        /// </summary>
        private IList<Comment> GetAllCommentByNonDeleted()
        {
            var comments = _commentService.GetAllCommentByNonDeleted();
            if (comments.ResultStatus == ResultStatus.Success)
                return comments.Data.Comments;
            return null;
        }

        /// <summary>
        /// Gridde seçilen yorum bilgilerini getir.
        /// </summary>
        private void GetCommentObject()
        {
            if (gvComment.SelectedRowsCount > 0)
            {
                int.TryParse(gvComment.GetRowCellValue(gvComment.FocusedRowHandle, "Id").ToString(), out _commentId);
                var comment = _commentService.GetComment(_commentId);
                if (comment.ResultStatus == ResultStatus.Success)
                {
                    var user = _userService.GetUser(comment.Data.Comment.UserId);
                    var book = _bookService.GetBook(comment.Data.Comment.BookId);
                    txtFirstName.Text = user.Data.User.FirstName;
                    txtLastName.Text = user.Data.User.LastName;
                    txtBookName.Text = book.Data.Book.Name;
                    txtContent.Text = comment.Data.Comment.CommentText;
                    dateCommentDate.DateTime = comment.Data.Comment.CreatedDate;
                }
                else
                    Alert.Show(comment.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Sistemdeki yorumu onaylar.
        /// </summary>
        private void ApproveComment()
        {
            if (txtContent.Text == null)
            {
                Alert.Show("İçerik boş olamaz.", ResultStatus.Warning);
                return;
            }
            var approvedComment = _commentService.ApproveComment(_commentId, "Admin");
            if (approvedComment.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(approvedComment.Message, ResultStatus.Success);
                BindCommentList(); ;
                ClearItems();
            }
            else Alert.Show(approvedComment.Message, ResultStatus.Info);
        }

        /// <summary>
        /// Seçilen yorumu kaldırır.
        /// </summary>
        private void DeleteComment(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Yorum kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _commentService.DeleteComment(_commentId, "Admin");
                Alert.Show(result.Message, ResultStatus.Success);
                BindCommentList();
                ClearItems();
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _commentId = -1;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Kullanıcı ad ve soyada göre yorum ara.
        /// </summary>
        private void SearchCommentByUser()
        {
            string searchText = txtSearchByUser.Text;
            if (IsNullOrEmpty(searchText)) BindCommentList();

            var comments = _commentService.FindCommentsByUserName(searchText);
            if (comments.ResultStatus == ResultStatus.Success) BindCommentList(comments.Data.Comments);
            else BindCommentList();
        }

        /// <summary>
        /// Kitap adına göre yorum ara.
        /// </summary>
        private void SearchCommentByBook()
        {
            string searchText = txtSearchByBook.Text;
            if (IsNullOrEmpty(searchText)) BindCommentList();

            var comments = _commentService.FindCommentsByBookName(searchText);
            if (comments.ResultStatus == ResultStatus.Success) BindCommentList(comments.Data.Comments);
            else BindCommentList();
        }

        #endregion Methods

        #region Buttons

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_commentId > -1) DeleteComment(sender, e);
            else Alert.Show(Messages.Comment.NotFound(isPlural: false), ResultStatus.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_commentId > -1) ApproveComment();
            else
            {
                Alert.Show("Lütfen bir yorum seçiniz.", ResultStatus.Error);
            }
        }

        #endregion Buttons

        #region Events

        private void txtSearchByUser_TextChanged(object sender, EventArgs e)
        {
            SearchCommentByUser();
        }

        private void txtSearchByBook_TextChanged(object sender, EventArgs e)
        {
            SearchCommentByBook();
        }

        private void ceAllComment_CheckedChanged(object sender, EventArgs e)
        {
            if (ceAllComment.Checked) GetAllCommentByNonApproved();
            else BindCommentList(); ;
        }

        #endregion Events

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog { Filter = "Excel Documents (*.xls)|*.xls", FileName = "comments.xls" };
            if (sfd.ShowDialog() == DialogResult.OK) gcComment.ExportToXls(sfd.FileName);
        }

        #endregion

        #region GridControl

        /// <summary>
        /// Grid üzerindeki yorumlara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcComment_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var contextMenu = new ContextMenu();
                contextMenu.MenuItems.Add(new MenuItem("Yorumu Kaldır", DeleteComment));

                int rowIndex = gvComment.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvComment.ClearSelection();
                    gvComment.SelectRow(rowIndex);
                    _commentId = Convert.ToInt32(gvComment.GetRowCellValue(rowIndex, "Id"));
                    contextMenu.Show(gcComment, new Point(e.X, e.Y));
                }
            }
        }

        private void gvComment_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetCommentObject();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer
    }
}
