﻿
namespace LibraryIMS.App
{
    partial class BookOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookOperations));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.btnImage = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.panelTop = new System.Windows.Forms.Panel();
            this.ceAllBook = new DevExpress.XtraEditors.CheckEdit();
            this.cbCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.cbWriters1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lnkExcel = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.lblClock = new System.Windows.Forms.Label();
            this.gbBook = new System.Windows.Forms.GroupBox();
            this.cbWriters = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbPublisher = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtStock = new DevExpress.XtraEditors.TextEdit();
            this.txtShelfNum = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReleaseDate = new DevExpress.XtraEditors.DateEdit();
            this.cbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtPageNum = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.writerlbl = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAbout = new DevExpress.XtraEditors.MemoEdit();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.gcBook = new DevExpress.XtraGrid.GridControl();
            this.gvBook = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllBook.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters1.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.gbBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelfNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbout.Properties)).BeginInit();
            this.panelBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).BeginInit();
            this.panelGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnImage
            // 
            this.btnImage.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnImage.Appearance.Options.UseFont = true;
            this.btnImage.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnImage.Location = new System.Drawing.Point(213, 220);
            this.btnImage.Name = "btnImage";
            this.btnImage.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnImage.Size = new System.Drawing.Size(58, 23);
            this.btnImage.TabIndex = 89;
            this.btnImage.Text = "Seç";
            this.btnImage.Click += new System.EventHandler(this.btnImage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(20, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 192);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 88;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(1001, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 17);
            this.label15.TabIndex = 85;
            this.label15.Text = "Açıklama:";
            // 
            // btnClear
            // 
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(1471, 138);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(121, 36);
            this.btnClear.TabIndex = 13;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.ceAllBook);
            this.panelTop.Controls.Add(this.cbCategory);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.cbWriters1);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Controls.Add(this.txtSearch);
            this.panelTop.Controls.Add(this.label10);
            this.panelTop.Controls.Add(this.panel2);
            this.panelTop.Controls.Add(this.gbBook);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1629, 311);
            this.panelTop.TabIndex = 95;
            // 
            // ceAllBook
            // 
            this.ceAllBook.Location = new System.Drawing.Point(1127, 279);
            this.ceAllBook.Name = "ceAllBook";
            this.ceAllBook.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ceAllBook.Properties.Appearance.Options.UseFont = true;
            this.ceAllBook.Properties.Caption = "Stokta Olmayanları Gizle";
            this.ceAllBook.Size = new System.Drawing.Size(213, 22);
            this.ceAllBook.TabIndex = 121;
            this.ceAllBook.CheckedChanged += new System.EventHandler(this.ceAllBook_CheckedChanged);
            // 
            // cbCategory
            // 
            this.cbCategory.Location = new System.Drawing.Point(787, 279);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Properties.AllowFocused = false;
            this.cbCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCategory.Properties.NullValuePrompt = "Kategoriler";
            this.cbCategory.Size = new System.Drawing.Size(185, 20);
            this.cbCategory.TabIndex = 94;
            this.cbCategory.SelectedIndexChanged += new System.EventHandler(this.cbCategory_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(688, 280);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 17);
            this.label4.TabIndex = 94;
            this.label4.Text = "Kategoriler:";
            // 
            // cbWriters1
            // 
            this.cbWriters1.Location = new System.Drawing.Point(481, 280);
            this.cbWriters1.Name = "cbWriters1";
            this.cbWriters1.Properties.AllowFocused = false;
            this.cbWriters1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriters1.Properties.NullValuePrompt = "Yazarlar";
            this.cbWriters1.Size = new System.Drawing.Size(185, 20);
            this.cbWriters1.TabIndex = 94;
            this.cbWriters1.SelectedIndexChanged += new System.EventHandler(this.cbWriters1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(401, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 93;
            this.label1.Text = "Yazarlar:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(114, 279);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(259, 20);
            this.txtSearch.TabIndex = 90;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(29, 280);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 17);
            this.label10.TabIndex = 92;
            this.label10.Text = "Kitap Ara:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lnkExcel);
            this.panel2.Controls.Add(this.lblClock);
            this.panel2.Location = new System.Drawing.Point(1346, 274);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(258, 30);
            this.panel2.TabIndex = 91;
            // 
            // lnkExcel
            // 
            this.lnkExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lnkExcel.Appearance.Options.UseFont = true;
            this.lnkExcel.Location = new System.Drawing.Point(3, 8);
            this.lnkExcel.Name = "lnkExcel";
            this.lnkExcel.Size = new System.Drawing.Size(38, 17);
            this.lnkExcel.TabIndex = 90;
            this.lnkExcel.Text = "Excel";
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(63, 8);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(183, 18);
            this.lblClock.TabIndex = 18;
            this.lblClock.Text = "29 Aralık 2020 24:24:24";
            // 
            // gbBook
            // 
            this.gbBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBook.Controls.Add(this.cbWriters);
            this.gbBook.Controls.Add(this.cbPublisher);
            this.gbBook.Controls.Add(this.txtStock);
            this.gbBook.Controls.Add(this.txtShelfNum);
            this.gbBook.Controls.Add(this.label2);
            this.gbBook.Controls.Add(this.txtReleaseDate);
            this.gbBook.Controls.Add(this.cbStatus);
            this.gbBook.Controls.Add(this.btnImage);
            this.gbBook.Controls.Add(this.pictureBox1);
            this.gbBook.Controls.Add(this.label15);
            this.gbBook.Controls.Add(this.btnClear);
            this.gbBook.Controls.Add(this.btnDelete);
            this.gbBook.Controls.Add(this.btnSave);
            this.gbBook.Controls.Add(this.txtPageNum);
            this.gbBook.Controls.Add(this.txtName);
            this.gbBook.Controls.Add(this.label3);
            this.gbBook.Controls.Add(this.label16);
            this.gbBook.Controls.Add(this.label6);
            this.gbBook.Controls.Add(this.label8);
            this.gbBook.Controls.Add(this.label14);
            this.gbBook.Controls.Add(this.writerlbl);
            this.gbBook.Controls.Add(this.label13);
            this.gbBook.Controls.Add(this.txtAbout);
            this.gbBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbBook.Location = new System.Drawing.Point(12, 17);
            this.gbBook.Name = "gbBook";
            this.gbBook.Size = new System.Drawing.Size(1601, 251);
            this.gbBook.TabIndex = 1;
            this.gbBook.TabStop = false;
            this.gbBook.Text = "Kitap Bilgileri";
            // 
            // cbWriters
            // 
            this.cbWriters.Location = new System.Drawing.Point(436, 81);
            this.cbWriters.Name = "cbWriters";
            this.cbWriters.Properties.AllowFocused = false;
            this.cbWriters.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriters.Properties.NullValuePrompt = "Yazar";
            this.cbWriters.Size = new System.Drawing.Size(185, 20);
            this.cbWriters.TabIndex = 3;
            // 
            // cbPublisher
            // 
            this.cbPublisher.Location = new System.Drawing.Point(435, 127);
            this.cbPublisher.Name = "cbPublisher";
            this.cbPublisher.Properties.AllowFocused = false;
            this.cbPublisher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPublisher.Properties.NullValuePrompt = "Yayınevi";
            this.cbPublisher.Size = new System.Drawing.Size(185, 20);
            this.cbPublisher.TabIndex = 4;
            // 
            // txtStock
            // 
            this.txtStock.Location = new System.Drawing.Point(761, 78);
            this.txtStock.Name = "txtStock";
            this.txtStock.Properties.AllowFocused = false;
            this.txtStock.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtStock.Properties.NullValuePrompt = "Stok Sayısı";
            this.txtStock.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtStock.Size = new System.Drawing.Size(214, 22);
            this.txtStock.TabIndex = 7;
            // 
            // txtShelfNum
            // 
            this.txtShelfNum.Location = new System.Drawing.Point(761, 35);
            this.txtShelfNum.Name = "txtShelfNum";
            this.txtShelfNum.Properties.AllowFocused = false;
            this.txtShelfNum.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtShelfNum.Properties.NullValuePrompt = "Raf Numarası";
            this.txtShelfNum.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtShelfNum.Size = new System.Drawing.Size(214, 22);
            this.txtShelfNum.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(642, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 93;
            this.label2.Text = "Yayın Tarihi:";
            // 
            // txtReleaseDate
            // 
            this.txtReleaseDate.EditValue = null;
            this.txtReleaseDate.Location = new System.Drawing.Point(761, 125);
            this.txtReleaseDate.Name = "txtReleaseDate";
            this.txtReleaseDate.Properties.AllowFocused = false;
            this.txtReleaseDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtReleaseDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtReleaseDate.Properties.CalendarTimeProperties.Mask.EditMask = "";
            this.txtReleaseDate.Properties.CalendarTimeProperties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtReleaseDate.Properties.Mask.EditMask = "";
            this.txtReleaseDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtReleaseDate.Properties.NullValuePrompt = "Yayın Tarihi";
            this.txtReleaseDate.Size = new System.Drawing.Size(214, 20);
            this.txtReleaseDate.TabIndex = 8;
            // 
            // cbStatus
            // 
            this.cbStatus.Location = new System.Drawing.Point(761, 172);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Properties.AllowFocused = false;
            this.cbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbStatus.Properties.NullValuePrompt = "Durum";
            this.cbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbStatus.Size = new System.Drawing.Size(214, 20);
            this.cbStatus.TabIndex = 9;
            // 
            // btnDelete
            // 
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(1471, 87);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDelete.Size = new System.Drawing.Size(121, 36);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Kaldır";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(1471, 36);
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnSave.Size = new System.Drawing.Size(121, 36);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Kaydet";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPageNum
            // 
            this.txtPageNum.Location = new System.Drawing.Point(435, 173);
            this.txtPageNum.Name = "txtPageNum";
            this.txtPageNum.Properties.AllowFocused = false;
            this.txtPageNum.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtPageNum.Properties.MaxLength = 8;
            this.txtPageNum.Properties.NullValuePrompt = "Sayfa Sayısı";
            this.txtPageNum.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtPageNum.Size = new System.Drawing.Size(186, 22);
            this.txtPageNum.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(435, 35);
            this.txtName.Name = "txtName";
            this.txtName.Properties.AllowFocused = false;
            this.txtName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtName.Properties.NullValuePrompt = "Adı";
            this.txtName.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtName.Size = new System.Drawing.Size(186, 22);
            this.txtName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(302, 175);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 27;
            this.label3.Text = "Sayfa Sayısı:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(302, 128);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 17);
            this.label16.TabIndex = 83;
            this.label16.Text = "Yayınevi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(642, 37);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "Raf Numarası:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(642, 175);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Durum:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(302, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 17);
            this.label14.TabIndex = 80;
            this.label14.Text = "Adı:";
            // 
            // writerlbl
            // 
            this.writerlbl.AutoSize = true;
            this.writerlbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.writerlbl.Location = new System.Drawing.Point(302, 80);
            this.writerlbl.Name = "writerlbl";
            this.writerlbl.Size = new System.Drawing.Size(59, 17);
            this.writerlbl.TabIndex = 79;
            this.writerlbl.Text = "Yazarı:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(642, 82);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 17);
            this.label13.TabIndex = 79;
            this.label13.Text = "Stok Sayısı:";
            // 
            // txtAbout
            // 
            this.txtAbout.Location = new System.Drawing.Point(1087, 37);
            this.txtAbout.Name = "txtAbout";
            this.txtAbout.Properties.AllowFocused = false;
            this.txtAbout.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtAbout.Properties.NullValuePrompt = "Açıklama";
            this.txtAbout.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtAbout.Size = new System.Drawing.Size(356, 158);
            this.txtAbout.TabIndex = 10;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.lblMessage);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 721);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1629, 104);
            this.panelBottom.TabIndex = 97;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMessage.Location = new System.Drawing.Point(1629, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 18);
            this.lblMessage.TabIndex = 1;
            // 
            // gcBook
            // 
            this.gcBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcBook.Location = new System.Drawing.Point(16, 326);
            this.gcBook.MainView = this.gvBook;
            this.gcBook.Name = "gcBook";
            this.gcBook.Size = new System.Drawing.Size(1601, 380);
            this.gcBook.TabIndex = 85;
            this.gcBook.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBook});
            // 
            // gvBook
            // 
            this.gvBook.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvBook.Appearance.Empty.Options.UseBackColor = true;
            this.gvBook.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvBook.Appearance.Row.Options.UseFont = true;
            this.gvBook.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvBook.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Gold;
            formatConditionRuleExpression1.Appearance.BackColor2 = System.Drawing.Color.MediumPurple;
            formatConditionRuleExpression1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Appearance.Options.UseFont = true;
            formatConditionRuleExpression1.Expression = "[GeneralStatus] = 1";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.MediumPurple;
            formatConditionRuleExpression2.Appearance.BackColor2 = System.Drawing.Color.Gold;
            formatConditionRuleExpression2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Appearance.Options.UseFont = true;
            formatConditionRuleExpression2.Expression = "[Stock] == 0";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.gvBook.FormatRules.Add(gridFormatRule1);
            this.gvBook.FormatRules.Add(gridFormatRule2);
            this.gvBook.GridControl = this.gcBook;
            this.gvBook.Name = "gvBook";
            this.gvBook.OptionsBehavior.Editable = false;
            this.gvBook.OptionsBehavior.ReadOnly = true;
            this.gvBook.OptionsCustomization.AllowFilter = false;
            this.gvBook.OptionsFind.AllowFindPanel = false;
            this.gvBook.OptionsMenu.EnableColumnMenu = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvBook.OptionsView.ShowGroupPanel = false;
            this.gvBook.OptionsView.ShowIndicator = false;
            this.gvBook.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvBook.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBook_RowCellClick_1);
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gcBook);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 0);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1629, 825);
            this.panelGrid.TabIndex = 96;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick_1);
            // 
            // BookOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 825);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BookOperations";
            this.Text = "BookOperations";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllBook.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters1.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbBook.ResumeLayout(false);
            this.gbBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelfNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbout.Properties)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).EndInit();
            this.panelGrid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnImage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.HyperlinkLabelControl lnkExcel;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.GroupBox gbBook;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtPageNum;
        private DevExpress.XtraEditors.TextEdit txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label writerlbl;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Label lblMessage;
        private DevExpress.XtraGrid.GridControl gcBook;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBook;
        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.Timer timer;
        private DevExpress.XtraEditors.ComboBoxEdit cbStatus;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.DateEdit txtReleaseDate;
        private DevExpress.XtraEditors.TextEdit txtStock;
        private DevExpress.XtraEditors.TextEdit txtShelfNum;
        private DevExpress.XtraEditors.MemoEdit txtAbout;
        private DevExpress.XtraEditors.ComboBoxEdit cbPublisher;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriters;
        private DevExpress.XtraEditors.ComboBoxEdit cbCategory;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriters1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ceAllBook;
    }
}