﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.App
{
    public partial class UserWriterOperations : Form
    {

        #region Fields

        private readonly IImageHelper _imageHelper;
        private readonly IWriterService _writerService;
        private int _writerId = -1;
        private string _fileName = string.Empty;

        #endregion Fields

        #region Constructor

        public UserWriterOperations(IWriterService writerService, IImageHelper imageHelper)
        {
            _writerService = writerService;
            _imageHelper = imageHelper;
            InitializeComponent();
            BindWriterList();
        }

        #endregion Constuctor

        #region Methods

        private void BindWriterList(IList<Writer> listWriter = null)
        {
            if (listWriter == null) listWriter = GetAllWriterByNonDeleted();

            gcWriter.DataSource = listWriter;
            gvWriter.Columns[0].Visible = false;
            gvWriter.Columns[3].Visible = false;
            gvWriter.Columns[5].Visible = false;
            gvWriter.Columns[6].Visible = false;
            gvWriter.Columns[7].Visible = false;
            panelGrid.Visible = listWriter.Count > 0;
            lblMessage.Text = $"{listWriter.Count} adet yazar listeleniyor.  ";
        }

        

        /// <summary>
        /// Tüm aktif yazarların listesini getir.
        /// </summary>
        private IList<Writer> GetAllWriterByNonDeleted()
        {
            var writers = _writerService.GetAllWriterByNonDeleted();
            if (writers.ResultStatus == ResultStatus.Success)
                return writers.Data.Writers;
            return null;
        }

        /// <summary>
        /// Gridde seçilen yazar bilgilerini getir.
        /// </summary>
        private void GetWriterObject()
        {
            if (gvWriter.SelectedRowsCount > 0)
            {
                lblControl.Visible = true;
                int.TryParse(gvWriter.GetRowCellValue(gvWriter.FocusedRowHandle, "Id").ToString(), out _writerId);
                var writer = _writerService.GetWriter(_writerId);
                var writerBookNumber = _writerService.GetNumberOfBooksForWriter(_writerId);
                if (writer.ResultStatus == ResultStatus.Success)
                {
                    lblNumberOfBooks.Text = Convert.ToString(writerBookNumber);
                    txtName.Text = writer.Data.Writer.Name;
                    dateBirth.DateTime = writer.Data.Writer.DateOfBirth;
                    txtBiography.Text = writer.Data.Writer.Biography;
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{writer.Data.Writer.Picture}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(writer.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _writerId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            lblControl.Visible = false;
            lblNumberOfBooks.Visible = false;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Ada göre yazar ara.
        /// </summary>
        private void SearchWriter()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindWriterList();

            var writers = _writerService.FindWritersByText(searchText);
            if (writers.ResultStatus == ResultStatus.Success) BindWriterList(writers.Data.Writers);
            else BindWriterList();
        }


        #endregion Methods

        #region Buttons

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        #endregion Buttons

        #region Events

        private void txtSearch_TextChanged_1(object sender, EventArgs e)
        {
            SearchWriter();
        }
      

        #endregion Events

        #region GridControl

        private void gvWriter_RowCellClick_1(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetWriterObject();
        }

        #endregion GridControl

        #region LinkLabel

        private void lnkExcel_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "writers.xls";
            if (sfd.ShowDialog() == DialogResult.OK) gcWriter.ExportToXls(sfd.FileName);
        }

        #endregion

        #region Timer

        private void timer_Tick_1(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        
    }
}
