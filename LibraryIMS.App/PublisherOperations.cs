﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class PublisherOperations : Form
    {
        private readonly IPublisherService _publisherService;
        private int _publisherId = -1;

        public PublisherOperations(IPublisherService publisherService)
        {
            _publisherService = publisherService;
            InitializeComponent();
        }

        private void PublisherOperations_Load(object sender, EventArgs e)
        {
            BindPublisherList();
            BindComboBox();
        }

        
        private void BindComboBox()
        {
            cbStatus.Properties.Items.Clear();
            cbStatus.Properties.Items.AddRange(Enum.GetValues(typeof(GeneralStatus)));
        }

        private void gvPublisher_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetPublisherObject();
        }
        

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchPublisher();
        }

        #region Methods 
        private void BindPublisherList(IList<Publisher> listPublisher = null)
        {
            if (listPublisher == null) listPublisher = GetAllPublisherByNonDeleted();
            gcPublisher.DataSource = listPublisher;
            gvPublisher.Columns[0].Visible = false;
            gvPublisher.Columns[4].Visible = false;
        }
        private IList<Publisher> GetAllPublisherByNonDeleted()
        {
            var publishers = _publisherService.GetAllPublisherByNonDeleted();
            if (publishers.ResultStatus == ResultStatus.Success)
                return publishers.Data.Publishers;
            return null;
        }
        private void GetPublisherObject()
        {
            if (gvPublisher.SelectedRowsCount > 0)
            {
                int.TryParse(gvPublisher.GetRowCellValue(gvPublisher.FocusedRowHandle, "Id").ToString(), out _publisherId);
                var publisher = _publisherService.GetPublisher(_publisherId);
                if (publisher.ResultStatus == ResultStatus.Success)
                {
                    txtPublisherName.Text = publisher.Data.Publisher.Name;
                    txtPublisherDescription.Text = publisher.Data.Publisher.Description;
                    cbStatus.SelectedItem = Enum.Parse(typeof(GeneralStatus), publisher.Data.Publisher.GeneralStatus.ToString());
                }
                else
                    Alert.Show(publisher.Message, ResultStatus.Error);
            }
        }
        private void AddPublisher()
        {
            if (txtPublisherName.Text == "" || cbStatus.SelectedIndex == -1 || txtPublisherDescription.Text == "")
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            var publisher = new PublisherAddDto
            {
                Name = txtPublisherName.Text,
                Description = txtPublisherDescription.Text,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            var added = _publisherService.AddPublisher(publisher);
            if (added.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(added.Message, ResultStatus.Success);
                BindPublisherList();
                ClearItems();
            }
            else Alert.Show(added.Message, ResultStatus.Error);
        }
        private void UpdatePublisher()
        {
            if (txtPublisherName.Text == "" || cbStatus.SelectedIndex == -1 || txtPublisherDescription.Text == "")
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            var publisher = new PublisherUpdateDto
            {
                Id = _publisherId,
                Name = txtPublisherName.Text,
                Description = txtPublisherDescription.Text,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            var updatedPublisher = _publisherService.UpdatePublisher(publisher);
            if (updatedPublisher.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(updatedPublisher.Message, ResultStatus.Success);
                BindPublisherList();
                ClearItems();
            }
            else Alert.Show(updatedPublisher.Message, ResultStatus.Error);
        }
        private void DeletePublisher(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Yayıncı kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _publisherService.DeletePublisher(_publisherId);
                if (result.ResultStatus == ResultStatus.Success)
                {
                    Alert.Show(result.Message, ResultStatus.Success);
                    ClearItems();
                    BindPublisherList();
                }
                else
                    Alert.Show(result.Message, ResultStatus.Success);
            }
        }
        private void ClearItems()
        {
            _publisherId = -1;
            FormControls.ClearFormControls(this);
        }
        private void SearchPublisher()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindPublisherList();

            var publishers = _publisherService.FindPublishersByText(searchText);
            if (publishers.ResultStatus == ResultStatus.Success) BindPublisherList(publishers.Data.Publishers);
            else BindPublisherList();
        }
        #endregion Methods

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_publisherId > -1) UpdatePublisher();
            else AddPublisher();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_publisherId > -1) DeletePublisher(sender, e);
            else Alert.Show(Messages.Publisher.NotFound(isPlural: false), ResultStatus.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        #endregion Buttons

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
