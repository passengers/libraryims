﻿using LibraryIMS.Data.Abstract;
using LibraryIMS.Data.EntityFramework.UnitOfWork;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Concrete;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    static class Program
    {
        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<Main>()
                .AddScoped<IBookService, BookManager>()
                .AddScoped<IBookCategoryService, BookCategoryManager>()
                .AddScoped<ICategoryService, CategoryManager>()
                .AddScoped<ICommentService, CommentManager>()
                .AddScoped<IContactService, ContactManager>()
                .AddScoped<IFavoriteBookService, FavoriteBookManager>()
                .AddScoped<IPublisherService, PublisherManager>()
                .AddScoped<IUserService, UserManager>()
                .AddScoped<IUserBookService, UserBookManager>()
                .AddScoped<IWriterService, WriterManager>()
                .AddScoped<IImageHelper,ImageHelper>()
                .AddScoped<IUnitOfWork, UnitOfWork>();
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var services = new ServiceCollection();

            ConfigureServices(services);

            using (ServiceProvider serviceProvider = services.BuildServiceProvider())
            {
                var pageMain = serviceProvider.GetRequiredService<Main>();
                Application.Run(pageMain);
            }
        }
    }
}
