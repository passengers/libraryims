﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class FavoriteBookOperations : Form
    {

        #region Field

        private readonly IBookService _bookService;
        private readonly IImageHelper _imageHelper;
        private readonly IWriterService _writerService;
        private readonly IPublisherService _publisherService;
        private readonly IFavoriteBookService _favoritebookService;
        private readonly ICategoryService _categoryService;
        private readonly IBookCategoryService _bookCategoryService;

        private int _bookId = -1;
        private int _writerId = -1;
        private int _publisherId = -1;
        private int _userId = -1;
        private string _fileName = string.Empty;

        #endregion Field

        #region Constructor

        public FavoriteBookOperations(IBookService bookService, IImageHelper imageHelper, IWriterService writerService, IPublisherService publisherService, int userId, IFavoriteBookService favoritebookService, ICategoryService categoryService, IBookCategoryService bookCategoryService)
        {
            _bookService = bookService;
            _imageHelper = imageHelper;
            _writerService = writerService;
            _publisherService = publisherService;
            _userId = userId;
            _favoritebookService = favoritebookService;
            _categoryService = categoryService;
            _bookCategoryService = bookCategoryService;

            InitializeComponent();
            BindComboBox();
            BindBookList();
            
        }

        #endregion Constructor

        #region Methods

        private void BindBookList(IList<Book> listBook = null)
        {
            if (listBook == null) listBook = GetAllFavoriteBookByNonDeleted();

            gcFavoriteBook.DataSource = listBook;
            gvFavoriteBook.Columns[1].Visible = false;
            gvFavoriteBook.Columns[2].Visible = false;
            gvFavoriteBook.Columns[5].Visible = false;
            gvFavoriteBook.Columns[6].Visible = false;
            gvFavoriteBook.Columns[7].Visible = false;
            gvFavoriteBook.Columns[8].Visible = false;
            gvFavoriteBook.Columns[9].Visible = false;
            gvFavoriteBook.Columns[10].Visible = false;
            gvFavoriteBook.Columns[11].Visible = false;
            gvFavoriteBook.Columns[12].Visible = false;
            gvFavoriteBook.Columns[13].Visible = false;
            gvFavoriteBook.Columns[14].Visible = false;
            gvFavoriteBook.Columns[15].Visible = false;
            gvFavoriteBook.Columns[16].Visible = false;
            gvFavoriteBook.Columns[17].Visible = false;
            gvFavoriteBook.Columns[18].Visible = false;
            gvFavoriteBook.Columns[19].Visible = false;
            gvFavoriteBook.Columns[20].Visible = false;
            gvFavoriteBook.Columns[21].Visible = false;
            gvFavoriteBook.Columns[22].Visible = false;
            gvFavoriteBook.Columns[23].Visible = false;
            gvFavoriteBook.Columns[24].Visible = false;

            panelGrid.Visible = listBook.Count > 0;
            lblMessage.Text = $"{listBook.Count} adet kitap listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            var book = _bookService.GetAllBookByNonDeleted();
            var writers = _writerService.GetAllWriterByNonDeleted();
            var publishers = _publisherService.GetAllPublisher();
            var categories = _categoryService.GetAllCategoryByNonDeleted();

            if (book.ResultStatus == ResultStatus.Success)
            {
                foreach (var writer in writers.Data.Writers)
                {
                    cbWriters.Properties.Items.Add(writer.Name);
                    cbWriters1.Properties.Items.Add(writer.Name);
                }
                foreach (var publisher in publishers.Data.Publishers)
                {
                    cbPublisher.Properties.Items.Add(publisher.Name);
                }
                foreach (var category in categories.Data.Categories)
                {
                    cbCategory.Properties.Items.Add(category.Name);
                }
            }
            else Alert.Show(book.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Tüm aktif kitapların listesini getir.
        /// </summary>
        private IList<Book> GetAllFavoriteBookByNonDeleted()
        {
            var books = _favoritebookService.GetAllFavoriteBookByUser(_userId);
            if (books.ResultStatus == ResultStatus.Success)
                return books.Data.Books;
            return null;
        }

        /// <summary>
        /// Gridde seçilen kitapların bilgilerini getir.
        /// </summary>
        private void GetFavoriteBookObject()
        {
            if (gvFavoriteBook.SelectedRowsCount > 0)
            {
                lblControl.Visible = true;
                lblControl2.Visible = true;
                lblControl3.Visible = true;
                int.TryParse(gvFavoriteBook.GetRowCellValue(gvFavoriteBook.FocusedRowHandle, "Id").ToString(), out _bookId);
                var book = _bookService.GetBook(_bookId);

                if (book.ResultStatus == ResultStatus.Success)
                {
                    _writerId = Convert.ToInt32(gvFavoriteBook.GetRowCellValue(gvFavoriteBook.FocusedRowHandle, "WriterId"));
                    _publisherId = Convert.ToInt32(gvFavoriteBook.GetRowCellValue(gvFavoriteBook.FocusedRowHandle, "PublisherId"));
                    var writer = _writerService.GetWriter(_writerId);
                    var publisher = _publisherService.GetPublisher(_publisherId);

                    lblNumberOfReads.Text = Convert.ToString(book.Data.Book.NumberOfReads);
                    lblNumberOfComment.Text = Convert.ToString(book.Data.Book.NumberOfComments);
                    lblNumberOfFavorite.Text = Convert.ToString(book.Data.Book.NumberOfFavorites);
                    txtName.Text = book.Data.Book.Name;
                    cbWriters.SelectedItem = writer.Data.Writer.Name.ToString();
                    cbPublisher.SelectedItem = publisher.Data.Publisher.Name.ToString();
                    txtPageNum.Text = Convert.ToString(book.Data.Book.NumberOfPages);
                    txtShelfNum.Text = Convert.ToString(book.Data.Book.Place);
                    txtReleaseDate.DateTime = Convert.ToDateTime(book.Data.Book.ReleaseDate);

                    txtAbout.Text = book.Data.Book.Description;
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{book.Data.Book.Thumbnail}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(book.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Kitap adı veya yazarına göre kitap ara.
        /// </summary>
        private void SearchBook()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText))
            {
                BindBookList();
                return;
            }
            var books = _favoritebookService.FindFavoriteBooksByText(searchText,_userId);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Kategori adına göre kitap ara.
        /// </summary>
        private void SearchFavoriteBooksByCategory()
        {
            var books = _favoritebookService.FindFavoriteBooksByCategory(cbCategory.SelectedItem.ToString(),_userId);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Yazar adına göre kitap ara.
        /// </summary>
        private void SearchFavoriteBooksByWriter()
        {
            var books = _favoritebookService.FindFavoriteBooksByWriterName(cbWriters1.SelectedItem.ToString(),_userId);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _bookId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            FormControls.ClearFormControls(this);
        }

        private void DeleteFavoriteBook()
        {
            var favoritedbook = _favoritebookService.GetFavoriteBook(_bookId,_userId);
            if (favoritedbook.ResultStatus == ResultStatus.Success)
            {
                if (XtraMessageBox.Show("Kitap kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var result = _favoritebookService.HardDeleteFavoriteBook(favoritedbook.Data);
                    if (result.ResultStatus == ResultStatus.Success)
                    {
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        BindBookList();
                    }
                    else
                        Alert.Show(result.Message, ResultStatus.Error);
                }
            }
            else
                Alert.Show(favoritedbook.Message, ResultStatus.Error);
        }

      

        #endregion Methods

        #region Buttons

        private void btnFavorite_Click(object sender, EventArgs e)
        {
            DeleteFavoriteBook();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        #endregion Buttons

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "member.xls";
            if (sfd.ShowDialog() == DialogResult.OK) gcFavoriteBook.ExportToXls(sfd.FileName);
        }

        #endregion LinkLabel

        #region GridControl

        private void gvFavoriteBook_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetFavoriteBookObject();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchBook();
        }

        private void cbWriters1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWriters1.SelectedIndex != -1)
            {
                SearchFavoriteBooksByWriter();
            }
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCategory.SelectedIndex != -1)
            {
                SearchFavoriteBooksByCategory();
            }
        }

        #endregion Events


    }
}
