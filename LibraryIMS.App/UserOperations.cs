﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;

namespace LibraryIMS.App
{
    public partial class UserOperations : Form
    {
        #region Field

        private readonly IUserService _userService;
        private readonly IImageHelper _imageHelper;

        // Gridde seçilen kullanıcının Id bilgisi
        private int _userId = -1;
        private string _fileName = string.Empty;

        #endregion Field

        #region Constructor

        public UserOperations(IUserService userService, IImageHelper imageHelper)
        {
            _userService = userService;
            _imageHelper = imageHelper;
            InitializeComponent();
            BindComboBox();
            BindUserList();
        }

        #endregion Constructor

        #region Methods

        private void BindUserList(IList<User> listUser = null)
        {
            if (listUser == null) listUser = GetAllUserByNonDeleted();

            gcUser.DataSource = listUser.OrderBy(u=>u.FirstName);
            gvUser.Columns[3].Visible = false;
            gvUser.Columns[4].Visible = false;
            gvUser.Columns[5].Visible = false;
            gvUser.Columns[7].Visible = false;
            gvUser.Columns[8].Visible = false;
            gvUser.Columns[10].Visible = false;
            gvUser.Columns[11].Visible = false;
            gvUser.Columns[12].Visible = false;
            gvUser.Columns[13].Visible = false;
            gvUser.Columns[14].Visible = false;
            gvUser.Columns[15].Visible = false;
            gvUser.Columns[16].Visible = false;
            gvUser.Columns[18].Visible = false;
            gvUser.Columns[20].Visible = false;
            panelGrid.Visible = listUser.Count > 0;
            lblMessage.Text = $"{listUser.Count} adet kullanıcı listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            cbAccess.Properties.Items.Clear(); cbStatus.Properties.Items.Clear();
            cbAccess.Properties.Items.AddRange(Enum.GetValues(typeof(AccessStatus)));
            cbStatus.Properties.Items.AddRange(Enum.GetValues(typeof(GeneralStatus)));
        }

        /// <summary>
        /// Tüm aktif kullanıcıların listesini getir.
        /// </summary>
        private IList<User> GetAllUserByNonDeleted()
        {
            var users = _userService.GetAllUserByNonDeleted();
            if (users.ResultStatus == ResultStatus.Success)
                return users.Data.Users;
            return null;
        }

        /// <summary>
        /// Gridde seçilen kullanıcı bilgilerini getir.
        /// </summary>
        private void GetUserObject()
        {
            if (gvUser.SelectedRowsCount > 0)
            {
                int.TryParse(gvUser.GetRowCellValue(gvUser.FocusedRowHandle, "Id").ToString(), out _userId);
                var user = _userService.GetUser(_userId);
                if (user.ResultStatus == ResultStatus.Success)
                {
                    txtName.Text = user.Data.User.FirstName;
                    txtSurname.Text = user.Data.User.LastName;
                    txtUsername.Text = user.Data.User.UserName;
                    txtPassword.Text = PasswordHelper.Decrypt(user.Data.User.Password);
                    txtMail.Text = user.Data.User.Email;
                    cbGender.SelectedItem = user.Data.User.Gender;
                    txtTelephone.Text = user.Data.User.PhoneNumber;
                    dateBirth.DateTime = user.Data.User.DateBirth;
                    txtAbout.Text = user.Data.User.About;
                    cbAccess.SelectedItem = Enum.Parse(typeof(AccessStatus), user.Data.User.AccessStatus.ToString());
                    cbStatus.SelectedItem = Enum.Parse(typeof(GeneralStatus), user.Data.User.GeneralStatus.ToString());
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{user.Data.User.Picture}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream),new Size(175,200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(user.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Sisteme yeni üye ekler.
        /// </summary>
        private void AddUser()
        {
            IAppResult<ImageUploadedDto> uploadedImageDtoResult = new AppResult<ImageUploadedDto>().Fail("Uyarı");
            if (!FormControls.CheckFormControls(gbUser))
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }

            if (_fileName!="")
            {
                uploadedImageDtoResult = _imageHelper.Upload(txtUsername.Text, _fileName, PictureType.User);
            }
            var user = new UserAddDto
            {
                FirstName = txtName.Text,
                LastName = txtSurname.Text,
                UserName = txtUsername.Text,
                Email = txtMail.Text,
                Password = PasswordHelper.Encrypt(txtPassword.Text),
                Gender = cbGender.Text,
                PhoneNumber = txtTelephone.Text,
                DateBirth = dateBirth.DateTime,
                About = txtAbout.Text,
                AccessStatus = (AccessStatus)cbAccess.SelectedItem,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem,
                Picture = uploadedImageDtoResult.ResultStatus == ResultStatus.Success
                ? uploadedImageDtoResult.Data.FullName
                : "userImages/defaultUser.png"
            };
            var added = _userService.AddUser(user, "Admin");
            if (added.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(added.Message, ResultStatus.Success);
                BindUserList();
                ClearItems();
            }
            else Alert.Show(added.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Sisteme yeni üye ekler.
        /// </summary>
        private void UpdateUser()
        {
            if (!FormControls.CheckFormControls(gbUser))
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            var user = new UserUpdateDto
            {
                Id = _userId,
                FirstName = txtName.Text,
                LastName = txtSurname.Text,
                UserName = txtUsername.Text,
                Password = PasswordHelper.Encrypt(txtPassword.Text),
                Email = txtMail.Text,
                Gender = cbGender.Text,
                PhoneNumber = txtTelephone.Text,
                DateBirth = dateBirth.DateTime,
                About = txtAbout.Text,
                AccessStatus = (AccessStatus)cbAccess.SelectedItem,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            bool isNewPictureUploaded = false;
            var oldUserPicture = _userService.GetUser(_userId).Data.User.Picture;
            if (_fileName != string.Empty)
            {
                var uploadedImageDtoResult = _imageHelper.Upload(txtUsername.Text, _fileName, PictureType.User);
                user.Picture = uploadedImageDtoResult.ResultStatus == ResultStatus.Success
                ? uploadedImageDtoResult.Data.FullName
                : "userImages/defaultUser.png";
                if (oldUserPicture != "userImages/defaultUser.png")
                {
                    isNewPictureUploaded = true;
                }
            }
            else
                user.Picture = oldUserPicture;
            var updatedUser = _userService.UpdateUser(user, "Admin");
            if (updatedUser.ResultStatus == ResultStatus.Success)
            {
                if (isNewPictureUploaded)
                {
                    _imageHelper.Delete(oldUserPicture);
                }
                Alert.Show(updatedUser.Message, ResultStatus.Success);
                BindUserList();
                ClearItems();
            }
            else Alert.Show(updatedUser.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Seçilen kullanıcıyı kaldırır.
        /// </summary>
        /// 
        private void DeleteUser(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Üye kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _userService.DeleteUser(_userId, "Admin");
                Alert.Show(result.Message, ResultStatus.Success);
                ClearItems();
                BindUserList();
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _userId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Ad, Soyad veya kullanıcı adına göre kullanıcı ara.
        /// </summary>
        private void SearchUser()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindUserList();

            var users = _userService.FindUsersByText(searchText);
            if (users.ResultStatus == ResultStatus.Success) BindUserList(users.Data.Users);
            else BindUserList();
        }

        #endregion Methods

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Length != 8)
            {
                Alert.Show("Şifre 8 karakterden oluşmalıdır.", ResultStatus.Warning);
                return;
            }
            if (_userId > -1) UpdateUser();
            else AddUser();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_userId > -1) DeleteUser(sender, e);
            else Alert.Show(Messages.User.NotFound(isPlural: false), ResultStatus.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnImage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "JPEG|*.jpg", ValidateNames = true, Multiselect = false })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    _fileName = ofd.FileName;
                    var stream = new FileStream(_fileName, FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
            }
        }

        #endregion

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog {Filter = "Excel Documents (*.xls)|*.xls", FileName = "users.xls"};
            if (sfd.ShowDialog() == DialogResult.OK) gcUser.ExportToXls(sfd.FileName);
        }

        #endregion

        #region GridControl

        /// <summary>
        /// Grid üzerindeki kullanıcılara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcUser_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var contextMenu = new ContextMenu();
                contextMenu.MenuItems.Add(new MenuItem("Kullanıcıyı Kaldır", DeleteUser));

                int rowIndex = gvUser.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvUser.ClearSelection();
                    gvUser.SelectRow(rowIndex);
                    _userId = Convert.ToInt32(gvUser.GetRowCellValue(rowIndex, "Id"));
                    contextMenu.Show(gcUser, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }
        private void gvUser_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetUserObject();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchUser();
        }

        #endregion
    }
}
