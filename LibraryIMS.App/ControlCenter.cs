﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Services.Abstract;
using static System.String;

namespace LibraryIMS.App
{
    public partial class ControlCenter : Form
    {

        #region Fields

        private readonly IUserService _userService;
        private readonly IBookService _bookService;
        private readonly ICategoryService _categoryService;
        private readonly IContactService _contactService;
        private readonly ICommentService _commentService;
        private readonly IWriterService _writerService;
        private readonly IPublisherService _publisherService;
        private int _generalId = -1;
        private string _userName;
        private string _bookName;
        private string _categoryName;
        private string _contactName;
        private string _commentName;
        private string _publisherName;
        private string _writerName;
        private bool isUser;
        private bool isBook;
        private bool isCategory;
        private bool isContact;
        private bool isComment;
        private bool isWriter;
        private bool isPublisher;

        #endregion Fields

        #region Constructor

        public ControlCenter(IUserService userService, IBookService bookService, ICategoryService categoryService, IContactService contactService, ICommentService commentService, IWriterService writerService, IPublisherService publisherService)
        {
            _userService = userService;
            _bookService = bookService;
            _categoryService = categoryService;
            _contactService = contactService;
            _commentService = commentService;
            _writerService = writerService;
            _publisherService = publisherService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Tüm silinen kitapların listesini getir.
        /// </summary>
        private void GetAllDeletedBooks(IList<Book> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var books = _bookService.GetAllDeletedBook();
                if (books.ResultStatus == ResultStatus.Success)
                {
                    listDeleted = books.Data.Books;
                }
                else Alert.Show(books.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.UpdatedDate);
                gvDeleted.Columns[1].Visible = false;
                gvDeleted.Columns[2].Visible = false;
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[4].Visible = false;
                gvDeleted.Columns[10].Visible = false;
                gvDeleted.Columns[11].Visible = false;
                gvDeleted.Columns[12].Visible = false;
                gvDeleted.Columns[13].Visible = false;
                gvDeleted.Columns[14].Visible = false;
                gvDeleted.Columns[15].Visible = false;
                gvDeleted.Columns[16].Visible = false;
                gvDeleted.Columns[17].Visible = false;
                gvDeleted.Columns[18].Visible = false;
                gvDeleted.Columns[19].Visible = false;
                gvDeleted.Columns[24].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir kitap bulunamadı", ResultStatus.Info);
            }
        }
        /// <summary>
        /// Tüm silinen kullanıcıların listesini getir.
        /// </summary>
        private void GetAllDeletedUsers(IList<User> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var users = _userService.GetAllDeletedUser();
                if (users.ResultStatus == ResultStatus.Success)
                {
                    listDeleted = users.Data.Users;
                }
                else Alert.Show(users.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.UpdatedDate);
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[4].Visible = false;
                gvDeleted.Columns[5].Visible = false;
                gvDeleted.Columns[7].Visible = false;
                gvDeleted.Columns[8].Visible = false;
                gvDeleted.Columns[10].Visible = false;
                gvDeleted.Columns[11].Visible = false;
                gvDeleted.Columns[12].Visible = false;
                gvDeleted.Columns[13].Visible = false;
                gvDeleted.Columns[14].Visible = false;
                gvDeleted.Columns[15].Visible = false;
                gvDeleted.Columns[16].Visible = false;
                gvDeleted.Columns[18].Visible = false;
                gvDeleted.Columns[20].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir kullanıcı bulunamadı", ResultStatus.Info);
            }

        }
        /// <summary>
        /// Tüm silinen kategorilerin listesini getir.
        /// </summary>
        private void GetAllDeletedCategories(IList<Category> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var categories = _categoryService.GetAllDeletedCategory();
                if (categories.ResultStatus == ResultStatus.Success)
                    listDeleted = categories.Data.Categories;
                else Alert.Show(categories.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.ParentId);
                gvDeleted.Columns[0].Visible = false;
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[4].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir kategori bulunamadı", ResultStatus.Info);
            }
        }
        /// <summary>
        /// Tüm silinen yazarların listesini getir.
        /// </summary>
        private void GetAllDeletedWriters(IList<Writer> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var writers = _writerService.GetAllDeletedWriter();
                if (writers.ResultStatus == ResultStatus.Success)
                {
                    listDeleted = writers.Data.Writers;
                }
                else Alert.Show(writers.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.Name);
                gvDeleted.Columns[0].Visible = false;
                gvDeleted.Columns[2].Visible = false;
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[6].Visible = false;
                gvDeleted.Columns[7].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir yazar bulunamadı", ResultStatus.Info);
            }
        }
        /// <summary>
        /// Tüm silinen yayınevlerinin listesini getir.
        /// </summary>
        private void GetAllDeletedPublishers(IList<Publisher> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var publishers = _publisherService.GetAllDeletedPublisher();
                if (publishers.ResultStatus == ResultStatus.Success)
                {
                    listDeleted = publishers.Data.Publishers;
                }
                else Alert.Show(publishers.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.Name);
                gvDeleted.Columns[0].Visible = false;
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[4].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir yayınevi bulunamadı", ResultStatus.Info);
            }
        }
        /// <summary>
        /// Tüm silinen yorumların listesini getir.
        /// </summary>
        private void GetAllDeletedComments(IList<Comment> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var comments = _commentService.GetAllDeletedComment();
                if (comments.ResultStatus == ResultStatus.Success)
                {
                    listDeleted = comments.Data.Comments;
                }
                else Alert.Show(comments.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.UpdatedDate);
                gvDeleted.Columns[0].Visible = false;
                gvDeleted.Columns[1].Visible = false;
                gvDeleted.Columns[2].Visible = false;
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[5].Visible = false;
                gvDeleted.Columns[6].Visible = false;
                gvDeleted.Columns[8].Visible = false;
                gvDeleted.Columns[10].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir yorum bulunamadı", ResultStatus.Info);
            }
        }
        /// <summary>
        /// Tüm silinen mesajların listesini getir.
        /// </summary>
        private void GetAllDeletedContacts(IList<Contact> listDeleted = null)
        {
            if (listDeleted == null)
            {
                var contacts = _contactService.GetAllDeletedContact();
                if (contacts.ResultStatus == ResultStatus.Success)
                    listDeleted = contacts.Data.Contacts;
                else Alert.Show(contacts.Message, ResultStatus.Error);
            }
            if (listDeleted.Count > 0)
            {
                panel5.Visible = true;
                gcDeleted.DataSource = null;
                gvDeleted.Columns.Clear();
                gcDeleted.DataSource = listDeleted.OrderBy(c => c.UpdatedDate);
                gvDeleted.Columns[0].Visible = false;
                gvDeleted.Columns[1].Visible = false;
                gvDeleted.Columns[3].Visible = false;
                gvDeleted.Columns[6].Visible = false;
                gvDeleted.Columns[7].Visible = false;
                gvDeleted.Columns[8].Visible = false;
            }
            else
            {
                panel5.Visible = false;
                Alert.Show("Silinenler arasında bir mesaj bulunamadı", ResultStatus.Info);
            }
        }

        private void ClearItems()
        {
            _generalId = -1;
            isBook = false;
            isUser = false;
            isCategory = false;
            isComment = false;
            isContact = false;
            isPublisher = false;
            isWriter = false;
        }

        #endregion Methods

        #region Buttons

        private void btnComment_Click(object sender, EventArgs e)
        {
            ClearItems();
            isComment = true;
            GetAllDeletedComments();
        }
        private void btnPublishers_Click(object sender, EventArgs e)
        {
            ClearItems();
            isPublisher = true;
            GetAllDeletedPublishers();
        }
        private void btnWriters_Click(object sender, EventArgs e)
        {
            ClearItems();
            isWriter = true;
            GetAllDeletedWriters();
        }
        private void btnCategories_Click(object sender, EventArgs e)
        {
            ClearItems();
            isCategory = true;
            GetAllDeletedCategories();
        }
        private void btnBooks_Click(object sender, EventArgs e)
        {
            ClearItems();
            isBook = true;
            GetAllDeletedBooks();
        }
        private void btnUsers_Click(object sender, EventArgs e)
        {
            ClearItems();
            isUser = true;
            GetAllDeletedUsers();
        }
        private void btnContact_Click(object sender, EventArgs e)
        {
            ClearItems();
            isContact = true;
            GetAllDeletedContacts();
        }
        private void btnHardDelete_Click(object sender, EventArgs e)
        {
            if (_generalId > 0)
            {
                if (isUser)
                {
                    if (XtraMessageBox.Show(
                        $"{_userName} adlı kullanıcı arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _userService.HardDeleteUser(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isUser = true;
                        GetAllDeletedUsers();
                    }
                }
                else if (isBook)
                {
                    if (XtraMessageBox.Show(
                        $"{_bookName} adlı kitap arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _bookService.HardDeleteBook(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isBook = true;
                        GetAllDeletedBooks();
                    }
                }
                else if (isCategory)
                {
                    if (XtraMessageBox.Show(
                        $"{_categoryName} adlı kategori arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _categoryService.HardDeleteCategory(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isCategory = true;
                        GetAllDeletedCategories();
                    }
                }
                else if (isContact)
                {
                    if (XtraMessageBox.Show(
                        $"{_contactName} adlı kullanıcının mesajı arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _contactService.HardDeleteContact(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isContact = true;
                        GetAllDeletedContacts();
                    }
                }
                else if (isComment)
                {
                    if (XtraMessageBox.Show(
                        $"{_commentName} adlı kullanıcının yorumu arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _commentService.HardDeleteComment(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isComment = true;
                        GetAllDeletedComments();
                    }
                }
                else if (isWriter)
                {
                    if (XtraMessageBox.Show(
                        $"{_writerName} adlı yazar arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _writerService.HardDeleteWriter(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isWriter = true;
                        GetAllDeletedWriters();
                    }
                }
                else if (isPublisher)
                {
                    if (XtraMessageBox.Show(
                        $"{_publisherName} adlı yayınevi arşivden silinecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _publisherService.HardDeletePublisher(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isPublisher = true;
                        GetAllDeletedPublishers();
                    }
                }
                else
                    Alert.Show("İşlem gerçekleşirken bir hata oluştu", ResultStatus.Warning);
            }
            else
                Alert.Show("Lütfen silinecek bir öğe seçiniz.", ResultStatus.Info);
        }
        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (_generalId > 0)
            {
                if (isUser)
                {
                    if (XtraMessageBox.Show(
                        $"{_userName} adlı kullanıcı arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _userService.UndoDeleteUser(_generalId, "Admin");
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isUser = true;
                        GetAllDeletedUsers();
                    }
                }
                else if (isBook)
                {
                    if (XtraMessageBox.Show(
                        $"{_bookName} adlı kitap arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _bookService.UndoDeleteBook(_generalId, "Admin");
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isBook = true;
                        GetAllDeletedBooks();
                    }
                }
                else if (isCategory)
                {
                    if (XtraMessageBox.Show(
                        $"{_categoryName} adlı kategori arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _categoryService.UndoDeleteCategory(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isCategory = true;
                        GetAllDeletedCategories();
                    }
                }
                else if (isContact)
                {
                    if (XtraMessageBox.Show(
                        $"{_contactName} adlı kullanıcının mesajı arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _contactService.UndoDeleteContact(_generalId, "Admin");
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isContact = true;
                        GetAllDeletedContacts();
                    }
                }
                else if (isComment)
                {
                    if (XtraMessageBox.Show(
                        $"{_commentName} adlı kullanıcının yorumu arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _commentService.UndoDeleteComment(_generalId, "Admin");
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isComment = true;
                        GetAllDeletedComments();
                    }
                }
                else if (isWriter)
                {
                    if (XtraMessageBox.Show(
                        $"{_writerName} adlı yazar arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _writerService.UndoDeleteWriter(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isWriter = true;
                        GetAllDeletedWriters();
                    }
                }
                else if (isPublisher)
                {
                    if (XtraMessageBox.Show(
                        $"{_publisherName} adlı yayınevi arşivden geri getirilecektir. Emin misiniz?",
                        "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var result = _publisherService.UndoDeletePublisher(_generalId);
                        if (result.ResultStatus != ResultStatus.Success)
                        {
                            Alert.Show(result.Message, ResultStatus.Error);
                            return;
                        }
                        Alert.Show(result.Message, ResultStatus.Success);
                        ClearItems();
                        isPublisher = true;
                        GetAllDeletedPublishers();
                    }
                }
                else
                    Alert.Show("İşlem gerçekleşirken bir hata oluştu", ResultStatus.Warning);
            }
            else
                Alert.Show("Lütfen geri getirilecek bir öğe seçiniz.", ResultStatus.Info);
        }
        private void GetDeletingObject()
        {
            if (gvDeleted.SelectedRowsCount > 0)
            {
                int.TryParse(gvDeleted.GetRowCellValue(gvDeleted.FocusedRowHandle, "Id").ToString(), out _generalId);
                if (isUser) _userName = _userService.GetUser(_generalId).Data.User.UserName;
                if (isBook) _bookName = _bookService.GetBook(_generalId).Data.Book.Name;
                if (isCategory) _categoryName = _categoryService.GetCategory(_generalId).Data.Category.Name;
                if (isWriter) _writerName = _writerService.GetWriter(_generalId).Data.Writer.Name;
                if (isPublisher) _publisherName = _publisherService.GetPublisher(_generalId).Data.Publisher.Name;
                if (isComment) _commentName = _commentService.GetComment(_generalId).Data.Comment.User.UserName;
                if (isContact) _contactName = _contactService.GetContact(_generalId).Data.Contact.User.UserName;
            }
        }
        private void SearchBooksByCategory()
        {
            string searchText = txtSearch.Text;
            if (isUser)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedUsers();
                var users = _userService.FindDeletedUsersByText(searchText);
                if (users.ResultStatus == ResultStatus.Success)
                    GetAllDeletedUsers(users.Data.Users);
                else GetAllDeletedUsers();
            }
            else if (isBook)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedBooks();
                var books = _bookService.FindDeletedBooksByText(searchText);
                if (books.ResultStatus == ResultStatus.Success)
                    GetAllDeletedBooks(books.Data.Books);
                else GetAllDeletedBooks();
            }
            else if (isCategory)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedCategories();
                var categories = _categoryService.FindDeletedCategoriesByText(searchText);
                if (categories.ResultStatus == ResultStatus.Success)
                    GetAllDeletedCategories(categories.Data.Categories);
                else GetAllDeletedCategories();
            }
            else if (isContact)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedContacts();
                var contacts = _contactService.FindDeletedContactsByUserName(searchText);
                if (contacts.ResultStatus == ResultStatus.Success)
                    GetAllDeletedContacts(contacts.Data.Contacts);
                else GetAllDeletedContacts();
            }
            else if (isComment)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedComments();
                var comments = _commentService.FindDeletedCommentsByText(searchText);
                if (comments.ResultStatus == ResultStatus.Success)
                    GetAllDeletedComments(comments.Data.Comments);
                else GetAllDeletedComments();
            }
            else if (isWriter)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedWriters();
                var writers = _writerService.FindDeletedWritersByText(searchText);
                if (writers.ResultStatus == ResultStatus.Success)
                    GetAllDeletedWriters(writers.Data.Writers);
                else GetAllDeletedWriters();
            }
            else if (isPublisher)
            {
                if (IsNullOrEmpty(searchText)) GetAllDeletedPublishers();
                var publishers = _publisherService.FindDeletedPublishersByText(searchText);
                if (publishers.ResultStatus == ResultStatus.Success)
                    GetAllDeletedPublishers(publishers.Data.Publishers);
                else GetAllDeletedPublishers();
            }
        }

        #endregion Buttons

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchBooksByCategory();
        }

        #endregion Events

        #region GridControl

        private void gvDeleted_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetDeletingObject();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog { Filter = "Excel Documents (*.xls)|*.xls", FileName = "deleted.xls" };
            if (sfd.ShowDialog() == DialogResult.OK) gcDeleted.ExportToXls(sfd.FileName);
        }

        #endregion LinkLabel

    }
}
