﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using static System.String;

namespace LibraryIMS.App
{
    public partial class ContactOperations : Form
    {

        #region Fields

        private readonly IContactService _contactService;

        // Gridde seçilen kullanıcının Id bilgisi
        private int _contactId = -1;

        #endregion Fields

        #region Form Load

        private void ContactOperations_Load(object sender, EventArgs e)
        {
            BindContactList();
        }

        #endregion Form Load

        #region Constructor

        public ContactOperations(IContactService contactService)
        {
            _contactService = contactService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        private void BindContactList(IList<Contact> listContact = null)
        {
            if (listContact == null) listContact = GetAllContactByNonDeleted();

            gcContact.DataSource = listContact.OrderBy(c => c.CreatedDate);
            gvContact.Columns[0].Visible = false;
            gvContact.Columns[1].Visible = false;
            gvContact.Columns[3].Visible = false;
            gvContact.Columns[6].Visible = false;
            gvContact.Columns[7].Visible = false;
            gvContact.Columns[8].Visible = false;
            lblMessage.Text = $"{listContact.Count} adet mesaj listeleniyor.  ";
        }

        /// <summary>
        /// Tüm aktif mesajların listesini getir.
        /// </summary>
        private IList<Contact> GetAllContactByNonDeleted()
        {
            var contacts = _contactService.GetAllContactByNonDeleted();
            if (contacts.ResultStatus == ResultStatus.Success)
                return contacts.Data.Contacts;
            return null;
        }

        /// <summary>
        /// Gridde seçilen iletişim bilgilerini getir.
        /// </summary>
        private void GetContactObject()
        {
            if (gvContact.SelectedRowsCount > 0)
            {
                int.TryParse(gvContact.GetRowCellValue(gvContact.FocusedRowHandle, "Id").ToString(), out _contactId);
                var contact = _contactService.GetContact(_contactId);
                if (contact.ResultStatus == ResultStatus.Success)
                {
                    txtFirstName.Text = contact.Data.Contact.User.FirstName;
                    txtLastName.Text = contact.Data.Contact.User.LastName;
                    txtContent.Text = contact.Data.Contact.Content;
                    dateContactDate.DateTime = contact.Data.Contact.CreatedDate;
                }
                else
                    Alert.Show(contact.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Seçilen kullanıcıyı kaldırır.
        /// </summary>
        /// 
        private void DeleteContact(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Mesaj silinecektir. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _contactService.DeleteContact(_contactId, "Admin");
                Alert.Show(result.Message, ResultStatus.Success);
                ClearItems();
                BindContactList();
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _contactId = -1;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Ad veya soyad'a göre mesaj ara.
        /// </summary>
        private void SearchContact()
        {
            string searchText = txtSearchByUser.Text;
            if (IsNullOrEmpty(searchText)) BindContactList();

            var contacts = _contactService.FindContactsByUserName(searchText);
            if (contacts.ResultStatus == ResultStatus.Success) BindContactList(contacts.Data.Contacts);
            else BindContactList();
        }

        #endregion Methods

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog { Filter = "Excel Documents (*.xls)|*.xls", FileName = "messages.xls" };
            if (sfd.ShowDialog() == DialogResult.OK) gcContact.ExportToXls(sfd.FileName);
        }

        #endregion

        #region Buttons

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_contactId > -1) DeleteContact(sender, e);
            else Alert.Show(Messages.Contact.NotFound(isPlural: false), ResultStatus.Error);
        }

        #endregion

        #region GridControl

        private void gcContact_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var contextMenu = new ContextMenu();
                contextMenu.MenuItems.Add(new MenuItem("Mesajı Kaldır", DeleteContact));

                int rowIndex = gvContact.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvContact.ClearSelection();
                    gvContact.SelectRow(rowIndex);
                    _contactId = Convert.ToInt32(gvContact.GetRowCellValue(rowIndex, "Id"));
                    contextMenu.Show(gcContact, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        private void gvContact_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetContactObject();
        }

        #endregion GridControl

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        #region Events

        private void txtSearchByUser_TextChanged(object sender, EventArgs e)
        {
            SearchContact();
        }

        #endregion Events
    }
}
