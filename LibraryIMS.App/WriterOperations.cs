﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.App
{
    public partial class WriterOperations : Form
    {
        #region Fields

        private readonly IImageHelper _imageHelper;
        private readonly IWriterService _writerService;
        private int _writerId = -1;
        private string _fileName = string.Empty;

        #endregion Fields

        #region Constructor

        public WriterOperations(IWriterService writerService, IImageHelper imageHelper)
        {
            _writerService = writerService;
            _imageHelper = imageHelper;
            InitializeComponent();
        }

        #endregion Constructor

        #region FormLoad

        private void WriterOperations_Load(object sender, EventArgs e)
        {
            BindWriterList();
            BindComboBox();
        }

        #endregion FormLoad

        #region Methods

        private void BindWriterList(IList<Writer> listWriter = null)
        {
            if (listWriter == null) listWriter = GetAllWriterByNonDeleted();

            gcWriter.DataSource = listWriter.OrderBy(w=>w.Name);
            gvWriter.Columns[0].Visible = false;
            gvWriter.Columns[2].Visible = false;
            gvWriter.Columns[3].Visible = false;
            gvWriter.Columns[6].Visible = false;
            gvWriter.Columns[7].Visible = false;
            panelGrid.Visible = listWriter.Count > 0;
            lblMessage.Text = $"{listWriter.Count} adet yazar listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            cbStatus.Properties.Items.Clear();
            cbStatus.Properties.Items.AddRange(Enum.GetValues(typeof(GeneralStatus)));
        }

        /// <summary>
        /// Tüm aktif yazarların listesini getir.
        /// </summary>
        private IList<Writer> GetAllWriterByNonDeleted()
        {
            var writers = _writerService.GetAllWriterByNonDeleted();
            if (writers.ResultStatus == ResultStatus.Success)
                return writers.Data.Writers;
            return null;
        }

        /// <summary>
        /// Gridde seçilen yazar bilgilerini getir.
        /// </summary>
        private void GetWriterObject()
        {
            if (gvWriter.SelectedRowsCount > 0)
            {
                lblControl.Visible = true;
                int.TryParse(gvWriter.GetRowCellValue(gvWriter.FocusedRowHandle, "Id").ToString(), out _writerId);
                var writer = _writerService.GetWriter(_writerId);
                var writerBookNumber = _writerService.GetNumberOfBooksForWriter(_writerId);
                if (writer.ResultStatus == ResultStatus.Success)
                {
                    lblControl.Visible = true;
                    lblNumberOfBooks.Visible = true;
                    lblNumberOfBooks.Text = Convert.ToString(writerBookNumber);
                    txtName.Text = writer.Data.Writer.Name;
                    dateBirth.DateTime = writer.Data.Writer.DateOfBirth;
                    txtBiography.Text = writer.Data.Writer.Biography;
                    cbStatus.SelectedItem = Enum.Parse(typeof(GeneralStatus), writer.Data.Writer.GeneralStatus.ToString());
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{writer.Data.Writer.Picture}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(writer.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Sisteme yeni yazar ekler.
        /// </summary>
        private void AddWriter()
        {
            if (!FormControls.CheckFormControls(gbWriter))
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            var uploadedImageDtoResult = _imageHelper.Upload(txtName.Text, _fileName, PictureType.Writer);
            var writer = new WriterAddDto
            {
                Name = txtName.Text,
                DateOfBirth = dateBirth.DateTime,
                Biography = txtBiography.Text,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem,
                Picture = uploadedImageDtoResult.ResultStatus == ResultStatus.Success
                    ? uploadedImageDtoResult.Data.FullName
                    : "writerImages/defaultWriter.jpg"
            };
            var added = _writerService.AddWriter(writer);
            if (added.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(added.Message, ResultStatus.Success);
                BindWriterList();
                ClearItems();
            }
            else Alert.Show(added.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Sistemdeki yazar bilgisini günceller.
        /// </summary>
        private void UpdateWriter()
        {
            if (!FormControls.CheckFormControls(gbWriter))
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            var writer = new WriterUpdateDto
            {
                Id = _writerId,
                Name = txtName.Text,
                DateOfBirth = dateBirth.DateTime,
                Biography = txtBiography.Text,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            bool isNewPictureUploaded = false;
            var oldWriterPicture = _writerService.GetWriter(_writerId).Data.Writer.Picture;
            if (_fileName != string.Empty)
            {
                var uploadedImageDtoResult = _imageHelper.Upload(txtName.Text, _fileName, PictureType.Writer);
                writer.Picture = uploadedImageDtoResult.ResultStatus == ResultStatus.Success
                ? uploadedImageDtoResult.Data.FullName
                : "writerImages/defaultWriter.jpg";
                if (oldWriterPicture != "writerImages/defaultWriter.jpg")
                {
                    isNewPictureUploaded = true;
                }
            }
            else
                writer.Picture = oldWriterPicture;
            var updatedWriter = _writerService.UpdateWriter(writer);
            if (updatedWriter.ResultStatus == ResultStatus.Success)
            {
                if (isNewPictureUploaded)
                {
                    _imageHelper.Delete(oldWriterPicture);
                }
                Alert.Show(updatedWriter.Message, ResultStatus.Success);
                BindWriterList();
                ClearItems();
            }
            else Alert.Show(updatedWriter.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Seçilen yazarı kaldırır.
        /// </summary>
        /// 
        private void DeleteWriter(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Yazar kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _writerService.DeleteWriter(_writerId);
                Alert.Show(result.Message, ResultStatus.Success);
                BindWriterList();
                ClearItems();
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _writerId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            lblControl.Visible = false;
            lblNumberOfBooks.Visible = false;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Ada göre yazar ara.
        /// </summary>
        private void SearchWriter()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindWriterList();

            var writers = _writerService.FindWritersByText(searchText);
            if (writers.ResultStatus == ResultStatus.Success) BindWriterList(writers.Data.Writers);
            else BindWriterList();
        }

        #endregion Methods

        #region Buttons

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_writerId > -1) DeleteWriter(sender, e);
            else Alert.Show(Messages.Writer.NotFound(isPlural: false), ResultStatus.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_writerId > -1) UpdateWriter();
            else AddWriter();
        }

        private void btnImage_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg", ValidateNames = true, Multiselect = false })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    _fileName = ofd.FileName;
                    var stream = new FileStream(_fileName, FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
            }
        }

        #endregion Buttons

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchWriter();
        }

        #endregion Events

        #region GridControl

        /// <summary>
        /// Grid üzerindeki yazarlara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcWriter_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var contextMenu = new ContextMenu();
                contextMenu.MenuItems.Add(new MenuItem("Yazarı Kaldır", DeleteWriter));

                int rowIndex = gvWriter.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvWriter.ClearSelection();
                    gvWriter.SelectRow(rowIndex);
                    _writerId = Convert.ToInt32(gvWriter.GetRowCellValue(rowIndex, "Id"));
                    contextMenu.Show(gcWriter, new Point(e.X, e.Y));
                }
            }
        }

        private void gvWriter_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetWriterObject();
        }

        #endregion GridControl

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "Excel Documents (*.xls)|*.xls", FileName = "writers.xls"
            };
            if (sfd.ShowDialog() == DialogResult.OK) gcWriter.ExportToXls(sfd.FileName);
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer
    }
}
