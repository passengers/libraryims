﻿
namespace LibraryIMS.App
{
    partial class BookCategoryOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookCategoryOperations));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ceAllBook = new DevExpress.XtraEditors.CheckEdit();
            this.cbSearchBookByCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtBookName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gbBookCategory = new System.Windows.Forms.GroupBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.cbAllCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblDeleted = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lnkExcel = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.lblClock = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gcCategory = new DevExpress.XtraGrid.GridControl();
            this.gvCategory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcBook = new DevExpress.XtraGrid.GridControl();
            this.gvBook = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllBook.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSearchBookByCategory.Properties)).BeginInit();
            this.gbBookCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbAllCategory.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.ceAllBook);
            this.panel1.Controls.Add(this.cbSearchBookByCategory);
            this.panel1.Controls.Add(this.txtBookName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.gbBookCategory);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1503, 278);
            this.panel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Location = new System.Drawing.Point(1281, 233);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(194, 39);
            this.panel6.TabIndex = 99;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(34, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 16);
            this.label2.TabIndex = 23;
            this.label2.Text = "Kitaba Ait Kategoriler";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.SeaGreen;
            this.label5.Location = new System.Drawing.Point(18, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 25);
            this.label5.TabIndex = 22;
            this.label5.Text = "•";
            // 
            // ceAllBook
            // 
            this.ceAllBook.Location = new System.Drawing.Point(505, 246);
            this.ceAllBook.Name = "ceAllBook";
            this.ceAllBook.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ceAllBook.Properties.Appearance.Options.UseFont = true;
            this.ceAllBook.Properties.Caption = "Kategorisi Olmayanları Göster";
            this.ceAllBook.Size = new System.Drawing.Size(259, 22);
            this.ceAllBook.TabIndex = 98;
            this.ceAllBook.CheckedChanged += new System.EventHandler(this.ceAllBook_CheckedChanged);
            // 
            // cbSearchBookByCategory
            // 
            this.cbSearchBookByCategory.Location = new System.Drawing.Point(232, 247);
            this.cbSearchBookByCategory.Name = "cbSearchBookByCategory";
            this.cbSearchBookByCategory.Properties.AllowFocused = false;
            this.cbSearchBookByCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSearchBookByCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbSearchBookByCategory.Size = new System.Drawing.Size(207, 20);
            this.cbSearchBookByCategory.TabIndex = 97;
            this.cbSearchBookByCategory.SelectedIndexChanged += new System.EventHandler(this.cbSearchBookByCategory_SelectedIndexChanged);
            // 
            // txtBookName
            // 
            this.txtBookName.Location = new System.Drawing.Point(232, 215);
            this.txtBookName.Name = "txtBookName";
            this.txtBookName.Size = new System.Drawing.Size(207, 20);
            this.txtBookName.TabIndex = 93;
            this.txtBookName.TextChanged += new System.EventHandler(this.txtBookName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(15, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 17);
            this.label1.TabIndex = 96;
            this.label1.Text = "Kategoriye Göre Kitap Ara:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(15, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 17);
            this.label10.TabIndex = 94;
            this.label10.Text = "Kitap Ara:";
            // 
            // gbBookCategory
            // 
            this.gbBookCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBookCategory.Controls.Add(this.simpleButton1);
            this.gbBookCategory.Controls.Add(this.btnClear);
            this.gbBookCategory.Controls.Add(this.btnDelete);
            this.gbBookCategory.Controls.Add(this.btnSave);
            this.gbBookCategory.Controls.Add(this.cbAllCategory);
            this.gbBookCategory.Controls.Add(this.lblDeleted);
            this.gbBookCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbBookCategory.Location = new System.Drawing.Point(12, 12);
            this.gbBookCategory.Name = "gbBookCategory";
            this.gbBookCategory.Size = new System.Drawing.Size(1466, 197);
            this.gbBookCategory.TabIndex = 3;
            this.gbBookCategory.TabStop = false;
            this.gbBookCategory.Text = "Kitap Kategori İşlemleri";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.simpleButton1.Location = new System.Drawing.Point(787, 18);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.simpleButton1.Size = new System.Drawing.Size(676, 176);
            this.simpleButton1.TabIndex = 104;
            this.simpleButton1.Text = resources.GetString("simpleButton1.Text");
            // 
            // btnClear
            // 
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(6, 158);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(121, 36);
            this.btnClear.TabIndex = 102;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(306, 158);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDelete.Size = new System.Drawing.Size(121, 36);
            this.btnDelete.TabIndex = 100;
            this.btnDelete.Text = "Sil";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(155, 158);
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnSave.Size = new System.Drawing.Size(121, 36);
            this.btnSave.TabIndex = 101;
            this.btnSave.Text = "Ekle";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbAllCategory
            // 
            this.cbAllCategory.Location = new System.Drawing.Point(220, 80);
            this.cbAllCategory.Name = "cbAllCategory";
            this.cbAllCategory.Properties.AllowFocused = false;
            this.cbAllCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbAllCategory.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbAllCategory.Size = new System.Drawing.Size(207, 20);
            this.cbAllCategory.TabIndex = 99;
            // 
            // lblDeleted
            // 
            this.lblDeleted.AutoSize = true;
            this.lblDeleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblDeleted.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDeleted.Location = new System.Drawing.Point(3, 80);
            this.lblDeleted.Name = "lblDeleted";
            this.lblDeleted.Size = new System.Drawing.Size(129, 17);
            this.lblDeleted.TabIndex = 98;
            this.lblDeleted.Text = "Tüm Kategoriler:";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.lnkExcel);
            this.panel3.Controls.Add(this.lblClock);
            this.panel3.Location = new System.Drawing.Point(819, 233);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(295, 39);
            this.panel3.TabIndex = 2;
            // 
            // lnkExcel
            // 
            this.lnkExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lnkExcel.Appearance.Options.UseFont = true;
            this.lnkExcel.Location = new System.Drawing.Point(9, 13);
            this.lnkExcel.Name = "lnkExcel";
            this.lnkExcel.Size = new System.Drawing.Size(38, 17);
            this.lnkExcel.TabIndex = 96;
            this.lnkExcel.Text = "Excel";
            this.lnkExcel.Click += new System.EventHandler(this.lnkExcel_Click);
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(87, 15);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(183, 18);
            this.lblClock.TabIndex = 91;
            this.lblClock.Text = "29 Aralık 2020 24:24:24";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 476);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1503, 44);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.gcBook);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 278);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1503, 198);
            this.panel4.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.gcCategory);
            this.panel5.Location = new System.Drawing.Point(1107, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(384, 189);
            this.panel5.TabIndex = 88;
            // 
            // gcCategory
            // 
            this.gcCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcCategory.Location = new System.Drawing.Point(16, 15);
            this.gcCategory.MainView = this.gvCategory;
            this.gcCategory.Name = "gcCategory";
            this.gcCategory.Size = new System.Drawing.Size(355, 160);
            this.gcCategory.TabIndex = 87;
            this.gcCategory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCategory});
            // 
            // gvCategory
            // 
            this.gvCategory.Appearance.Empty.BackColor = System.Drawing.SystemColors.Control;
            this.gvCategory.Appearance.Empty.Options.UseBackColor = true;
            this.gvCategory.Appearance.Row.BackColor = System.Drawing.Color.LightSkyBlue;
            this.gvCategory.Appearance.Row.BackColor2 = System.Drawing.Color.Moccasin;
            this.gvCategory.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gvCategory.Appearance.Row.Options.UseBackColor = true;
            this.gvCategory.Appearance.Row.Options.UseFont = true;
            this.gvCategory.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvCategory.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gvCategory.GridControl = this.gcCategory;
            this.gvCategory.Name = "gvCategory";
            this.gvCategory.OptionsBehavior.Editable = false;
            this.gvCategory.OptionsBehavior.ReadOnly = true;
            this.gvCategory.OptionsCustomization.AllowFilter = false;
            this.gvCategory.OptionsFind.AllowFindPanel = false;
            this.gvCategory.OptionsMenu.EnableColumnMenu = false;
            this.gvCategory.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvCategory.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvCategory.OptionsView.ShowGroupPanel = false;
            this.gvCategory.OptionsView.ShowIndicator = false;
            this.gvCategory.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvCategory.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvCategory_RowCellClick);
            // 
            // gcBook
            // 
            this.gcBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcBook.Location = new System.Drawing.Point(12, 18);
            this.gcBook.MainView = this.gvBook;
            this.gcBook.Name = "gcBook";
            this.gcBook.Size = new System.Drawing.Size(1089, 160);
            this.gcBook.TabIndex = 86;
            this.gcBook.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBook});
            // 
            // gvBook
            // 
            this.gvBook.Appearance.Empty.BackColor = System.Drawing.SystemColors.Control;
            this.gvBook.Appearance.Empty.Options.UseBackColor = true;
            this.gvBook.Appearance.Row.BackColor = System.Drawing.Color.Tan;
            this.gvBook.Appearance.Row.BackColor2 = System.Drawing.Color.AntiqueWhite;
            this.gvBook.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gvBook.Appearance.Row.Options.UseBackColor = true;
            this.gvBook.Appearance.Row.Options.UseFont = true;
            this.gvBook.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvBook.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gvBook.GridControl = this.gcBook;
            this.gvBook.Name = "gvBook";
            this.gvBook.OptionsBehavior.Editable = false;
            this.gvBook.OptionsBehavior.ReadOnly = true;
            this.gvBook.OptionsCustomization.AllowFilter = false;
            this.gvBook.OptionsFind.AllowFindPanel = false;
            this.gvBook.OptionsMenu.EnableColumnMenu = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvBook.OptionsView.ShowGroupPanel = false;
            this.gvBook.OptionsView.ShowIndicator = false;
            this.gvBook.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvBook.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBook_RowCellClick);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // BookCategoryOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1503, 520);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BookCategoryOperations";
            this.Text = "BookCategoryOperations";
            this.Load += new System.EventHandler(this.BookCategoryOperations_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllBook.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSearchBookByCategory.Properties)).EndInit();
            this.gbBookCategory.ResumeLayout(false);
            this.gbBookCategory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbAllCategory.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox gbBookCategory;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraGrid.GridControl gcBook;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBook;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.GridControl gcCategory;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCategory;
        private System.Windows.Forms.TextBox txtBookName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.ComboBoxEdit cbSearchBookByCategory;
        private DevExpress.XtraEditors.ComboBoxEdit cbAllCategory;
        private System.Windows.Forms.Label lblDeleted;
        private DevExpress.XtraEditors.HyperlinkLabelControl lnkExcel;
        private System.Windows.Forms.Label lblClock;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Timer timer;
        private DevExpress.XtraEditors.CheckEdit ceAllBook;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
    }
}