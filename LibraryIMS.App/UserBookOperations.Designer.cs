﻿
namespace LibraryIMS.App
{
    partial class UserBookOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression3 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression4 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression5 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression6 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserBookOperations));
            this.gcUserBook = new DevExpress.XtraGrid.GridControl();
            this.gvUserBook = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.gcUser = new DevExpress.XtraGrid.GridControl();
            this.gvUser = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.gcBook = new DevExpress.XtraGrid.GridControl();
            this.gvBook = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtBook = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.gbUserBook = new System.Windows.Forms.GroupBox();
            this.lblNumberOfComment = new System.Windows.Forms.Label();
            this.lblNumberOfFavorite = new System.Windows.Forms.Label();
            this.lblNumberOfReads = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gcUserBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).BeginInit();
            this.gbUserBook.SuspendLayout();
            this.SuspendLayout();
            // 
            // gcUserBook
            // 
            this.gcUserBook.Location = new System.Drawing.Point(12, 295);
            this.gcUserBook.MainView = this.gvUserBook;
            this.gcUserBook.Name = "gcUserBook";
            this.gcUserBook.Size = new System.Drawing.Size(1605, 238);
            this.gcUserBook.TabIndex = 111;
            this.gcUserBook.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUserBook});
            // 
            // gvUserBook
            // 
            this.gvUserBook.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvUserBook.Appearance.Empty.Options.UseBackColor = true;
            this.gvUserBook.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvUserBook.Appearance.Row.Options.UseFont = true;
            this.gvUserBook.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvUserBook.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.LightGreen;
            formatConditionRuleExpression1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Appearance.Options.UseFont = true;
            formatConditionRuleExpression1.Expression = "[ParentId] = 0";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            formatConditionRuleExpression2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Appearance.Options.UseFont = true;
            formatConditionRuleExpression2.Expression = "[ParentId] != 0";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.gvUserBook.FormatRules.Add(gridFormatRule1);
            this.gvUserBook.FormatRules.Add(gridFormatRule2);
            this.gvUserBook.GridControl = this.gcUserBook;
            this.gvUserBook.Name = "gvUserBook";
            this.gvUserBook.OptionsBehavior.Editable = false;
            this.gvUserBook.OptionsBehavior.ReadOnly = true;
            this.gvUserBook.OptionsCustomization.AllowFilter = false;
            this.gvUserBook.OptionsFind.AllowFindPanel = false;
            this.gvUserBook.OptionsMenu.EnableColumnMenu = false;
            this.gvUserBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvUserBook.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvUserBook.OptionsView.ShowGroupPanel = false;
            this.gvUserBook.OptionsView.ShowIndicator = false;
            this.gvUserBook.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvUserBook.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvUserBook_RowCellClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(9, 272);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 17);
            this.label2.TabIndex = 112;
            this.label2.Text = "Kullanıcı Adı Ara:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(147, 272);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(186, 20);
            this.txtSearch.TabIndex = 113;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // gcUser
            // 
            this.gcUser.Location = new System.Drawing.Point(17, 66);
            this.gcUser.MainView = this.gvUser;
            this.gcUser.Name = "gcUser";
            this.gcUser.Size = new System.Drawing.Size(321, 173);
            this.gcUser.TabIndex = 115;
            this.gcUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUser});
            // 
            // gvUser
            // 
            this.gvUser.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvUser.Appearance.Empty.Options.UseBackColor = true;
            this.gvUser.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvUser.Appearance.Row.Options.UseFont = true;
            this.gvUser.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvUser.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleExpression3.Appearance.BackColor = System.Drawing.Color.LightGreen;
            formatConditionRuleExpression3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression3.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression3.Appearance.Options.UseFont = true;
            formatConditionRuleExpression3.Expression = "[ParentId] = 0";
            gridFormatRule3.Rule = formatConditionRuleExpression3;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Name = "Format1";
            formatConditionRuleExpression4.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            formatConditionRuleExpression4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression4.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression4.Appearance.Options.UseFont = true;
            formatConditionRuleExpression4.Expression = "[ParentId] != 0";
            gridFormatRule4.Rule = formatConditionRuleExpression4;
            this.gvUser.FormatRules.Add(gridFormatRule3);
            this.gvUser.FormatRules.Add(gridFormatRule4);
            this.gvUser.GridControl = this.gcUser;
            this.gvUser.Name = "gvUser";
            this.gvUser.OptionsBehavior.Editable = false;
            this.gvUser.OptionsBehavior.ReadOnly = true;
            this.gvUser.OptionsCustomization.AllowFilter = false;
            this.gvUser.OptionsFind.AllowFindPanel = false;
            this.gvUser.OptionsMenu.EnableColumnMenu = false;
            this.gvUser.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvUser.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvUser.OptionsView.ShowGroupPanel = false;
            this.gvUser.OptionsView.ShowIndicator = false;
            this.gvUser.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvUser.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvUser_RowCellClick);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(117, 37);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(186, 23);
            this.txtUser.TabIndex = 116;
            this.txtUser.TextChanged += new System.EventHandler(this.txtUser_TextChanged);
            // 
            // gcBook
            // 
            this.gcBook.Location = new System.Drawing.Point(344, 66);
            this.gcBook.MainView = this.gvBook;
            this.gcBook.Name = "gcBook";
            this.gcBook.Size = new System.Drawing.Size(349, 173);
            this.gcBook.TabIndex = 117;
            this.gcBook.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBook});
            // 
            // gvBook
            // 
            this.gvBook.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvBook.Appearance.Empty.Options.UseBackColor = true;
            this.gvBook.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvBook.Appearance.Row.Options.UseFont = true;
            this.gvBook.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvBook.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Name = "Format0";
            formatConditionRuleExpression5.Appearance.BackColor = System.Drawing.Color.LightGreen;
            formatConditionRuleExpression5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression5.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression5.Appearance.Options.UseFont = true;
            formatConditionRuleExpression5.Expression = "[ParentId] = 0";
            gridFormatRule5.Rule = formatConditionRuleExpression5;
            gridFormatRule6.ApplyToRow = true;
            gridFormatRule6.Name = "Format1";
            formatConditionRuleExpression6.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            formatConditionRuleExpression6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression6.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression6.Appearance.Options.UseFont = true;
            formatConditionRuleExpression6.Expression = "[ParentId] != 0";
            gridFormatRule6.Rule = formatConditionRuleExpression6;
            this.gvBook.FormatRules.Add(gridFormatRule5);
            this.gvBook.FormatRules.Add(gridFormatRule6);
            this.gvBook.GridControl = this.gcBook;
            this.gvBook.Name = "gvBook";
            this.gvBook.OptionsBehavior.Editable = false;
            this.gvBook.OptionsBehavior.ReadOnly = true;
            this.gvBook.OptionsCustomization.AllowFilter = false;
            this.gvBook.OptionsFind.AllowFindPanel = false;
            this.gvBook.OptionsMenu.EnableColumnMenu = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvBook.OptionsView.ShowGroupPanel = false;
            this.gvBook.OptionsView.ShowIndicator = false;
            this.gvBook.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvBook.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBook_RowCellClick);
            // 
            // txtBook
            // 
            this.txtBook.Location = new System.Drawing.Point(420, 37);
            this.txtBook.Name = "txtBook";
            this.txtBook.Size = new System.Drawing.Size(186, 23);
            this.txtBook.TabIndex = 118;
            this.txtBook.TextChanged += new System.EventHandler(this.txtBook_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(14, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 17);
            this.label1.TabIndex = 119;
            this.label1.Text = "Kullanıcı Adı";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(341, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 120;
            this.label3.Text = "Kitap Adı";
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(699, 203);
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnSave.Size = new System.Drawing.Size(121, 36);
            this.btnSave.TabIndex = 121;
            this.btnSave.Text = "Ekle";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(1502, 539);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDelete.Size = new System.Drawing.Size(115, 29);
            this.btnDelete.TabIndex = 124;
            this.btnDelete.Text = "Seçileni Kaldır";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // gbUserBook
            // 
            this.gbUserBook.Controls.Add(this.lblNumberOfComment);
            this.gbUserBook.Controls.Add(this.lblNumberOfFavorite);
            this.gbUserBook.Controls.Add(this.btnSave);
            this.gbUserBook.Controls.Add(this.lblNumberOfReads);
            this.gbUserBook.Controls.Add(this.txtBook);
            this.gbUserBook.Controls.Add(this.label3);
            this.gbUserBook.Controls.Add(this.gcUser);
            this.gbUserBook.Controls.Add(this.label1);
            this.gbUserBook.Controls.Add(this.gcBook);
            this.gbUserBook.Controls.Add(this.txtUser);
            this.gbUserBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbUserBook.Location = new System.Drawing.Point(12, 21);
            this.gbUserBook.Name = "gbUserBook";
            this.gbUserBook.Size = new System.Drawing.Size(1601, 245);
            this.gbUserBook.TabIndex = 125;
            this.gbUserBook.TabStop = false;
            this.gbUserBook.Text = "Kullanıcı ve Kitap Atama Bilgileri";
            // 
            // lblNumberOfComment
            // 
            this.lblNumberOfComment.AutoSize = true;
            this.lblNumberOfComment.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfComment.Location = new System.Drawing.Point(1190, 213);
            this.lblNumberOfComment.Name = "lblNumberOfComment";
            this.lblNumberOfComment.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfComment.TabIndex = 118;
            // 
            // lblNumberOfFavorite
            // 
            this.lblNumberOfFavorite.AutoSize = true;
            this.lblNumberOfFavorite.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfFavorite.Location = new System.Drawing.Point(846, 212);
            this.lblNumberOfFavorite.Name = "lblNumberOfFavorite";
            this.lblNumberOfFavorite.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfFavorite.TabIndex = 116;
            // 
            // lblNumberOfReads
            // 
            this.lblNumberOfReads.AutoSize = true;
            this.lblNumberOfReads.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfReads.Location = new System.Drawing.Point(426, 213);
            this.lblNumberOfReads.Name = "lblNumberOfReads";
            this.lblNumberOfReads.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfReads.TabIndex = 114;
            // 
            // UserBookOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 580);
            this.Controls.Add(this.gbUserBook);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gcUserBook);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UserBookOperations";
            this.Text = "UserBookOperations";
            this.Load += new System.EventHandler(this.UserBookOperations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcUserBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).EndInit();
            this.gbUserBook.ResumeLayout(false);
            this.gbUserBook.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcUserBook;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUserBook;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearch;
        private DevExpress.XtraGrid.GridControl gcUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUser;
        private System.Windows.Forms.TextBox txtUser;
        private DevExpress.XtraGrid.GridControl gcBook;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBook;
        private System.Windows.Forms.TextBox txtBook;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private System.Windows.Forms.GroupBox gbUserBook;
        private System.Windows.Forms.Label lblNumberOfComment;
        private System.Windows.Forms.Label lblNumberOfFavorite;
        private System.Windows.Forms.Label lblNumberOfReads;
    }
}