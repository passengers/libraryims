﻿using LibraryIMS.App.Utilities;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using System;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class AddCommentOperations : Form
    {
        private readonly ICommentService _commentService;
        private readonly IBookService _bookService;
        private readonly IUserService _userService;
        private int _bookId = -1;
        private int _userId = -1;
        public AddCommentOperations(ICommentService commentService, IBookService bookService, IUserService userService, int bookId, int userId)
        {
            _commentService = commentService;
            _bookService = bookService;
            _userService = userService;
            _bookId = bookId;
            _userId = userId;
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddCommentOperations_Load(object sender, EventArgs e)
        {
            AddBookObject(_bookId);
        }
        private void AddBookObject(int bookId)
        {
            var book = _bookService.GetBook(bookId);
            txtBookName.Text = book.Data.Book.Name;
            txtDescription.Text = book.Data.Book.Description;
            lblRating.Text = book.Data.Book.RatingAverage.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var yorum = txtYorum.Text;
            if (!(String.IsNullOrEmpty(yorum))) 
            {
                var commentDTO = new CommentAddDto
                {
                    CommentText = yorum,
                    UserId = _userId,
                    BookId = _bookId
                };
                var result = _commentService.AddComment(commentDTO, _userService.GetUser(_userId).Data.User.UserName);
                if (result.ResultStatus == ResultStatus.Success)
                    Alert.Show(result.Message, ResultStatus.Success);
                else
                    Alert.Show(result.Message, ResultStatus.Error);
            }
            else
            {
                Alert.Show("Yorum Girmediniz.", ResultStatus.Error);
            }
        }
    }
}
