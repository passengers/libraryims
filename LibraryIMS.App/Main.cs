﻿using System;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Services.Abstract;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;

namespace LibraryIMS.App
{
    public partial class Main : Form
    {
        #region Field

        private readonly IUserBookService _userBookService;
        private readonly IFavoriteBookService _favoriteBookService;
        private readonly IUserService _userService;
        private readonly IImageHelper _imageHelper;
        private readonly ICategoryService _categoryService;
        private readonly IPublisherService _publisherService;
        private readonly IWriterService _writerService;
        private readonly ICommentService _commentService;
        private readonly IBookService _bookService;
        private readonly IContactService _contactService;
        private readonly IBookCategoryService _bookCategoryService;
        private int _userId;
        private string _userName;

        #endregion Field

        #region Constructor

        public Main(IUserService userService, IImageHelper imageHelper, ICategoryService categoryService, IPublisherService publisherService, IWriterService writerService, ICommentService commentService, IBookService bookService, IContactService contactService, IBookCategoryService bookCategoryService, IFavoriteBookService favoriteBookService, IUserBookService userBookService)
        {
            _userService = userService;
            _imageHelper = imageHelper;
            _categoryService = categoryService;
            _publisherService = publisherService;
            _writerService = writerService;
            _commentService = commentService;
            _bookService = bookService;
            _contactService = contactService;
            _bookCategoryService = bookCategoryService;
            _favoriteBookService = favoriteBookService;
            _userBookService = userBookService;

            InitializeComponent();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Parametre olarak bir form nesnesi alır ve bu formu PageMain üzerindeki MdiContainer üzerinde gösterir.
        /// </summary>
        private void GetForm(Form form, bool connect = true)
        {
            centerPanel.Controls.Clear();
            if (connect)
            {
                form.MdiParent = this;
                form.Dock = DockStyle.Fill;
                form.FormBorderStyle = FormBorderStyle.None;
                centerPanel.Controls.Add(form);
                form.Show();
            }
        }

        #endregion Methods

        #region Buttons

        private void btnLogin_Click(object sender, System.EventArgs e)
        {
            var login = new Login(_userService);
            var result = login.ShowDialog();
            if (result != DialogResult.OK) return;
            topPanel.Visible = false;
            var user = _userService.GetUserByUserName(login.UserName);
            if (user.ResultStatus != ResultStatus.Success) return;
            _userId = user.Data.User.Id;
            _userName = user.Data.User.FirstName + " " + user.Data.User.LastName;
            switch (user.Data.User.AccessStatus)
            {
                case AccessStatus.Manager:
                    adminPanel.Visible = true;
                    break;
                case AccessStatus.Member:
                    userPanel.Visible = true;
                    break;
                case AccessStatus.Visitor:
                    userPanel.Visible = true;
                    break;
                case AccessStatus.Personnel:
                    userPanel.Visible = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        private void btnExit_Click(object sender, System.EventArgs e)
        {
            if (XtraMessageBox.Show(
                $"Uygualamdan çıkış yapılacaktır. Onaylıyor musunuz?",
                "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                    Application.Exit();
            }
        }

        private void btnUserInfo_Click(object sender, System.EventArgs e)
        {
            var userForm = new UserOperations(_userService,_imageHelper);
            GetForm(userForm);
        }
        private void btnBookInfo_Click(object sender, System.EventArgs e)
        {
            var bookForm = new BookOperations(_bookService, _imageHelper, _writerService, _publisherService, _bookCategoryService, _categoryService);
            GetForm(bookForm);
        }
        private void btnCategoryInfo_Click(object sender, System.EventArgs e)
        {
            var categoryForm = new CategoryOperations(_categoryService,_bookCategoryService);
            var result = categoryForm.ShowDialog();
            if (result == DialogResult.OK) Close();
        }
        private void btnWriterInfo_Click(object sender, System.EventArgs e)
        {
            var writerForm = new WriterOperations(_writerService, _imageHelper);
            GetForm(writerForm);
        }
        private void btnPublisherInfo_Click(object sender, System.EventArgs e)
        {
            var publisherForm = new PublisherOperations(_publisherService);
            var result = publisherForm.ShowDialog();
            if (result == DialogResult.OK) Close();
        }
        private void btnCommentInfo_Click(object sender, System.EventArgs e)
        {
            var writerForm = new CommentOperations(_commentService,_bookService,_userService);
            GetForm(writerForm);
        }
        private void btnContactInfo_Click(object sender, System.EventArgs e)
        {
            var contactForm = new ContactOperations(_contactService);
            GetForm(contactForm);
        }
        private void btnUserBookInfo_Click(object sender, System.EventArgs e)
        {
            var userBookForm = new UserBookOperations(_userBookService, _userService, _bookService);
            var result = userBookForm.ShowDialog();
            if (result == DialogResult.OK) Close();
        }
        private void btnUserRoleInfo_Click(object sender, System.EventArgs e)
        {
            var userRoleForm = new UserRoleOperations(_userService);
            var result = userRoleForm.ShowDialog();
            if (result == DialogResult.OK) Close();
        }
        private void btnBookCategoryInfo_Click(object sender, System.EventArgs e)
        {
            var bookCategoriesForm = new BookCategoryOperations(_categoryService,_bookService,_bookCategoryService);
            GetForm(bookCategoriesForm);
        }
        private void btnControlCenter_Click(object sender, EventArgs e)
        {
            var controlCenter = new ControlCenter(_userService,_bookService,_categoryService,_contactService,_commentService,_writerService,_publisherService);
            GetForm(controlCenter);
        }
        private void btnAdminClose_Click(object sender, System.EventArgs e)
        {
            if (XtraMessageBox.Show(
                $"{_userName} çıkışınız yapılacaktır. Onaylıyor musunuz?",
                "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                centerPanel.Controls.Clear();
                topPanel.Visible = true;
                adminPanel.Visible = false;
                Alert.Show(_userName + " çıkışınız başarıyla gerçekleşmiştir.", ResultStatus.Success);
            }
        }

        private void btnArchiveInfo_Click(object sender, System.EventArgs e)
        {
            var archiveForm = new ArchiveOperations(_bookService, _imageHelper, _writerService, _publisherService, _userId, _favoriteBookService,_categoryService,_bookCategoryService,_commentService,_userService);
            GetForm(archiveForm);
        }
        private void btnFavoriteInfo_Click(object sender, System.EventArgs e)
        {
            var favoriteBook = new FavoriteBookOperations(_bookService,_imageHelper,_writerService,_publisherService,_userId,_favoriteBookService,_categoryService,_bookCategoryService);
            GetForm(favoriteBook);
        }
        private void btnReadInfo_Click(object sender, System.EventArgs e)
        {
            var readForm = new ReadOperations(_userBookService, _userService, _writerService, _bookService, _publisherService, _imageHelper, _userId, _categoryService, _bookCategoryService);
            GetForm(readForm);
        }
        private void btnComment_Click(object sender, System.EventArgs e)
        {
            var commenterForm = new UserCommentOperations(_commentService, _userService, _userId);
            GetForm(commenterForm);
        }
        private void btnWriter_Click(object sender, System.EventArgs e)
        {
            var userWriterForm = new UserWriterOperations(_writerService, _imageHelper);
            GetForm(userWriterForm);
        }
        private void btnProfileInfo_Click(object sender, System.EventArgs e)
        {
            var profileForm = new ProfileOperation(_userService, _imageHelper, _userId);
            GetForm(profileForm);
        }
        private void btnMessageInfo_Click(object sender, System.EventArgs e)
        {
            var messageForm = new MessageOperation(_contactService, _userId, _userService);
            var result = messageForm.ShowDialog();
            if (result == DialogResult.OK) Close();
        }
        private void btnUserClose_Click(object sender, System.EventArgs e)
        {
            if (XtraMessageBox.Show(
                $"{_userName} çıkışınız yapılacaktır. Onaylıyor musunuz?",
                "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                centerPanel.Controls.Clear();
                topPanel.Visible = true;
                userPanel.Visible = false;
                Alert.Show(_userName + " çıkışınız başarıyla gerçekleşmiştir.", ResultStatus.Success);
            }
        }

        #endregion Buttons
    }
}
