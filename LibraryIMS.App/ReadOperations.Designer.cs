﻿
namespace LibraryIMS.App
{
    partial class ReadOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadOperations));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression3 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression4 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.gbArchive = new System.Windows.Forms.GroupBox();
            this.cbCategories = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.cbWriters1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNumberOfComment = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.lblControl3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lnkExcel = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblClock = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRead = new DevExpress.XtraEditors.SimpleButton();
            this.lblNumberOfFavorite = new System.Windows.Forms.Label();
            this.txtAbout = new DevExpress.XtraEditors.MemoEdit();
            this.lblControl2 = new System.Windows.Forms.Label();
            this.lblNumberOfReads = new System.Windows.Forms.Label();
            this.lblControl = new System.Windows.Forms.Label();
            this.cbWriters = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbPublisher = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtStock = new DevExpress.XtraEditors.TextEdit();
            this.txtShelfNum = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReleaseDate = new DevExpress.XtraEditors.DateEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPageNum = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.writerlbl = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gcArchive = new DevExpress.XtraGrid.GridControl();
            this.gvArchive = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.gbArchive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategories.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters1.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelfNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcArchive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvArchive)).BeginInit();
            this.SuspendLayout();
            // 
            // gbArchive
            // 
            this.gbArchive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbArchive.Controls.Add(this.cbCategories);
            this.gbArchive.Controls.Add(this.label8);
            this.gbArchive.Controls.Add(this.cbWriters1);
            this.gbArchive.Controls.Add(this.label4);
            this.gbArchive.Controls.Add(this.lblNumberOfComment);
            this.gbArchive.Controls.Add(this.label10);
            this.gbArchive.Controls.Add(this.txtSearch);
            this.gbArchive.Controls.Add(this.btnClear);
            this.gbArchive.Controls.Add(this.lblControl3);
            this.gbArchive.Controls.Add(this.panel2);
            this.gbArchive.Controls.Add(this.btnRead);
            this.gbArchive.Controls.Add(this.lblNumberOfFavorite);
            this.gbArchive.Controls.Add(this.txtAbout);
            this.gbArchive.Controls.Add(this.lblControl2);
            this.gbArchive.Controls.Add(this.lblNumberOfReads);
            this.gbArchive.Controls.Add(this.lblControl);
            this.gbArchive.Controls.Add(this.cbWriters);
            this.gbArchive.Controls.Add(this.cbPublisher);
            this.gbArchive.Controls.Add(this.txtStock);
            this.gbArchive.Controls.Add(this.txtShelfNum);
            this.gbArchive.Controls.Add(this.label2);
            this.gbArchive.Controls.Add(this.txtReleaseDate);
            this.gbArchive.Controls.Add(this.label15);
            this.gbArchive.Controls.Add(this.txtPageNum);
            this.gbArchive.Controls.Add(this.txtName);
            this.gbArchive.Controls.Add(this.label3);
            this.gbArchive.Controls.Add(this.label16);
            this.gbArchive.Controls.Add(this.label6);
            this.gbArchive.Controls.Add(this.label14);
            this.gbArchive.Controls.Add(this.writerlbl);
            this.gbArchive.Controls.Add(this.label13);
            this.gbArchive.Controls.Add(this.pictureBox1);
            this.gbArchive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbArchive.Location = new System.Drawing.Point(13, 87);
            this.gbArchive.Margin = new System.Windows.Forms.Padding(4);
            this.gbArchive.Name = "gbArchive";
            this.gbArchive.Padding = new System.Windows.Forms.Padding(4);
            this.gbArchive.Size = new System.Drawing.Size(1788, 395);
            this.gbArchive.TabIndex = 2;
            this.gbArchive.TabStop = false;
            this.gbArchive.Text = "Kitap Bilgileri";
            // 
            // cbCategories
            // 
            this.cbCategories.Location = new System.Drawing.Point(540, 336);
            this.cbCategories.Margin = new System.Windows.Forms.Padding(5);
            this.cbCategories.Name = "cbCategories";
            this.cbCategories.Properties.AllowFocused = false;
            this.cbCategories.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCategories.Properties.NullValuePrompt = "Kategoriler";
            this.cbCategories.Size = new System.Drawing.Size(272, 22);
            this.cbCategories.TabIndex = 122;
            this.cbCategories.SelectedIndexChanged += new System.EventHandler(this.cbCategories_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(418, 338);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 18);
            this.label8.TabIndex = 121;
            this.label8.Text = "Kategoriler:";
            // 
            // cbWriters1
            // 
            this.cbWriters1.Location = new System.Drawing.Point(121, 336);
            this.cbWriters1.Margin = new System.Windows.Forms.Padding(5);
            this.cbWriters1.Name = "cbWriters1";
            this.cbWriters1.Properties.AllowFocused = false;
            this.cbWriters1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriters1.Properties.NullValuePrompt = "Yazarlar";
            this.cbWriters1.Size = new System.Drawing.Size(270, 22);
            this.cbWriters1.TabIndex = 120;
            this.cbWriters1.SelectedIndexChanged += new System.EventHandler(this.cbWriters1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(8, 336);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 18);
            this.label4.TabIndex = 119;
            this.label4.Text = "Yazarlar:";
            // 
            // lblNumberOfComment
            // 
            this.lblNumberOfComment.AutoSize = true;
            this.lblNumberOfComment.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfComment.Location = new System.Drawing.Point(1587, 262);
            this.lblNumberOfComment.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfComment.Name = "lblNumberOfComment";
            this.lblNumberOfComment.Size = new System.Drawing.Size(0, 20);
            this.lblNumberOfComment.TabIndex = 118;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(8, 297);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 18);
            this.label10.TabIndex = 95;
            this.label10.Text = "Kitap Ara:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(147, 297);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(270, 26);
            this.txtSearch.TabIndex = 93;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnClear
            // 
            this.btnClear.Appearance.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(1591, 233);
            this.btnClear.Margin = new System.Windows.Forms.Padding(5);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(146, 41);
            this.btnClear.TabIndex = 112;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblControl3
            // 
            this.lblControl3.AutoSize = true;
            this.lblControl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblControl3.ForeColor = System.Drawing.Color.DarkRed;
            this.lblControl3.Location = new System.Drawing.Point(1068, 246);
            this.lblControl3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblControl3.Name = "lblControl3";
            this.lblControl3.Size = new System.Drawing.Size(117, 18);
            this.lblControl3.TabIndex = 117;
            this.lblControl3.Text = "Yorum Sayısı: ";
            this.lblControl3.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lnkExcel);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.lblClock);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(821, 310);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(717, 44);
            this.panel2.TabIndex = 94;
            // 
            // lnkExcel
            // 
            this.lnkExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lnkExcel.Appearance.Options.UseFont = true;
            this.lnkExcel.Location = new System.Drawing.Point(4, 10);
            this.lnkExcel.Margin = new System.Windows.Forms.Padding(4);
            this.lnkExcel.Name = "lnkExcel";
            this.lnkExcel.Size = new System.Drawing.Size(38, 17);
            this.lnkExcel.TabIndex = 90;
            this.lnkExcel.Text = "Excel";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(227, 11);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Stokta Olmayanlar";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(95, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "Aktif Kitap";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.ForeColor = System.Drawing.Color.LightCoral;
            this.label7.Location = new System.Drawing.Point(207, 7);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 31);
            this.label7.TabIndex = 20;
            this.label7.Text = "•";
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(448, 11);
            this.lblClock.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(224, 24);
            this.lblClock.TabIndex = 18;
            this.lblClock.Text = "29 Aralık 2020 24:24:24";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.LightBlue;
            this.label5.Location = new System.Drawing.Point(75, 7);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 31);
            this.label5.TabIndex = 20;
            this.label5.Text = "•";
            // 
            // btnRead
            // 
            this.btnRead.Appearance.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnRead.Appearance.Options.UseFont = true;
            this.btnRead.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRead.ImageOptions.Image")));
            this.btnRead.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnRead.Location = new System.Drawing.Point(1343, 232);
            this.btnRead.Margin = new System.Windows.Forms.Padding(4);
            this.btnRead.Name = "btnRead";
            this.btnRead.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnRead.Size = new System.Drawing.Size(220, 42);
            this.btnRead.TabIndex = 12;
            this.btnRead.Text = "Okuduklarımdan \r\nÇıkar";
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // lblNumberOfFavorite
            // 
            this.lblNumberOfFavorite.AutoSize = true;
            this.lblNumberOfFavorite.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfFavorite.Location = new System.Drawing.Point(1084, 246);
            this.lblNumberOfFavorite.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfFavorite.Name = "lblNumberOfFavorite";
            this.lblNumberOfFavorite.Size = new System.Drawing.Size(0, 20);
            this.lblNumberOfFavorite.TabIndex = 116;
            // 
            // txtAbout
            // 
            this.txtAbout.Location = new System.Drawing.Point(1317, 25);
            this.txtAbout.Margin = new System.Windows.Forms.Padding(4);
            this.txtAbout.Name = "txtAbout";
            this.txtAbout.Properties.AllowFocused = false;
            this.txtAbout.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtAbout.Properties.NullValuePrompt = "Açıklama";
            this.txtAbout.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtAbout.Size = new System.Drawing.Size(443, 190);
            this.txtAbout.TabIndex = 102;
            // 
            // lblControl2
            // 
            this.lblControl2.AutoSize = true;
            this.lblControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblControl2.ForeColor = System.Drawing.Color.DarkRed;
            this.lblControl2.Location = new System.Drawing.Point(704, 246);
            this.lblControl2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblControl2.Name = "lblControl2";
            this.lblControl2.Size = new System.Drawing.Size(213, 18);
            this.lblControl2.TabIndex = 115;
            this.lblControl2.Text = "Favorilere Eklenme Sayısı: ";
            this.lblControl2.Visible = false;
            // 
            // lblNumberOfReads
            // 
            this.lblNumberOfReads.AutoSize = true;
            this.lblNumberOfReads.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfReads.Location = new System.Drawing.Point(524, 247);
            this.lblNumberOfReads.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumberOfReads.Name = "lblNumberOfReads";
            this.lblNumberOfReads.Size = new System.Drawing.Size(0, 20);
            this.lblNumberOfReads.TabIndex = 114;
            // 
            // lblControl
            // 
            this.lblControl.AutoSize = true;
            this.lblControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblControl.ForeColor = System.Drawing.Color.DarkRed;
            this.lblControl.Location = new System.Drawing.Point(349, 246);
            this.lblControl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblControl.Name = "lblControl";
            this.lblControl.Size = new System.Drawing.Size(131, 18);
            this.lblControl.TabIndex = 113;
            this.lblControl.Text = "Okunma Sayısı: ";
            this.lblControl.Visible = false;
            // 
            // cbWriters
            // 
            this.cbWriters.Location = new System.Drawing.Point(479, 82);
            this.cbWriters.Margin = new System.Windows.Forms.Padding(4);
            this.cbWriters.Name = "cbWriters";
            this.cbWriters.Properties.AllowFocused = false;
            this.cbWriters.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriters.Properties.NullValuePrompt = "Yazar";
            this.cbWriters.Size = new System.Drawing.Size(247, 22);
            this.cbWriters.TabIndex = 95;
            // 
            // cbPublisher
            // 
            this.cbPublisher.Location = new System.Drawing.Point(478, 139);
            this.cbPublisher.Margin = new System.Windows.Forms.Padding(4);
            this.cbPublisher.Name = "cbPublisher";
            this.cbPublisher.Properties.AllowFocused = false;
            this.cbPublisher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPublisher.Properties.NullValuePrompt = "Yayınevi";
            this.cbPublisher.Size = new System.Drawing.Size(247, 22);
            this.cbPublisher.TabIndex = 96;
            // 
            // txtStock
            // 
            this.txtStock.Location = new System.Drawing.Point(910, 76);
            this.txtStock.Margin = new System.Windows.Forms.Padding(4);
            this.txtStock.Name = "txtStock";
            this.txtStock.Properties.AllowFocused = false;
            this.txtStock.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtStock.Properties.NullValuePrompt = "Stok Sayısı";
            this.txtStock.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtStock.Size = new System.Drawing.Size(285, 24);
            this.txtStock.TabIndex = 99;
            // 
            // txtShelfNum
            // 
            this.txtShelfNum.Location = new System.Drawing.Point(910, 23);
            this.txtShelfNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtShelfNum.Name = "txtShelfNum";
            this.txtShelfNum.Properties.AllowFocused = false;
            this.txtShelfNum.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtShelfNum.Properties.NullValuePrompt = "Raf Numarası";
            this.txtShelfNum.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtShelfNum.Size = new System.Drawing.Size(285, 24);
            this.txtShelfNum.TabIndex = 98;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(752, 138);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 111;
            this.label2.Text = "Yayın Tarihi:";
            // 
            // txtReleaseDate
            // 
            this.txtReleaseDate.EditValue = null;
            this.txtReleaseDate.Location = new System.Drawing.Point(910, 134);
            this.txtReleaseDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtReleaseDate.Name = "txtReleaseDate";
            this.txtReleaseDate.Properties.AllowFocused = false;
            this.txtReleaseDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtReleaseDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtReleaseDate.Properties.CalendarTimeProperties.Mask.EditMask = "";
            this.txtReleaseDate.Properties.CalendarTimeProperties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtReleaseDate.Properties.Mask.EditMask = "";
            this.txtReleaseDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtReleaseDate.Properties.NullValuePrompt = "Yayın Tarihi";
            this.txtReleaseDate.Size = new System.Drawing.Size(285, 22);
            this.txtReleaseDate.TabIndex = 100;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(1218, 23);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 20);
            this.label15.TabIndex = 110;
            this.label15.Text = "Açıklama:";
            // 
            // txtPageNum
            // 
            this.txtPageNum.Location = new System.Drawing.Point(478, 195);
            this.txtPageNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtPageNum.Name = "txtPageNum";
            this.txtPageNum.Properties.AllowFocused = false;
            this.txtPageNum.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtPageNum.Properties.MaxLength = 8;
            this.txtPageNum.Properties.NullValuePrompt = "Sayfa Sayısı";
            this.txtPageNum.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtPageNum.Size = new System.Drawing.Size(248, 24);
            this.txtPageNum.TabIndex = 97;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(478, 25);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.Properties.AllowFocused = false;
            this.txtName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtName.Properties.NullValuePrompt = "Adı";
            this.txtName.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtName.Size = new System.Drawing.Size(248, 24);
            this.txtName.TabIndex = 94;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(349, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 20);
            this.label3.TabIndex = 105;
            this.label3.Text = "Sayfa Sayısı:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(349, 139);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 20);
            this.label16.TabIndex = 109;
            this.label16.Text = "Yayınevi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(752, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 20);
            this.label6.TabIndex = 103;
            this.label6.Text = "Raf Numarası:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(349, 27);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 20);
            this.label14.TabIndex = 108;
            this.label14.Text = "Adı:";
            // 
            // writerlbl
            // 
            this.writerlbl.AutoSize = true;
            this.writerlbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.writerlbl.Location = new System.Drawing.Point(349, 80);
            this.writerlbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.writerlbl.Name = "writerlbl";
            this.writerlbl.Size = new System.Drawing.Size(68, 20);
            this.writerlbl.TabIndex = 107;
            this.writerlbl.Text = "Yazarı:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(752, 81);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 20);
            this.label13.TabIndex = 106;
            this.label13.Text = "Stok Sayısı:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(27, 27);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(289, 209);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 88;
            this.pictureBox1.TabStop = false;
            // 
            // gcArchive
            // 
            this.gcArchive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcArchive.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.gcArchive.Location = new System.Drawing.Point(2, 492);
            this.gcArchive.MainView = this.gvArchive;
            this.gcArchive.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.gcArchive.Name = "gcArchive";
            this.gcArchive.Size = new System.Drawing.Size(1549, 254);
            this.gcArchive.TabIndex = 96;
            this.gcArchive.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvArchive});
            // 
            // gvArchive
            // 
            this.gvArchive.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvArchive.Appearance.Empty.Options.UseBackColor = true;
            this.gvArchive.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvArchive.Appearance.Row.Options.UseFont = true;
            this.gvArchive.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvArchive.DetailHeight = 674;
            this.gvArchive.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleExpression3.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            formatConditionRuleExpression3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression3.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression3.Appearance.Options.UseFont = true;
            formatConditionRuleExpression3.Expression = "[GeneralStatus] = 1";
            gridFormatRule3.Rule = formatConditionRuleExpression3;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Name = "Format1";
            formatConditionRuleExpression4.Appearance.BackColor = System.Drawing.Color.IndianRed;
            formatConditionRuleExpression4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression4.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression4.Appearance.Options.UseFont = true;
            formatConditionRuleExpression4.Expression = "[GeneralStatus] != 1";
            gridFormatRule4.Rule = formatConditionRuleExpression4;
            this.gvArchive.FormatRules.Add(gridFormatRule3);
            this.gvArchive.FormatRules.Add(gridFormatRule4);
            this.gvArchive.GridControl = this.gcArchive;
            this.gvArchive.Name = "gvArchive";
            this.gvArchive.OptionsBehavior.Editable = false;
            this.gvArchive.OptionsBehavior.ReadOnly = true;
            this.gvArchive.OptionsCustomization.AllowFilter = false;
            this.gvArchive.OptionsFind.AllowFindPanel = false;
            this.gvArchive.OptionsMenu.EnableColumnMenu = false;
            this.gvArchive.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvArchive.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvArchive.OptionsView.ShowGroupPanel = false;
            this.gvArchive.OptionsView.ShowIndicator = false;
            this.gvArchive.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvArchive.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvArchive_RowCellClick);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // ReadOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1800, 747);
            this.Controls.Add(this.gcArchive);
            this.Controls.Add(this.gbArchive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReadOperations";
            this.Text = "a";
            this.Load += new System.EventHandler(this.ReadOperations_Load);
            this.gbArchive.ResumeLayout(false);
            this.gbArchive.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategories.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters1.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelfNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcArchive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvArchive)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbArchive;
        private System.Windows.Forms.Label lblNumberOfComment;
        private System.Windows.Forms.Label lblControl3;
        private System.Windows.Forms.Label lblNumberOfFavorite;
        private DevExpress.XtraEditors.SimpleButton btnRead;
        private System.Windows.Forms.Label lblControl2;
        private System.Windows.Forms.Label lblNumberOfReads;
        private System.Windows.Forms.Label lblControl;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriters;
        private DevExpress.XtraEditors.ComboBoxEdit cbPublisher;
        private DevExpress.XtraEditors.TextEdit txtStock;
        private DevExpress.XtraEditors.TextEdit txtShelfNum;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.DateEdit txtReleaseDate;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit txtPageNum;
        private DevExpress.XtraEditors.TextEdit txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label writerlbl;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.MemoEdit txtAbout;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.HyperlinkLabelControl lnkExcel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.GridControl gcArchive;
        private DevExpress.XtraGrid.Views.Grid.GridView gvArchive;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriters1;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.ComboBoxEdit cbCategories;
        private System.Windows.Forms.Timer timer;
    }
}