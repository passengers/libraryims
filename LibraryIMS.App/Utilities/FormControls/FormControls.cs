﻿using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace LibraryIMS.App.Utilities.FormControls
{
    static class FormControls
    {
        public static bool CheckFormControls(GroupBox groupBox)
        {
            int counter = 0;
            foreach (Control ctrl in groupBox.Controls)
            {
                if (ctrl is TextEdit || ctrl is DateEdit)
                {
                    if ((ctrl as BaseEdit).Text == "")
                    {
                        counter++;
                    }
                }
                else if (ctrl is ComboBoxEdit)
                {
                    if ((ctrl as ComboBoxEdit).SelectedIndex == 0)
                    {
                        counter++;
                    }
                }
            }
            if (counter > 0) return false;
            else return true;
        }
        public static void ClearFormControls(Control Ctrl)
        {
            foreach (Control ctrl in Ctrl.Controls)
            {
                BaseEdit editor = ctrl as BaseEdit;

                if (editor != null)
                    editor.EditValue = null;

                ClearFormControls(ctrl);
            }
        }
    }
}
