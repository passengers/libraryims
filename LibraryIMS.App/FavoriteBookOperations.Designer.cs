﻿
namespace LibraryIMS.App
{
    partial class FavoriteBookOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FavoriteBookOperations));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lblMessage = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.gcFavoriteBook = new DevExpress.XtraGrid.GridControl();
            this.gvFavoriteBook = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblNumberOfComment = new System.Windows.Forms.Label();
            this.lblControl3 = new System.Windows.Forms.Label();
            this.lblNumberOfFavorite = new System.Windows.Forms.Label();
            this.lblControl2 = new System.Windows.Forms.Label();
            this.lblNumberOfReads = new System.Windows.Forms.Label();
            this.lblControl = new System.Windows.Forms.Label();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.cbWriters = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbPublisher = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtShelfNum = new DevExpress.XtraEditors.TextEdit();
            this.panelTop = new System.Windows.Forms.Panel();
            this.cbCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.cbWriters1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lnkExcel = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.lblClock = new System.Windows.Forms.Label();
            this.gbFavoriteBook = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReleaseDate = new DevExpress.XtraEditors.DateEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPageNum = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.writerlbl = new System.Windows.Forms.Label();
            this.txtAbout = new DevExpress.XtraEditors.MemoEdit();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnFavorite = new DevExpress.XtraEditors.SimpleButton();
            this.panelBottom.SuspendLayout();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcFavoriteBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFavoriteBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelfNum.Properties)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters1.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.gbFavoriteBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMessage.Location = new System.Drawing.Point(1629, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 18);
            this.lblMessage.TabIndex = 1;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.lblMessage);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 732);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1629, 115);
            this.panelBottom.TabIndex = 100;
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gcFavoriteBook);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 380);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1629, 467);
            this.panelGrid.TabIndex = 99;
            // 
            // gcFavoriteBook
            // 
            this.gcFavoriteBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcFavoriteBook.Location = new System.Drawing.Point(12, 18);
            this.gcFavoriteBook.MainView = this.gvFavoriteBook;
            this.gcFavoriteBook.Name = "gcFavoriteBook";
            this.gcFavoriteBook.Size = new System.Drawing.Size(1601, 328);
            this.gcFavoriteBook.TabIndex = 86;
            this.gcFavoriteBook.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFavoriteBook});
            // 
            // gvFavoriteBook
            // 
            this.gvFavoriteBook.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvFavoriteBook.Appearance.Empty.Options.UseBackColor = true;
            this.gvFavoriteBook.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvFavoriteBook.Appearance.Row.Options.UseFont = true;
            this.gvFavoriteBook.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvFavoriteBook.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.NavajoWhite;
            formatConditionRuleExpression1.Appearance.BackColor2 = System.Drawing.Color.DeepSkyBlue;
            formatConditionRuleExpression1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Appearance.Options.UseFont = true;
            formatConditionRuleExpression1.Expression = "[GeneralStatus] = 1";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.NavajoWhite;
            formatConditionRuleExpression2.Appearance.BackColor2 = System.Drawing.Color.DeepSkyBlue;
            formatConditionRuleExpression2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Appearance.Options.UseFont = true;
            formatConditionRuleExpression2.Expression = "[Stock]  ==  0";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.gvFavoriteBook.FormatRules.Add(gridFormatRule1);
            this.gvFavoriteBook.FormatRules.Add(gridFormatRule2);
            this.gvFavoriteBook.GridControl = this.gcFavoriteBook;
            this.gvFavoriteBook.Name = "gvFavoriteBook";
            this.gvFavoriteBook.OptionsBehavior.Editable = false;
            this.gvFavoriteBook.OptionsBehavior.ReadOnly = true;
            this.gvFavoriteBook.OptionsCustomization.AllowFilter = false;
            this.gvFavoriteBook.OptionsFind.AllowFindPanel = false;
            this.gvFavoriteBook.OptionsMenu.EnableColumnMenu = false;
            this.gvFavoriteBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvFavoriteBook.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvFavoriteBook.OptionsView.ShowGroupPanel = false;
            this.gvFavoriteBook.OptionsView.ShowIndicator = false;
            this.gvFavoriteBook.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvFavoriteBook.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvFavoriteBook_RowCellClick);
            // 
            // lblNumberOfComment
            // 
            this.lblNumberOfComment.AutoSize = true;
            this.lblNumberOfComment.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfComment.Location = new System.Drawing.Point(1190, 213);
            this.lblNumberOfComment.Name = "lblNumberOfComment";
            this.lblNumberOfComment.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfComment.TabIndex = 118;
            // 
            // lblControl3
            // 
            this.lblControl3.AutoSize = true;
            this.lblControl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblControl3.ForeColor = System.Drawing.Color.DarkRed;
            this.lblControl3.Location = new System.Drawing.Point(1077, 211);
            this.lblControl3.Name = "lblControl3";
            this.lblControl3.Size = new System.Drawing.Size(112, 17);
            this.lblControl3.TabIndex = 117;
            this.lblControl3.Text = "Yorum Sayısı: ";
            this.lblControl3.Visible = false;
            // 
            // lblNumberOfFavorite
            // 
            this.lblNumberOfFavorite.AutoSize = true;
            this.lblNumberOfFavorite.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfFavorite.Location = new System.Drawing.Point(846, 212);
            this.lblNumberOfFavorite.Name = "lblNumberOfFavorite";
            this.lblNumberOfFavorite.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfFavorite.TabIndex = 116;
            // 
            // lblControl2
            // 
            this.lblControl2.AutoSize = true;
            this.lblControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblControl2.ForeColor = System.Drawing.Color.DarkRed;
            this.lblControl2.Location = new System.Drawing.Point(635, 212);
            this.lblControl2.Name = "lblControl2";
            this.lblControl2.Size = new System.Drawing.Size(205, 17);
            this.lblControl2.TabIndex = 115;
            this.lblControl2.Text = "Favorilere Eklenme Sayısı: ";
            this.lblControl2.Visible = false;
            // 
            // lblNumberOfReads
            // 
            this.lblNumberOfReads.AutoSize = true;
            this.lblNumberOfReads.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblNumberOfReads.Location = new System.Drawing.Point(426, 213);
            this.lblNumberOfReads.Name = "lblNumberOfReads";
            this.lblNumberOfReads.Size = new System.Drawing.Size(0, 17);
            this.lblNumberOfReads.TabIndex = 114;
            // 
            // lblControl
            // 
            this.lblControl.AutoSize = true;
            this.lblControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.lblControl.ForeColor = System.Drawing.Color.DarkRed;
            this.lblControl.Location = new System.Drawing.Point(295, 212);
            this.lblControl.Name = "lblControl";
            this.lblControl.Size = new System.Drawing.Size(125, 17);
            this.lblControl.TabIndex = 113;
            this.lblControl.Text = "Okunma Sayısı: ";
            this.lblControl.Visible = false;
            // 
            // btnClear
            // 
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(1471, 142);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(121, 36);
            this.btnClear.TabIndex = 112;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cbWriters
            // 
            this.cbWriters.Location = new System.Drawing.Point(429, 78);
            this.cbWriters.Name = "cbWriters";
            this.cbWriters.Properties.AllowFocused = false;
            this.cbWriters.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriters.Properties.NullValuePrompt = "Yazar";
            this.cbWriters.Size = new System.Drawing.Size(185, 20);
            this.cbWriters.TabIndex = 95;
            // 
            // cbPublisher
            // 
            this.cbPublisher.Location = new System.Drawing.Point(428, 124);
            this.cbPublisher.Name = "cbPublisher";
            this.cbPublisher.Properties.AllowFocused = false;
            this.cbPublisher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPublisher.Properties.NullValuePrompt = "Yayınevi";
            this.cbPublisher.Size = new System.Drawing.Size(185, 20);
            this.cbPublisher.TabIndex = 96;
            // 
            // txtShelfNum
            // 
            this.txtShelfNum.Location = new System.Drawing.Point(754, 32);
            this.txtShelfNum.Name = "txtShelfNum";
            this.txtShelfNum.Properties.AllowFocused = false;
            this.txtShelfNum.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtShelfNum.Properties.NullValuePrompt = "Raf Numarası";
            this.txtShelfNum.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtShelfNum.Size = new System.Drawing.Size(214, 22);
            this.txtShelfNum.TabIndex = 98;
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.cbCategory);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Controls.Add(this.cbWriters1);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.txtSearch);
            this.panelTop.Controls.Add(this.label10);
            this.panelTop.Controls.Add(this.panel2);
            this.panelTop.Controls.Add(this.gbFavoriteBook);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1629, 380);
            this.panelTop.TabIndex = 98;
            // 
            // cbCategory
            // 
            this.cbCategory.Location = new System.Drawing.Point(772, 341);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Properties.AllowFocused = false;
            this.cbCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCategory.Properties.NullValuePrompt = "Kategoriler";
            this.cbCategory.Size = new System.Drawing.Size(185, 20);
            this.cbCategory.TabIndex = 98;
            this.cbCategory.SelectedIndexChanged += new System.EventHandler(this.cbCategory_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(673, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 97;
            this.label1.Text = "Kategoriler:";
            // 
            // cbWriters1
            // 
            this.cbWriters1.Location = new System.Drawing.Point(470, 341);
            this.cbWriters1.Name = "cbWriters1";
            this.cbWriters1.Properties.AllowFocused = false;
            this.cbWriters1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriters1.Properties.NullValuePrompt = "Yazarlar";
            this.cbWriters1.Size = new System.Drawing.Size(185, 20);
            this.cbWriters1.TabIndex = 96;
            this.cbWriters1.SelectedIndexChanged += new System.EventHandler(this.cbWriters1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(379, 344);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 17);
            this.label4.TabIndex = 93;
            this.label4.Text = "Yazar Ara:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(98, 341);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(259, 20);
            this.txtSearch.TabIndex = 90;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(12, 344);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 17);
            this.label10.TabIndex = 92;
            this.label10.Text = "Kitap Ara:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lnkExcel);
            this.panel2.Controls.Add(this.lblClock);
            this.panel2.Location = new System.Drawing.Point(1340, 341);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(264, 30);
            this.panel2.TabIndex = 91;
            // 
            // lnkExcel
            // 
            this.lnkExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lnkExcel.Appearance.Options.UseFont = true;
            this.lnkExcel.Location = new System.Drawing.Point(3, 8);
            this.lnkExcel.Name = "lnkExcel";
            this.lnkExcel.Size = new System.Drawing.Size(38, 17);
            this.lnkExcel.TabIndex = 90;
            this.lnkExcel.Text = "Excel";
            this.lnkExcel.Click += new System.EventHandler(this.lnkExcel_Click);
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(63, 10);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(183, 18);
            this.lblClock.TabIndex = 18;
            this.lblClock.Text = "29 Aralık 2020 24:24:24";
            // 
            // gbFavoriteBook
            // 
            this.gbFavoriteBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFavoriteBook.Controls.Add(this.lblNumberOfComment);
            this.gbFavoriteBook.Controls.Add(this.lblControl3);
            this.gbFavoriteBook.Controls.Add(this.lblNumberOfFavorite);
            this.gbFavoriteBook.Controls.Add(this.lblControl2);
            this.gbFavoriteBook.Controls.Add(this.lblNumberOfReads);
            this.gbFavoriteBook.Controls.Add(this.lblControl);
            this.gbFavoriteBook.Controls.Add(this.btnClear);
            this.gbFavoriteBook.Controls.Add(this.cbWriters);
            this.gbFavoriteBook.Controls.Add(this.cbPublisher);
            this.gbFavoriteBook.Controls.Add(this.txtShelfNum);
            this.gbFavoriteBook.Controls.Add(this.label2);
            this.gbFavoriteBook.Controls.Add(this.txtReleaseDate);
            this.gbFavoriteBook.Controls.Add(this.label15);
            this.gbFavoriteBook.Controls.Add(this.txtPageNum);
            this.gbFavoriteBook.Controls.Add(this.txtName);
            this.gbFavoriteBook.Controls.Add(this.label3);
            this.gbFavoriteBook.Controls.Add(this.label16);
            this.gbFavoriteBook.Controls.Add(this.label6);
            this.gbFavoriteBook.Controls.Add(this.label14);
            this.gbFavoriteBook.Controls.Add(this.writerlbl);
            this.gbFavoriteBook.Controls.Add(this.txtAbout);
            this.gbFavoriteBook.Controls.Add(this.pictureBox1);
            this.gbFavoriteBook.Controls.Add(this.btnFavorite);
            this.gbFavoriteBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbFavoriteBook.Location = new System.Drawing.Point(12, 73);
            this.gbFavoriteBook.Name = "gbFavoriteBook";
            this.gbFavoriteBook.Size = new System.Drawing.Size(1601, 252);
            this.gbFavoriteBook.TabIndex = 1;
            this.gbFavoriteBook.TabStop = false;
            this.gbFavoriteBook.Text = "Kitap Bilgileri";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(635, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 111;
            this.label2.Text = "Yayın Tarihi:";
            // 
            // txtReleaseDate
            // 
            this.txtReleaseDate.EditValue = null;
            this.txtReleaseDate.Location = new System.Drawing.Point(754, 80);
            this.txtReleaseDate.Name = "txtReleaseDate";
            this.txtReleaseDate.Properties.AllowFocused = false;
            this.txtReleaseDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtReleaseDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtReleaseDate.Properties.CalendarTimeProperties.Mask.EditMask = "";
            this.txtReleaseDate.Properties.CalendarTimeProperties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtReleaseDate.Properties.Mask.EditMask = "";
            this.txtReleaseDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txtReleaseDate.Properties.NullValuePrompt = "Yayın Tarihi";
            this.txtReleaseDate.Size = new System.Drawing.Size(214, 20);
            this.txtReleaseDate.TabIndex = 100;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(994, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 17);
            this.label15.TabIndex = 110;
            this.label15.Text = "Açıklama:";
            // 
            // txtPageNum
            // 
            this.txtPageNum.Location = new System.Drawing.Point(428, 170);
            this.txtPageNum.Name = "txtPageNum";
            this.txtPageNum.Properties.AllowFocused = false;
            this.txtPageNum.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtPageNum.Properties.MaxLength = 8;
            this.txtPageNum.Properties.NullValuePrompt = "Sayfa Sayısı";
            this.txtPageNum.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtPageNum.Size = new System.Drawing.Size(186, 22);
            this.txtPageNum.TabIndex = 97;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(428, 32);
            this.txtName.Name = "txtName";
            this.txtName.Properties.AllowFocused = false;
            this.txtName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtName.Properties.NullValuePrompt = "Adı";
            this.txtName.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtName.Size = new System.Drawing.Size(186, 22);
            this.txtName.TabIndex = 94;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(295, 172);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 105;
            this.label3.Text = "Sayfa Sayısı:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(295, 125);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 17);
            this.label16.TabIndex = 109;
            this.label16.Text = "Yayınevi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(635, 34);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 17);
            this.label6.TabIndex = 103;
            this.label6.Text = "Raf Numarası:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(295, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 17);
            this.label14.TabIndex = 108;
            this.label14.Text = "Adı:";
            // 
            // writerlbl
            // 
            this.writerlbl.AutoSize = true;
            this.writerlbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.writerlbl.Location = new System.Drawing.Point(295, 77);
            this.writerlbl.Name = "writerlbl";
            this.writerlbl.Size = new System.Drawing.Size(59, 17);
            this.writerlbl.TabIndex = 107;
            this.writerlbl.Text = "Yazarı:";
            // 
            // txtAbout
            // 
            this.txtAbout.Location = new System.Drawing.Point(1080, 34);
            this.txtAbout.Name = "txtAbout";
            this.txtAbout.Properties.AllowFocused = false;
            this.txtAbout.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtAbout.Properties.NullValuePrompt = "Açıklama";
            this.txtAbout.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtAbout.Size = new System.Drawing.Size(356, 158);
            this.txtAbout.TabIndex = 102;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(20, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(251, 192);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 88;
            this.pictureBox1.TabStop = false;
            // 
            // btnFavorite
            // 
            this.btnFavorite.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFavorite.ImageOptions.Image")));
            this.btnFavorite.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnFavorite.Location = new System.Drawing.Point(1471, 36);
            this.btnFavorite.Name = "btnFavorite";
            this.btnFavorite.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnFavorite.Size = new System.Drawing.Size(121, 36);
            this.btnFavorite.TabIndex = 12;
            this.btnFavorite.Text = "Favorilerimden\r\nKaldır";
            this.btnFavorite.Click += new System.EventHandler(this.btnFavorite_Click);
            // 
            // FavoriteBookOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1629, 847);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FavoriteBookOperations";
            this.Text = "FavoriteBookOperations";
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcFavoriteBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFavoriteBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelfNum.Properties)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriters1.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbFavoriteBook.ResumeLayout(false);
            this.gbFavoriteBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReleaseDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageNum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAbout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Panel panelGrid;
        private DevExpress.XtraGrid.GridControl gcFavoriteBook;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFavoriteBook;
        private System.Windows.Forms.Label lblNumberOfComment;
        private System.Windows.Forms.Label lblControl3;
        private System.Windows.Forms.Label lblNumberOfFavorite;
        private System.Windows.Forms.Label lblControl2;
        private System.Windows.Forms.Label lblNumberOfReads;
        private System.Windows.Forms.Label lblControl;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriters;
        private DevExpress.XtraEditors.ComboBoxEdit cbPublisher;
        private DevExpress.XtraEditors.TextEdit txtShelfNum;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.HyperlinkLabelControl lnkExcel;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.GroupBox gbFavoriteBook;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.DateEdit txtReleaseDate;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit txtPageNum;
        private DevExpress.XtraEditors.TextEdit txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label writerlbl;
        private DevExpress.XtraEditors.MemoEdit txtAbout;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnFavorite;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.ComboBoxEdit cbCategory;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriters1;
    }
}