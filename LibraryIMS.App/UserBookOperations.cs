﻿using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class UserBookOperations : Form
    {
        private readonly IUserBookService _userbookService;
        private readonly IUserService _userService;
        private readonly IBookService _bookService;
        private int _userId = -1;
        private int _bookId = -1;
        private int _userbookUserId = -1;
        private int _userbookBookId = -1;
        private string searchText;

        public UserBookOperations(IUserBookService userbookService, IUserService userService, IBookService bookService)
        {
            _userbookService = userbookService;
            _userService = userService;
            _bookService = bookService;
            InitializeComponent();
        }

        private void SearchUserBook()
        {
            gcUserBook.DataSource = null;
            searchText = txtSearch.Text;
            var userbooks = _userbookService.FindUserBookByText(searchText);
            if (userbooks.ResultStatus == ResultStatus.Success)
            {
                var _new = from a in userbooks.Data.Userbooks.ToList()
                           select new
                           {
                               UserId = a.UserId,
                               UserName = a.User.UserName,
                               BookId = a.BookId,
                               BookName = a.Book.Name,
                               BookStatus = a.BookStatus
                           };
                if (userbooks.ResultStatus == ResultStatus.Success)
                {                   
                    gcUserBook.DataSource = _new;
                    gvUserBook.Columns[0].Visible = false;
                    gvUserBook.Columns[2].Visible = false;
                }
                else BindUserBookList();
            }
        }
        private void BindUserBookList(IList<UserBook> listUserBook = null)
        {
            gcUserBook.DataSource = listUserBook;
            if(listUserBook != null)
            {
                gvUserBook.Columns[0].Visible = false;
                gvUserBook.Columns[4].Visible = false;
            }
            
        }
        private void BindUserList(IList<User> listUser = null)
        {
            if (listUser != null)
            {
                var _result = from a in listUser
                           select new
                           {
                               Id = a.Id,
                               UserName = a.UserName,
                               Ad = a.FirstName,
                               Soyad = a.LastName
                           };
                gcUser.DataSource = _result;
                gvUser.Columns[0].Visible = false;
            }
            else
            {
                gcUser.DataSource = listUser;
                gvUser.Columns[0].Visible = true;
            }         
        }
        private void BindBookList(IList<Book> listBook = null)
        {
            if (listBook != null)
            {
                var _result = from a in listBook
                              select new
                              {
                                  Id = a.Id,
                                  KitapAdı = a.Name,
                              };
                gcBook.DataSource = _result;
                gvBook.Columns[0].Visible = false;
            }
            else
            {
                gcBook.DataSource = listBook;
                gvBook.Columns[0].Visible = true;
            }
        }
        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            SearchUser();
        }
        private void SearchUser()
        {
            string searchUser = txtUser.Text;
            if (String.IsNullOrEmpty(searchUser))   BindUserList();

                var users = _userService.FindUsersByText(searchUser);
                if (users.ResultStatus == ResultStatus.Success) BindUserList(users.Data.Users);
                else BindUserList();
        }
        private void SearchBook()
        {
            string searchBook = txtBook.Text;
            if (String.IsNullOrEmpty(searchBook)) BindBookList();

            var books = _bookService.FindBooksByText(searchBook);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {                   
            SearchUserBook();
        }

        private void txtBook_TextChanged(object sender, EventArgs e)
        {
            SearchBook();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_userId > -1 && _bookId > -1) 
            {
                AddUserBook();
            }
            else
            {
                Alert.Show("Lütfen kullanıcı ve kitap seçin.", ResultStatus.Warning);
            } 
        }
        private void AddUserBook()
        {
            if (_bookId == -1 || _userId == -1)
            {
                Alert.Show("Lütfen kullanıcı ve kitap seçin.", ResultStatus.Warning);
                return;
            }
            var userbook = new UserBookAddDto
            {
                UserId = _userId,
                BookId = _bookId,
            };
            var added = _userbookService.AddUserBook(userbook);
            if (added.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(added.Message, ResultStatus.Success);
                BindUserBookList();
                ClearItems();
            }
            else Alert.Show(added.Message, ResultStatus.Error);
        }
        private void ClearItems()
        {
            _userId = -1;
            _bookId = -1;
            FormControls.ClearFormControls(this);
        }

        private void gvUser_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        { 
            GetUserObject();
        }
        private void GetUserObject()
        {
            if (gvUser.SelectedRowsCount > 0)
            {
                int.TryParse(gvUser.GetRowCellValue(gvUser.FocusedRowHandle, "Id").ToString(), out _userId);
                var user = _userService.GetUser(_userId);
                if (user.ResultStatus != ResultStatus.Success)
                    Alert.Show(user.Message, ResultStatus.Error);
                txtUser.Text = user.Data.User.UserName;
            }
        }

        private void gvBook_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetBookObject();
        }
        private void GetBookObject()
        {
            if (gvBook.SelectedRowsCount > 0)
            {
                int.TryParse(gvBook.GetRowCellValue(gvBook.FocusedRowHandle, "Id").ToString(), out _bookId);
                var book = _bookService.GetBook(_bookId);
                if (book.ResultStatus != ResultStatus.Success)
                    Alert.Show(book.Message, ResultStatus.Error);
                txtBook.Text = book.Data.Book.Name;
            }
        }

        private void UserBookOperations_Load(object sender, EventArgs e)
        {
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_userbookUserId > -1 && _userbookBookId > -1)
            {
                DeleteUserBook();
            }
            else
            {
                Alert.Show("Silinirken kullanıcı ve kitap seçiminde hata oluştu.", ResultStatus.Warning);
            }
        }

        private void gvUserBook_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            int.TryParse(gvUserBook.GetRowCellValue(gvUserBook.FocusedRowHandle, "UserId").ToString(), out _userbookUserId);
            int.TryParse(gvUserBook.GetRowCellValue(gvUserBook.FocusedRowHandle, "BookId").ToString(), out _userbookBookId);
        }
        private void DeleteUserBook()
        {
            if (gvUserBook.SelectedRowsCount > 0)
            {
                var userbook = _userbookService.HardDeleteUserBook(_userbookUserId, _userbookBookId);
                if (userbook.ResultStatus == ResultStatus.Success)
                {
                    Alert.Show(userbook.Message, ResultStatus.Success);
                    ClearItems();
                }
                else
                {
                    Alert.Show(userbook.Message, ResultStatus.Error);
                }
            }
            else
            {
                Alert.Show("Kullanıcı silinirken Hata oluştu.", ResultStatus.Error);
            }
        }
    }
}
