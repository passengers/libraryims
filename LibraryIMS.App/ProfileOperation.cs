﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;

namespace LibraryIMS.App
{
    public partial class ProfileOperation : Form
    {
        #region Field

        private readonly IUserService _userService;
        private readonly IImageHelper _imageHelper;

   
        private int _userId = -1;
        private string _fileName = string.Empty;

        #endregion Field

        #region Constructor


        public ProfileOperation(IUserService userService, IImageHelper imageHelper,int userId)
        {

            _userId = userId;
            _userService = userService;
            _imageHelper = imageHelper;
            InitializeComponent();
        }
        #endregion Constructor

        #region Methods
        private void GetUserObject()
        {
            var user = _userService.GetUser(_userId);
            if (user.ResultStatus == ResultStatus.Success)
            {
                txtName.Text = user.Data.User.FirstName;
                txtSurname.Text = user.Data.User.LastName;
                txtUsername.Text = user.Data.User.UserName;
                txtPassword.Text = PasswordHelper.Decrypt(user.Data.User.Password);
                txtMail.Text = user.Data.User.Email;
                cbGender.SelectedItem = user.Data.User.Gender;
                txtTelephone.Text = user.Data.User.PhoneNumber;
                dateBirth.DateTime = user.Data.User.DateBirth;
                txtAbout.Text = user.Data.User.About;
                var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{user.Data.User.Picture}", FileMode.OpenOrCreate);
                pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                stream.Flush(); stream.Close();
            }
            else
                Alert.Show(user.Message, ResultStatus.Error);
        }
        private void ClearItems()
        {
            _userId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            FormControls.ClearFormControls(this);
        }
         private void UpdateUser()
        {
            
            var user = new UserUpdateDto
            {
                Id = _userId,
                FirstName = txtName.Text,
                LastName = txtSurname.Text,
                UserName = txtUsername.Text,
                Password = PasswordHelper.Encrypt(txtPassword.Text),
                Email = txtMail.Text,
                Gender = cbGender.Text,
                PhoneNumber = txtTelephone.Text,
                DateBirth = dateBirth.DateTime,
                About = txtAbout.Text,
            };
            bool isNewPictureUploaded = false;
            var oldUserPicture = _userService.GetUser(_userId).Data.User.Picture;
            if (_fileName != string.Empty)
            {
                var uploadedImageDtoResult = _imageHelper.Upload(txtUsername.Text, _fileName, PictureType.User);
                user.Picture = uploadedImageDtoResult.ResultStatus == ResultStatus.Success
                ? uploadedImageDtoResult.Data.FullName
                : "userImages/defaultUser.png";
                if (oldUserPicture != "userImages/defaultUser.png")
                {
                    isNewPictureUploaded = true;
                }
            }
            else
                user.Picture = oldUserPicture;
            var updatedUser = _userService.UpdateUser(user, "Admin");
            if (updatedUser.ResultStatus == ResultStatus.Success)
            {
                if (isNewPictureUploaded)
                {
                    _imageHelper.Delete(oldUserPicture);
                }
                Alert.Show(updatedUser.Message, ResultStatus.Success);
                ClearItems();
            }
            else Alert.Show(updatedUser.Message, ResultStatus.Error);
        }
        private void DeleteUser(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Üye kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var result = _userService.DeleteUser(_userId, "Admin");
                Alert.Show(result.Message, ResultStatus.Success);
                ClearItems();
            }
        }

        private void ProfileOperation_Load(object sender, EventArgs e)
        {
            GetUserObject();
        }
        #endregion Methods

        #region Button

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Length != 8)
            {
                Alert.Show("Şifre 8 karakterden oluşmalıdır.", ResultStatus.Warning);
                return;
            }
            if (_userId > -1) UpdateUser();
            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_userId > -1) DeleteUser(sender, e);
            else Alert.Show(Messages.User.NotFound(isPlural: false), ResultStatus.Error);
        }

        #endregion Button
    }
}
