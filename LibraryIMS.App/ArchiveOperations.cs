﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Data.Utilities;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.App
{
    public partial class ArchiveOperations : Form
    {

        #region Field

        private readonly IBookService _bookService;
        private readonly IImageHelper _imageHelper;
        private readonly IWriterService _writerService;
        private readonly IPublisherService _publisherService;
        private readonly IFavoriteBookService _favoritebookService;
        private readonly ICategoryService _categoryService;
        private readonly IBookCategoryService _bookCategoryService;
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;

        private int _bookId = -1;
        private int _writerId = -1;
        private int _publisherId = -1;
        private int _userId = -1;
        private string _fileName = string.Empty;

        #endregion Field

        #region Constructor

        public ArchiveOperations(IBookService bookService, IImageHelper imageHelper, IWriterService writerService, IPublisherService publisherService, 
            int userId, IFavoriteBookService favoritebookService, ICategoryService categoryService,IBookCategoryService bookCategoryService, ICommentService commentService, 
            IUserService userService)
        {
            _bookService = bookService;
            _imageHelper = imageHelper;
            _writerService = writerService;
            _publisherService = publisherService;
            _userId = userId;
            _favoritebookService = favoritebookService;
            _categoryService = categoryService;
            _bookCategoryService = bookCategoryService;
            _commentService = commentService;
            _userService = userService;

            InitializeComponent();
            BindComboBox();
            BindBookList();
        }

        #endregion Constructor

        #region Methods

        private void BindBookList(IList<Book> listBook = null)
        {
            if (listBook == null) listBook = GetAllBookByNonDeleted();

            gcArchive.DataSource = listBook;
            gvArchive.Columns[2].Visible = false;
            gvArchive.Columns[7].Visible = false;
            gvArchive.Columns[8].Visible = false;
            gvArchive.Columns[9].Visible = false;
            gvArchive.Columns[10].Visible = false;
            gvArchive.Columns[11].Visible = false;
            gvArchive.Columns[12].Visible = false;
            gvArchive.Columns[13].Visible = false;
            gvArchive.Columns[14].Visible = false;
            gvArchive.Columns[15].Visible = false;
            gvArchive.Columns[16].Visible = false;
            gvArchive.Columns[17].Visible = false;
            gvArchive.Columns[18].Visible = false;
            gvArchive.Columns[19].Visible = false;
            gvArchive.Columns[20].Visible = false;
            gvArchive.Columns[21].Visible = false;
            gvArchive.Columns[22].Visible = false;
            gvArchive.Columns[23].Visible = false;
            gvArchive.Columns[24].Visible = false;

            panelGrid.Visible = listBook.Count > 0;
            lblMessage.Text = $"{listBook.Count} adet kitap listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            var book = _bookService.GetAllBookByNonDeleted();
            var writers = _writerService.GetAllWriterByNonDeleted();
            var publishers = _publisherService.GetAllPublisherByNonDeleted();
            var categories = _categoryService.GetAllCategoryByNonDeleted();

            if (book.ResultStatus == ResultStatus.Success)
            {
                foreach (var writer in writers.Data.Writers)
                {
                    cbWriters.Properties.Items.Add(writer.Name);
                }
                foreach (var publisher in publishers.Data.Publishers)
                {
                    cbPublisher.Properties.Items.Add(publisher.Name);
                }
                foreach (var writer in writers.Data.Writers)
                {
                    cbWriters1.Properties.Items.Add(writer.Name);
                }
                foreach (var category in categories.Data.Categories)
                {
                    cbCategories.Properties.Items.Add(category.Name);
                }
            }
            else Alert.Show(book.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Tüm aktif kitapların listesini getir.
        /// </summary>
        private IList<Book> GetAllBookByNonDeleted()
        {
            var books = _bookService.GetAllBookByNonDeleted();
            if (books.ResultStatus == ResultStatus.Success)
                return books.Data.Books;
            return null;
        }

        /// <summary>
        /// Gridde seçilen kitapların bilgilerini getir.
        /// </summary>
        private void GetBookObject()
        {
            if (gvArchive.SelectedRowsCount > 0)
            {
                lblControl.Visible = true;
                lblControl2.Visible = true;
                lblControl3.Visible = true;
                int.TryParse(gvArchive.GetRowCellValue(gvArchive.FocusedRowHandle, "Id").ToString(), out _bookId);
                var book = _bookService.GetBook(_bookId);
                
                if (book.ResultStatus == ResultStatus.Success)
                {
                    _writerId = Convert.ToInt32(gvArchive.GetRowCellValue(gvArchive.FocusedRowHandle, "WriterId"));
                    _publisherId = Convert.ToInt32(gvArchive.GetRowCellValue(gvArchive.FocusedRowHandle, "PublisherId"));
                    var writer = _writerService.GetWriter(_writerId);
                    var publisher = _publisherService.GetPublisher(_publisherId);

                    lblNumberOfReads.Text = Convert.ToString(book.Data.Book.NumberOfReads);
                    lblNumberOfComment.Text = Convert.ToString(book.Data.Book.NumberOfComments);
                    lblNumberOfFavorite.Text = Convert.ToString(book.Data.Book.NumberOfFavorites);
                    txtName.Text = book.Data.Book.Name;
                    cbWriters.SelectedItem = writer.Data.Writer.Name.ToString();
                    cbPublisher.SelectedItem = publisher.Data.Publisher.Name.ToString();
                    txtPageNum.Text = Convert.ToString(book.Data.Book.NumberOfPages);
                    txtShelfNum.Text = Convert.ToString(book.Data.Book.Place);
                    txtReleaseDate.DateTime = Convert.ToDateTime(book.Data.Book.ReleaseDate);

                    txtAbout.Text = book.Data.Book.Description;
                    var stream = new FileStream($"{Directory.GetCurrentDirectory()}\\img\\{book.Data.Book.Thumbnail}", FileMode.OpenOrCreate);
                    pictureBox1.Image = Helpers.ImageResize(Image.FromStream(stream), new Size(175, 200));
                    stream.Flush(); stream.Close();
                }
                else
                    Alert.Show(book.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Kitap adı veya yazarına göre kitap ara.
        /// </summary>
        private void SearchBook()
        {
            string searchText = txtSearch.Text;
            if (String.IsNullOrEmpty(searchText)) BindBookList();

            var books = _bookService.FindBooksByText(searchText);
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _bookId = -1;
            pictureBox1.Image = null;
            _fileName = string.Empty;
            FormControls.ClearFormControls(this);
        }

        private void AddFavoriteBook()
        {
            var favoritebook = new FavoriteBookAddDto
            {
                BookId = _bookId,
                UserId = _userId,
                
            };
            var favoritedbook = _favoritebookService.AddFavoriteBook(favoritebook);
            if (favoritedbook.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(favoritedbook.Message, ResultStatus.Success);

                ClearItems();
            }
            else Alert.Show(favoritedbook.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Kategori adına göre kitap ara.
        /// </summary>
        private void SearchBooksByCategory()
        {
            var books = _bookCategoryService.FindBooksByCategoryName(cbCategories.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Yazar adına göre kitap ara.
        /// </summary>
        private void SearchBooksByWriter()
        {
            var books = _bookService.FindBooksByWriterName(cbWriters1.SelectedItem.ToString());
            if (books.ResultStatus == ResultStatus.Success) BindBookList(books.Data.Books);
            else BindBookList();
        }

        /// <summary>
        /// Stokta olmayan kitapları getir.
        /// </summary>
        private void GetAllBookByNonStock()
        {
            var books = _bookService.GetAllBookByNonStock();
            if (books.ResultStatus == ResultStatus.Success)
                BindBookList(books.Data.Books);
            else BindBookList();
        }

        #endregion Methods

        #region Buttons

        private void btnFavorite_Click(object sender, EventArgs e)
        {
            AddFavoriteBook();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnAddComment_Click(object sender, EventArgs e)
        {
            if(_bookId > -1)
            {
                var AddCommentForm = new AddCommentOperations(_commentService, _bookService, _userService, _bookId, _userId);
                var result = AddCommentForm.ShowDialog();
                if (result == DialogResult.OK) Close();
            }
            else
                Alert.Show("Kitap seçmediniz.", ResultStatus.Error);
        }

        #endregion Buttons

        #region LinkLabel

        private void lnkExcel_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "member.xls";
            if (sfd.ShowDialog() == DialogResult.OK) gcArchive.ExportToXls(sfd.FileName);
        }

        #endregion LinkLabel

        #region GridControl

        private void gvArchive_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetBookObject();
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion Timer

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchBook();
        }

        private void cbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCategories.SelectedIndex != -1)
            {
                SearchBooksByCategory();
            }
        }

        private void cbWriters1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWriters1.SelectedIndex != -1)
            {
                SearchBooksByWriter();
            }
        }

        private void ceAllBook_CheckedChanged(object sender, EventArgs e)
        {
            if (ceAllBook.Checked) GetAllBookByNonStock();
            else BindBookList();
        }


        #endregion

        
    }
}
