﻿using DevExpress.XtraEditors;
using LibraryIMS.App.Utilities;
using LibraryIMS.App.Utilities.FormControls;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using static System.String;

namespace LibraryIMS.App
{
    public partial class CategoryOperations : Form
    {
        #region Field

        private readonly ICategoryService _categoryService;
        private readonly IBookCategoryService _bookCategoryService;
        private int _categoryId = -1;
        private bool _isParent;

        #endregion Field

        #region Constructor

        public CategoryOperations(ICategoryService categoryService, IBookCategoryService bookCategoryService)
        {
            _categoryService = categoryService;
            _bookCategoryService = bookCategoryService;
            InitializeComponent();
        }

        #endregion Constructor

        #region Form Load

        private void CategoryOperations_Load(object sender, EventArgs e)
        {
            BindCategoryList();
            BindComboBox();
        }

        #endregion FormLoad

        #region Methods 

        private void BindCategoryList(IList<Category> listCategory = null)
        {
            if (listCategory == null) listCategory = GetAllCategoryByNonDeleted();
            BindComboBox();
            gcCategory.DataSource = listCategory.OrderBy(c=>c.ParentId);
            gvCategory.Columns[0].Visible = false;
            gvCategory.Columns[3].Visible = false;
            gvCategory.Columns[4].Visible = false;
            lblMessage.Text = $"{listCategory.Count} adet kategori listeleniyor.  ";
        }

        /// <summary>
        /// Tüm comboboxları doldur.
        /// </summary>
        private void BindComboBox()
        {
            cbParent.Properties.Items.Clear(); cbStatus.Properties.Items.Clear();
            cbStatus.Properties.Items.AddRange(Enum.GetValues(typeof(GeneralStatus)));
            var categories = _categoryService.GetAllCategoryByNonDeleted();
            if (categories.ResultStatus == ResultStatus.Success)
                foreach (var category in categories.Data.Categories)
                {
                    cbParent.Properties.Items.Add(category.Name);
                }
            else Alert.Show(categories.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Tüm aktif kategorilerin listesini getir.
        /// </summary>
        private IList<Category> GetAllCategoryByNonDeleted()
        {
            var categories = _categoryService.GetAllCategoryByNonDeleted();
            if (categories.ResultStatus == ResultStatus.Success)
                return categories.Data.Categories;
            return null;
        }
        
        /// <summary>
        /// Gridde seçilen kategori bilgilerini getir.
        /// </summary>
        private void GetCategoryObject()
        {
            if (gvCategory.SelectedRowsCount > 0)
            {
                int.TryParse(gvCategory.GetRowCellValue(gvCategory.FocusedRowHandle, "Id").ToString(), out _categoryId);
                var category = _categoryService.GetCategory(_categoryId);
                if (category.ResultStatus == ResultStatus.Success)
                {
                    if (category.Data.Category.ParentId == 0) cbParent.Enabled = false;
                    else cbParent.Enabled = true;
                    txtCategoryName.Text = category.Data.Category.Name;
                    if(category.Data.Category.ParentId == 0) cbParent.SelectedIndex=-1;
                    else cbParent.SelectedItem = _categoryService.GetCategoryByParent(category.Data.Category.ParentId).Data.Category.Name;
                    cbStatus.SelectedItem = Enum.Parse(typeof(GeneralStatus), category.Data.Category.GeneralStatus.ToString());
                    ceIsParent.Checked = category.Data.Category.ParentId != 0;
                }
                else
                    Alert.Show(category.Message, ResultStatus.Error);
            }
        }

        /// <summary>
        /// Sisteme yeni kategori ekler.
        /// </summary>
        private void AddCategory()
        {
            if (txtCategoryName.Text == "" || cbStatus.SelectedIndex == -1)
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            if (_isParent && cbParent.SelectedIndex == -1)
            {
               Alert.Show("Lütfen ilgili alt kategoriyi seçiniz.",ResultStatus.Warning);
               return;
            }
            var category = new CategoryAddDto
            {
                Name = txtCategoryName.Text,
                ParentId = _isParent ? cbParent.SelectedIndex :0,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            var added = _categoryService.AddCategory(category);
            if (added.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(added.Message, ResultStatus.Success);
                BindCategoryList();
                ClearItems();
            }
            else Alert.Show(added.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Sistemdeki kategori günceller.
        /// </summary>
        private void UpdateCategory()
        {
            if (txtCategoryName.Text == "" || cbStatus.SelectedIndex == -1)
            {
                Alert.Show("Lütfen tüm alanları doldurunuz.", ResultStatus.Warning);
                return;
            }
            if (_isParent && cbParent.SelectedIndex == -1)
            {
                Alert.Show("Lütfen ilgili alt kategoriyi seçiniz.", ResultStatus.Warning);
                return;
            }
            var category = new CategoryUpdateDto
            {
                Id = _categoryId,
                Name = txtCategoryName.Text,
                ParentId = _isParent ? cbParent.SelectedIndex : 0,
                GeneralStatus = (GeneralStatus)cbStatus.SelectedItem
            };
            var updatedCategory = _categoryService.UpdateCategory(category);
            if (updatedCategory.ResultStatus == ResultStatus.Success)
            {
                Alert.Show(updatedCategory.Message, ResultStatus.Success);
                BindCategoryList();
                ClearItems();
            }
            else Alert.Show(updatedCategory.Message, ResultStatus.Error);
        }

        /// <summary>
        /// Seçilen kategoriyi ve ilgili alt kategorileri kaldırır.
        /// </summary>
        private void DeleteCategory(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Kategori kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (XtraMessageBox.Show("Bağlı olduğu alt kategoriler ve ait olduğu kitabın kategori bilgisi varsa onlar da silinecektir. Devam etmek istiyor musunuz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var result = _categoryService.DeleteCategory(_categoryId);
                    var deleteBookCategory = _bookCategoryService.HardDeleteBookCategoryByCategoryId(_categoryId);
                    var parents = _categoryService.GetParentCategories(_categoryId);
                    var sleep = 1500;
                    if (result.ResultStatus == ResultStatus.Success)
                    {
                        if (parents.ResultStatus == ResultStatus.Success)
                        {
                            if (deleteBookCategory.ResultStatus==ResultStatus.Success)
                            {
                                var count = parents.Data.Categories.Count;
                                for (var i = 0; i < count; i++)
                                {
                                    _categoryService.DeleteCategory(parents.Data.Categories[i].Id);
                                    _bookCategoryService.HardDeleteBookCategoryByCategoryId(parents.Data.Categories[i].Id);
                                }
                                Alert.Show(result.Message, ResultStatus.Success);
                                Thread.Sleep(sleep);
                                ClearItems();
                                BindCategoryList();
                                Alert.Show(deleteBookCategory.Message, ResultStatus.Success);
                            }
                            else
                                Alert.Show(deleteBookCategory.Message, ResultStatus.Error);
                        }
                        else
                            Alert.Show(parents.Message, ResultStatus.Error);
                    }
                    else
                        Alert.Show(result.Message, ResultStatus.Error);
                }
            }
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            _categoryId = -1;
            _isParent = false;
            cbParent.Enabled = true;
            FormControls.ClearFormControls(this);
        }

        /// <summary>
        /// Kategori adına göre kategori ara.
        /// </summary>
        private void SearchCategory()
        {
            string searchText = txtSearch.Text;
            if (IsNullOrEmpty(searchText)) {BindCategoryList(); return;}

            var categories = _categoryService.FindCategoriesByText(searchText);
            if (categories.ResultStatus == ResultStatus.Success) BindCategoryList(categories.Data.Categories);
            else BindCategoryList();
        }

        #endregion Methods

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_categoryId > -1) UpdateCategory();
            else AddCategory();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_categoryId > -1) DeleteCategory(sender, e);
            else Alert.Show(Messages.Category.NotFound(isPlural: false), ResultStatus.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearItems();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Buttons

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchCategory();
        }

        private void ceIsParent_CheckedChanged(object sender, EventArgs e)
        {
            _isParent = ceIsParent.CheckState == CheckState.Checked;
            cbParent.Enabled = _isParent;
        }

        #endregion Events

        #region GridControl

        private void gcCategory_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var contextMenu = new ContextMenu();
                contextMenu.MenuItems.Add(new MenuItem("Kategoriyi Kaldır", DeleteCategory));

                int rowIndex = gvCategory.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvCategory.ClearSelection();
                    gvCategory.SelectRow(rowIndex);
                    _categoryId = Convert.ToInt32(gvCategory.GetRowCellValue(rowIndex, "Id"));
                    contextMenu.Show(gcCategory, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        private void gvCategory_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            GetCategoryObject();
        }

        #endregion GridControl
    }
}
