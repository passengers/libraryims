﻿
namespace LibraryIMS.App
{
    partial class PublisherOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression3 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression4 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PublisherOperations));
            this.gbPublisher = new System.Windows.Forms.GroupBox();
            this.txtPublisherDescription = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.cbStatus = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtPublisherName = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.gcPublisher = new DevExpress.XtraGrid.GridControl();
            this.gvPublisher = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnBack = new DevExpress.XtraEditors.SimpleButton();
            this.gbPublisher.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPublisherDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPublisherName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPublisher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPublisher)).BeginInit();
            this.SuspendLayout();
            // 
            // gbPublisher
            // 
            this.gbPublisher.Controls.Add(this.txtPublisherDescription);
            this.gbPublisher.Controls.Add(this.label1);
            this.gbPublisher.Controls.Add(this.cbStatus);
            this.gbPublisher.Controls.Add(this.txtPublisherName);
            this.gbPublisher.Controls.Add(this.label12);
            this.gbPublisher.Controls.Add(this.label10);
            this.gbPublisher.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbPublisher.Location = new System.Drawing.Point(12, 12);
            this.gbPublisher.Name = "gbPublisher";
            this.gbPublisher.Size = new System.Drawing.Size(488, 141);
            this.gbPublisher.TabIndex = 108;
            this.gbPublisher.TabStop = false;
            this.gbPublisher.Text = "Yayıncı Bilgileri";
            // 
            // txtPublisherDescription
            // 
            this.txtPublisherDescription.Location = new System.Drawing.Point(161, 69);
            this.txtPublisherDescription.Name = "txtPublisherDescription";
            this.txtPublisherDescription.Properties.AllowFocused = false;
            this.txtPublisherDescription.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtPublisherDescription.Properties.NullValuePrompt = "Ad";
            this.txtPublisherDescription.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtPublisherDescription.Size = new System.Drawing.Size(186, 22);
            this.txtPublisherDescription.TabIndex = 108;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(11, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 107;
            this.label1.Text = "Yayıncı Açıklama:";
            // 
            // cbStatus
            // 
            this.cbStatus.Location = new System.Drawing.Point(161, 108);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Properties.AllowFocused = false;
            this.cbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbStatus.Properties.Items.AddRange(new object[] {
            "Stabil",
            "Giremez",
            "Borclu"});
            this.cbStatus.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbStatus.Size = new System.Drawing.Size(186, 20);
            this.cbStatus.TabIndex = 105;
            // 
            // txtPublisherName
            // 
            this.txtPublisherName.Location = new System.Drawing.Point(161, 32);
            this.txtPublisherName.Name = "txtPublisherName";
            this.txtPublisherName.Properties.AllowFocused = false;
            this.txtPublisherName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtPublisherName.Properties.NullValuePrompt = "Ad";
            this.txtPublisherName.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtPublisherName.Size = new System.Drawing.Size(186, 22);
            this.txtPublisherName.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(9, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 17);
            this.label12.TabIndex = 106;
            this.label12.Text = "Durum:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(9, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 17);
            this.label10.TabIndex = 93;
            this.label10.Text = "Yayıncı Adı:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(21, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 107;
            this.label2.Text = "Yayıncı Ara:";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(173, 177);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(186, 20);
            this.txtSearch.TabIndex = 109;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // gcPublisher
            // 
            this.gcPublisher.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPublisher.Location = new System.Drawing.Point(24, 232);
            this.gcPublisher.MainView = this.gvPublisher;
            this.gcPublisher.Name = "gcPublisher";
            this.gcPublisher.Size = new System.Drawing.Size(865, 292);
            this.gcPublisher.TabIndex = 110;
            this.gcPublisher.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPublisher});
            // 
            // gvPublisher
            // 
            this.gvPublisher.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvPublisher.Appearance.Empty.Options.UseBackColor = true;
            this.gvPublisher.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvPublisher.Appearance.Row.Options.UseFont = true;
            this.gvPublisher.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvPublisher.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleExpression3.Appearance.BackColor = System.Drawing.Color.LightGreen;
            formatConditionRuleExpression3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression3.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression3.Appearance.Options.UseFont = true;
            formatConditionRuleExpression3.Expression = "[ParentId] = 0";
            gridFormatRule3.Rule = formatConditionRuleExpression3;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Name = "Format1";
            formatConditionRuleExpression4.Appearance.BackColor = System.Drawing.Color.LightSalmon;
            formatConditionRuleExpression4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression4.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression4.Appearance.Options.UseFont = true;
            formatConditionRuleExpression4.Expression = "[ParentId] != 0";
            gridFormatRule4.Rule = formatConditionRuleExpression4;
            this.gvPublisher.FormatRules.Add(gridFormatRule3);
            this.gvPublisher.FormatRules.Add(gridFormatRule4);
            this.gvPublisher.GridControl = this.gcPublisher;
            this.gvPublisher.Name = "gvPublisher";
            this.gvPublisher.OptionsBehavior.Editable = false;
            this.gvPublisher.OptionsBehavior.ReadOnly = true;
            this.gvPublisher.OptionsCustomization.AllowFilter = false;
            this.gvPublisher.OptionsFind.AllowFindPanel = false;
            this.gvPublisher.OptionsMenu.EnableColumnMenu = false;
            this.gvPublisher.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvPublisher.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvPublisher.OptionsView.ShowGroupPanel = false;
            this.gvPublisher.OptionsView.ShowIndicator = false;
            this.gvPublisher.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvPublisher.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvPublisher_RowCellClick);
            // 
            // btnClear
            // 
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(768, 135);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(121, 36);
            this.btnClear.TabIndex = 113;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.ImageOptions.Image")));
            this.btnDelete.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnDelete.Location = new System.Drawing.Point(768, 91);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDelete.Size = new System.Drawing.Size(121, 36);
            this.btnDelete.TabIndex = 111;
            this.btnDelete.Text = "Kaldır";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(768, 49);
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnSave.Size = new System.Drawing.Size(121, 36);
            this.btnSave.TabIndex = 112;
            this.btnSave.Text = "Kaydet";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnBack
            // 
            this.btnBack.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.ImageOptions.Image")));
            this.btnBack.Location = new System.Drawing.Point(26, 530);
            this.btnBack.Name = "btnBack";
            this.btnBack.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBack.Size = new System.Drawing.Size(121, 36);
            this.btnBack.TabIndex = 114;
            this.btnBack.Text = "Geri Dön";
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // PublisherOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 572);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gcPublisher);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gbPublisher);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PublisherOperations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PublisherOperations";
            this.Load += new System.EventHandler(this.PublisherOperations_Load);
            this.gbPublisher.ResumeLayout(false);
            this.gbPublisher.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPublisherDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPublisherName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPublisher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPublisher)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbPublisher;
        private DevExpress.XtraEditors.TextEdit txtPublisherName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.ComboBoxEdit cbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearch;
        private DevExpress.XtraGrid.GridControl gcPublisher;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPublisher;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtPublisherDescription;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnBack;
    }
}