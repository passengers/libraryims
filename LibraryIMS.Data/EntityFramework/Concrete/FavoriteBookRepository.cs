﻿using LibraryIMS.Core.Data.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using System.Data.Entity;

namespace LibraryIMS.Data.EntityFramework.Concrete
{
    public class FavoriteBookRepository : GenericRepository<FavoriteBook>, IFavoriteBookRepository
    {
        public FavoriteBookRepository(DbContext context) : base(context)
        {
        }
    }
}
