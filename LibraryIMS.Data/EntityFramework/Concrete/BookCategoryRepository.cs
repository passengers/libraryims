﻿using LibraryIMS.Core.Data.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using System.Data.Entity;

namespace LibraryIMS.Data.EntityFramework.Concrete
{
    public class BookCategoryRepository : GenericRepository<BookCategory>, IBookCategoryRepository
    {
        public BookCategoryRepository(DbContext context) : base(context)
        {
        }
    }
}
