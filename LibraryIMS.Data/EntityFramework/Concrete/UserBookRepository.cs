﻿using LibraryIMS.Core.Data.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using System.Data.Entity;

namespace LibraryIMS.Data.EntityFramework.Concrete
{
    public class UserBookRepository : GenericRepository<UserBook>, IUserBookRepository
    {
        public UserBookRepository(DbContext context) : base(context)
        {
        }
    }
}
