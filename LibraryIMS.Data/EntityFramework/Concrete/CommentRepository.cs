﻿using LibraryIMS.Core.Data.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using System.Data.Entity;

namespace LibraryIMS.Data.EntityFramework.Concrete
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        public CommentRepository(DbContext context) : base(context)
        {
        }
    }
}
