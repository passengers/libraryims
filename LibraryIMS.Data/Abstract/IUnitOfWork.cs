﻿using System;

namespace LibraryIMS.Data.Abstract
{
    /// <summary>
    /// Oluşturacağımız UnitofWork sınıfının içermesi gereken metotlara ait imzaları bu kısımda tanımlayacağız.
    /// UnitofWork sınıfını türetirken doğrudan değil bu interface'i kullanarak türeteceğiz Böylece dependency injection yaparak
    /// kodumuza esneklik kazandırmış olacağız.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Veritabanında işlemlerin yapılması emrini veren kısım olacak
        /// Repository içerisinde kuyruğa aldığımız tüm işlemler bu metot çalıştırıldığı anda sırası ile veritabanında değişikliğe uğrayacak
        /// </summary>
        int SaveChanges();

        IBookRepository Books { get; }
        IBookCategoryRepository BookCategories { get; }
        ICategoryRepository Categories { get; }
        ICommentRepository Comments { get; }
        IContactRepository Contacts { get; }
        IFavoriteBookRepository FavoriteBooks { get; }
        IPublisherRepository Publishers { get; }
        IUserBookRepository UserBooks { get; }
        IUserRepository Users { get; }
        IWriterRepository Writers { get; }
    }
}
