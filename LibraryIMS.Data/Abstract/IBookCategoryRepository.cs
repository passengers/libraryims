﻿using LibraryIMS.Core.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Data.Abstract
{
    public interface IBookCategoryRepository : IGenericRepository<BookCategory>
    {
    }
}
