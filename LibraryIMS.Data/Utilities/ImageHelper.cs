﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Core.Utilities.Extensions;
using LibraryIMS.Entities.Entities.Dtos;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace LibraryIMS.Data.Utilities
{
    public class ImageHelper : IImageHelper
    {
        private readonly string _wwwroot;
        private const string imgFolder = "img";
        private const string userImagesFolder = "userImages";
        private const string bookImagesFolder = "bookImages";
        private const string writerImagesFolder = "writerImages";
        private string _message = "";

        public ImageHelper()
        {
            _wwwroot = Directory.GetCurrentDirectory();
        }

        public IAppResult<ImageUploadedDto> Upload(string name, string fileName, PictureType pictureType, string folderName = null)
        {
            /* Eğer folderName değişkeni null gelir ise, o zaman resim tipine göre (PictureType) klasör adı ataması yapılır. */
            if (folderName == null)
            {
                if (pictureType == PictureType.User) folderName = userImagesFolder;
                else if (pictureType == PictureType.Book) folderName = bookImagesFolder;
                else folderName = writerImagesFolder;
            }

            /* Eğer folderName değişkeni ile gelen klasör adı sistemimizde mevcut değilse, yeni bir klasör oluşturulur. */
            if (!Directory.Exists($"{_wwwroot}/{imgFolder}/{folderName}"))
            {
                Directory.CreateDirectory($"{_wwwroot}/{imgFolder}/{folderName}");
            }
            /* Resimin yüklenme sırasındaki ilk adı oldFileName adlı değişkene atanır. */
            string oldFileName = Path.GetFileNameWithoutExtension(fileName);

            /* Resimin uzantısı fileExtension adlı değişkene atanır. */
            string fileExtension = Path.GetExtension(fileName);

            Regex regex = new Regex("[*'\",._&#^@]");
            name = regex.Replace(name, string.Empty);

            DateTime dateTime = DateTime.Now;

            /*
            // Parametre ile gelen değerler kullanılarak yeni bir resim adı oluşturulur.
            // Örn: CengizhanDinar_587_5_38_12_3_10_2020.png
            */
            string newFileName = $"{name}_{dateTime.FullDateAndTimeStringWithUnderscore()}{fileExtension}";

            /* Kendi parametrelerimiz ile sistemimize uygun yeni bir dosya yolu (path) oluşturulur. */
            var path = Path.Combine($"{_wwwroot}/{imgFolder}/{folderName}", newFileName);

            /* Sistemimiz için oluşturulan yeni dosya yoluna resim kopyalanır. */
            File.Copy(fileName, path);

            /* Resim tipine göre kullanıcı için bir mesaj oluşturulur. */
            if (pictureType == PictureType.User)
                _message = $"{name} adlı kullanıcının resmi başarıyla yüklenmiştir.";
            else if (pictureType == PictureType.Writer)
                _message = $"{name} adlı yazarın resmi başarıyla yüklenmiştir.";
            else if (pictureType == PictureType.Book)
                _message = $"{name} adlı kitabın kapak resmi başarıyla yüklenmiştir.";

            return new AppResult<ImageUploadedDto>().Success(new ImageUploadedDto
            {
                FullName = $"{folderName}/{newFileName}",
                OldName = oldFileName,
                Extension = fileExtension,
                FolderName = folderName,
                Path = path
            });
        }

        public IAppResult<ImageDeletedDto> Delete(string pictureName)
        {
            var fileToDelete = Path.Combine($"{_wwwroot}/{imgFolder}/", pictureName);
            if (File.Exists(fileToDelete))
            {
                var fileInfo = new FileInfo(fileToDelete);
                var imageDeletedDto = new ImageDeletedDto
                {
                    FullName = pictureName,
                    Extension = fileInfo.Extension,
                    Path = fileInfo.FullName,
                    Size = fileInfo.Length
                };
                File.Delete(fileToDelete);
                return new AppResult<ImageDeletedDto>().Success(imageDeletedDto);
            }
            return new AppResult<ImageDeletedDto>().Fail($"Böyle bir resim bulunamadı.");
        }
    }
}
