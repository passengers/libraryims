﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Data.Utilities
{
    public interface IImageHelper
    {
        IAppResult<ImageUploadedDto> Upload(string name, string fileName, PictureType pictureType, string folderName = null);
        IAppResult<ImageDeletedDto> Delete(string pictureName);
    }
}
