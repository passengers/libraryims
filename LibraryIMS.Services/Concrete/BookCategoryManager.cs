﻿using System.Collections.Generic;
using System.Linq;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Kitap-Kategori tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class BookCategoryManager : ManagerBase, IBookCategoryService
    {
        public BookCategoryManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public IAppResult<BookListDto> GetAllBookByNonTagged()
        {
            var entities = UnitOfWork.BookCategories.GetAll(null, bc => 
                bc.Book, bc => 
                bc.Category);
            var books = UnitOfWork.Books.GetAll(b=>
                b.GeneralStatus==GeneralStatus.Active, b => 
                b.Publisher, b => 
                b.Writer);
            if (entities.Count <= -1) return new AppResult<BookListDto>().Fail(Messages.BookCategories.NotFound());
            {
                if (books.Count <= -1) return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
                IList<Book> list = books.Where(book => entities.All(bc => bc.BookId != book.Id)).ToList();
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
        }
        public IAppResult<CategoryListDto> GetCategoriesByBookId(int id)
        {
            var entities = UnitOfWork.BookCategories.GetAll(bc => 
                bc.BookId == id, bc => 
                bc.Book, bc => 
                bc.Category);
            var categories = UnitOfWork.Categories.GetAll(c=>c.GeneralStatus==GeneralStatus.Active);
            if (entities.Count <= -1) return new AppResult<CategoryListDto>().Fail(Messages.BookCategories.NotFound());
            {
                if (categories.Count <= -1)
                    return new AppResult<CategoryListDto>().Fail(Messages.Category.NotFound(isPlural: true));
                IList<Category> list = categories.Where(category => entities.Any(bc => bc.CategoryId == category.Id)).ToList();
                return new AppResult<CategoryListDto>().Success(new CategoryListDto { Categories = list });
            }
        }
        public IAppResult<BookListDto> FindBooksByCategoryName(string categoryName)
        {
            var entities = UnitOfWork.BookCategories.GetAll(bc => 
                bc.Category.Name == categoryName, 
                bc => bc.Category, bc => bc.Book);
            var books = UnitOfWork.Books.GetAll(b=>
                b.GeneralStatus==GeneralStatus.Active, 
                b => b.Publisher, b => b.Writer);
            if (entities.Count <= -1 || books.Count <= -1)
                return new AppResult<BookListDto>().Fail(Messages.BookCategories.NotFound());
            {
                if (books.Count <= -1) return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
                IList<Book> list = books.Where(book => entities.Any(bc => bc.BookId == book.Id)).ToList();
                return new AppResult<BookListDto>().Success(new BookListDto {Books = list});
            }
        }
        public IAppResult HardDeleteBookCategory(int categoryId, int bookId)
        {
            var getDeleting = UnitOfWork.BookCategories.Get(bc => bc.BookId == bookId && bc.CategoryId == categoryId, bc => bc.Book, bc => bc.Category);
            if (getDeleting == null) return new AppResult().Fail(Messages.BookCategories.NotFound());
            var bookName = getDeleting.Book.Name;
            var categoryName = getDeleting.Category.Name;
            UnitOfWork.BookCategories.Delete(getDeleting);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.BookCategories.HardDelete(bookName, categoryName));
        }
        public IAppResult HardDeleteBookCategoryByCategoryId(int categoryId)
        {
            var getDeleting = UnitOfWork.BookCategories.GetAll(bc=> bc.CategoryId == categoryId, bc => bc.Book, bc => bc.Category);
            if (getDeleting == null) return new AppResult().Fail(Messages.BookCategories.NotFound());
            var categoryName = getDeleting[0].Category.Name;
            foreach (var bookCategory in getDeleting)
            {
                UnitOfWork.BookCategories.Delete(bookCategory);
            }
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.BookCategories.HardDeleteByCategory(categoryName));
        }
        public IAppResult AddBookCategory(BookCategoryAddDto entity)
        {
            if (entity == null) return new AppResult().Fail(Messages.BookCategories.NotFound());
            var isThere = UnitOfWork.BookCategories.Any(bc => bc.BookId == entity.BookId && bc.CategoryId == entity.CategoryId);
            if (isThere) return new AppResult().Fail(Messages.BookCategories.IsThere());
            var newEntity = Mapper.Map<BookCategory>(entity);
            UnitOfWork.BookCategories.Add(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.BookCategories.Add(newEntity.Book.Name, newEntity.Category.Name));
        }
    }
}