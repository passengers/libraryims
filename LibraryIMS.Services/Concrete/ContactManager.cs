﻿using System;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// İletişim tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class ContactManager : ManagerBase, IContactService
    {
        public ContactManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IAppResult<ContactListDto> FindDeletedContactsByUserName(string text)
        {
            var entities = UnitOfWork.Contacts.GetAll(c =>
                (c.User.FirstName.Contains(text) || c.User.LastName.Contains(text)) &&
                c.GeneralStatus != GeneralStatus.Active, c => c.User);
            return entities.Count > -1
                ? new AppResult<ContactListDto>().Success(new ContactListDto { Contacts = entities })
                : new AppResult<ContactListDto>().Fail(Messages.Contact.NotFound(isPlural: true));
        }

        public IAppResult HardDeleteContact(int contactId)
        {
            var entity = UnitOfWork.Contacts.Find(contactId);
            if (entity == null) return new AppResult().Fail(Messages.Contact.NotFound(isPlural: false));
            var userName = entity.User.UserName;
            UnitOfWork.Contacts.Delete(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Contact.HardDelete(userName));
        }
        public IAppResult<ContactDto> GetContact(int contactId)
        {
            var entity = UnitOfWork.Contacts.Get(c => c != null && c.Id == contactId, c => c.User);
            return entity != null 
                ? new AppResult<ContactDto>().Success(new ContactDto { Contact = entity }) 
                : new AppResult<ContactDto>().Fail(Messages.Contact.NotFound(isPlural: false));
        }
        public IAppResult<ContactListDto> GetAllDeletedContact()
        {
            var entities = UnitOfWork.Contacts.GetAll(c => 
                c != null && 
                c.GeneralStatus == GeneralStatus.Deleted,
                c => c.User);
            return entities.Count > -1 
                ? new AppResult<ContactListDto>().Success(new ContactListDto { Contacts = entities }) 
                : new AppResult<ContactListDto>().Fail(Messages.Contact.NotFound(isPlural: true));
        }
        public IAppResult<ContactListDto> GetAllContactByNonDeleted()
        {
            var entities = UnitOfWork.Contacts.GetAll(c => c.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1 
                ? new AppResult<ContactListDto>().Success(new ContactListDto { Contacts = entities }) 
                : new AppResult<ContactListDto>().Fail(Messages.Contact.NotFound(isPlural: true));
        }
        public IAppResult DeleteContact(int contactId, string updatedByName)
        {
            var entity = UnitOfWork.Contacts.Find(contactId);
            if (entity == null) return new AppResult().Fail(Messages.Contact.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Deleted;
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate=DateTime.Now;
            UnitOfWork.Contacts.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Contact.Delete(entity.User.UserName));
        }
        public IAppResult<ContactListDto> FindContactsByUserName(string text)
        {
            var entities = UnitOfWork.Contacts.GetAll(c =>
                (c.User.FirstName.Contains(text) || c.User.LastName.Contains(text)) && 
                c.GeneralStatus == GeneralStatus.Active, c => c.User);
            return entities.Count > -1 
                ? new AppResult<ContactListDto>().Success(new ContactListDto { Contacts = entities }) 
                : new AppResult<ContactListDto>().Fail(Messages.Contact.NotFound(isPlural: true));
        }
        public IAppResult UndoDeleteContact(int contactId, string updatedByName)
        {
            var entity = UnitOfWork.Contacts.Find(contactId);
            if (entity == null) return new AppResult().Fail(Messages.Contact.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Active;
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate=DateTime.Now;
            UnitOfWork.Contacts.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Contact.UndoDelete(entity.User.UserName));
        }
        public IAppResult AddContact(ContactAddDto entity, string createdByName)
        {
            if (entity == null) return new AppResult().Fail(Messages.Contact.NotFound(isPlural: false));
            var newEntity = Mapper.Map<Contact>(entity);
            newEntity.CreatedByName = createdByName;
            newEntity.UpdatedByName = createdByName;
            UnitOfWork.Contacts.Add(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Contact.Add(newEntity.User.UserName));
        }
    }
}