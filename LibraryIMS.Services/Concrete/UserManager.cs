﻿using System;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Kullanıcı tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class UserManager : ManagerBase, IUserService
    {
        public UserManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IAppResult AddUser(UserAddDto entity, string createdByName)
        {
            if (entity == null) return new AppResult().Fail(Messages.User.NotFound(isPlural: false));
            if (UnitOfWork.Users.Any(u => u.UserName == entity.UserName))
                return new AppResult().Fail(Messages.User.IsThere(entity.UserName));
            {
                if (UnitOfWork.Users.Any(u => u.Email == entity.Email))
                    return new AppResult().Fail(Messages.User.IsThere(entity.Email));
                var newEntity = Mapper.Map<User>(entity);
                newEntity.CreatedByName = createdByName;
                newEntity.UpdatedByName = createdByName;
                UnitOfWork.Users.Add(newEntity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.User.Add(newEntity.UserName));
            }
        }
        public IAppResult DeleteUser(int userId, string updatedByName)
        {
            var entity = UnitOfWork.Users.Find(userId);
            if (entity == null) return new AppResult().Fail(Messages.User.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Deleted;
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate = DateTime.Now;
            UnitOfWork.Users.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.User.Delete(entity.UserName));
        }
        public IAppResult<UserDto> GetUser(int userId)
        {
            var entity = UnitOfWork.Users.Get(u => u.Id == userId);
            return entity != null
                ? new AppResult<UserDto>().Success(new UserDto { User = entity })
                : new AppResult<UserDto>().Fail(Messages.User.NotFound(isPlural: false));
        }
        public IAppResult<UserDto> GetUserByUserName(string userName)
        {
            var entity = UnitOfWork.Users.Get(u => u != null && u.UserName == userName);
            return entity != null
                ? new AppResult<UserDto>().Success(new UserDto { User = entity })
                : new AppResult<UserDto>().Fail(Messages.User.NotFound(isPlural: false));
        }

        public IAppResult<UserListDto> FindDeletedUsersByText(string text)
        {
            var entities = UnitOfWork.Users.GetAll(u =>
                (u.FirstName.Contains(text) || u.LastName.Contains(text) || u.UserName.Contains(text)) &&
                u.GeneralStatus != GeneralStatus.Active);
            return entities.Count > -1
                ? new AppResult<UserListDto>().Success(new UserListDto { Users = entities })
                : new AppResult<UserListDto>().Fail(Messages.User.NotFound(isPlural: true));
        }

        public IAppResult<UserListDto> FindUsersByRole(AccessStatus accessStatus)
        {
            var entities = UnitOfWork.Users.GetAll(u =>
                u.AccessStatus == accessStatus &&
                u.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1
                ? new AppResult<UserListDto>().Success(new UserListDto { Users = entities })
                : new AppResult<UserListDto>().Fail(Messages.User.NotFound(isPlural: true));
        }
        public IAppResult HardDeleteUser(int userId)
        {
            var entity = UnitOfWork.Users.Find(userId);
            if (entity == null) return new AppResult().Fail(Messages.User.NotFound(isPlural: false));
            var userName = entity.UserName;
            UnitOfWork.Users.Delete(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.User.HardDelete(userName));
        }
        public IAppResult UndoDeleteUser(int userId, string updatedByName)
        {
            var entity = UnitOfWork.Users.Find(userId);
            if (entity == null) return new AppResult().Fail(Messages.User.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Active;
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate = DateTime.Now;
            UnitOfWork.Users.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.User.UndoDelete(entity.UserName));
        }
        public IAppResult UpdateUser(UserUpdateDto entity, string updatedByName)
        {
            var oldEntity = UnitOfWork.Users.Find(entity.Id);
            var isNewUsername = false;
            var isNewEmail = false;
            if (oldEntity == null) return new AppResult().Fail(Messages.User.NotFound(isPlural: false));
            if (oldEntity.UserName != entity.UserName) isNewUsername = true;
            if (oldEntity.Email != entity.Email) isNewEmail = true;

            if (isNewUsername && UnitOfWork.Users.Any(u => u.UserName == entity.UserName))
                return new AppResult().Fail(Messages.User.IsThere(entity.UserName));
            
            if (isNewEmail && UnitOfWork.Users.Any(u => u.Email == entity.Email))
                return new AppResult().Fail(Messages.User.IsThere(entity.Email));

            var newEntity = Mapper.Map(entity, oldEntity);
            newEntity.UpdatedByName = updatedByName;
            UnitOfWork.Users.Update(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.User.Update(newEntity.UserName));

        }
        public IAppResult UpdateUserRole(UserDto entity, string updatedByName)
        {
            var oldEntity = UnitOfWork.Users.Find(entity.User.Id);
            if (oldEntity == null) return new AppResult().Fail(Messages.User.NotFound(isPlural: false));
            var newEntity = Mapper.Map(entity, oldEntity);
            newEntity.UpdatedByName = updatedByName;
            UnitOfWork.Users.Update(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.User.RoleUpdate(newEntity.UserName));
        }
        public IAppResult<UserListDto> GetAllDeletedUser()
        {
            var entities = UnitOfWork.Users.GetAll(u => u != null && u.GeneralStatus == GeneralStatus.Deleted);
            return entities.Count > -1
                ? new AppResult<UserListDto>().Success(new UserListDto { Users = entities })
                : new AppResult<UserListDto>().Fail(Messages.User.NotFound(isPlural: true));
        }
        public IAppResult<UserListDto> GetAllUserByNonDeleted()
        {
            var entities = UnitOfWork.Users.GetAll(u => u != null && u.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1
                ? new AppResult<UserListDto>().Success(new UserListDto { Users = entities })
                : new AppResult<UserListDto>().Fail(Messages.User.NotFound(isPlural: true));
        }
        public IAppResult<UserListDto> FindUsersByText(string text)
        {
            var entities = UnitOfWork.Users.GetAll(u =>
                (u.FirstName.Contains(text) || u.LastName.Contains(text) || u.UserName.Contains(text)) &&
                u.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1
                ? new AppResult<UserListDto>().Success(new UserListDto { Users = entities })
                : new AppResult<UserListDto>().Fail(Messages.User.NotFound(isPlural: true));
        }
    }
}