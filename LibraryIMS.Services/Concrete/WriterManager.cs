﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Yazar tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class WriterManager : ManagerBase, IWriterService
    {
        public WriterManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IAppResult<WriterListDto> GetAllDeletedWriter()
        {
            var entities = UnitOfWork.Writers.GetAll(w => w.GeneralStatus == GeneralStatus.Deleted);
            return entities.Count > -1 
                ? new AppResult<WriterListDto>().Success(new WriterListDto { Writers = entities }) 
                : new AppResult<WriterListDto>().Fail(Messages.Writer.NotFound(isPlural: true));
        }
        public IAppResult<WriterListDto> GetAllWriterByNonDeleted()
        {
            var entities = UnitOfWork.Writers.GetAll(w => w.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1 
                ? new AppResult<WriterListDto>().Success(new WriterListDto { Writers = entities }) 
                : new AppResult<WriterListDto>().Fail(Messages.Writer.NotFound(isPlural: true));
        }
        public IAppResult<WriterListDto> FindWritersByText(string text)
        {
            var entities = UnitOfWork.Writers.GetAll(w => w.Name.Contains(text) && w.GeneralStatus==GeneralStatus.Active);
            return entities.Count > -1 
                ? new AppResult<WriterListDto>().Success(new WriterListDto { Writers = entities }) 
                : new AppResult<WriterListDto>().Fail(Messages.Writer.NotFound(isPlural: true));
        }

        public IAppResult<WriterListDto> FindDeletedWritersByText(string text)
        {
            var entities = UnitOfWork.Writers.GetAll(w => w.Name.Contains(text) && w.GeneralStatus != GeneralStatus.Active);
            return entities.Count > -1
                ? new AppResult<WriterListDto>().Success(new WriterListDto { Writers = entities })
                : new AppResult<WriterListDto>().Fail(Messages.Writer.NotFound(isPlural: true));
        }

        public int GetNumberOfBooksForWriter(int writerId)
        {
            var result = UnitOfWork.Books.Count(w => w.WriterId == writerId && w.GeneralStatus == GeneralStatus.Active);
            return result;
        }
        public IAppResult HardDeleteWriter(int writerId)
        {
            var entity = UnitOfWork.Writers.Find(writerId);
            if (entity == null) return new AppResult().Fail(Messages.Writer.NotFound(isPlural: false));
            var writerName = entity.Name;
            UnitOfWork.Writers.Delete(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Writer.HardDelete(writerName));
        }
        public IAppResult<WriterDto> GetWriter(int writerId)
        {
            var entity = UnitOfWork.Writers.Get(w => w != null && w.Id == writerId);
            return entity != null 
                ? new AppResult<WriterDto>().Success(new WriterDto { Writer = entity }) 
                : new AppResult<WriterDto>().Fail(Messages.Writer.NotFound(isPlural: false));
        }
        public IAppResult DeleteWriter(int writerId)
        {
            var entity = UnitOfWork.Writers.Find(writerId);
            if (entity == null) return new AppResult().Fail(Messages.Writer.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Deleted;
            UnitOfWork.Writers.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Writer.Delete(entity.Name));
        }
        public IAppResult UndoDeleteWriter(int writerId)
        {
            var entity = UnitOfWork.Writers.Find(writerId);
            if (entity == null) return new AppResult().Fail(Messages.Writer.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Active;
            UnitOfWork.Writers.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Writer.UndoDelete(entity.Name));
        }
        public IAppResult AddWriter(WriterAddDto entity)
        {
            if (entity == null) return new AppResult().Fail(Messages.Writer.NotFound(isPlural: false));
            if (UnitOfWork.Writers.Any(w => w != null && w.Name == entity.Name))
                return new AppResult().Fail(Messages.Writer.IsThere(entity.Name));
            var newEntity = Mapper.Map<Writer>(entity);
            UnitOfWork.Writers.Add(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Writer.Add(newEntity.Name));
        }
        public IAppResult UpdateWriter(WriterUpdateDto entity)
        {
            var oldEntity = UnitOfWork.Writers.Find(entity.Id);
            if (oldEntity == null) return new AppResult().Fail(Messages.Writer.NotFound(isPlural: false));
            var newEntity = Mapper.Map(entity, oldEntity);
            UnitOfWork.Writers.Update(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Writer.Update(newEntity.Name));
        }
    }
}