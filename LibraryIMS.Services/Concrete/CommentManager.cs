﻿using System;
using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Yorumlar tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class CommentManager : ManagerBase, ICommentService
    {
        public CommentManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IAppResult<CommentListDto> GetAllCommentByNonDeleted()
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                c != null && c.GeneralStatus == GeneralStatus.Active,
                c => c.User, c => c.Book);
            return entities.Count > -1
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }

        public IAppResult<CommentListDto> FindDeletedCommentsByText(string text)
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                    (c.User.FirstName.Contains(text) || c.User.LastName.Contains(text) || c.Book.Name.Contains(text)) &&
                    c.GeneralStatus != GeneralStatus.Active,
                c => c.Book, c => c.User);
            return entities.Count > -1
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }

        public IAppResult HardDeleteComment(int commentId)
        {
            var entity = UnitOfWork.Comments.Find(commentId);
            if (entity == null) return new AppResult().Fail(Messages.Comment.NotFound(isPlural: false));
            var userName = entity.User.UserName;
            UnitOfWork.Comments.Delete(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Comment.HardDelete(userName));
        }
        public IAppResult<CommentListDto> GetAllDeletedComment()
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                c != null && c.GeneralStatus == GeneralStatus.Deleted,
                c => c.User, c => c.Book);
            return entities.Count > -1
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }
        public IAppResult<CommentDto> GetComment(int commentId)
        {
            var entity = UnitOfWork.Comments.Get(c => c != null && c.Id == commentId, c => c.User, c => c.Book);
            return entity != null
                ? new AppResult<CommentDto>().Success(new CommentDto { Comment = entity })
                : new AppResult<CommentDto>().Fail(Messages.Comment.NotFound(isPlural: false));
        }
        public IAppResult<CommentListDto> GetAllCommentByNonApproved()
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                c != null &&
                c.GeneralStatus == GeneralStatus.WaitingApproval,
                c => c.User, c => c.Book);
            return entities.Count > -1
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }
        public IAppResult DeleteComment(int commentId, string updatedByName)
        {
            var entity = UnitOfWork.Comments.Find(commentId);
            if (entity == null) return new AppResult().Fail(Messages.Comment.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Deleted;
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate = DateTime.Now;
            entity.Book.NumberOfComments -= 1;
            UnitOfWork.Comments.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Comment.Delete(entity.User.UserName));
        }
        public IAppResult UndoDeleteComment(int commentId, string updatedByName)
        {
            var entity = UnitOfWork.Comments.Find(commentId);
            if (entity == null) return new AppResult().Fail(Messages.Comment.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.WaitingApproval;
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate = DateTime.Now;
            UnitOfWork.Comments.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Comment.UndoDelete(entity.User.UserName));
        }
        public IAppResult AddComment(CommentAddDto entity, string createdByName)
        {
            if (entity == null) return new AppResult().Fail(Messages.Comment.NotFound(isPlural: false));
            var newEntity = Mapper.Map<Comment>(entity);
            newEntity.CreatedByName = createdByName;
            newEntity.UpdatedByName = createdByName;
            UnitOfWork.Comments.Add(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Comment.Add(newEntity.User.UserName));
        }
        public IAppResult<CommentListDto> FindCommentsByBookName(string bookName)
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                c.Book.Name.Contains(bookName) &&
                c.GeneralStatus == GeneralStatus.Active,
                c => c.Book, c => c.User);
            return entities.Count > -1
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }
        public IAppResult<CommentListDto> FindCommentsByUserName(string text)
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                c != null &&
                (c.User.FirstName.Contains(text) || c.User.LastName.Contains(text)) &&
                c.GeneralStatus == GeneralStatus.Active,
                c => c.User, c => c.Book);
            return entities.Count > -1
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }
        public IAppResult ApproveComment(int commentId, string updatedByName)
        {
            var entity = UnitOfWork.Comments.Find(commentId);
            if (entity == null) return new AppResult().Fail(Messages.Comment.NotFound(isPlural: false));
            if (entity.GeneralStatus == GeneralStatus.Active) return new AppResult().Fail(Messages.Comment.Approved(entity.User.UserName));
            entity.UpdatedByName = updatedByName;
            entity.UpdatedDate = DateTime.Now;
            entity.GeneralStatus = GeneralStatus.Active;
            entity.Book.NumberOfComments += 1;
            UnitOfWork.Comments.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Comment.Approve(entity.User.UserName));
        }
        public IAppResult<CommentListDto> FindCommentsByUserNameAndBookName(string username, string bookname)
        {
            var entities = UnitOfWork.Comments.GetAll(c =>
                c != null &&
                (c.User.UserName.Contains(username) || c.User.FirstName.Contains(username) || c.User.LastName.Contains(username)) &&
                c.GeneralStatus == GeneralStatus.Active && c.Book.Name.Contains(bookname),
                c => c.User, c => c.Book);
            return entities.Count > 0
                ? new AppResult<CommentListDto>().Success(new CommentListDto { Comments = entities })
                : new AppResult<CommentListDto>().Fail(Messages.Comment.NotFound(isPlural: true));
        }
    }
}