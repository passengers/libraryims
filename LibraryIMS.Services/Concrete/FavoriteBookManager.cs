﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Favori Kitaplar tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class FavoriteBookManager : ManagerBase, IFavoriteBookService
    {
        public FavoriteBookManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public IAppResult AddFavoriteBook(FavoriteBookAddDto entity)
        {
            var userName = UnitOfWork.Users.Find(entity.UserId).UserName;
            var bookName = UnitOfWork.Books.Find(entity.BookId).Name;
            
            if (entity != null)
            {
                if (!(UnitOfWork.FavoriteBooks.Any(u => u.BookId == entity.BookId && u.UserId == entity.UserId) ))
                {
                    var newEntity = Mapper.Map<FavoriteBook>(entity);
                    UnitOfWork.FavoriteBooks.Add(newEntity);
                    newEntity.Book.NumberOfFavorites += 1;
                    UnitOfWork.SaveChanges();
                    return new AppResult().Success(Messages.FavoriteBook.Add(userName, bookName));
                }
                return new AppResult().Fail(Messages.FavoriteBook.IsThere(userName, bookName));
            }
            return new AppResult().Fail(Messages.FavoriteBook.NotFound(isPlural: false));
        }
        public IAppResult HardDeleteFavoriteBook(FavoriteBookDto entity)
        {
            var userName = UnitOfWork.Users.Find(entity.FavoriteBook.UserId).UserName;
            var bookName = UnitOfWork.Books.Find(entity.FavoriteBook.BookId).Name;
            var favoritebook = UnitOfWork.FavoriteBooks.Get(x => x.UserId == entity.FavoriteBook.UserId && x.BookId == entity.FavoriteBook.BookId,x=>x.Book,x=>x.User);
            if (favoritebook != null)
            {
                entity.FavoriteBook.Book.NumberOfFavorites -= 1;
                UnitOfWork.FavoriteBooks.Delete(favoritebook);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.FavoriteBook.HardDelete(userName, bookName));
            }
            return new AppResult().Fail(Messages.FavoriteBook.NotFound(isPlural: false));
        }
        public IAppResult<BookListDto> GetAllFavoriteBookByUser(int userId)
        {
            var entities = UnitOfWork.FavoriteBooks.GetAll(bc => bc.UserId == userId, bc => bc.Book, bc => bc.User);
            var books = UnitOfWork.Books.GetAll(b => b.GeneralStatus == GeneralStatus.Active);
            IList<Book> list = new List<Book>();
            if (entities.Count > -1 && books.Count > -1)
            {
                foreach (var book in books)
                {
                    if (entities.Any(bc => bc.BookId == book.Id))
                        list.Add(book);
                }
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(true));
        }
        public IAppResult<FavoriteBookDto> GetFavoriteBook(int bookId,int userId)
        {
            var entity = UnitOfWork.FavoriteBooks.Get(u => u.BookId == bookId && u.UserId == userId );
            if (entity != null)
                return new AppResult<FavoriteBookDto>().Success(new FavoriteBookDto { FavoriteBook = entity });
            return new AppResult<FavoriteBookDto>().Fail(Messages.FavoriteBook.NotFound(isPlural: false));
        }
        public IAppResult<BookListDto> FindFavoriteBooksByText(string text , int userId)
        {
            var entities = UnitOfWork.FavoriteBooks.GetAll(x => x.UserId == userId, x => x.Book, x => x.User);
            var books = UnitOfWork.Books.GetAll(u => u.Name.Contains(text) && u.GeneralStatus == GeneralStatus.Active);
            IList<Book> list = new List<Book>();
            if (entities.Count > -1 && books.Count > -1)
            {
                foreach (var book in books)
                {
                    if (entities.Any(bc => bc.BookId == book.Id))
                        list.Add(book);
                }
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(true));
        }
        public IAppResult<BookListDto> FindFavoriteBooksByWriterName(string text,int userId)
        {
            var entities = UnitOfWork.FavoriteBooks.GetAll(x => x.UserId == userId, x => x.Book, x => x.User);
            var books = UnitOfWork.Books.GetAll(u => u.Writer.Name.Contains(text) && u.GeneralStatus == GeneralStatus.Active);
            IList<Book> list = new List<Book>();
            if (entities.Count > -1 && books.Count > -1)
            {
                foreach (var book in books)
                {
                    if (entities.Any(bc => bc.BookId == book.Id))
                        list.Add(book);
                }
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(true));
        }

        public IAppResult<BookListDto> FindFavoriteBooksByCategory(string categoryName,int userId)
        {
            var categories = UnitOfWork.BookCategories.GetAll(x => x.Category.Name.Contains(categoryName), x => x.Book , x => x.Category);
            var entities = UnitOfWork.FavoriteBooks.GetAll(x => x.UserId == userId, x => x.Book, x => x.User);
           // var books = UnitOfWork.Books.GetAll(u => u.BookCategories.Contains(Text) && u.GeneralStatus == GeneralStatus.Active);
            IList<Book> list = new List<Book>();
            if (entities.Count > -1 && categories.Count > -1)
            {
                foreach (var category in categories)
                {
                    if (entities.Any(bc => bc.BookId == category.Book.Id))
                        list.Add(category.Book);
                }
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(true));
        }

        public IAppResult<BookListDto> GetAllFavoriteBookByNonStock(int userId)
        {
            var entities = UnitOfWork.FavoriteBooks.GetAll(bc => bc.UserId == userId, bc => bc.Book, bc => bc.User);
            var books = UnitOfWork.Books.GetAll(u => u.Stock != 0 && u.GeneralStatus == GeneralStatus.Active);
            IList<Book> list = new List<Book>();
            if (entities.Count > -1 && books.Count > -1)
            {
                foreach (var book in books)
                {
                    if (entities.Any(bc => bc.BookId == book.Id))
                        list.Add(book);
                }
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(true));
        }
    }
}