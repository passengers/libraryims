﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Kullanıcı-Kitap tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class UserBookManager : ManagerBase, IUserBookService
    {
        
        public UserBookManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public IAppResult AddUserBook(UserBookAddDto entity)
        {
            var userName = UnitOfWork.Users.Find(entity.UserId).UserName;
            var bookName = UnitOfWork.Books.Find(entity.BookId).Name;
            if (entity != null)
            {
                if (!(UnitOfWork.UserBooks.Any(u => u.BookId == entity.BookId && u.UserId == entity.UserId)))
                {
                        var newEntity = Mapper.Map<UserBook>(entity);
                        newEntity.User = UnitOfWork.Users.Find(entity.UserId);
                        newEntity.CreatedDate = DateTime.Now;
                        UnitOfWork.UserBooks.Add(newEntity);
                        UnitOfWork.SaveChanges();
                        return new AppResult().Success(Messages.UserBook.Add(userName, bookName));
                }
                return new AppResult().Fail(Messages.UserBook.IsThere(userName, bookName));
            }
            return new AppResult().Fail(Messages.UserBook.NotFound(isPlural: false));
        }
        public IAppResult HardDeleteUserBook(int userId, int bookId)
        {
            var userName = UnitOfWork.Users.Find(userId).UserName;
            var bookName = UnitOfWork.Books.Find(bookId).Name;
            var entity = UnitOfWork.UserBooks.Get(x => x.UserId == userId && x.BookId == bookId);
            if (entity != null)
            {
                UnitOfWork.UserBooks.Delete(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.UserBook.HardDelete(userName, bookName));
            }
            return new AppResult().Fail(Messages.UserBook.NotFound(isPlural: false));
        }
        public IAppResult UpdateUserBook(UserBookUpdateDto entity)
        {
            var userName = UnitOfWork.Users.Find(entity.UserId).UserName;
            var bookName = UnitOfWork.Books.Find(entity.BookId).Name;
            var oldEntity = UnitOfWork.UserBooks.Find(entity.Id);
            if (oldEntity != null)
            {
                var newEntity = Mapper.Map(entity, oldEntity);
                UnitOfWork.UserBooks.Update(newEntity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.UserBook.Update(userName, bookName));
            }
            return new AppResult().Fail(Messages.UserBook.NotFound(isPlural: false));
        }
        public IAppResult<UserBookListDto> GetAllUserBook()
        {
            var entities = UnitOfWork.UserBooks.GetAll();
            if (entities.Count > -1)
                return new AppResult<UserBookListDto>().Success(new UserBookListDto { Userbooks = entities });
            return new AppResult<UserBookListDto>().Fail(Messages.UserBook.NotFound(isPlural: true));
        }
        public IAppResult<UserBookListDto> FindUserBookByText(string text)
        {
            if (text != "")
            {
                var user = UnitOfWork.Users.GetAll(u => u.UserName.Contains(text));
                var book = UnitOfWork.UserBooks.GetAll(null, a => a.Book);
                List<UserBook> entities2 = new List<UserBook>();
                foreach (var item in user)
                {
                    foreach (var item2 in book)
                    {
                        if (item.Id == item2.UserId)
                            entities2.Add(item2);
                    }
                }
                if (entities2.Count > -1)
                    return new AppResult<UserBookListDto>().Success(new UserBookListDto { Userbooks = entities2 });
            }
            return new AppResult<UserBookListDto>().Fail(Messages.UserBook.NotFound(isPlural: true));
        }

        public IAppResult<BookListDto> GetAllReadBooksByUser(int userId)
        {
            var entities = UnitOfWork.UserBooks.GetAll(bc => bc.UserId == userId, bc => bc.Book, bc => bc.User);
            var books = UnitOfWork.Books.GetAll(b => b.GeneralStatus == GeneralStatus.Active);
            IList<Book> list = new List<Book>();
            if (entities.Count > -1 && books.Count > -1)
            {
                foreach (var book in books)
                {
                    if (entities.Any(bc => bc.BookId == book.Id))
                        list.Add(book);
                }
                return new AppResult<BookListDto>().Success(new BookListDto { Books = list });
            }
            return new AppResult<BookListDto>().Fail(Messages.UserBook.NotFound(true));
        }


    }
}