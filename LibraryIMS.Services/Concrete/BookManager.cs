﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;


namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Kitap tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class BookManager : ManagerBase, IBookService
    {
        public BookManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public IAppResult AddBook(BookAddDto entity, string createdByName)
        {
            if (entity != null)
            {
                if (!(UnitOfWork.Books.Any(u => u.Name == entity.Name)))
                {
                    var newEntity = Mapper.Map<Book>(entity);
                    newEntity.CreatedByName = createdByName;
                    newEntity.UpdatedByName = createdByName;
                    UnitOfWork.Books.Add(newEntity);
                    newEntity.Writer.NumberOfBooks += 1;
                    UnitOfWork.SaveChanges();
                    return new AppResult().Success(Messages.Book.Add(newEntity.Name));
                }
                return new AppResult().Fail(Messages.User.IsThere(entity.Name));
            }
            return new AppResult().Fail(Messages.Book.NotFound(isPlural: false));
        }
        public IAppResult DeleteBook(int bookId, string updatedByName)
        {
            var entity = UnitOfWork.Books.Find(bookId);
            if (entity != null)
            {
                entity.GeneralStatus = GeneralStatus.Deleted;
                entity.Stock -= 1;
                entity.Writer.NumberOfBooks -= 1;
                entity.UpdatedByName = updatedByName;
                UnitOfWork.Books.Update(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Book.Delete(entity.Name));
            }
            return new AppResult().Fail(Messages.Book.NotFound(isPlural: false));
        }
        public IAppResult<BookDto> GetBook(int bookId)
        {
            var entity = UnitOfWork.Books.Get(u => u.Id == bookId);
            if (entity != null)
                return new AppResult<BookDto>().Success(new BookDto { Book = entity });
            return new AppResult<BookDto>().Fail(Messages.Book.NotFound(isPlural: false));
        }
        public IAppResult HardDeleteBook(int bookId)
        {
            var entity = UnitOfWork.Books.Find(bookId);
            if (entity != null)
            {
                entity.Writer.NumberOfBooks -= 1;
                var bookName = entity.Name;
                UnitOfWork.Books.Delete(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Book.HardDelete(bookName));
            }
            return new AppResult().Fail(Messages.Book.NotFound(isPlural: false));
        }
        public IAppResult UndoDeleteBook(int bookId, string updatedByName)
        {
            var entity = UnitOfWork.Books.Find(bookId);
            if (entity != null)
            {
                entity.GeneralStatus = GeneralStatus.Active;
                entity.UpdatedByName = updatedByName;
                UnitOfWork.Books.Update(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Book.UndoDelete(entity.Name));
            }
            return new AppResult().Fail(Messages.Book.NotFound(isPlural: false));
        }
        public IAppResult UpdateBook(BookUpdateDto entity, string updatedByName)
        {
            var oldEntity = UnitOfWork.Books.Find(entity.Id);
            if (oldEntity != null)
            {
                var newEntity = Mapper.Map(entity, oldEntity);
                newEntity.UpdatedByName = updatedByName;
                UnitOfWork.Books.Update(newEntity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Book.Update(newEntity.Name));
            }
            return new AppResult().Fail(Messages.Book.NotFound(isPlural: false));
        }
        public IAppResult<BookListDto> GetAllDeletedBook()
        {
            var entities = UnitOfWork.Books.GetAll(u => u.GeneralStatus == GeneralStatus.Deleted);
            if (entities.Count > -1)
                return new AppResult<BookListDto>().Success(new BookListDto { Books = entities });
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
        }
        public IAppResult<BookListDto> GetAllBookByNonDeleted()
        {
            var entities = UnitOfWork.Books.GetAll(u => u.GeneralStatus == GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<BookListDto>().Success(new BookListDto { Books = entities });
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
        }
        public IAppResult<BookListDto> FindBooksByText(string text)
        {
            var entities = UnitOfWork.Books.GetAll(u => u.Name.Contains(text) && u.GeneralStatus == GeneralStatus.Active || u.Writer.Name.Contains(text) && u.GeneralStatus ==GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<BookListDto>().Success(new BookListDto { Books = entities });
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
        }
        public IAppResult<BookListDto> FindBooksByWriterName(string text)
        {
            var entities = UnitOfWork.Books.GetAll(u => u.Writer.Name.Contains(text) && u.GeneralStatus == GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<BookListDto>().Success(new BookListDto { Books = entities });
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
        }
        public IAppResult<BookListDto> GetAllBookByNonStock()
        {
            var entities = UnitOfWork.Books.GetAll(u => u.Stock != 0 && u.GeneralStatus == GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<BookListDto>().Success(new BookListDto { Books = entities });
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
        }
        public IAppResult<BookListDto> FindDeletedBooksByText(string text)
        {
            var entities = UnitOfWork.Books.GetAll(u => u.Name.Contains(text) && u.GeneralStatus != GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<BookListDto>().Success(new BookListDto { Books = entities });
            return new AppResult<BookListDto>().Fail(Messages.Book.NotFound(isPlural: true));
        }
        
    }
}

