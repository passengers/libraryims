﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Yayınevi tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class PublisherManager : ManagerBase, IPublisherService
    {
        public PublisherManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public IAppResult AddPublisher(PublisherAddDto entity)
        {
            if (entity != null)
            {
                if (!(UnitOfWork.Publishers.Any(u => u.Name == entity.Name)))
                {
                    if (!(UnitOfWork.Publishers.Any(u => u.Description == entity.Description)))
                    {
                        var newEntity = Mapper.Map<Publisher>(entity);
                        UnitOfWork.Publishers.Add(newEntity);
                        UnitOfWork.SaveChanges();
                        return new AppResult().Success(Messages.Publisher.Add(newEntity.Name));
                    }
                    return new AppResult().Fail(Messages.Publisher.IsThere(entity.Description));
                }
                return new AppResult().Fail(Messages.Publisher.IsThere(entity.Name));
            }
            return new AppResult().Fail(Messages.Publisher.NotFound(isPlural: false));
        }
        public IAppResult DeletePublisher(int publisherId)
        {
            var entity = UnitOfWork.Publishers.Find(publisherId);
            if (entity != null)
            {
                entity.GeneralStatus = GeneralStatus.Deleted;
                UnitOfWork.Publishers.Update(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Publisher.Delete(entity.Name));
            }
            return new AppResult().Fail(Messages.Publisher.NotFound(isPlural: false));
        }
        public IAppResult HardDeletePublisher(int publisherId)
        {
            var entity = UnitOfWork.Publishers.Find(publisherId);
            if (entity != null)
            {
                var publisherName = entity.Name;
                UnitOfWork.Publishers.Delete(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Publisher.HardDelete(publisherName));
            }
            return new AppResult().Fail(Messages.Publisher.NotFound(isPlural: false));
        }
        public IAppResult UndoDeletePublisher(int publisherId)
        {
            var entity = UnitOfWork.Publishers.Find(publisherId);
            if (entity != null)
            {
                entity.GeneralStatus = GeneralStatus.Active;
                UnitOfWork.Publishers.Update(entity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Publisher.UndoDelete(entity.Name));
            }
            return new AppResult().Fail(Messages.Publisher.NotFound(isPlural: false));
        }
        public IAppResult UpdatePublisher(PublisherUpdateDto entity)
        {
            var oldEntity = UnitOfWork.Publishers.Find(entity.Id);
            if (oldEntity != null)
            {
                var newEntity = Mapper.Map(entity, oldEntity);
                UnitOfWork.Publishers.Update(newEntity);
                UnitOfWork.SaveChanges();
                return new AppResult().Success(Messages.Publisher.Update(newEntity.Name));
            }
            return new AppResult().Fail(Messages.Publisher.NotFound(isPlural: false));
        }
        public IAppResult<PublisherDto> GetPublisher(int publisherId)
        {
            var entity = UnitOfWork.Publishers.Get(u => u.Id == publisherId);
            if (entity != null)
                return new AppResult<PublisherDto>().Success(new PublisherDto { Publisher = entity });
            return new AppResult<PublisherDto>().Fail(Messages.Publisher.NotFound(isPlural: false));
        }
        public IAppResult<PublisherListDto> GetAllPublisher()
        {
            var entities = UnitOfWork.Publishers.GetAll();
            if (entities.Count > -1)
                return new AppResult<PublisherListDto>().Success(new PublisherListDto { Publishers = entities });
            return new AppResult<PublisherListDto>().Fail(Messages.Publisher.NotFound(isPlural: true));
        }
        public IAppResult<PublisherListDto> GetAllDeletedPublisher()
        {
            var entities = UnitOfWork.Publishers.GetAll(u => u.GeneralStatus == GeneralStatus.Deleted);
            if (entities.Count > -1)
                return new AppResult<PublisherListDto>().Success(new PublisherListDto { Publishers = entities });
            return new AppResult<PublisherListDto>().Fail(Messages.Publisher.NotFound(isPlural: true));
        }
        public IAppResult<PublisherListDto> GetAllPublisherByNonDeleted()
        {
            var entities = UnitOfWork.Publishers.GetAll(u => u.GeneralStatus == GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<PublisherListDto>().Success(new PublisherListDto { Publishers = entities });
            return new AppResult<PublisherListDto>().Fail(Messages.Publisher.NotFound(isPlural: true));
        }
        public IAppResult<PublisherListDto> FindPublishersByText(string text)
        {
            var entities = UnitOfWork.Publishers.GetAll(u => u.Name.Contains(text));
            if (entities.Count > -1)
                return new AppResult<PublisherListDto>().Success(new PublisherListDto { Publishers = entities });
            return new AppResult<PublisherListDto>().Fail(Messages.Publisher.NotFound(isPlural: true));
        }

        public IAppResult<PublisherListDto> FindDeletedPublishersByText(string text)
        {
            var entities = UnitOfWork.Publishers.GetAll(u => u.Name.Contains(text) && u.GeneralStatus!=GeneralStatus.Active);
            if (entities.Count > -1)
                return new AppResult<PublisherListDto>().Success(new PublisherListDto { Publishers = entities });
            return new AppResult<PublisherListDto>().Fail(Messages.Publisher.NotFound(isPlural: true));
        }
    }
}