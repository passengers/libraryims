﻿using AutoMapper;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Services.AutoMapper;
using System;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Tüm Manager sınıflarımızda kullanacağımız arabirimleri her manager sınıfının constructorlarına tek tek uygulamamak için
    /// ManagerBase sınıfından miras alarak Dependency Injection ile yapıcı metotların oluşmasını sağlarız.
    /// </summary>
    public class ManagerBase
    {
        public ManagerBase(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        private readonly Lazy<IMapper> _lazy = new Lazy<IMapper>(() =>
        {
            var configuration = new MapperConfiguration(configure =>
            {
                configure.AddProfile<EntityProfiles>();
            });
            return configuration.CreateMapper();
        });
        protected IUnitOfWork UnitOfWork { get; }
        protected IMapper Mapper => _lazy.Value;
    }
}
