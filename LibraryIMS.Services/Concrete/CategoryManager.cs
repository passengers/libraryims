﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Core.Result.Concrete;
using LibraryIMS.Data.Abstract;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;
using LibraryIMS.Services.Abstract;
using LibraryIMS.Services.Utilities;

namespace LibraryIMS.Services.Concrete
{
    /// <summary>
    /// Kategori tablosu işlemleri için kullanılacak sınıf.
    /// </summary>
    public class CategoryManager : ManagerBase, ICategoryService
    {
        public CategoryManager(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IAppResult DeleteCategory(int categoryId)
        {
            var entity = UnitOfWork.Categories.Find(categoryId);
            if (entity == null) return new AppResult().Fail(Messages.Category.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Deleted;
            UnitOfWork.Categories.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Category.Delete(entity.Name));
        }
        public IAppResult HardDeleteCategory(int categoryId)
        {
            var entity = UnitOfWork.Categories.Find(categoryId);
            if (entity == null) return new AppResult().Fail(Messages.Category.NotFound(isPlural: false));
            var categoryName = entity.Name;
            UnitOfWork.Categories.Delete(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Category.HardDelete(categoryName));
        }
        public IAppResult UndoDeleteCategory(int categoryId)
        {
            var entity = UnitOfWork.Categories.Find(categoryId);
            if (entity == null) return new AppResult().Fail(Messages.Category.NotFound(isPlural: false));
            entity.GeneralStatus = GeneralStatus.Active;
            UnitOfWork.Categories.Update(entity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Category.UndoDelete(entity.Name));
        }
        public IAppResult AddCategory(CategoryAddDto entity)
        {
            if (entity == null) return new AppResult().Fail(Messages.Category.NotFound(false));
            if (UnitOfWork.Categories.Any(c => c.Name == entity.Name))
                return new AppResult().Fail(Messages.Category.IsThere(entity.Name));
            var newEntity = Mapper.Map<Category>(entity);
            UnitOfWork.Categories.Add(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Category.Add(newEntity.Name));
        }
        public IAppResult<CategoryDto> GetCategory(int categoryId)
        {
            var entity = UnitOfWork.Categories.Get(c => c != null && c.Id == categoryId);
            return entity != null 
                ? new AppResult<CategoryDto>().Success(new CategoryDto { Category = entity }) 
                : new AppResult<CategoryDto>().Fail(Messages.Category.NotFound(isPlural: false));
        }
        public IAppResult<CategoryListDto> GetAllDeletedCategory()
        {
            var entities = UnitOfWork.Categories.GetAll(c => c != null && c.GeneralStatus == GeneralStatus.Deleted);
            return entities.Count > -1 
                ? new AppResult<CategoryListDto>().Success(new CategoryListDto { Categories = entities }) 
                : new AppResult<CategoryListDto>().Fail(Messages.Category.NotFound(isPlural: true));
        }
        public IAppResult UpdateCategory(CategoryUpdateDto entity)
        {
            var oldEntity = UnitOfWork.Categories.Find(entity.Id);
            if (oldEntity == null) return new AppResult().Fail(Messages.Category.NotFound(isPlural: false));
            var newEntity = Mapper.Map(entity, oldEntity);
            UnitOfWork.Categories.Update(newEntity);
            UnitOfWork.SaveChanges();
            return new AppResult().Success(Messages.Category.Update(newEntity.Name));
        }
        public IAppResult<CategoryListDto> GetAllCategoryByNonDeleted()
        {
            var entities = UnitOfWork.Categories.GetAll(c => c != null && c.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1 
                ? new AppResult<CategoryListDto>().Success(new CategoryListDto { Categories = entities }) 
                : new AppResult<CategoryListDto>().Fail(Messages.Category.NotFound(isPlural: true));
        }
        public IAppResult<CategoryDto> GetCategoryByParent(int parentId)
        {
            var entity = UnitOfWork.Categories.Get(c => c != null && c.Id == parentId);
            return entity != null 
                ? new AppResult<CategoryDto>().Success(new CategoryDto { Category = entity }) 
                : new AppResult<CategoryDto>().Fail(Messages.Category.NotFound(isPlural: false));
        }
        public IAppResult<CategoryListDto> FindCategoriesByText(string text)
        {
            var entities = UnitOfWork.Categories.GetAll(c => c.Name.Contains(text) && c.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1 
                ? new AppResult<CategoryListDto>().Success(new CategoryListDto { Categories = entities }) 
                : new AppResult<CategoryListDto>().Fail(Messages.Category.NotFound(isPlural: true));
        }

        public IAppResult<CategoryListDto> FindDeletedCategoriesByText(string text)
        {
            var entities = UnitOfWork.Categories.GetAll(c => c.Name.Contains(text) && c.GeneralStatus != GeneralStatus.Active);
            return entities.Count > -1
                ? new AppResult<CategoryListDto>().Success(new CategoryListDto { Categories = entities })
                : new AppResult<CategoryListDto>().Fail(Messages.Category.NotFound(isPlural: true));
        }

        public IAppResult<CategoryListDto> GetParentCategories(int categoryId)
        {
            var entities = UnitOfWork.Categories.GetAll(c => c.ParentId == categoryId && c.GeneralStatus == GeneralStatus.Active);
            return entities.Count > -1 
                ? new AppResult<CategoryListDto>().Success(new CategoryListDto { Categories = entities }) 
                : new AppResult<CategoryListDto>().Fail(Messages.Category.NotFound(isPlural: true));
        }
        public IAppResult<CategoryDto> GetCategoryByName(string categoryName)
        {
            var entity = UnitOfWork.Categories.Get(c => c.Name == categoryName && c.GeneralStatus==GeneralStatus.Active);
            return entity != null 
                ? new AppResult<CategoryDto>().Success(new CategoryDto { Category = entity }) 
                : new AppResult<CategoryDto>().Fail(Messages.Category.NotFound(isPlural: false));
        }
    }
}