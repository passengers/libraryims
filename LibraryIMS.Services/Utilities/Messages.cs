﻿namespace LibraryIMS.Services.Utilities
{
    public static class Messages
    {
        public static class User
        {
            public static string RoleUpdate(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcının rolü başarıyla güncellenmiştir.";
            }
            public static string IsThere(string userName)
            {
                return $"{userName} bilgisine sahip kullanıcı zaten mevcuttur.";
            }
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir kullanıcı bulunamadı.";
                return "Böyle bir kullanıcı bulunamadı.";
            }
            public static string Add(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcı başarıyla eklenmiştir.";
            }
            public static string Update(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcı başarıyla güncellenmiştir.";
            }
            public static string Delete(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcı başarıyla silinmiştir.";
            }
            public static string HardDelete(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcı başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcı başarıyla arşivden geri getirilmiştir.";
            }
        }
        public static class Category
        {
            public static string IsThere(string categoryName)
            {
                return $"{categoryName} adlı kategori zaten mevcuttur.";
            }
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir katgori bulunamadı.";
                return "Böyle bir kategori bulunamadı.";
            }
            public static string Add(string categoryName)
            {
                return $"{categoryName} adlı kategori başarıyla eklenmiştir.";
            }
            public static string Update(string categoryName)
            {
                return $"{categoryName} adlı kategori başarıyla güncellenmiştir.";
            }
            public static string Delete(string categoryName)
            {
                return $"{categoryName} adlı kategori başarıyla silinmiştir.";
            }
            public static string HardDelete(string categoryName)
            {
                return $"{categoryName} adlı kategori başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string categoryName)
            {
                return $"{categoryName} adlı kategori başarıyla arşivden geri getirilmiştir.";
            }
        }
        public static class Publisher
        {
            public static string IsThere(string publisherName)
            {
                return $"{publisherName} adlı yayınevi zaten mevcuttur.";
            }
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir yayınevi bulunamadı.";
                return "Böyle bir yayınevi bulunamadı.";
            }
            public static string Add(string publisherName)
            {
                return $"{publisherName} adlı yayınevi başarıyla eklenmiştir.";
            }
            public static string Update(string publisherName)
            {
                return $"{publisherName} adlı yayınevi başarıyla güncellenmiştir.";
            }
            public static string Delete(string publisherName)
            {
                return $"{publisherName} adlı yayınevi başarıyla silinmiştir.";
            }
            public static string HardDelete(string publisherName)
            {
                return $"{publisherName} adlı yayınevi başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string publisherName)
            {
                return $"{publisherName} adlı yayınevi başarıyla arşivden geri getirilmiştir.";
            }
        }
        public static class Writer
        {
            public static string IsThere(string writerName)
            {
                return $"{writerName} adlı yazar zaten mevcuttur.";
            }
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir yazar bulunamadı.";
                return "Böyle bir yazar bulunamadı.";
            }
            public static string Add(string writerName)
            {
                return $"{writerName} adlı yazar başarıyla eklenmiştir.";
            }
            public static string Update(string writerName)
            {
                return $"{writerName} adlı yazar başarıyla güncellenmiştir.";
            }
            public static string Delete(string writerName)
            {
                return $"{writerName} adlı yazar başarıyla silinmiştir.";
            }
            public static string HardDelete(string writerName)
            {
                return $"{writerName} adlı yazar başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string writerName)
            {
                return $"{writerName} adlı yazar başarıyla arşivden geri getirilmiştir.";
            }
        }
        public static class Comment
        {
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir yorum bulunamadı.";
                return "Böyle bir yorum bulunamadı.";
            }
            public static string Add(string userName)
            {
                return $"{userName} adlı kullanıcının yorumu başarıyla eklenmiştir.";
            }
            public static string Delete(string userName)
            {
                return $"{userName} adlı kullanıcının yorumu başarıyla silinmiştir.";
            }
            public static string HardDelete(string userName)
            {
                return $"{userName} adlı kullanıcının yorumu başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string userName)
            {
                return $"{userName} adlı kullanıcının yorumu başarıyla arşivden geri getirilmiştir.";
            }
            public static string Approve(string userName)
            {
                return $"{userName} adlı kullanıcının yorumu başarıyla onaylanmıştır.";
            }
            public static string Approved(string userName)
            {
                return $"{userName} adlı kullanıcının yorumu zaten onaylıdır.";
            }
        }
        public static class Book
        {
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir kitap bulunamadı.";
                return "Böyle bir kitap bulunamadı.";
            }
            public static string Add(string Name)
            {
                return $"{Name} adlı kitap başarıyla eklenmiştir.";
            }
            public static string Update(string Name)
            {
                return $"{Name} adlı kitap başarıyla güncellenmiştir.";
            }
            public static string Delete(string Name)
            {
                return $"{Name} adlı kitap başarıyla silinmiştir.";
            }
            public static string HardDelete(string Name)
            {
                return $"{Name} adlı kitap başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string Name)
            {
                return $"{Name} adlı kitap başarıyla arşivden geri getirilmiştir.";
            }
        }
        public static class UserBook
        {
            public static string IsThere(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıda {bookName} kitabı zaten mevcuttur.";
            }
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir kullanıcı bulunamadı.";
                return "Böyle bir kullanıcı bulunamadı.";
            }
            public static string Add(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıya {bookName} kitabı eklenmiştir.";
            }
            public static string Update(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcı {bookName} kitabı ile güncellenmiştir.";
            }
            public static string Delete(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıdan {bookName} kitabı silinmiştir.";
            }
            public static string HardDelete(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıdan {bookName} kitabı silinip veritabanından kaldırıldı.";
            }
        }
        public static class Contact
        {
            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir mesaj bulunamadı.";
                return "Böyle bir mesaj bulunamadı.";
            }
            public static string Add(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcının mesajı başarıyla eklenmiştir.";
            }
            public static string Delete(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcının mesajı başarıyla silinmiştir.";
            }
            public static string HardDelete(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcının mesajı başarıyla veritabanından silinmiştir.";
            }
            public static string UndoDelete(string userName)
            {
                return $"{userName} kullanıcı adlı kullanıcının mesajı başarıyla arşivden geri getirilmiştir.";
            }
        }
        public static class BookCategories
        {
            public static string NotClicked()
            {
                return "Herhangi bir kitap veya kategori seçilmedi.";
            }
            public static string NotFound()
            {
                return "Kitaba ait kategori(ler) bulunamadı.";
            }
            public static string HardDelete(string bookName,string categoryName)
            {
                return $"{bookName} adlı kitaba ait {categoryName} kategorisi başarıyla veritabanından silinmiştir.";
            }
            public static string HardDeleteByCategory(string categoryName)
            {
                return $"{categoryName} kategorisine ait tüm kitap kategorileri başarıyla veritabanından silinmiştir.";
            }
            public static string Add(string bookName, string categoryName)
            {
                return $"{bookName} adlı kitaba ait {categoryName} kategorisi başarıyla eklenmiştir.";
            }
            public static string IsThere()
            {
                return $"Bu kitaba ait seçilen kategori zaten tanımlıdır.";
            }
        }
        public static class FavoriteBook
        {
            public static string IsThere(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıda {bookName} kitabı zaten mevcuttur.";
            }

            public static string NotFound(bool isPlural)
            {
                if (isPlural) return "Herhangi bir kullanıcı bulunamadı.";
                return "Böyle bir kullanıcı bulunamadı.";
            }

            public static string Add(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıya {bookName} kitabı eklenmiştir.";
            }

            public static string HardDelete(string userName, string bookName)
            {
                return $"{userName} adlı kullanıcıdan {bookName} kitabı silinip veritabanından kaldırıldı.";
            }
        }
    }
}
