﻿using System;
using AutoMapper;
using LibraryIMS.Entities.Entities.Concrete;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.AutoMapper
{
    public class EntityProfiles : Profile
    {
        public EntityProfiles()
        {
            CreateMap<User, UserAddDto>().ReverseMap()
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(x => DateTime.Now));
            CreateMap<User, UserUpdateDto>().ReverseMap()
                .ForMember(dest => dest.UpdatedDate,
                opt => opt.MapFrom(x => DateTime.Now));
            CreateMap<Book, BookAddDto>().ReverseMap()
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(x => DateTime.Now));
            CreateMap<Book, BookUpdateDto>().ReverseMap()
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(x => DateTime.Now));
            CreateMap<Comment, CommentAddDto>().ReverseMap()
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.GeneralStatus,
                    opt => opt.MapFrom(x => GeneralStatus.WaitingApproval));
            CreateMap<Contact, ContactAddDto>().ReverseMap()
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.UpdatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.GeneralStatus,
                    opt => opt.MapFrom(x => GeneralStatus.Active));
            CreateMap<UserBook, UserBookAddDto>().ReverseMap()
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(x => DateTime.Now))
                .ForMember(dest => dest.BookStatus,
                opt => opt.MapFrom(x => BookStatus.Reading));
            CreateMap<UserBook, UserBookUpdateDto>().ReverseMap()
                .ForMember(dest => dest.BookStatus,
                    opt => opt.MapFrom(x => BookStatus.Read));
            CreateMap<Category, CategoryAddDto>().ReverseMap();
            CreateMap<Category, CategoryUpdateDto>().ReverseMap();
            CreateMap<Publisher, PublisherAddDto>().ReverseMap();
            CreateMap<Publisher, PublisherUpdateDto>().ReverseMap();
            CreateMap<Writer, WriterAddDto>().ReverseMap();
            CreateMap<Writer, WriterUpdateDto>().ReverseMap();
            CreateMap<BookCategory, BookCategoryAddDto>().ReverseMap();
            CreateMap<FavoriteBook, FavoriteBookUpdateDto>().ReverseMap();
            CreateMap<FavoriteBook, FavoriteBookAddDto>().ReverseMap();
        }
    }
}
