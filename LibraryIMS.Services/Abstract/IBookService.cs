﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IBookService
    {
        IAppResult AddBook(BookAddDto entity, string createdByName);
        IAppResult DeleteBook(int bookId, string updatedByName);
        IAppResult<BookDto> GetBook(int bookId);
        IAppResult HardDeleteBook(int bookId);
        IAppResult UndoDeleteBook(int bookId, string updatedByName);
        IAppResult UpdateBook(BookUpdateDto entity, string updatedByName);
        IAppResult<BookListDto> GetAllDeletedBook();
        IAppResult<BookListDto> GetAllBookByNonDeleted();
        IAppResult<BookListDto> FindBooksByText(string text);
        IAppResult<BookListDto> FindBooksByWriterName(string text);
        IAppResult<BookListDto> GetAllBookByNonStock();
        IAppResult<BookListDto> FindDeletedBooksByText(string text);
    }
}
