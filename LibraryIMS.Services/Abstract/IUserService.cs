﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IUserService
    {
        IAppResult<UserListDto> GetAllDeletedUser();
        IAppResult<UserListDto> GetAllUserByNonDeleted();
        IAppResult<UserListDto> FindUsersByText(string text);
        IAppResult<UserListDto> FindDeletedUsersByText(string text);
        IAppResult<UserListDto> FindUsersByRole(AccessStatus accessStatus);

        IAppResult HardDeleteUser(int userId);
        IAppResult<UserDto> GetUser(int userId);
        IAppResult<UserDto> GetUserByUserName(string userName);
        IAppResult DeleteUser(int userId, string updatedByName);
        IAppResult UndoDeleteUser(int userId, string updatedByName);
        IAppResult AddUser(UserAddDto entity, string createdByName);
        IAppResult UpdateUser(UserUpdateDto entity, string updatedByName);
        IAppResult UpdateUserRole(UserDto entity, string updatedByName);
    }
}