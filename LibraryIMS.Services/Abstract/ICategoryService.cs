﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface ICategoryService
    {
        IAppResult<CategoryListDto> GetAllDeletedCategory();
        IAppResult<CategoryListDto> GetAllCategoryByNonDeleted();
        IAppResult<CategoryListDto> FindCategoriesByText(string text);
        IAppResult<CategoryListDto> FindDeletedCategoriesByText(string text);
        IAppResult<CategoryListDto> GetParentCategories(int categoryId);
        IAppResult<CategoryDto> GetCategoryByParent(int parentId);
        IAppResult<CategoryDto> GetCategoryByName(string categoryName);
        IAppResult DeleteCategory(int categoryId);
        IAppResult HardDeleteCategory(int categoryId);
        IAppResult UndoDeleteCategory(int categoryId);
        IAppResult AddCategory(CategoryAddDto entity);
        IAppResult<CategoryDto> GetCategory(int categoryId);
        IAppResult UpdateCategory(CategoryUpdateDto entity);
    }
}
