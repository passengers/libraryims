﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IContactService
    {
        IAppResult<ContactListDto> GetAllDeletedContact();
        IAppResult<ContactListDto> GetAllContactByNonDeleted();
        IAppResult<ContactListDto> FindContactsByUserName(string text);
        IAppResult<ContactListDto> FindDeletedContactsByUserName(string text);
        IAppResult HardDeleteContact(int contactId);
        IAppResult<ContactDto> GetContact(int contactId);
        IAppResult DeleteContact(int contactId, string updatedByName);
        IAppResult UndoDeleteContact(int contactId, string updatedByName);
        IAppResult AddContact(ContactAddDto entity, string createdByName);
    }
}
