﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IUserBookService
    {
        IAppResult AddUserBook(UserBookAddDto entity);
        IAppResult HardDeleteUserBook(int userId, int bookId);
        IAppResult UpdateUserBook(UserBookUpdateDto entity);
        IAppResult<UserBookListDto> FindUserBookByText(string text);
        IAppResult<UserBookListDto> GetAllUserBook();
        IAppResult<BookListDto> GetAllReadBooksByUser(int userId);
    }
}
