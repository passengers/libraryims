﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Data.EntityFramework.UnitOfWork;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IFavoriteBookService
    {
        IAppResult AddFavoriteBook(FavoriteBookAddDto entity);
        IAppResult HardDeleteFavoriteBook(FavoriteBookDto entity);
        IAppResult<BookListDto> GetAllFavoriteBookByUser( int userId);
        IAppResult<FavoriteBookDto> GetFavoriteBook(int bookId,int userId);
        IAppResult<BookListDto> FindFavoriteBooksByText(string text,int userId);
        IAppResult<BookListDto> FindFavoriteBooksByWriterName(string text,int userId);
        IAppResult<BookListDto> FindFavoriteBooksByCategory(string text, int userId);
        IAppResult<BookListDto> GetAllFavoriteBookByNonStock(int userId);
    }
}
