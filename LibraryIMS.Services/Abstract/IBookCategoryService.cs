﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IBookCategoryService
    {
        IAppResult<BookListDto> GetAllBookByNonTagged();
        IAppResult<CategoryListDto> GetCategoriesByBookId(int id);
        IAppResult HardDeleteBookCategory(int categoryId, int bookId);
        IAppResult AddBookCategory(BookCategoryAddDto entity);
        IAppResult<BookListDto> FindBooksByCategoryName(string categoryName);
        IAppResult HardDeleteBookCategoryByCategoryId(int categoryId);
    }
}
