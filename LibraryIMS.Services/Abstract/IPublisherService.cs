﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IPublisherService
    {
        IAppResult AddPublisher(PublisherAddDto entity);
        IAppResult DeletePublisher(int publisherId);
        IAppResult HardDeletePublisher(int publisherId);
        IAppResult UndoDeletePublisher(int publisherId);
        IAppResult UpdatePublisher(PublisherUpdateDto entity);

        IAppResult<PublisherDto> GetPublisher(int publisherId);
        IAppResult<PublisherListDto> GetAllPublisher();
        IAppResult<PublisherListDto> GetAllDeletedPublisher();
        IAppResult<PublisherListDto> GetAllPublisherByNonDeleted();
        IAppResult<PublisherListDto> FindPublishersByText(string text);
        IAppResult<PublisherListDto> FindDeletedPublishersByText(string text);
    }
}
