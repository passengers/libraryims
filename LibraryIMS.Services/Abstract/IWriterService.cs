﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface IWriterService
    {
        IAppResult<WriterListDto> GetAllDeletedWriter();
        IAppResult<WriterListDto> GetAllWriterByNonDeleted();
        IAppResult<WriterListDto> FindWritersByText(string text);
        IAppResult<WriterListDto> FindDeletedWritersByText(string text);

        int GetNumberOfBooksForWriter(int writerId);
        IAppResult HardDeleteWriter(int writerId);
        IAppResult<WriterDto> GetWriter(int writerId);
        IAppResult DeleteWriter(int writerId);
        IAppResult UndoDeleteWriter(int writerId);
        IAppResult AddWriter(WriterAddDto entity);
        IAppResult UpdateWriter(WriterUpdateDto entity);
    }
}