﻿using LibraryIMS.Core.Result.Abstract;
using LibraryIMS.Entities.Entities.Dtos;

namespace LibraryIMS.Services.Abstract
{
    public interface ICommentService
    {
        IAppResult<CommentListDto> GetAllCommentByNonDeleted();
        IAppResult<CommentListDto> GetAllDeletedComment();
        IAppResult<CommentListDto> GetAllCommentByNonApproved();
        IAppResult<CommentListDto> FindCommentsByBookName(string bookName);
        IAppResult<CommentListDto> FindCommentsByUserName(string text);
        IAppResult<CommentListDto> FindDeletedCommentsByText(string text);
        IAppResult HardDeleteComment(int commentId);
        IAppResult<CommentDto> GetComment(int commentId);
        IAppResult DeleteComment(int commentId, string updatedByName);
        IAppResult UndoDeleteComment(int commentId, string updatedByName);
        IAppResult AddComment(CommentAddDto entity, string createdByName);
        IAppResult ApproveComment(int commentId, string updatedByName);
        IAppResult<CommentListDto> FindCommentsByUserNameAndBookName(string username, string bookname);
    }
}
