﻿namespace LibraryIMS.Common
{
    #region GenericResult
    public class Result<T>
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
    #endregion
}
