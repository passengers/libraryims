﻿using LibraryIMS.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace LibraryService.Common
{
    public class ORMBase<ET, OT> : IORM<ET>
        where ET : class, new()
        where OT : class, new()
    {
        private static OT _current;
        public static OT Current
        {
            get
            {
                if (_current == null)
                    _current = new OT();
                return _current;
            }
        }

        private Type ETType
        {
            get
            {
                return typeof(ET);
            }
        }

        private TableAtt _tableAtt
        {
            get
            {
                var tAttributes = ETType.GetCustomAttributes(typeof(TableAtt), false);
                if (tAttributes != null && tAttributes.Any())
                {
                    TableAtt tbl = (TableAtt)tAttributes[0];
                    return tbl;
                }
                return null;
            }
        }

        public Result<bool> Delete(ET entity)
        {
            object guID = "";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Tools.Connection;
            string query = string.Format("delete from {0}", _tableAtt.TableName);
            PropertyInfo[] properties = ETType.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (pi.Name == _tableAtt.IdentityColumn)
                {
                    guID = pi.GetValue(entity);
                    break;
                }
            }
            query += string.Format(" where {0}=", _tableAtt.PrimaryColumn);
            query += string.Format("{0}", guID);
            cmd.CommandText = query;
            return cmd.Exec();
        }

        public Result<bool> Insert(ET entity)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Tools.Connection;
            string query = "insert into";
            query += string.Format(" {0}(", _tableAtt.TableName);
            string values = " values(";

            PropertyInfo[] properties = ETType.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (pi.Name == _tableAtt.IdentityColumn)
                {
                    continue;
                }
                Object value = pi.GetValue(entity);
                if (value == null) continue;
                query += string.Format("{0},", pi.Name);
                values += string.Format("@{0},", pi.Name);
                cmd.Parameters.AddWithValue(string.Format("@{0}", pi.Name), value);
            }

            query = query.Remove(query.Length - 1, 1);
            values = values.Remove(values.Length - 1, 1);
            query += string.Format(") {0})", values);

            cmd.CommandText = query;

            return cmd.Exec();
        }

        public Result<List<ET>> Select()
        {
            string query = "select * from ";
            query += _tableAtt.TableName;
            SqlDataAdapter da = new SqlDataAdapter(query, Tools.Connection);
            return da.ToList<ET>();
        }

        public Result<bool> Update(ET entity)
        {
            object guID = "";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Tools.Connection;
            string query = string.Format("update {0} set ",_tableAtt.TableName);
            PropertyInfo[] properties = ETType.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (pi.Name == _tableAtt.IdentityColumn)
                {
                    guID = pi.GetValue(entity);
                    continue;
                }
                Object value = pi.GetValue(entity);
                if (value == null) continue;
                query += string.Format("{0}=", pi.Name);
                query += string.Format("@{0},", pi.Name);
                cmd.Parameters.AddWithValue(string.Format("@{0}", pi.Name), value);
            }
            query = query.Remove(query.Length - 1, 1);
            query += string.Format(" where {0}=", _tableAtt.PrimaryColumn);
            query += string.Format("{0}", guID);
            cmd.CommandText = query;
            return cmd.Exec();
        }
    }
}
