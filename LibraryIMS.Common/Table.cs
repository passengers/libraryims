﻿using System;

namespace LibraryService.Common
{
    public class TableAtt : Attribute
    {
        public string TableName { get; set; }
        public string PrimaryColumn { get; set; }
        public string IdentityColumn { get; set; }
    }
}