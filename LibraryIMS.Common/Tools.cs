﻿using DevExpress.XtraEditors;
using LibraryIMS.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;

namespace LibraryService.Common
{
    public static class Tools
    {
        private static SqlConnection _connection;
        public static SqlConnection Connection
        {
            get
            {
                if (_connection == null)
                    _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DBModel"].ConnectionString);
                return _connection;
            }
            set { _connection = value; }
        }

        public static void ClearFormControls(Control Ctrl)
        {
            foreach (Control ctrl in Ctrl.Controls)
            {
                BaseEdit editor = ctrl as BaseEdit;

                if (editor != null)
                    editor.EditValue = null;

                ClearFormControls(ctrl);
            }            
        }

        public static bool CheckFormControls(GroupBox GBox)
        {
            int counter = 0;
            foreach (Control ctrl in GBox.Controls)
            {
                if (ctrl is TextEdit || ctrl is DateEdit)
                {
                    if ((ctrl as BaseEdit).Text == "")
                    {
                        counter++;
                    }
                }
                else if (ctrl is ComboBoxEdit)
                {
                    if ((ctrl as ComboBoxEdit).SelectedIndex == 0)
                    {
                        counter++;
                    }
                }
            }
            if (counter > 0) return false;
            else return true;
        }

        public static Result<List<ET>> ToList<ET>(this SqlDataAdapter da) where ET : class, new()
        {
            try
            {
                DataTable dt = new DataTable();
                da.Fill(dt);
                Type type = typeof(ET);
                List<ET> list = new List<ET>();
                PropertyInfo[] properties = type.GetProperties();

                foreach (DataRow dr in dt.Rows)
                {
                    ET tip = new ET();
                    
                    foreach (PropertyInfo pi in properties)
                    {
                        object value = dr[pi.Name];
                        if (value != null)
                            pi.SetValue(tip, value);
                    }
                    list.Add(tip);
                }

                return new Result<List<ET>>
                {
                    IsSuccess = true,
                    Message = "İşlem Başarılı.",
                    Data = list                    
                };
            }
            catch (Exception ex)
            {
                return new Result<List<ET>>
                {
                    IsSuccess = false,
                    Message = "Hata! " + ex.Message
                };
            }
        }
        public static Result<bool> Exec(this SqlCommand cmd)
        {
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();

                int result = cmd.ExecuteNonQuery();
                return new Result<bool>
                {
                    IsSuccess = true,
                    Message = "İşlem Başarılı.",
                    Data = result > 0
                };
            }
            catch (Exception ex)
            {
                return new Result<bool>
                {
                    IsSuccess = false,
                    Message = "Hata!" + ex.Message
                };
            }
            finally
            {
                if (cmd.Connection.State != ConnectionState.Closed)
                    cmd.Connection.Close();
            }
        }

        public static bool IsConnected { get; set; }
        public static int AccessLevel { get; set; }
        public static int Identity { get; set; }
    }
}
