﻿namespace LibraryIMS.Common.Enums
{
    public enum BookStatus
    {
        Read,
        Reading,
        WillRead
    }
    public enum BanStatus
    {
        Stable,
        EntryBan,
        DebtBan
    }
    public enum AccessStatus
    {
        Manager,
        Personnel,
        Member,
        Visitor
    }
}
