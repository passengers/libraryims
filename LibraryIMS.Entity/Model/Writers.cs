using LibraryService.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Writers", PrimaryColumn = "WriterID", IdentityColumn = "WriterID")]
    public partial class Writers
    {
        public Writers()
        {
            Books = new HashSet<Books>();
        }

        [Key]
        public int WriterID { get; set; }

        [Required]
        [StringLength(100)]
        public string NameSurname { get; set; }

        public virtual ICollection<Books> Books { get; set; }
    }

    [TableAtt(TableName = "Writers", PrimaryColumn = "WriterID", IdentityColumn = "WriterID")]
    public partial class WritersDTO
    {
        public int WriterID { get; set; }
        public string NameSurname { get; set; }
    }
}
