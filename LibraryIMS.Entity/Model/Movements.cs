using LibraryService.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Movements", PrimaryColumn = "ID", IdentityColumn = "ID")]    
    public partial class Movements
    {        
        public int ID { get; set; }
        [Required]
        [StringLength(11)]
        public string VisitorTC { get; set; }
        [StringLength(50)]
        [Required]
        public string NameSurname { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool? IsVisitor { get; set; }
        public bool? Status { get; set; }
    }
}
