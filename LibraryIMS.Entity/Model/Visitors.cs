using LibraryService.Common;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Visitors", PrimaryColumn = "VisitorID", IdentityColumn = "VisitorID")]
    public partial class Visitors
    {
        [Key]
        public int VisitorID { get; set; }

        [Required]
        [StringLength(11)]
        public string TC { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Surname { get; set; }

        [Required]
        [StringLength(50)]
        public string Telephone { get; set; }

        public int? AccessID { get; set; }

        [Required]
        [StringLength(11)]
        public string AddingPerson { get; set; }

    }
}
