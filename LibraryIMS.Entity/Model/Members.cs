using LibraryService.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Members", PrimaryColumn = "MemberID", IdentityColumn = "MemberID")]
    public partial class Members
    {
        public Members()
        {
            Temp = new HashSet<Temp>();
        }

        [Key]
        public int MemberID { get; set; }

        [Required]
        [StringLength(11)]
        public string TC { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Surname { get; set; }

        [Required]
        [StringLength(50)]
        public string Username { get; set; }

        [Required]
        [StringLength(8)]
        public string Password { get; set; }

        [Required]
        [StringLength(50)]
        public string Gender { get; set; }

        public DateTime DateBirth { get; set; }

        public DateTime DateAdded { get; set; }

        [Required]
        [StringLength(50)]
        public string Telephone { get; set; }

        [StringLength(50)]
        public string Mail { get; set; }

        public int? BanSituation { get; set; }

        public int? AccessID { get; set; }

        public virtual ICollection<Temp> Temp { get; set; }
    }

    [TableAtt(TableName = "Members", PrimaryColumn = "MemberID", IdentityColumn = "MemberID")]
    public partial class MembersDTO
    {
        public int MemberID { get; set; }
        public string TC { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public DateTime DateBirth { get; set; }
        public DateTime? DateAdded { get; set; }
        public string Telephone { get; set; }
        public string Mail { get; set; }
        public int BanSituation { get; set; }
        public int AccessID { get; set; }
    }
}
