using LibraryService.Common;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Temp", PrimaryColumn = "ID", IdentityColumn = "ID")]
    public partial class Temp
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int BookID { get; set; }
        public int StatusID { get; set; }

        public virtual Books Books { get; set; }
        public virtual Members Members { get; set; }
    }

    [TableAtt(TableName = "Temp", PrimaryColumn = "ID", IdentityColumn = "ID")]
    public partial class TempDTO
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int BookID { get; set; }
        public int StatusID { get; set; }
    }
}
