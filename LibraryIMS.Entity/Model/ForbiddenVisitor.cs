using LibraryService.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "ForbiddenVisitor", PrimaryColumn = "ID", IdentityColumn = "ID")]
    public partial class ForbiddenVisitor
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string NameSurname { get; set; }

        [Required]
        [StringLength(11)]
        public string VisitorTC { get; set; }

        [Required]
        [StringLength(11)]
        public string BlockerTC { get; set; }

        public DateTime? DateBlock { get; set; }

        public int? BanSituation { get; set; }
    }
}
