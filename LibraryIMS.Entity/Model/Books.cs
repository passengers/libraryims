using LibraryService.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Books", PrimaryColumn = "ID", IdentityColumn = "ID")]
    public partial class Books
    {
        public Books()
        {
            Temp = new HashSet<Temp>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string BookName { get; set; }

        public int WriterID { get; set; }

        public int PublisherID { get; set; }

        public int CategoryID { get; set; }

        public int SubCategoryID { get; set; }

        [Required]
        [StringLength(8)]
        public string Shelf { get; set; }

        public int Page { get; set; }

        public int Stock { get; set; }

        public DateTime DatePublication { get; set; }

        [Required]
        [StringLength(50)]
        public string ISBN { get; set; }

        [StringLength(100)]
        public string BookStatement { get; set; }

        public virtual Categories Categories { get; set; }
        public virtual Publishers Publishers { get; set; }
        public virtual SubCategories SubCategories { get; set; }
        public virtual Writers Writers { get; set; }
        public virtual ICollection<Temp> Temp { get; set; }
    }

    [TableAtt(TableName = "Books", PrimaryColumn = "ID", IdentityColumn = "ID")]
    public partial class BooksDTO
    {
        public int ID { get; set; }
        public string BookName { get; set; }
        public int WriterID { get; set; }
        public int PublisherID { get; set; }
        public int CategoryID { get; set; }
        public int SubCategoryID { get; set; }
        public string Shelf { get; set; }
        public int Page { get; set; }
        public int Stock { get; set; }
        public DateTime? DatePublication { get; set; }
        public string ISBN { get; set; }
        public string BookStatement { get; set; }
    }
}
