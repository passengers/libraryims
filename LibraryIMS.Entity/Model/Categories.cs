using LibraryService.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Categories", PrimaryColumn = "CategoryID", IdentityColumn = "CategoryID")]
    public partial class Categories
    {
        public Categories()
        {
            Books = new HashSet<Books>();
        }

        [Key]
        public int CategoryID { get; set; }

        [Required]
        [StringLength(50)]
        public string CategoryName { get; set; }

        public virtual ICollection<Books> Books { get; set; }
    }

    [TableAtt(TableName = "Categories", PrimaryColumn = "CategoryID", IdentityColumn = "CategoryID")]
    public partial class CategoriesDTO
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
