using LibraryService.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "SubCategories", PrimaryColumn = "SubCategoryID", IdentityColumn = "SubCategoryID")]
    public partial class SubCategories
    {
        public SubCategories()
        {
            Books = new HashSet<Books>();
        }

        [Key]
        public int SubCategoryID { get; set; }

        [Required]
        [StringLength(50)]
        public string SubCategoryName { get; set; }

        public virtual ICollection<Books> Books { get; set; }
    }

    [TableAtt(TableName = "SubCategories", PrimaryColumn = "SubCategoryID", IdentityColumn = "SubCategoryID")]
    public partial class SubCategoriesDTO
    {
        public int SubCategoryID { get; set; }
        public string SubCategoryName { get; set; }
    }
}
