using LibraryService.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryIMS.Entity.Model
{
    [TableAtt(TableName = "Publishers", PrimaryColumn = "PublisherID", IdentityColumn = "PublisherID")]
    public partial class Publishers
    {
        public Publishers()
        {
            Books = new HashSet<Books>();
        }

        [Key]
        public int PublisherID { get; set; }

        [Required]
        [StringLength(50)]
        public string PublisherName { get; set; }

        public virtual ICollection<Books> Books { get; set; }
    }

    [TableAtt(TableName = "Publishers", PrimaryColumn = "PublisherID", IdentityColumn = "PublisherID")]
    public partial class PublishersDTO
    {
        public int PublisherID { get; set; }
        public string PublisherName { get; set; }
    }
}
