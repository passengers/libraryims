using System.Data.Entity;

namespace LibraryIMS.Entity.Model
{
    public partial class LibraryDB : DbContext
    {
        public LibraryDB()
            : base("name=LibraryDB")
        {
        }

        public virtual DbSet<Books> Books { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<ForbiddenVisitor> ForbiddenVisitor { get; set; }
        public virtual DbSet<Members> Members { get; set; }
        public virtual DbSet<Movements> Movements { get; set; }
        public virtual DbSet<Publishers> Publishers { get; set; }
        public virtual DbSet<SubCategories> SubCategories { get; set; }
        public virtual DbSet<Temp> Temp { get; set; }
        public virtual DbSet<Visitors> Visitors { get; set; }
        public virtual DbSet<Writers> Writers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Books>()
                .Property(e => e.BookName)
                .IsUnicode(false);

            modelBuilder.Entity<Books>()
                .Property(e => e.Shelf)
                .IsUnicode(false);

            modelBuilder.Entity<Books>()
                .Property(e => e.ISBN)
                .IsUnicode(false);

            modelBuilder.Entity<Books>()
                .Property(e => e.BookStatement)
                .IsUnicode(false);

            modelBuilder.Entity<Books>()
                .HasMany(e => e.Temp)
                .WithRequired(e => e.Books)
                .HasForeignKey(e => e.BookID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Categories>()
                .Property(e => e.CategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<Categories>()
                .HasMany(e => e.Books)
                .WithRequired(e => e.Categories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ForbiddenVisitor>()
                .Property(e => e.NameSurname)
                .IsUnicode(false);

            modelBuilder.Entity<ForbiddenVisitor>()
                .Property(e => e.VisitorTC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ForbiddenVisitor>()
                .Property(e => e.BlockerTC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.TC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Password)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Telephone)
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .Property(e => e.Mail)
                .IsUnicode(false);

            modelBuilder.Entity<Members>()
                .HasMany(e => e.Temp)
                .WithRequired(e => e.Members)
                .HasForeignKey(e => e.UserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Movements>()
                .Property(e => e.VisitorTC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Movements>()
                .Property(e => e.NameSurname)
                .IsUnicode(false);

            modelBuilder.Entity<Publishers>()
                .Property(e => e.PublisherName)
                .IsUnicode(false);

            modelBuilder.Entity<Publishers>()
                .HasMany(e => e.Books)
                .WithRequired(e => e.Publishers)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubCategories>()
                .Property(e => e.SubCategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<SubCategories>()
                .HasMany(e => e.Books)
                .WithRequired(e => e.SubCategories)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Visitors>()
                .Property(e => e.TC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Visitors>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Visitors>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Visitors>()
                .Property(e => e.Telephone)
                .IsUnicode(false);

            modelBuilder.Entity<Visitors>()
                .Property(e => e.AddingPerson)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Writers>()
                .Property(e => e.NameSurname)
                .IsUnicode(false);

            modelBuilder.Entity<Writers>()
                .HasMany(e => e.Books)
                .WithRequired(e => e.Writers)
                .WillCascadeOnDelete(false);
        }
    }
}
