﻿using LibraryIMS.Core.Result.Abstract;

namespace LibraryIMS.Core.Result.Concrete
{
    /// <summary>
    /// IAppResult arabiriminde imzaladığımız propları bu sınıfta tanımlıyoruz.
    /// Burada iki farklı yapı ile Geri dönüş tipleri oluşturulmuştur.
    /// Success dönüşü ile durumun başarılı olduğunu setleme işlemini burada yaparak kod maliyetini azaltıyoruz.
    /// Fail dönüşü ile de aynı şekilde durumun hatalı olduğunu setleme işlemini burada gerçekleştiriyoruz.
    /// Bu geri dönüş tiplerinde sadece ilgili Başarılı, Başarısız durumunu çağırarak parametre olarak Mesaj gönderilir.
    /// </summary>
    public class AppResult : IAppResult
    {
        public AppResult Success(string message)
        {
            return new AppResult { ResultStatus = ResultStatus.Success, Message = message };
        }
        public AppResult Fail(string message)
        {
            return new AppResult { ResultStatus = ResultStatus.Error, Message = message };
        }

        public ResultStatus ResultStatus { get; private set; }
        public string Message { get; private set; }
    }

    /// <summary>
    /// IAppResult arabiriminde imzaladığımız propları bu sınıfta tanımlıyoruz.
    /// Burada iki farklı yapı ile Geri dönüş tipleri oluşturulmuştur.
    /// Success dönüşü ile durumun başarılı olduğunu setleme işlemini burada yaparak kod maliyetini azaltıyoruz.
    /// Fail dönüşü ile de aynı şekilde durumun hatalı olduğunu setleme işlemini burada gerçekleştiriyoruz.
    /// Bu geri dönüş tiplerinde sadece ilgili Başarılı, Başarısız durumunu çağırarak parametre olarak Mesaj ve Data gönderilir.
    /// </summary>
    public class AppResult<T> : IAppResult<T> where T : class
    {
        public AppResult<T> Success(T data)
        {
            return new AppResult<T> { ResultStatus = ResultStatus.Success, Data = data };
        }
        public AppResult<T> Fail(string message)
        {
            return new AppResult<T> { ResultStatus = ResultStatus.Error, Message = message };
        }

        public ResultStatus ResultStatus { get; private set; }
        public string Message { get; private set; }
        public T Data { get; private set; }
    }
}
