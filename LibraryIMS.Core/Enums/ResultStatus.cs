﻿/// <summary>
/// Projemiz içerisinde kullanacağımız durumları bu tarz classlar içerisinde yazarak bir araya toplamış olacağız.
/// Bu sayede bir durum ataması yaparken ya da kontrol gerçekleştirirken bu enumlar'ı kullanmak yeterli olacak.
/// Bu durum üzerinde ilgili CRUD operasyonlarının dönüş tipinde yardımcı olacak durumlar barındırılır.
/// </summary>
public enum ResultStatus
{
    Success = 0,
    Error = 1,
    Warning = 2,
    Info = 3
}