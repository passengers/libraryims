﻿using System;
using System.Data;
using System.Linq;
using System.Drawing;
using LibraryIMS.ORM;
using System.Windows.Forms;
using LibraryService.Common;
using LibraryIMS.ORM.ORMRepo;
using DevExpress.XtraEditors;
using LibraryIMS.Entity.Model;
using System.Collections.Generic;

namespace LibraryIMS.UI
{
    public partial class AddBook : Form
    {

        #region Ctor

        public AddBook()
        {
            InitializeComponent();
            BindBookList();
            this.txtStatement.Text = "NULL";
        }

        #endregion

        #region Field

        // Kitap var mı ?
        private bool _isThere;

        // Güncelleme yapılacak kitabın ID bilgisi
        private int _bookID = -1;

        #endregion

        #region Methods

        private void BindBookList(List<object> listBook = null)
        {
            if (listBook == null) listBook = this.GetBookList();

            gcBook.DataSource = listBook;
            gvBook.Columns[0].Visible = false;
            panelGrid.Visible = listBook.Count() > 0;
            lblMessage.Text = string.Format("{0} adet ziyaret listeleniyor.  ", listBook.Count());
        }

        /// <summary>
        /// Kitap tablosundaki int değerlere göre diğer ilgili tablolardan string değerleri geri döndürür.
        /// İlgili combobox'ın içini doldurur.
        /// </summary>
        private WritersDTO GetWriter(int id)
        {
            var list = WritersORM.Current.Select();

            if (cbWriter.Properties.Items.Count == 0)
            {
                cbWriter.Properties.Items.Insert(0, "Seçiniz");
                cbWriter.SelectedIndex = 0;

                foreach (var x in list.Data)
                {
                    cbWriter.Properties.Items.Add(x.NameSurname);
                    cbWriter.Properties.EditFormat.FormatString = x.WriterID.ToString();
                }
            }

            if (list.IsSuccess) return list.Data.First(x=>x.WriterID==id);
            else return null;
        }
        private PublishersDTO GetPublisher(int id)
        {
            var list = PublishersORM.Current.Select();

            if (cbPublisher.Properties.Items.Count == 0)
            {
                cbPublisher.Properties.Items.Insert(0, "Seçiniz");
                cbPublisher.SelectedIndex = 0;

                foreach (var x in list.Data)
                {
                    cbPublisher.Properties.Items.Add(x.PublisherName);
                    cbPublisher.Properties.EditFormat.FormatString = x.PublisherID.ToString();
                }
            }

            if (list.IsSuccess) return list.Data.First(x => x.PublisherID == id);
            else return null;
        }
        private CategoriesDTO GetCategory(int id)
        {
            var list = CategoriesORM.Current.Select();

            if (cbCategory.Properties.Items.Count == 0)
            {
                cbCategory.Properties.Items.Insert(0, "Seçiniz");
                cbCategory.SelectedIndex = 0;

                foreach (var x in list.Data)
                {
                    cbCategory.Properties.Items.Add(x.CategoryName);
                    cbCategory.Properties.EditFormat.FormatString = x.CategoryID.ToString();
                }
            }

            if (list.IsSuccess) return list.Data.First(x => x.CategoryID == id);
            else return null;
        }
        private SubCategoriesDTO GetSubCategory(int id)
        {
            var list = SubCategoriesORM.Current.Select();

            if (cbSubCategory.Properties.Items.Count == 0)
            {
                cbSubCategory.Properties.Items.Insert(0, "Seçiniz");
                cbSubCategory.SelectedIndex = 0;

                foreach (var x in list.Data)
                {
                    cbSubCategory.Properties.Items.Add(x.SubCategoryName);
                    cbSubCategory.Properties.EditFormat.FormatString = x.SubCategoryID.ToString();
                }
            }

            if (list.IsSuccess) return list.Data.First(x => x.SubCategoryID == id);
            else return null;
        }        

        /// <summary>
        /// Tüm kitapların listesini getir.
        /// </summary>
        private List<object> GetBookList()
        {
            var list =
            BooksORM.Current.Select().Data.
            OrderBy(x => x.BookName).
            Select(x => new
            {
                ID = x.ID,
                Kitap_Adı = x.BookName,
                Yazar_Adı = GetWriter(x.WriterID).NameSurname,
                Yayınevi= GetPublisher(x.PublisherID).PublisherName,
                Kategori=GetCategory(x.CategoryID).CategoryName,
                Alt_Kategori=GetSubCategory(x.SubCategoryID).SubCategoryName,
                Raf_No=x.Shelf,
                Stok=x.Stock,
                Yayın_Tarihi=x.DatePublication
            });

            return list.Cast<object>().ToList();
        }

        /// <summary>
        /// Kitabın var olup olmadıgını kontrol et.
        /// </summary>
        private void CheckIsThere()
        {
            var result = BooksORM.Current.Select().Data.Where(x => x.BookName == txtBookName.Text && GetWriter(x.WriterID).NameSurname==cbWriter.SelectedItem.ToString()).Any();

            _isThere = result;
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            Tools.ClearFormControls(this);
            _bookID = -1;
        }

        /// <summary>
        /// Sisteme yeni kitap ekler veya günceller.
        /// </summary>
        private void AddBookModel()
        {
            CheckIsThere();
            if (!Tools.CheckFormControls(gbBook))
            {
                XtraMessageBox.Show("Lütfen tüm alanları doldurunuz!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string DataError;
            bool DataSuccess = false;

            var bookModel = new BooksDTO
            {
                BookName=txtBookName.Text,
                WriterID=WritersORM.Current.Select().Data.First(x=>x.NameSurname==cbWriter.SelectedItem.ToString()).WriterID,
                PublisherID=PublishersORM.Current.Select().Data.First(x=>x.PublisherName==cbPublisher.SelectedItem.ToString()).PublisherID,
                CategoryID=CategoriesORM.Current.Select().Data.First(x=>x.CategoryName==cbCategory.SelectedItem.ToString()).CategoryID,
                SubCategoryID= SubCategoriesORM.Current.Select().Data.First(x => x.SubCategoryName == cbSubCategory.SelectedItem.ToString()).SubCategoryID,
                Shelf=txtShelf.Text,
                Page=Convert.ToInt32(txtPage.Text),
                Stock=Convert.ToInt32(txtStock.Text),
                DatePublication=datePublicate.DateTime,
                ISBN=txtISBN.Text,
                BookStatement=txtStatement.Text
            };

            // Yeni kitap ekler.
            if (_bookID == -1 && !_isThere)
            {
                var book = BooksORM.Current.Insert(bookModel);
                DataSuccess = book.Data;

                if (!book.IsSuccess)
                {
                    XtraMessageBox.Show("Kitap eklenirken bir hata oluştu!\n\n" + book.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            else
            {
                // Kayıt guncellenecek.
                if (_bookID != -1)
                {
                    bookModel.ID = _bookID;
                    var book = BooksORM.Current.Update(bookModel);
                    DataSuccess = book.Data;

                    if (!book.IsSuccess)
                    {
                        XtraMessageBox.Show("Kitap güncelleme sırasında bir hata oluştu!\n\n" + book.Message, "Hata",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show("Kitap sistemde zaten kayıtlı!", "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (DataSuccess)
            {
                XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ClearItems();
                this.BindBookList();
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Gridde seçilen kitap bilgilerini getir.
        /// </summary>
        private void GetBookObject()
        {
            if (this.gvBook.SelectedRowsCount > 0)
            {
                int.TryParse(gvBook.GetRowCellValue(gvBook.FocusedRowHandle, "ID").ToString(), out this._bookID);
                var model = BooksORM.Current.Select().Data.Where(x => x.ID == _bookID).ToList();
                if (model.Count > 0)
                {
                    this.txtBookName.Text = model[0].BookName;
                    this.cbWriter.Text = GetWriter(model[0].WriterID).NameSurname;
                    this.cbPublisher.Text = GetPublisher(model[0].PublisherID).PublisherName;
                    this.cbCategory.Text = GetCategory(model[0].CategoryID).CategoryName;
                    this.cbSubCategory.Text = GetSubCategory(model[0].SubCategoryID).SubCategoryName;
                    this.txtPage.Text = Convert.ToString(model[0].Page);
                    this.txtShelf.Text = model[0].Shelf;
                    this.datePublicate.DateTime = model[0].DatePublication.Value;
                    this.txtStock.Text = Convert.ToString(model[0].Stock);
                    this.txtISBN.Text = model[0].ISBN;
                    this.txtStatement.Text = model[0].BookStatement;
                }
            }
        }

        /// <summary>
        /// Seçilen kitabı kaldır.
        /// </summary>
        private void DeleteBook(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Kitap kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var model = new BooksDTO
                {
                    ID = _bookID
                };
                var result = BooksORM.Current.Delete(model);
                if (!result.IsSuccess)
                {
                    XtraMessageBox.Show("Kitap kaldırılırken bir hata oluştu!\n\n" + result.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    XtraMessageBox.Show("İşleminiz başarıyla gerçekleştirilmiştir.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.ClearItems();
                    this.BindBookList();
                }
            }
        }

        /// <summary>
        /// Kitap ve yazar adına göre kitap ara.
        /// </summary>
        private void SearchBook()
        {
            string searchText = this.txtSearch.Text;

            if (String.IsNullOrEmpty(searchText))
                this.BindBookList();

            var list =
            BooksORM.Current.Select().Data.
            Where(x => (x.BookName.ToLower().Contains(searchText) || GetWriter(x.WriterID).NameSurname.ToLower().Contains(searchText) )).
            OrderBy(x => x.BookName).
            Select(x => new
            {
                ID = x.ID,
                Kitap_Adı = x.BookName,
                Yazar_Adı = GetWriter(x.WriterID).NameSurname,
                Yayınevi = GetPublisher(x.PublisherID).PublisherName,
                Kategori = GetCategory(x.CategoryID).CategoryName,
                Alt_Kategori = GetSubCategory(x.SubCategoryID).SubCategoryName,
                Raf_No = x.Shelf,
                Stok = x.Stock,
                Yayın_Tarihi = x.DatePublication
            });
            this.BindBookList(list.Cast<object>().ToList());
        }

        #endregion

        #region Buttons

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearItems();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.AddBookModel();
        }

        #endregion

        #region Grid

        /// <summary>
        /// Grid üzerindeki kitaplara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcBook_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var cm = new ContextMenu();
                cm.MenuItems.Add(new MenuItem("Kitabı listeden kaldır", this.DeleteBook));

                int rowIndex = gvBook.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvBook.ClearSelection();
                    gvBook.SelectRow(rowIndex);
                    _bookID = Convert.ToInt32(gvBook.GetRowCellValue(rowIndex, "ID"));
                    cm.Show(gcBook, new Point(e.X, e.Y));
                }
            }
        }

        private void gvBook_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            this.GetBookObject();
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchBook();
        }

        #endregion

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "member.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                gcBook.ExportToXls(sfd.FileName);
            }
        }

        #endregion

    }
}
