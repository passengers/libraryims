﻿
namespace LibraryIMS.UI
{
    partial class AddBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddBook));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression3 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.lblMessage = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.gbBook = new System.Windows.Forms.GroupBox();
            this.datePublicate = new DevExpress.XtraEditors.DateEdit();
            this.cbWriter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbPublisher = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbSubCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbCategory = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtStock = new DevExpress.XtraEditors.TextEdit();
            this.txtStatement = new DevExpress.XtraEditors.TextEdit();
            this.txtISBN = new DevExpress.XtraEditors.TextEdit();
            this.txtShelf = new DevExpress.XtraEditors.TextEdit();
            this.txtBookName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPage = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.gvBook = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcBook = new DevExpress.XtraGrid.GridControl();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lnkExcel = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.label9 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblClock = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panelBottom.SuspendLayout();
            this.gbBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datePublicate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePublicate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSubCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStock.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtISBN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBookName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).BeginInit();
            this.panelGrid.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMessage.Location = new System.Drawing.Point(1557, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 18);
            this.lblMessage.TabIndex = 1;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.lblMessage);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 541);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1557, 28);
            this.panelBottom.TabIndex = 94;
            // 
            // gbBook
            // 
            this.gbBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBook.Controls.Add(this.datePublicate);
            this.gbBook.Controls.Add(this.cbWriter);
            this.gbBook.Controls.Add(this.cbPublisher);
            this.gbBook.Controls.Add(this.cbSubCategory);
            this.gbBook.Controls.Add(this.cbCategory);
            this.gbBook.Controls.Add(this.btnClear);
            this.gbBook.Controls.Add(this.btnSave);
            this.gbBook.Controls.Add(this.txtStock);
            this.gbBook.Controls.Add(this.txtStatement);
            this.gbBook.Controls.Add(this.txtISBN);
            this.gbBook.Controls.Add(this.txtShelf);
            this.gbBook.Controls.Add(this.txtBookName);
            this.gbBook.Controls.Add(this.label3);
            this.gbBook.Controls.Add(this.label16);
            this.gbBook.Controls.Add(this.label6);
            this.gbBook.Controls.Add(this.label4);
            this.gbBook.Controls.Add(this.label8);
            this.gbBook.Controls.Add(this.label15);
            this.gbBook.Controls.Add(this.label14);
            this.gbBook.Controls.Add(this.txtPage);
            this.gbBook.Controls.Add(this.label12);
            this.gbBook.Controls.Add(this.label11);
            this.gbBook.Controls.Add(this.label2);
            this.gbBook.Controls.Add(this.label13);
            this.gbBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbBook.Location = new System.Drawing.Point(12, 17);
            this.gbBook.Name = "gbBook";
            this.gbBook.Size = new System.Drawing.Size(1529, 174);
            this.gbBook.TabIndex = 1;
            this.gbBook.TabStop = false;
            this.gbBook.Text = "Üye Kimlik Bilgileri";
            // 
            // datePublicate
            // 
            this.datePublicate.EditValue = null;
            this.datePublicate.Location = new System.Drawing.Point(851, 90);
            this.datePublicate.Name = "datePublicate";
            this.datePublicate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePublicate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datePublicate.Size = new System.Drawing.Size(186, 20);
            this.datePublicate.TabIndex = 91;
            // 
            // cbWriter
            // 
            this.cbWriter.Location = new System.Drawing.Point(152, 90);
            this.cbWriter.Name = "cbWriter";
            this.cbWriter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbWriter.Size = new System.Drawing.Size(186, 20);
            this.cbWriter.TabIndex = 90;
            // 
            // cbPublisher
            // 
            this.cbPublisher.Location = new System.Drawing.Point(152, 133);
            this.cbPublisher.Name = "cbPublisher";
            this.cbPublisher.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbPublisher.Size = new System.Drawing.Size(186, 20);
            this.cbPublisher.TabIndex = 90;
            // 
            // cbSubCategory
            // 
            this.cbSubCategory.Location = new System.Drawing.Point(503, 90);
            this.cbSubCategory.Name = "cbSubCategory";
            this.cbSubCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbSubCategory.Size = new System.Drawing.Size(186, 20);
            this.cbSubCategory.TabIndex = 90;
            // 
            // cbCategory
            // 
            this.cbCategory.Location = new System.Drawing.Point(503, 48);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCategory.Size = new System.Drawing.Size(186, 20);
            this.cbCategory.TabIndex = 90;
            // 
            // btnClear
            // 
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(1402, 102);
            this.btnClear.Name = "btnClear";
            this.btnClear.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnClear.Size = new System.Drawing.Size(121, 32);
            this.btnClear.TabIndex = 13;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(1402, 64);
            this.btnSave.Name = "btnSave";
            this.btnSave.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnSave.Size = new System.Drawing.Size(121, 32);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Kaydet";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtStock
            // 
            this.txtStock.Location = new System.Drawing.Point(851, 129);
            this.txtStock.Name = "txtStock";
            this.txtStock.Properties.AllowFocused = false;
            this.txtStock.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtStock.Properties.Mask.EditMask = "d";
            this.txtStock.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtStock.Properties.MaxLength = 3;
            this.txtStock.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtStock.Size = new System.Drawing.Size(186, 22);
            this.txtStock.TabIndex = 9;
            // 
            // txtStatement
            // 
            this.txtStatement.Location = new System.Drawing.Point(1172, 86);
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.Properties.AllowFocused = false;
            this.txtStatement.Properties.AutoHeight = false;
            this.txtStatement.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtStatement.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.Value;
            this.txtStatement.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtStatement.Size = new System.Drawing.Size(186, 65);
            this.txtStatement.TabIndex = 2;
            // 
            // txtISBN
            // 
            this.txtISBN.Location = new System.Drawing.Point(1172, 46);
            this.txtISBN.Name = "txtISBN";
            this.txtISBN.Properties.AllowFocused = false;
            this.txtISBN.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtISBN.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.Value;
            this.txtISBN.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtISBN.Size = new System.Drawing.Size(186, 22);
            this.txtISBN.TabIndex = 2;
            // 
            // txtShelf
            // 
            this.txtShelf.Location = new System.Drawing.Point(851, 46);
            this.txtShelf.Name = "txtShelf";
            this.txtShelf.Properties.AllowFocused = false;
            this.txtShelf.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtShelf.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.Value;
            this.txtShelf.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtShelf.Size = new System.Drawing.Size(186, 22);
            this.txtShelf.TabIndex = 2;
            // 
            // txtBookName
            // 
            this.txtBookName.Location = new System.Drawing.Point(152, 46);
            this.txtBookName.Name = "txtBookName";
            this.txtBookName.Properties.AllowFocused = false;
            this.txtBookName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtBookName.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.Value;
            this.txtBookName.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtBookName.Size = new System.Drawing.Size(186, 22);
            this.txtBookName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(370, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 27;
            this.label3.Text = "Kategori:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(25, 134);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(74, 17);
            this.label16.TabIndex = 83;
            this.label16.Text = "Yayınevi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(718, 134);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "Stok Sayısı:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(370, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Sayfa Sayısı:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(370, 91);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Alt Kategori:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(1070, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 17);
            this.label15.TabIndex = 80;
            this.label15.Text = "Açıklama:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(25, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 17);
            this.label14.TabIndex = 80;
            this.label14.Text = "Kitap Adı:";
            // 
            // txtPage
            // 
            this.txtPage.Location = new System.Drawing.Point(503, 132);
            this.txtPage.Name = "txtPage";
            this.txtPage.Properties.AllowFocused = false;
            this.txtPage.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtPage.Properties.Mask.EditMask = "d";
            this.txtPage.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPage.Properties.MaxLength = 4;
            this.txtPage.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EmptyValue | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txtPage.Size = new System.Drawing.Size(186, 22);
            this.txtPage.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(1071, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 17);
            this.label12.TabIndex = 79;
            this.label12.Text = "ISBN:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(718, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 17);
            this.label11.TabIndex = 79;
            this.label11.Text = "Yayın Tarihi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(25, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 79;
            this.label2.Text = "Yazar Adı:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(718, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 17);
            this.label13.TabIndex = 79;
            this.label13.Text = "Raf Numarası:";
            // 
            // gvBook
            // 
            this.gvBook.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvBook.Appearance.Empty.Options.UseBackColor = true;
            this.gvBook.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.gvBook.Appearance.Row.Options.UseFont = true;
            this.gvBook.Appearance.Row.Options.UseTextOptions = true;
            this.gvBook.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gvBook.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvBook.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression1.Appearance.Options.UseFont = true;
            formatConditionRuleExpression1.Expression = "[Stok] > 2";
            formatConditionRuleExpression1.PredefinedName = "Green Fill";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression2.Appearance.Options.UseFont = true;
            formatConditionRuleExpression2.Expression = "[Stok] = 0";
            formatConditionRuleExpression2.PredefinedName = "Red Fill";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Name = "Format2";
            formatConditionRuleExpression3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            formatConditionRuleExpression3.Appearance.Options.UseFont = true;
            formatConditionRuleExpression3.Expression = "[Stok] > 0 And [Stok] <= 2";
            formatConditionRuleExpression3.PredefinedName = "Yellow Fill, Yellow Text";
            gridFormatRule3.Rule = formatConditionRuleExpression3;
            this.gvBook.FormatRules.Add(gridFormatRule1);
            this.gvBook.FormatRules.Add(gridFormatRule2);
            this.gvBook.FormatRules.Add(gridFormatRule3);
            this.gvBook.GridControl = this.gcBook;
            this.gvBook.Name = "gvBook";
            this.gvBook.OptionsBehavior.Editable = false;
            this.gvBook.OptionsBehavior.ReadOnly = true;
            this.gvBook.OptionsCustomization.AllowFilter = false;
            this.gvBook.OptionsFind.AllowFindPanel = false;
            this.gvBook.OptionsMenu.EnableColumnMenu = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvBook.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvBook.OptionsView.ShowGroupPanel = false;
            this.gvBook.OptionsView.ShowIndicator = false;
            this.gvBook.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvBook.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvBook_RowCellClick);
            // 
            // gcBook
            // 
            this.gcBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcBook.Location = new System.Drawing.Point(12, 6);
            this.gcBook.MainView = this.gvBook;
            this.gcBook.Name = "gcBook";
            this.gcBook.Size = new System.Drawing.Size(1529, 259);
            this.gcBook.TabIndex = 85;
            this.gcBook.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBook});
            this.gcBook.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gcBook_MouseClick);
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gcBook);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 270);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1557, 299);
            this.panelGrid.TabIndex = 93;
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.txtSearch);
            this.panelTop.Controls.Add(this.label10);
            this.panelTop.Controls.Add(this.panel2);
            this.panelTop.Controls.Add(this.gbBook);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1557, 270);
            this.panelTop.TabIndex = 92;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(209, 234);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(259, 20);
            this.txtSearch.TabIndex = 90;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(38, 235);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(165, 17);
            this.label10.TabIndex = 92;
            this.label10.Text = "Kitap / Yazar Adı Ara:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lnkExcel);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.lblClock);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(881, 229);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(660, 35);
            this.panel2.TabIndex = 91;
            // 
            // lnkExcel
            // 
            this.lnkExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lnkExcel.Appearance.Options.UseFont = true;
            this.lnkExcel.Location = new System.Drawing.Point(25, 7);
            this.lnkExcel.Name = "lnkExcel";
            this.lnkExcel.Size = new System.Drawing.Size(38, 17);
            this.lnkExcel.TabIndex = 90;
            this.lnkExcel.Text = "Excel";
            this.lnkExcel.Click += new System.EventHandler(this.lnkExcel_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(348, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Stokta Yok";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(123, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 16);
            this.label18.TabIndex = 21;
            this.label18.Text = "Stokta Var";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(228, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "Stokta Azaldı";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.ForeColor = System.Drawing.Color.LightCoral;
            this.label7.Location = new System.Drawing.Point(333, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 25);
            this.label7.TabIndex = 20;
            this.label7.Text = "•";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label17.ForeColor = System.Drawing.Color.LightBlue;
            this.label17.Location = new System.Drawing.Point(108, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 25);
            this.label17.TabIndex = 20;
            this.label17.Text = "•";
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(471, 7);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(183, 18);
            this.lblClock.TabIndex = 18;
            this.lblClock.Text = "29 Aralık 2020 24:24:24";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.DarkKhaki;
            this.label5.Location = new System.Drawing.Point(213, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 25);
            this.label5.TabIndex = 20;
            this.label5.Text = "•";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // AddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1557, 569);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddBook";
            this.Text = "AddBook";
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.gbBook.ResumeLayout(false);
            this.gbBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datePublicate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datePublicate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbWriter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbPublisher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbSubCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStock.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtISBN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShelf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBookName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBook)).EndInit();
            this.panelGrid.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.GroupBox gbBook;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtStock;
        private DevExpress.XtraEditors.TextEdit txtBookName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.TextEdit txtPage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraGrid.Views.Grid.GridView gvBook;
        private DevExpress.XtraGrid.GridControl gcBook;
        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Timer timer;
        private DevExpress.XtraEditors.ComboBoxEdit cbPublisher;
        private DevExpress.XtraEditors.ComboBoxEdit cbWriter;
        private DevExpress.XtraEditors.ComboBoxEdit cbSubCategory;
        private DevExpress.XtraEditors.ComboBoxEdit cbCategory;
        private DevExpress.XtraEditors.TextEdit txtStatement;
        private DevExpress.XtraEditors.TextEdit txtISBN;
        private DevExpress.XtraEditors.TextEdit txtShelf;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.DateEdit datePublicate;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.HyperlinkLabelControl lnkExcel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
    }
}