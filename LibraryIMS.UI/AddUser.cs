﻿using System;
using System.Data;
using System.Linq;
using LibraryIMS.ORM;
using System.Windows.Forms;
using LibraryService.Common;
using DevExpress.XtraEditors;
using LibraryIMS.Entity.Model;
using LibraryIMS.Common.Enums;
using System.Collections.Generic;

namespace LibraryIMS.UI
{
    public partial class AddUser : Form
    {

        #region Ctor

        public AddUser()
        {
            InitializeComponent();
            BindMemberList();
        }

        #endregion

        #region Field

        // Kullanıcı kayıtlı mı ?
        private bool _isThere;

        // Güncelleme yapılacak üyenin ID bilgisi
        private int _memberID = -1;

        #endregion

        #region Methods

        private void BindMemberList(List<object> listMember = null)
        {
            if (listMember == null) listMember = this.GetMemberList();

            gcMember.DataSource = listMember;
            gvMember.Columns[0].Visible = false;
            gvMember.Columns[8].Visible = false;
            panelGrid.Visible = listMember.Count() > 0;
            lblMessage.Text = string.Format("{0} adet ziyaret listeleniyor.  ", listMember.Count());
        }

        /// <summary>
        /// Tüm üyelerin listesini getir.
        /// </summary>
        private List<object> GetMemberList()
        {
            var list =
            MembersORM.Current.Select().Data.
            OrderBy(x => x.Name).
            Select(x => new
            {
                ID = x.MemberID,
                Kimlik_No = x.TC,
                Ad = x.Name,
                Soyad = x.Surname,
                Kullanıcı_Adı = x.Username,
                Cinsiyet = x.Gender == "" ? null : x.Gender.Substring(0, 1),
                Doğum_Tarihi = x.DateBirth.ToString("D"),
                Telefon = x.Telephone,
                Ban_Durumu = x.BanSituation
            });
            return list.Cast<object>().ToList();
        }

        /// <summary>
        /// Üyenin kayıtlı olup olmadıgını kontrol et.
        /// </summary>
        private void CheckIsThere()
        {
            var list = MembersORM.Current.Select().Data.Where(x => x.TC == txtTC.Text).ToList();

            _isThere = list.Count > 0 ? true : false;
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            Tools.ClearFormControls(this);
            _memberID = -1;
        }

        /// <summary>
        /// Sisteme yeni üye ekler veya günceller.
        /// </summary>
        private void AddMember()
        {
            CheckIsThere();
            if (!Tools.CheckFormControls(gbMember))
            {
                XtraMessageBox.Show("Lütfen tüm alanları doldurunuz!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string DataError;
            bool DataSuccess = false;

            var memberModel = new MembersDTO
            {
                TC = this.txtTC.Text,
                Name = this.txtName.Text,
                Surname = this.txtSurname.Text,
                Username = this.txtUsername.Text,
                Password = this.txtPassword.Text,
                Gender = this.cbGender.Text,
                DateBirth = this.dateBirth.DateTime,
                DateAdded = DateTime.Now,
                Telephone = this.txtTelephone.Text,
                Mail = this.txtMail.Text,
                BanSituation = cbBan.SelectedIndex,
                AccessID = cbAccess.SelectedIndex
            };

            // Yeni üye ekler.
            if (!_isThere)
            {
                var member = MembersORM.Current.Insert(memberModel);
                DataSuccess = member.Data;

                if (!member.IsSuccess)
                {
                    XtraMessageBox.Show("Üye eklenirken bir hata oluştu!\n\n" + member.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                // Kayıt guncellenecek.
                if (_memberID != -1)
                {
                    memberModel.MemberID = _memberID;
                    var member = MembersORM.Current.Update(memberModel);
                    DataSuccess = member.Data;

                    if (!member.IsSuccess)
                    {
                        XtraMessageBox.Show("Üye güncelleme sırasında bir hata oluştu!\n\n" + member.Message, "Hata",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show("Üye sistemde zaten kayıtlı!", "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (DataSuccess)
            {
                XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ClearItems();
                this.BindMemberList();
                DialogResult = DialogResult.OK;
            }
            else
            {
                XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Kimlik kartı numarasına ve isme göre üye ara.
        /// </summary>
        private void SearchMember()
        {
            string searchText = this.txtSearch.Text;

            if (String.IsNullOrEmpty(searchText))
                this.BindMemberList();

            var list =
            MembersORM.Current.Select().Data.
            Where(x => (x.Name.ToLower().Contains(searchText) || x.Surname.ToLower().Contains(searchText) || x.TC.Contains(searchText))).
            OrderBy(x => x.Name).
            Select(x => new
            {
                ID = x.MemberID,
                Kimlik_No = x.TC,
                Ad = x.Name,
                Soyad = x.Surname,
                Kullanıcı_Adı = x.Username,
                Cinsiyet = x.Gender == "" ? null : x.Gender.Substring(0, 1),
                Doğum_Tarihi = x.DateBirth.ToString("D"),
                Telefon = x.Telephone,
                Ban_Durumu = x.BanSituation
            });
            this.BindMemberList(list.Cast<object>().ToList());
        }

        /// <summary>
        /// Gridde seçilen yasaklı ziyaretçi bilgisini getir.
        /// </summary>
        private void GetMemberObject()
        {
            if (this.gvMember.SelectedRowsCount > 0)
            {
                int.TryParse(gvMember.GetRowCellValue(gvMember.FocusedRowHandle, "ID").ToString(), out this._memberID);
                var model = MembersORM.Current.Select().Data.Where(x => x.MemberID == _memberID).ToList();
                if (model.Count > 0)
                {
                    this.txtTC.Text = model[0].TC;
                    this.txtName.Text = model[0].Name;
                    this.txtSurname.Text = model[0].Surname;
                    this.txtUsername.Text = model[0].Username;
                    this.txtPassword.Text = model[0].Password;
                    this.cbGender.SelectedItem = model[0].Gender;
                    this.dateBirth.DateTime = model[0].DateBirth.Date;
                    this.txtTelephone.Text = model[0].Telephone;
                    this.txtMail.Text = model[0].Mail;
                    this.cbAccess.SelectedItem = ((AccessStatus)model[0].AccessID).ToString();
                    this.cbBan.SelectedItem = ((BanStatus)model[0].BanSituation).ToString();
                }
            }
        }

        /// <summary>
        /// Seçilen Kullanıcıyı kaldır.
        /// </summary>
        private void DeleteMember(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Üye kaldırılacaktır. Emin misiniz?", "Soru", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var model = new MembersDTO
                {
                    MemberID = _memberID
                };
                var result = MembersORM.Current.Delete(model);
                if (!result.IsSuccess)
                {
                    XtraMessageBox.Show("Üye kaldırılırken bir hata oluştu!\n\n" + result.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    XtraMessageBox.Show("İşleminiz başarıyla gerçekleştirilmiştir.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.ClearItems();
                    this.BindMemberList();
                }
            }
        }

        #endregion

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtTC.Text.Length != 11) DevExpress.XtraEditors.XtraMessageBox.Show("Kimlik Numarasını kontrol ediniz!", "Hata",
                                         MessageBoxButtons.OK, MessageBoxIcon.Error);

            else if (txtPassword.Text.Length != 8) DevExpress.XtraEditors.XtraMessageBox.Show("Şifre 8 karakterden oluşmalıdır!", "Uyarı",
                                                   MessageBoxButtons.OK, MessageBoxIcon.Error);

            else this.AddMember();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearItems();
        }

        #endregion

        #region Grid

        /// <summary>
        /// Grid üzerindeki kullanıcılara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcMember_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var cm = new ContextMenu();
                cm.MenuItems.Add(new MenuItem("Üyeyi Kaldır", this.DeleteMember));

                int rowIndex = gvMember.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvMember.ClearSelection();
                    gvMember.SelectRow(rowIndex);
                    _memberID = Convert.ToInt32(gvMember.GetRowCellValue(rowIndex, "ID"));
                    cm.Show(gcMember, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        private void gvMember_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            this.GetMemberObject();
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchMember();
        }

        #endregion

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "member.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                gcMember.ExportToXls(sfd.FileName);
            }
        }

        #endregion

    }
}
