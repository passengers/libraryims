﻿
namespace LibraryIMS.UI
{
    partial class PageMain
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageMain));
            this.panelTop = new DevExpress.Utils.Layout.StackPanel();
            this.btnMember = new DevExpress.XtraEditors.SimpleButton();
            this.btnVisitor = new DevExpress.XtraEditors.SimpleButton();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.panelFooter = new DevExpress.Utils.Layout.StackPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelManager = new DevExpress.Utils.Layout.StackPanel();
            this.btnAddVisitor = new DevExpress.XtraEditors.SimpleButton();
            this.btnOldVisitor = new DevExpress.XtraEditors.SimpleButton();
            this.btnForbiddenVisitor = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnMemberInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnBookInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeliveryInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnSafetyInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnPenaltyInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnMClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelMember = new DevExpress.Utils.Layout.StackPanel();
            this.btnArchiveInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnProfileInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnBookList = new DevExpress.XtraEditors.SimpleButton();
            this.btnMessages = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeptInfo = new DevExpress.XtraEditors.SimpleButton();
            this.btnUClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelCenter = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFooter)).BeginInit();
            this.panelFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelManager)).BeginInit();
            this.panelManager.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMember)).BeginInit();
            this.panelMember.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Appearance.BackColor = System.Drawing.Color.MistyRose;
            this.panelTop.Appearance.BackColor2 = System.Drawing.Color.LightBlue;
            this.panelTop.Appearance.Options.UseBackColor = true;
            this.panelTop.Controls.Add(this.btnMember);
            this.panelTop.Controls.Add(this.btnVisitor);
            this.panelTop.Controls.Add(this.btnExit);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1296, 68);
            this.panelTop.TabIndex = 0;
            // 
            // btnMember
            // 
            this.btnMember.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMember.Appearance.Options.UseFont = true;
            this.btnMember.AutoSize = true;
            this.btnMember.AutoWidthInLayoutControl = true;
            this.btnMember.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnMember.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnMember.ImageOptions.SvgImage")));
            this.btnMember.Location = new System.Drawing.Point(3, 16);
            this.btnMember.Name = "btnMember";
            this.btnMember.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnMember.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnMember.Size = new System.Drawing.Size(140, 36);
            this.btnMember.TabIndex = 1;
            this.btnMember.Text = "Kullanıcı Girişi";
            this.btnMember.Click += new System.EventHandler(this.btnMember_Click);
            // 
            // btnVisitor
            // 
            this.btnVisitor.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnVisitor.Appearance.Options.UseFont = true;
            this.btnVisitor.AutoSize = true;
            this.btnVisitor.AutoWidthInLayoutControl = true;
            this.btnVisitor.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnVisitor.ImageOptions.SvgImage")));
            this.btnVisitor.Location = new System.Drawing.Point(149, 16);
            this.btnVisitor.Name = "btnVisitor";
            this.btnVisitor.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnVisitor.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnVisitor.Size = new System.Drawing.Size(139, 36);
            this.btnVisitor.TabIndex = 2;
            this.btnVisitor.Text = "Ziyaretçi Girişi";
            this.btnVisitor.Click += new System.EventHandler(this.btnVisitor_Click);
            // 
            // btnExit
            // 
            this.btnExit.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExit.Appearance.Options.UseFont = true;
            this.btnExit.AutoSize = true;
            this.btnExit.AutoWidthInLayoutControl = true;
            this.btnExit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnExit.ImageOptions.SvgImage")));
            this.btnExit.Location = new System.Drawing.Point(294, 16);
            this.btnExit.Name = "btnExit";
            this.btnExit.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnExit.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnExit.Size = new System.Drawing.Size(77, 36);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Çıkış";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panelFooter
            // 
            this.panelFooter.Appearance.BackColor = System.Drawing.Color.LightBlue;
            this.panelFooter.Appearance.BackColor2 = System.Drawing.Color.MistyRose;
            this.panelFooter.Appearance.Options.UseBackColor = true;
            this.panelFooter.Controls.Add(this.labelControl1);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.LayoutDirection = DevExpress.Utils.Layout.StackPanelLayoutDirection.RightToLeft;
            this.panelFooter.Location = new System.Drawing.Point(0, 653);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(1296, 68);
            this.panelFooter.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.ImageOptions.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("labelControl1.ImageOptions.SvgImage")));
            this.labelControl1.Location = new System.Drawing.Point(1071, 18);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(222, 32);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "          ACAY Bilgi Yönetim Sistemi  ";
            // 
            // panelManager
            // 
            this.panelManager.Appearance.BackColor = System.Drawing.Color.MistyRose;
            this.panelManager.Appearance.BackColor2 = System.Drawing.Color.LightBlue;
            this.panelManager.Appearance.Options.UseBackColor = true;
            this.panelManager.AutoSize = true;
            this.panelManager.Controls.Add(this.btnAddVisitor);
            this.panelManager.Controls.Add(this.btnOldVisitor);
            this.panelManager.Controls.Add(this.btnForbiddenVisitor);
            this.panelManager.Controls.Add(this.btnRefresh);
            this.panelManager.Controls.Add(this.btnMemberInfo);
            this.panelManager.Controls.Add(this.btnBookInfo);
            this.panelManager.Controls.Add(this.btnDeliveryInfo);
            this.panelManager.Controls.Add(this.btnSafetyInfo);
            this.panelManager.Controls.Add(this.btnPenaltyInfo);
            this.panelManager.Controls.Add(this.btnMClose);
            this.panelManager.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelManager.Location = new System.Drawing.Point(0, 68);
            this.panelManager.Name = "panelManager";
            this.panelManager.Size = new System.Drawing.Size(1296, 79);
            this.panelManager.TabIndex = 2;
            this.panelManager.Visible = false;
            // 
            // btnAddVisitor
            // 
            this.btnAddVisitor.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAddVisitor.Appearance.Options.UseFont = true;
            this.btnAddVisitor.AutoSize = true;
            this.btnAddVisitor.AutoWidthInLayoutControl = true;
            this.btnAddVisitor.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnAddVisitor.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnAddVisitor.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnAddVisitor.ImageOptions.SvgImage")));
            this.btnAddVisitor.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnAddVisitor.Location = new System.Drawing.Point(3, 3);
            this.btnAddVisitor.Name = "btnAddVisitor";
            this.btnAddVisitor.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnAddVisitor.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnAddVisitor.Size = new System.Drawing.Size(92, 73);
            this.btnAddVisitor.TabIndex = 2;
            this.btnAddVisitor.Text = "Ziyaret Kayıt";
            this.btnAddVisitor.Click += new System.EventHandler(this.btnAddVisitor_Click);
            // 
            // btnOldVisitor
            // 
            this.btnOldVisitor.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOldVisitor.Appearance.Options.UseFont = true;
            this.btnOldVisitor.AutoSize = true;
            this.btnOldVisitor.AutoWidthInLayoutControl = true;
            this.btnOldVisitor.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnOldVisitor.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnOldVisitor.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnOldVisitor.ImageOptions.SvgImage")));
            this.btnOldVisitor.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnOldVisitor.Location = new System.Drawing.Point(101, 3);
            this.btnOldVisitor.Name = "btnOldVisitor";
            this.btnOldVisitor.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnOldVisitor.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnOldVisitor.Size = new System.Drawing.Size(155, 73);
            this.btnOldVisitor.TabIndex = 3;
            this.btnOldVisitor.Text = "Geçmiş Ziyaret Listesi";
            this.btnOldVisitor.Click += new System.EventHandler(this.btnOldVisitor_Click);
            // 
            // btnForbiddenVisitor
            // 
            this.btnForbiddenVisitor.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnForbiddenVisitor.Appearance.Options.UseFont = true;
            this.btnForbiddenVisitor.AutoSize = true;
            this.btnForbiddenVisitor.AutoWidthInLayoutControl = true;
            this.btnForbiddenVisitor.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnForbiddenVisitor.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnForbiddenVisitor.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnForbiddenVisitor.ImageOptions.SvgImage")));
            this.btnForbiddenVisitor.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnForbiddenVisitor.Location = new System.Drawing.Point(262, 3);
            this.btnForbiddenVisitor.Name = "btnForbiddenVisitor";
            this.btnForbiddenVisitor.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnForbiddenVisitor.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnForbiddenVisitor.Size = new System.Drawing.Size(134, 73);
            this.btnForbiddenVisitor.TabIndex = 5;
            this.btnForbiddenVisitor.Text = "Yasaklı Ziyaretçiler";
            this.btnForbiddenVisitor.Click += new System.EventHandler(this.btnForbiddenVisitor_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.AutoSize = true;
            this.btnRefresh.AutoWidthInLayoutControl = true;
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnRefresh.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnRefresh.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnRefresh.ImageOptions.SvgImage")));
            this.btnRefresh.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnRefresh.Location = new System.Drawing.Point(402, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnRefresh.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnRefresh.Size = new System.Drawing.Size(54, 73);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Yenile";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnMemberInfo
            // 
            this.btnMemberInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMemberInfo.Appearance.Options.UseFont = true;
            this.btnMemberInfo.AutoSize = true;
            this.btnMemberInfo.AutoWidthInLayoutControl = true;
            this.btnMemberInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnMemberInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMemberInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnMemberInfo.ImageOptions.SvgImage")));
            this.btnMemberInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnMemberInfo.Location = new System.Drawing.Point(462, 3);
            this.btnMemberInfo.Name = "btnMemberInfo";
            this.btnMemberInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnMemberInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnMemberInfo.Size = new System.Drawing.Size(96, 73);
            this.btnMemberInfo.TabIndex = 6;
            this.btnMemberInfo.Text = "Üye İşlemleri";
            this.btnMemberInfo.Click += new System.EventHandler(this.btnMemberInfo_Click);
            // 
            // btnBookInfo
            // 
            this.btnBookInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBookInfo.Appearance.Options.UseFont = true;
            this.btnBookInfo.AutoSize = true;
            this.btnBookInfo.AutoWidthInLayoutControl = true;
            this.btnBookInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnBookInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnBookInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnBookInfo.ImageOptions.SvgImage")));
            this.btnBookInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnBookInfo.Location = new System.Drawing.Point(564, 3);
            this.btnBookInfo.Name = "btnBookInfo";
            this.btnBookInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnBookInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBookInfo.Size = new System.Drawing.Size(106, 73);
            this.btnBookInfo.TabIndex = 7;
            this.btnBookInfo.Text = "Kitap İşlemleri";
            this.btnBookInfo.Click += new System.EventHandler(this.btnBookInfo_Click);
            // 
            // btnDeliveryInfo
            // 
            this.btnDeliveryInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDeliveryInfo.Appearance.Options.UseFont = true;
            this.btnDeliveryInfo.AutoSize = true;
            this.btnDeliveryInfo.AutoWidthInLayoutControl = true;
            this.btnDeliveryInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnDeliveryInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDeliveryInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnDeliveryInfo.ImageOptions.SvgImage")));
            this.btnDeliveryInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnDeliveryInfo.Location = new System.Drawing.Point(676, 3);
            this.btnDeliveryInfo.Name = "btnDeliveryInfo";
            this.btnDeliveryInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnDeliveryInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDeliveryInfo.Size = new System.Drawing.Size(115, 73);
            this.btnDeliveryInfo.TabIndex = 9;
            this.btnDeliveryInfo.Text = "Teslim İşlemleri";
            this.btnDeliveryInfo.Click += new System.EventHandler(this.btnDeliveryInfo_Click);
            // 
            // btnSafetyInfo
            // 
            this.btnSafetyInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSafetyInfo.Appearance.Options.UseFont = true;
            this.btnSafetyInfo.AutoSize = true;
            this.btnSafetyInfo.AutoWidthInLayoutControl = true;
            this.btnSafetyInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnSafetyInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnSafetyInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnSafetyInfo.ImageOptions.SvgImage")));
            this.btnSafetyInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnSafetyInfo.Location = new System.Drawing.Point(797, 3);
            this.btnSafetyInfo.Name = "btnSafetyInfo";
            this.btnSafetyInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnSafetyInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnSafetyInfo.Size = new System.Drawing.Size(120, 73);
            this.btnSafetyInfo.TabIndex = 8;
            this.btnSafetyInfo.Text = "Emanet İşlemleri";
            this.btnSafetyInfo.Click += new System.EventHandler(this.btnSafetyInfo_Click);
            // 
            // btnPenaltyInfo
            // 
            this.btnPenaltyInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPenaltyInfo.Appearance.Options.UseFont = true;
            this.btnPenaltyInfo.AutoSize = true;
            this.btnPenaltyInfo.AutoWidthInLayoutControl = true;
            this.btnPenaltyInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnPenaltyInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnPenaltyInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnPenaltyInfo.ImageOptions.SvgImage")));
            this.btnPenaltyInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnPenaltyInfo.Location = new System.Drawing.Point(923, 3);
            this.btnPenaltyInfo.Name = "btnPenaltyInfo";
            this.btnPenaltyInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnPenaltyInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnPenaltyInfo.Size = new System.Drawing.Size(102, 73);
            this.btnPenaltyInfo.TabIndex = 10;
            this.btnPenaltyInfo.Text = "Ceza İşlemleri";
            this.btnPenaltyInfo.Click += new System.EventHandler(this.btnPenaltyInfo_Click);
            // 
            // btnMClose
            // 
            this.btnMClose.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMClose.Appearance.Options.UseFont = true;
            this.btnMClose.AutoSize = true;
            this.btnMClose.AutoWidthInLayoutControl = true;
            this.btnMClose.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnMClose.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMClose.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnMClose.ImageOptions.SvgImage")));
            this.btnMClose.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnMClose.Location = new System.Drawing.Point(1031, 3);
            this.btnMClose.Name = "btnMClose";
            this.btnMClose.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnMClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnMClose.Size = new System.Drawing.Size(54, 73);
            this.btnMClose.TabIndex = 11;
            this.btnMClose.Text = "Çıkış";
            this.btnMClose.Click += new System.EventHandler(this.btnMClose_Click);
            // 
            // panelMember
            // 
            this.panelMember.Appearance.BackColor = System.Drawing.Color.MistyRose;
            this.panelMember.Appearance.BackColor2 = System.Drawing.Color.LightBlue;
            this.panelMember.Appearance.Options.UseBackColor = true;
            this.panelMember.AutoSize = true;
            this.panelMember.Controls.Add(this.btnArchiveInfo);
            this.panelMember.Controls.Add(this.btnProfileInfo);
            this.panelMember.Controls.Add(this.btnBookList);
            this.panelMember.Controls.Add(this.btnMessages);
            this.panelMember.Controls.Add(this.btnDeptInfo);
            this.panelMember.Controls.Add(this.btnUClose);
            this.panelMember.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMember.Location = new System.Drawing.Point(0, 147);
            this.panelMember.Name = "panelMember";
            this.panelMember.Size = new System.Drawing.Size(1296, 79);
            this.panelMember.TabIndex = 3;
            this.panelMember.Visible = false;
            // 
            // btnArchiveInfo
            // 
            this.btnArchiveInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnArchiveInfo.Appearance.Options.UseFont = true;
            this.btnArchiveInfo.AutoSize = true;
            this.btnArchiveInfo.AutoWidthInLayoutControl = true;
            this.btnArchiveInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnArchiveInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnArchiveInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnArchiveInfo.ImageOptions.SvgImage")));
            this.btnArchiveInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnArchiveInfo.Location = new System.Drawing.Point(3, 3);
            this.btnArchiveInfo.Name = "btnArchiveInfo";
            this.btnArchiveInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnArchiveInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnArchiveInfo.Size = new System.Drawing.Size(54, 73);
            this.btnArchiveInfo.TabIndex = 2;
            this.btnArchiveInfo.Text = "Arşiv";
            this.btnArchiveInfo.Click += new System.EventHandler(this.btnArchiveInfo_Click);
            // 
            // btnProfileInfo
            // 
            this.btnProfileInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnProfileInfo.Appearance.Options.UseFont = true;
            this.btnProfileInfo.AutoSize = true;
            this.btnProfileInfo.AutoWidthInLayoutControl = true;
            this.btnProfileInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnProfileInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnProfileInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnProfileInfo.ImageOptions.SvgImage")));
            this.btnProfileInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnProfileInfo.Location = new System.Drawing.Point(63, 3);
            this.btnProfileInfo.Name = "btnProfileInfo";
            this.btnProfileInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnProfileInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnProfileInfo.Size = new System.Drawing.Size(105, 73);
            this.btnProfileInfo.TabIndex = 3;
            this.btnProfileInfo.Text = "Profil İşlemleri";
            this.btnProfileInfo.Click += new System.EventHandler(this.btnProfileInfo_Click);
            // 
            // btnBookList
            // 
            this.btnBookList.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBookList.Appearance.Options.UseFont = true;
            this.btnBookList.AutoSize = true;
            this.btnBookList.AutoWidthInLayoutControl = true;
            this.btnBookList.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnBookList.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnBookList.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnBookList.ImageOptions.SvgImage")));
            this.btnBookList.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnBookList.Location = new System.Drawing.Point(174, 3);
            this.btnBookList.Name = "btnBookList";
            this.btnBookList.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnBookList.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBookList.Size = new System.Drawing.Size(93, 73);
            this.btnBookList.TabIndex = 4;
            this.btnBookList.Text = "Kitap Listem";
            this.btnBookList.Click += new System.EventHandler(this.btnBookList_Click);
            // 
            // btnMessages
            // 
            this.btnMessages.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMessages.Appearance.Options.UseFont = true;
            this.btnMessages.AutoSize = true;
            this.btnMessages.AutoWidthInLayoutControl = true;
            this.btnMessages.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnMessages.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnMessages.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnMessages.ImageOptions.SvgImage")));
            this.btnMessages.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnMessages.Location = new System.Drawing.Point(273, 3);
            this.btnMessages.Name = "btnMessages";
            this.btnMessages.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnMessages.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnMessages.Size = new System.Drawing.Size(65, 73);
            this.btnMessages.TabIndex = 5;
            this.btnMessages.Text = "Mesajlar";
            this.btnMessages.Click += new System.EventHandler(this.btnMessages_Click);
            // 
            // btnDeptInfo
            // 
            this.btnDeptInfo.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDeptInfo.Appearance.Options.UseFont = true;
            this.btnDeptInfo.AutoSize = true;
            this.btnDeptInfo.AutoWidthInLayoutControl = true;
            this.btnDeptInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnDeptInfo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnDeptInfo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnDeptInfo.ImageOptions.SvgImage")));
            this.btnDeptInfo.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnDeptInfo.Location = new System.Drawing.Point(344, 3);
            this.btnDeptInfo.Name = "btnDeptInfo";
            this.btnDeptInfo.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnDeptInfo.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDeptInfo.Size = new System.Drawing.Size(102, 73);
            this.btnDeptInfo.TabIndex = 6;
            this.btnDeptInfo.Text = "Borç İşlemleri";
            this.btnDeptInfo.Click += new System.EventHandler(this.btnDeptInfo_Click);
            // 
            // btnUClose
            // 
            this.btnUClose.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUClose.Appearance.Options.UseFont = true;
            this.btnUClose.AutoSize = true;
            this.btnUClose.AutoWidthInLayoutControl = true;
            this.btnUClose.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnUClose.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnUClose.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnUClose.ImageOptions.SvgImage")));
            this.btnUClose.ImageOptions.SvgImageSize = new System.Drawing.Size(48, 48);
            this.btnUClose.Location = new System.Drawing.Point(452, 3);
            this.btnUClose.Name = "btnUClose";
            this.btnUClose.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.btnUClose.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnUClose.Size = new System.Drawing.Size(54, 73);
            this.btnUClose.TabIndex = 11;
            this.btnUClose.Text = "Çıkış";
            this.btnUClose.Click += new System.EventHandler(this.btnUClose_Click);
            // 
            // panelCenter
            // 
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCenter.Location = new System.Drawing.Point(0, 226);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(1296, 427);
            this.panelCenter.TabIndex = 5;
            // 
            // PageMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1296, 721);
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.panelMember);
            this.Controls.Add(this.panelManager);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Name = "PageMain";
            this.Text = "CAK Bilgi Yöentim Sistemi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PageMain_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelFooter)).EndInit();
            this.panelFooter.ResumeLayout(false);
            this.panelFooter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelManager)).EndInit();
            this.panelManager.ResumeLayout(false);
            this.panelManager.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelMember)).EndInit();
            this.panelMember.ResumeLayout(false);
            this.panelMember.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.Layout.StackPanel panelTop;
        private DevExpress.Utils.Layout.StackPanel panelFooter;
        private DevExpress.XtraEditors.SimpleButton btnMember;
        private DevExpress.XtraEditors.SimpleButton btnVisitor;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.Utils.Layout.StackPanel panelManager;
        private DevExpress.XtraEditors.SimpleButton btnAddVisitor;
        private DevExpress.XtraEditors.SimpleButton btnOldVisitor;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnForbiddenVisitor;
        private DevExpress.XtraEditors.SimpleButton btnMemberInfo;
        private DevExpress.XtraEditors.SimpleButton btnBookInfo;
        private DevExpress.XtraEditors.SimpleButton btnSafetyInfo;
        private DevExpress.XtraEditors.SimpleButton btnDeliveryInfo;
        private DevExpress.XtraEditors.SimpleButton btnPenaltyInfo;
        private DevExpress.XtraEditors.SimpleButton btnMClose;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.Utils.Layout.StackPanel panelMember;
        private DevExpress.XtraEditors.SimpleButton btnArchiveInfo;
        private DevExpress.XtraEditors.SimpleButton btnProfileInfo;
        private DevExpress.XtraEditors.SimpleButton btnBookList;
        private DevExpress.XtraEditors.SimpleButton btnMessages;
        private DevExpress.XtraEditors.SimpleButton btnDeptInfo;
        private DevExpress.XtraEditors.SimpleButton btnUClose;
        private System.Windows.Forms.Panel panelCenter;
    }
}

