﻿using LibraryIMS.Common.Enums;
using LibraryIMS.Entity.Model;
using LibraryIMS.ORM;
using LibraryIMS.ORM.ORMRepo;
using LibraryService.Common;
using System;
using System.Linq;
using System.Windows.Forms;

namespace LibraryIMS.UI
{
    public partial class PageLogin : Form
    {

        #region Ctor

        public PageLogin()
        {
            InitializeComponent();
        }

        #endregion

        #region Field

        private bool _isConnected { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Kullanıcının giriş yetkinlikleri kontrol edilir ve ilgili değer atamaları yapılır.
        /// </summary>
        private void AccessControl()
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
                      
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Kullanıcı adı ya da şifre boş bırakılamaz.", "Uyarı",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                _isConnected = false;
                return;
            }

            var get = MembersORM.Current.Select().Data.First(x=> x.Username==username);
            bool _isRegistered = get.Username == username;
            if (!_isRegistered)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Kullanıcı adınız sistemde tanımlı değil.", "Uyarı",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                _isConnected = false;
                return;
            }

            /// <summary>
            /// Engel durumuna göre sisteme girmesine izin verilmez ve neden bilgisi getirilir.
            /// </summary>
            BanStatus status = (BanStatus)get.BanSituation;
            string reason = status.ToString();
            bool _banned = get.BanSituation == (int)BanStatus.Stable;
            if (!_banned)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Sisteme giriş yapmanız yasaklanmıştır!\nYasak Nedeni: " + reason, "Uyarı",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                _isConnected = false;
                return;
            }

            bool _correct = get.Username == username && get.Password == password;
            if (!_correct)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Kullanıcı adı ya da şifre hatalı.", "Uyarı",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                _isConnected = false;
                return;
            }

            /// <summary>
            /// Kullanıcının bazı değerleri Result sınıfına gönderilmek üzere değişkenlere aktarılır.
            /// </summary>
            _isConnected = true;
            Tools.IsConnected = _isConnected;
            Tools.AccessLevel = get.AccessID;
            Tools.Identity = get.MemberID;

            /// <summary>
            /// Kullanıcının giriş saati tabloya aktarılır.
            /// </summary>
            Movements _movements = new Movements();
            _movements.VisitorTC = get.TC;
            _movements.NameSurname = get.Name + " " + get.Surname;
            _movements.DateStart = DateTime.Now;
            _movements.DateEnd = DateTime.Now;
            MovementsORM.Current.Insert(_movements);
            DialogResult = DialogResult.OK;
        }

        #endregion

        #region Buttons

        private void btnLogin_Click(object sender, EventArgs e)
        {
            AccessControl();
        }

        #endregion
    }
}
