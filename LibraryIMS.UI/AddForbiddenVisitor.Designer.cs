﻿
namespace LibraryIMS.UI
{
    partial class AddForbiddenVisitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddForbiddenVisitor));
            this.panelGrid = new System.Windows.Forms.Panel();
            this.gcForbidden = new DevExpress.XtraGrid.GridControl();
            this.gvForbidden = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelTop = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lnkExcel = new DevExpress.XtraEditors.HyperlinkLabelControl();
            this.label4 = new System.Windows.Forms.Label();
            this.lblClock = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gbForbiddenVisitor = new System.Windows.Forms.GroupBox();
            this.dateForbidden = new DevExpress.XtraEditors.DateEdit();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtTC = new DevExpress.XtraEditors.TextEdit();
            this.txtSurname = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcForbidden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvForbidden)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbForbiddenVisitor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateForbidden.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateForbidden.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSurname.Properties)).BeginInit();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gcForbidden);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 0);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1296, 569);
            this.panelGrid.TabIndex = 88;
            // 
            // gcForbidden
            // 
            this.gcForbidden.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcForbidden.Location = new System.Drawing.Point(12, 242);
            this.gcForbidden.MainView = this.gvForbidden;
            this.gcForbidden.Name = "gcForbidden";
            this.gcForbidden.Size = new System.Drawing.Size(1268, 293);
            this.gcForbidden.TabIndex = 85;
            this.gcForbidden.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvForbidden});
            this.gcForbidden.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gcForbidden_MouseClick);
            // 
            // gvForbidden
            // 
            this.gvForbidden.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvForbidden.Appearance.Empty.Options.UseBackColor = true;
            this.gvForbidden.Appearance.Row.BackColor = System.Drawing.Color.LightCoral;
            this.gvForbidden.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gvForbidden.Appearance.Row.Options.UseBackColor = true;
            this.gvForbidden.Appearance.Row.Options.UseFont = true;
            this.gvForbidden.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvForbidden.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gvForbidden.GridControl = this.gcForbidden;
            this.gvForbidden.Name = "gvForbidden";
            this.gvForbidden.OptionsBehavior.Editable = false;
            this.gvForbidden.OptionsBehavior.ReadOnly = true;
            this.gvForbidden.OptionsCustomization.AllowFilter = false;
            this.gvForbidden.OptionsFind.AllowFindPanel = false;
            this.gvForbidden.OptionsMenu.EnableColumnMenu = false;
            this.gvForbidden.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvForbidden.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvForbidden.OptionsView.ShowGroupPanel = false;
            this.gvForbidden.OptionsView.ShowIndicator = false;
            this.gvForbidden.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvForbidden.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvForbidden_RowCellClick);
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.txtSearch);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.panel2);
            this.panelTop.Controls.Add(this.gbForbiddenVisitor);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1296, 236);
            this.panelTop.TabIndex = 89;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(210, 207);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(186, 20);
            this.txtSearch.TabIndex = 26;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(38, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 17);
            this.label3.TabIndex = 27;
            this.label3.Text = "Ad / Kimlik No Ara:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lnkExcel);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.lblClock);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(840, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(440, 29);
            this.panel2.TabIndex = 25;
            // 
            // lnkExcel
            // 
            this.lnkExcel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.lnkExcel.Appearance.Options.UseFont = true;
            this.lnkExcel.Location = new System.Drawing.Point(3, 5);
            this.lnkExcel.Name = "lnkExcel";
            this.lnkExcel.Size = new System.Drawing.Size(38, 17);
            this.lnkExcel.TabIndex = 28;
            this.lnkExcel.Text = "Excel";
            this.lnkExcel.Click += new System.EventHandler(this.lnkExcel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(92, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 16);
            this.label4.TabIndex = 21;
            this.label4.Text = "Yasaklı Ziyaretçi";
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(241, 5);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(183, 18);
            this.lblClock.TabIndex = 18;
            this.lblClock.Text = "29 Aralık 2020 24:24:24";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.IndianRed;
            this.label2.Location = new System.Drawing.Point(77, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 25);
            this.label2.TabIndex = 20;
            this.label2.Text = "•";
            // 
            // gbForbiddenVisitor
            // 
            this.gbForbiddenVisitor.Controls.Add(this.dateForbidden);
            this.gbForbiddenVisitor.Controls.Add(this.btnClear);
            this.gbForbiddenVisitor.Controls.Add(this.btnSave);
            this.gbForbiddenVisitor.Controls.Add(this.txtName);
            this.gbForbiddenVisitor.Controls.Add(this.txtTC);
            this.gbForbiddenVisitor.Controls.Add(this.txtSurname);
            this.gbForbiddenVisitor.Controls.Add(this.label15);
            this.gbForbiddenVisitor.Controls.Add(this.label11);
            this.gbForbiddenVisitor.Controls.Add(this.label14);
            this.gbForbiddenVisitor.Controls.Add(this.label13);
            this.gbForbiddenVisitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbForbiddenVisitor.Location = new System.Drawing.Point(12, 17);
            this.gbForbiddenVisitor.Name = "gbForbiddenVisitor";
            this.gbForbiddenVisitor.Size = new System.Drawing.Size(1072, 154);
            this.gbForbiddenVisitor.TabIndex = 2;
            this.gbForbiddenVisitor.TabStop = false;
            this.gbForbiddenVisitor.Text = "Yasaklı Ziyaretçi Kimlik Bilgileri";
            // 
            // dateForbidden
            // 
            this.dateForbidden.EditValue = null;
            this.dateForbidden.Enabled = false;
            this.dateForbidden.Location = new System.Drawing.Point(198, 105);
            this.dateForbidden.Name = "dateForbidden";
            this.dateForbidden.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateForbidden.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateForbidden.Properties.Mask.EditMask = "G";
            this.dateForbidden.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateForbidden.Size = new System.Drawing.Size(186, 20);
            this.dateForbidden.TabIndex = 8;
            // 
            // btnClear
            // 
            this.btnClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.ImageOptions.Image")));
            this.btnClear.Location = new System.Drawing.Point(935, 99);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(121, 32);
            this.btnClear.TabIndex = 6;
            this.btnClear.Text = "Temizle";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(935, 54);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(121, 32);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Kaydet";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(658, 59);
            this.txtName.Name = "txtName";
            this.txtName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtName.Properties.NullValuePrompt = "Ad";
            this.txtName.Size = new System.Drawing.Size(186, 22);
            this.txtName.TabIndex = 2;
            // 
            // txtTC
            // 
            this.txtTC.Location = new System.Drawing.Point(198, 59);
            this.txtTC.Name = "txtTC";
            this.txtTC.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtTC.Properties.Mask.EditMask = "\\d{11}";
            this.txtTC.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTC.Properties.Mask.ShowPlaceHolders = false;
            this.txtTC.Properties.NullValuePrompt = "Kimlik No";
            this.txtTC.Size = new System.Drawing.Size(186, 22);
            this.txtTC.TabIndex = 1;
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(658, 104);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtSurname.Properties.NullValuePrompt = "Soyad";
            this.txtSurname.Size = new System.Drawing.Size(186, 22);
            this.txtSurname.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(26, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 17);
            this.label15.TabIndex = 81;
            this.label15.Text = "TC Kimlik No:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(26, 108);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "Yasaklandığı Tarihi:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(486, 61);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 17);
            this.label14.TabIndex = 80;
            this.label14.Text = "Ziyaretçi Adı:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(486, 106);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(130, 17);
            this.label13.TabIndex = 79;
            this.label13.Text = "Ziyaretçi Soyadı:";
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.lblMessage);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 541);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1296, 28);
            this.panelBottom.TabIndex = 90;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMessage.Location = new System.Drawing.Point(1296, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 18);
            this.lblMessage.TabIndex = 1;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // AddForbiddenVisitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1296, 569);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddForbiddenVisitor";
            this.Text = "AddForbiddenVisitor";
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcForbidden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvForbidden)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbForbiddenVisitor.ResumeLayout(false);
            this.gbForbiddenVisitor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateForbidden.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateForbidden.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSurname.Properties)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelGrid;
        private DevExpress.XtraGrid.GridControl gcForbidden;
        private DevExpress.XtraGrid.Views.Grid.GridView gvForbidden;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.GroupBox gbForbiddenVisitor;
        private DevExpress.XtraEditors.DateEdit dateForbidden;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtTC;
        private DevExpress.XtraEditors.TextEdit txtSurname;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.HyperlinkLabelControl lnkExcel;
    }
}