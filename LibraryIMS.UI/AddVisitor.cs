﻿using System;
using System.Data;
using System.Linq;
using LibraryIMS.ORM;
using System.Windows.Forms;
using LibraryService.Common;
using LibraryIMS.ORM.ORMRepo;
using DevExpress.XtraEditors;
using LibraryIMS.Entity.Model;
using System.Collections.Generic;

namespace LibraryIMS.UI
{
    public partial class AddVisitor : Form
    {

        #region Ctor

        public AddVisitor(int guID = -1)
        {
            InitializeComponent();
            BindActiveList();
            var member = MembersORM.Current.Select();
            _addingPerson = member.Data.Where(a => a.MemberID == Tools.Identity).Any() ? member.Data.First().TC : null;
            _guID = guID;
            dateStart.Text = DateTime.Now.ToString("MM.dd.yyyy HH:mm:ss");
            dateEnd.Text = DateTime.Now.ToString("MM.dd.yyyy HH:mm:ss");
            if (_guID == -1)
            {
                btnGiveUp.Name = "Temizle";
            }
            /// <summary>
            /// Ziyaret kaydı grid üzerinden seçilmiş ise form elemanlarını dolduracak.
            /// </summary>
            else
            {
                var info = MovementsORM.Current.Select().Data.Where(x => x.ID == _guID).ToList();
                var visit = VisitsORM.Current.Select().Data.Where(x => x.TC == info[0].VisitorTC).ToList();
                if (info.Count > 0 && visit.Count > 0)
                {
                    txtName.Text = visit[0].Name;
                    txtSurname.Text = visit[0].Surname;
                    txtTelephone.Text = visit[0].Telephone;
                    txtTC.Text = visit[0].TC;
                    txtTC.Enabled = false;
                }
            }
        }

        #endregion

        #region Field

        // Kullanıcı yasaklı mı ?
        private bool _isForbidden;

        // Kullanıcı kayıtlı mı ?
        private bool _isThere;

        // Ekleyen kullanıcı
        private string _addingPerson;

        // Güncelleme yapılacak kullanıcın tablodaki ID bilgisi
        private int _guID = -1;

        #endregion

        #region Methods

        private void BindActiveList(List<object> listMovement = null)
        {
            if (listMovement == null) listMovement = this.GetActiveList();

            gcActive.DataSource = listMovement;
            gvActive.Columns[0].Visible = false;
            panelGrid.Visible = listMovement.Count() > 0;
        }

        /// <summary>
        /// Aktif ziyaretçilerin listesini getir.
        /// </summary>
        private List<object> GetActiveList()
        {
            var actList =
            MovementsORM.Current.Select().Data.
            Where(x => x.Status == false && x.IsVisitor == true).
            OrderByDescending(x => x.DateStart).
            Select(x => new { ID = x.ID, TC_No = x.VisitorTC, Ad_Soyad = x.NameSurname, Giriş_Saati = x.DateStart.ToString("dd MMMM HH:mm:ss") });

            return actList.Cast<object>().ToList();
        }

        /// <summary>
        /// Ziyaretçinin yasaklı listede olup olmadıgını kontrol et.
        /// </summary>
        private void ForbiddenControl()
        {
            var list = ForbiddenVisitorORM.Current.Select().Data.Where(x => x.VisitorTC == txtTC.Text.Replace("\r\n", string.Empty)).ToList();

            _isForbidden = list.Count > 0 ? true : false;
        }

        /// <summary>
        /// Ziyaretçinin kayıtlı olup olmadıgını kontrol et.
        /// </summary>
        private void CheckIsThere()
        {
            var list = VisitsORM.Current.Select().Data.Where(x => x.TC == txtTC.Text).ToList();

            _isThere = list.Count > 0 ? true : false;
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            Tools.ClearFormControls(this);
            _guID = -1;
        }

        /// <summary>
        /// Sisteme yeni ziyaretci ekler veya günceller.
        /// </summary>
        private void AddVisit()
        {
            CheckIsThere();
            if (!Tools.CheckFormControls(gbVisitor))
            {
                XtraMessageBox.Show("Lütfen tüm alanları doldurunuz!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            bool DataSuccess = false;
            string DataError;

            var isIn = MovementsORM.Current.Select().Data.Where(x => x.VisitorTC == txtTC.Text && x.Status == false).Any();
            if (isIn && _guID == -1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Ziyaretçi sistemde kayıtlı ve aktif durumda!.", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_isForbidden)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Yasaklı ziyaretçi giriş yapamaz!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var visitModel = new Visitors
            {
                TC = this.txtTC.Text,
                Name = this.txtName.Text,
                Surname = this.txtSurname.Text,
                Telephone = this.txtTelephone.Text,
                AddingPerson = _addingPerson
            };

            // Yeni ziyaretçi ekler.
            if (!_isThere)
            {
                var visitor = VisitsORM.Current.Insert(visitModel);

                if (!visitor.IsSuccess)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Yeni ziyaretçi eklenirken bir hata oluştu!\n\n" + visitor.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            var movementModel = new Movements
            {
                VisitorTC = this.txtTC.Text,
                NameSurname = this.txtName.Text + " " + this.txtSurname.Text,
                DateStart = DateTime.Now,
                DateEnd = DateTime.Now,
                IsVisitor = true
            };

            // Yeni hareket ekler.
            if (_guID == -1)
            {
                var movement = MovementsORM.Current.Insert(movementModel);
                if (!movement.IsSuccess)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Yeni hareket eklenirken bir hata oluştu!\n\n" + movement.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                DataSuccess = movement.Data;
                DataError = movement.Message;
            }
            // Kayıt guncellenecek.
            else
            {
                movementModel.ID = _guID;

                var movement = MovementsORM.Current.Update(movementModel);
                if (!movement.IsSuccess)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Hareketler tablosunda güncelleme sırasında bir hata oluştu!\n\n" + movement.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                visitModel.VisitorID = VisitsORM.Current.Select().Data.First(x => x.TC == txtTC.Text).VisitorID;
                visitModel.Name = txtName.Text;
                visitModel.Surname = txtSurname.Text;
                visitModel.Telephone = txtTelephone.Text;
                visitModel.AddingPerson = _addingPerson;

                var visitor = VisitsORM.Current.Update(visitModel);
                if (!visitor.IsSuccess)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Ziyaretçi tablosunda güncelleme sırasında bir hata oluştu!\n\n" + visitor.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                DataSuccess = movement.Data;
            }

            if (DataSuccess)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ClearItems();
                this.BindActiveList();
                DialogResult = DialogResult.OK;
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// TC No'ya göre ziyaretçi bilgilerini getir.
        /// </summary>
        private void GetVisitorInfo()
        {
            var isIn = MovementsORM.Current.Select().Data.Where(x => x.VisitorTC == txtTC.Text && x.Status == false).Any();
            if (isIn)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Ziyaretçi sistemde aktif durumda.!", "Uyarı",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (_isForbidden)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Sorgulanan kişi yasaklı ziyaretçiler arasındadır!", "Uyarı",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (!string.IsNullOrEmpty(txtTC.Text))
            {
                string identityNumber = txtTC.Text;

                var list = VisitsORM.Current.Select().Data.Where(x => x.TC == identityNumber).ToList();

                if (list.Count > 0)
                {
                    txtName.Text = list[0].Name;
                    txtSurname.Text = list[0].Surname; 
                    txtTelephone.Text = list[0].Telephone;
                }
                else                
                    DevExpress.XtraEditors.XtraMessageBox.Show("Sorgulama sonucu kayıt bulunamamıştır!", "Uyarı",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                
            }
            else            
                DevExpress.XtraEditors.XtraMessageBox.Show("TC Kimlik No alanı boş!", "Uyarı",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        #endregion

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.AddVisit();
        }

        private void btnGiveUp_Click(object sender, EventArgs e)
        {
            if (_guID == -1)
            {
                this.ClearItems();
            }
            else this.Close();
        }

        private void btnService_Click(object sender, EventArgs e)
        {
            this.GetVisitorInfo();
        }

        #endregion

        #region Events

        private void txtTC_Leave(object sender, EventArgs e)
        {
            this.ForbiddenControl();
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion

    }
}
