﻿using DevExpress.XtraEditors;
using LibraryIMS.Common.Enums;
using LibraryIMS.Entity.Model;
using LibraryIMS.ORM;
using LibraryIMS.ORM.ORMRepo;
using LibraryService.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryIMS.UI
{
    public partial class AddForbiddenVisitor : Form
    {

        #region Field

        private int forbiddenId = -1;

        #endregion

        #region Ctor

        public AddForbiddenVisitor()
        {
            InitializeComponent();
            BindForbiddenVisitorList();
            dateForbidden.Text = DateTime.Now.ToString("MM.dd.yyyy HH:mm:ss");
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metoda gönderilen listeyi gride bas.
        /// </summary>
        private void BindForbiddenVisitorList(List<object> listForbidden = null)
        {
            if (listForbidden == null) listForbidden = this.GetForbiddenList();
            gcForbidden.DataSource = listForbidden;
            gvForbidden.Columns[0].Visible = false;
            gvForbidden.Columns[3].Visible = false;
            gvForbidden.Columns[5].Visible = false;
            lblMessage.Text = string.Format("{0} adet ziyaret listeleniyor.  ", listForbidden.Count());
        }

        /// <summary>
        /// Yasaklı ziyaretçilerin listesini getirir.
        /// </summary>
        private List<object> GetForbiddenList()
        {
            var list =
            ForbiddenVisitorORM.Current.Select().Data.OrderByDescending(x => x.DateBlock);
            return list.Cast<object>().ToList();
        }

        /// <summary>
        /// Db'ye yasaklı kişi kaydı ekler.
        /// </summary>
        private void AddForbiddenVisitorModel()
        {
            if (!Tools.CheckFormControls(gbForbiddenVisitor))
            {
                XtraMessageBox.Show("Lütfen tüm alanları doldurunuz!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string error;
            bool Data = false;

            var model = new ForbiddenVisitor
            {
                VisitorTC = txtTC.Text,
                NameSurname = txtName.Text + " " + txtSurname.Text,
                BlockerTC = MembersORM.Current.Select().Data.First(x => x.MemberID == Tools.Identity).TC,
                DateBlock = dateForbidden.DateTime,
                BanSituation = (int)BanStatus.EntryBan
            };

            if (forbiddenId == -1)
            {
                var forbidden = ForbiddenVisitorORM.Current.Insert(model);
                error = forbidden.Message;
                Data = forbidden.Data;
            }

            else
            {
                model.ID = forbiddenId;
                var forbidden = ForbiddenVisitorORM.Current.Update(model);
                error = forbidden.Message;
                Data = forbidden.Data;
            }


            if (Data)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.ClearFormItems();
                this.BindForbiddenVisitorList();
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!\n\n" + error, "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Gridde seçilen yasaklı ziyaretçi bilgisini getir.
        /// </summary>
        private void GetForbiddenVisitorObject()
        {
            if (this.gvForbidden.SelectedRowsCount > 0)
            {
                int.TryParse(gvForbidden.GetRowCellValue(gvForbidden.FocusedRowHandle, "ID").ToString(), out this.forbiddenId);
                var model = ForbiddenVisitorORM.Current.Select().Data.Where(x => x.ID == forbiddenId).ToList();
                if (model.Count > 0)
                {
                    var getNS = model[0].NameSurname.Split(' ');
                    this.txtTC.Text = model[0].VisitorTC;
                    this.txtName.Text = getNS[0];
                    this.txtSurname.Text = getNS[1];
                }
            }
        }

        /// <summary>
        /// Ziyaretçiyi yasaklı listesinden kaldırır.
        /// </summary>
        private void ForbiddenVisitor(object sender, EventArgs e)
        {
            var result = DevExpress.XtraEditors.XtraMessageBox.Show("Ziyaretçi yasaklı listesinden kaldırılacak. Emin misiniz?", "Uyarı",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                var model = new ForbiddenVisitor
                {
                    ID = forbiddenId
                };
                var forbidden = ForbiddenVisitorORM.Current.Delete(model);
                string error = forbidden.Message;
                bool Data = forbidden.Data;

                if (Data)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.ClearFormItems();
                    this.BindForbiddenVisitorList();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!\n\n" + error, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Kimlik kartı numarasına ve isme göre yasaklı ziyaretçi ara.
        /// </summary>
        private void SearchForbiddenVisitor()
        {
            string searchText = this.txtSearch.Text;

            if (String.IsNullOrEmpty(searchText))
                this.BindForbiddenVisitorList();

            var list =
            ForbiddenVisitorORM.Current.Select().Data.
            Where(x => (x.NameSurname.ToLower().Contains(searchText) || x.VisitorTC.Contains(searchText))).
            OrderByDescending(x => x.DateBlock);

            this.BindForbiddenVisitorList(list.Cast<object>().ToList());
        }

        /// <summary>
        /// Kayıt işlemi bittkten soona form elemanlarını temizle.
        /// </summary>
        private void ClearFormItems()
        {
            Tools.ClearFormControls(this);
            forbiddenId = -1;
        }

        #endregion

        #region Buttons

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(txtTC.Text) && txtTC.Text.Length == 11 && !txtTC.Text.StartsWith("0")))
                this.AddForbiddenVisitorModel();
            else
                DevExpress.XtraEditors.XtraMessageBox.Show("Kimlik No alanını kontrol ediniz!", "Hata",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearFormItems();
        }

        #endregion

        #region Grid

        private void gvForbidden_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            this.GetForbiddenVisitorObject();
        }

        /// <summary>
        /// Grid üzerindeki kullanıcılara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcForbidden_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var cm = new ContextMenu();
                cm.MenuItems.Add(new MenuItem("Yasaklı Listesinden Kaldır", this.ForbiddenVisitor));

                int rowIndex = gvForbidden.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvForbidden.ClearSelection();
                    gvForbidden.SelectRow(rowIndex);
                    forbiddenId = Convert.ToInt32(gvForbidden.GetRowCellValue(rowIndex, "ID"));
                    cm.Show(gcForbidden, new Point(e.X, e.Y));
                }
            }
        }

        #endregion

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchForbiddenVisitor();
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "forbiddenVisitor.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                gcForbidden.ExportToXls(sfd.FileName);
            }
        }

        #endregion

    }
}
