﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using LibraryService.Common;
using LibraryIMS.ORM.ORMRepo;
using System.Collections.Generic;

namespace LibraryIMS.UI
{
    public partial class VisitHistory : Form
    {

        #region Field

        private string namesurname = string.Empty;
        private string identity = string.Empty;

        #endregion

        #region Ctor

        public VisitHistory()
        {
            InitializeComponent();
            BindHistoryList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metoda gönderilen listeyi gride bas.
        /// </summary>
        private void BindHistoryList(List<object> listMovement = null)
        {
            if (listMovement == null) listMovement = this.GetAllHistory();

            gcHistory.DataSource = listMovement;
            gvHistory.Columns[4].Visible = false;
            lblMessage.Text = string.Format("{0} adet ziyaret listeleniyor.  ", listMovement.Count());
        }

        /// <summary>
        /// Geçmiş ziyaretlerin tümünü getir.
        /// </summary>
        private List<object> GetAllHistory()
        {
            var list =
            MovementsORM.Current.Select().Data.
            Where(x => x.Status == true).
            OrderByDescending(x => x.DateStart).
            Select(x => new { TC_No = x.VisitorTC, Ad_Soyad = x.NameSurname, Giriş_Saati = x.DateStart.ToString("f"), Çıkış_Saati = x.DateEnd.ToString("f"), IsVisitor = x.IsVisitor });
            return list.Cast<object>().ToList();
        }

        /// <summary>
        /// Geçmiş ziyaretlerde ad ve kimlik no ya göre arama yapar.
        /// </summary>
        private List<object> SearchWithNameandIdentity()
        {
            var list =
            MovementsORM.Current.Select().Data.
            Where(x => x.Status == true && x.NameSurname.ToLower().Contains(namesurname) && x.VisitorTC.Contains(identity)).
            OrderByDescending(x => x.DateStart).
            Select(x => new { TC_No = x.VisitorTC, Ad_Soyad = x.NameSurname, Giriş_Saati = x.DateStart.ToString("f"), Çıkış_Saati = x.DateEnd.ToString("f"), IsVisitor = x.IsVisitor });
            return list.Cast<object>().ToList();
        }

        /// <summary>
        /// Geçmiş ziyaretlerde tarihe göre arama yapar.
        /// </summary>
        private List<object> SearchWithDate()
        {
            var search = string.IsNullOrEmpty(identity) ? namesurname : identity;
            var list =
            MovementsORM.Current.Select().Data.
            Where(x => x.Status == true && (x.NameSurname.ToLower().Contains(search) || x.VisitorTC.Contains(search)) && x.DateStart.Date >= dateStart.DateTime && x.DateStart.Date <= dateEnd.DateTime).
            OrderByDescending(x => x.DateStart).
            Select(x => new { TC_No = x.VisitorTC, Ad_Soyad = x.NameSurname, Giriş_Saati = x.DateStart.ToString("f"), Çıkış_Saati = x.DateEnd.ToString("f"), IsVisitor = x.IsVisitor });
            return list.Cast<object>().ToList();
        }

        /// <summary>
        /// Form içindeki kontrolleri temizler.
        /// </summary>
        private void ClearItems()
        {
            Tools.ClearFormControls(this);
        }

        #endregion

        #region TextBox

        private void txtNS_TextChanged(object sender, EventArgs e)
        {
            namesurname = this.txtNS.Text;
            if (namesurname == "" && identity == "") BindHistoryList();
            else BindHistoryList(SearchWithNameandIdentity());
        }
        private void txtTC_TextChanged(object sender, EventArgs e)
        {
            identity = this.txtTC.Text;
            if (namesurname == "" && identity == "") BindHistoryList();
            else BindHistoryList(SearchWithNameandIdentity());
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion

        #region Buttons

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string dstart = this.dateStart.Text;
            string dend = this.dateEnd.Text;
            if (dateEnd.DateTime == dateEnd.Properties.MinValue) dateEnd.DateTime = DateTime.Now;
            if ((!string.IsNullOrEmpty(dstart) || !string.IsNullOrEmpty(dend))) BindHistoryList(SearchWithDate());
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearItems();
            this.BindHistoryList();
        }

        #endregion

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "history.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                gcHistory.ExportToXls(sfd.FileName);
            }
        }

        #endregion

    }
}
