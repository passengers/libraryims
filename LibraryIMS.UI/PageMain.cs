﻿using LibraryIMS.Common.Enums;
using LibraryIMS.Entity.Model;
using LibraryIMS.ORM;
using LibraryIMS.ORM.ORMRepo;
using LibraryService.Common;
using System;
using System.Linq;
using System.Windows.Forms;

namespace LibraryIMS.UI
{
    public partial class PageMain : Form
    {
        #region Field

        private bool isClicked = false;

        #endregion

        #region Ctor

        public PageMain()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Nesneleri kontrol et.
        /// </summary>        
        private void ObjectEnabled(int result)
        {
            panelTop.Visible = !Tools.IsConnected;
            var active = new ActiveList();
            switch (result)
            {
                case (int)AccessStatus.Manager:
                    panelManager.Visible = Tools.IsConnected;
                    GetForm(active,Tools.IsConnected);
                    break;
                case (int)AccessStatus.Member:
                    panelMember.Visible = Tools.IsConnected;
                    break;
            }            
        }

        /// <summary>
        /// Projedeki formları getir.
        /// </summary>
        private void GetForm(Form form_,bool connect=true)
        {
            panelCenter.Controls.Clear();
            if (connect)
            {                
                form_.MdiParent = this;
                form_.FormBorderStyle = FormBorderStyle.None;
                form_.Dock = DockStyle.Fill;
                panelCenter.Controls.Add(form_);
                form_.Show();
            }
        }

        #endregion

        #region Buttons

        private void btnMember_Click(object sender, EventArgs e)
        {
            var form = new PageLogin();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (Tools.IsConnected) ObjectEnabled(Tools.AccessLevel);
            }
        }

        private void btnVisitor_Click(object sender, EventArgs e)
        {

        }

        private void btnAddVisitor_Click(object sender, EventArgs e)
        {
            var form = new AddVisitor();
            GetForm(form);
        }

        private void btnOldVisitor_Click(object sender, EventArgs e)
        {
            var form = new VisitHistory();
            GetForm(form);
        }

        private void btnForbiddenVisitor_Click(object sender, EventArgs e)
        {
            var form = new AddForbiddenVisitor();
            GetForm(form);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            var form = new ActiveList();
            GetForm(form);
        }

        private void btnMemberInfo_Click(object sender, EventArgs e)
        {
            var form = new AddUser();
            GetForm(form);
        }

        private void btnBookInfo_Click(object sender, EventArgs e)
        {
            var form = new AddBook();
            GetForm(form);
        }

        private void btnDeliveryInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnSafetyInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnPenaltyInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnMClose_Click(object sender, EventArgs e)
        {
            isClicked = true;

            if (MessageBox.Show("Çıkış yapılacaktır. Onaylıyor musunuz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                var identity= MembersORM.Current.Select().Data.First(x => x.MemberID == Tools.Identity ).TC;
                var movement = MovementsORM.Current.Select().Data.Where(x => x.VisitorTC == identity && x.Status==false).ToList();

                var model = new Movements
                {
                    ID=movement[0].ID,
                    VisitorTC=movement[0].VisitorTC,
                    NameSurname=movement[0].NameSurname,
                    DateStart=movement[0].DateStart,
                    DateEnd=DateTime.Now,
                    IsVisitor=movement[0].IsVisitor,
                    Status=true
                };
                
                var result = MovementsORM.Current.Update(model);
                if (!result.IsSuccess)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Çıkış yapılırken bir hata meydana geldi!\n\n" + result.Message, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Tools.IsConnected = false;
                ObjectEnabled(Tools.AccessLevel);
            }
        }

        private void btnArchiveInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnProfileInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnBookList_Click(object sender, EventArgs e)
        {

        }

        private void btnMessages_Click(object sender, EventArgs e)
        {

        }

        private void btnDeptInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnUClose_Click(object sender, EventArgs e)
        {
            btnMClose.PerformClick();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Uygulama kapatılacaktır. Emin misiniz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                isClicked = true;
                Application.Exit();
            }
        }

        #endregion

        #region FormEvent

        private void PageMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isClicked)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion
    }
}
