﻿
namespace LibraryIMS.UI
{
    partial class AddVisitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddVisitor));
            this.gcActive = new DevExpress.XtraGrid.GridControl();
            this.gvActive = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.panelTop = new System.Windows.Forms.Panel();
            this.gbVisitor = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblClock = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateStart = new DevExpress.XtraEditors.DateEdit();
            this.dateEnd = new DevExpress.XtraEditors.DateEdit();
            this.btnService = new DevExpress.XtraEditors.SimpleButton();
            this.btnGiveUp = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtTC = new DevExpress.XtraEditors.TextEdit();
            this.txtSurname = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTelephone = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvActive)).BeginInit();
            this.panelGrid.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.gbVisitor.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSurname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcActive
            // 
            this.gcActive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcActive.Location = new System.Drawing.Point(12, 6);
            this.gcActive.MainView = this.gvActive;
            this.gcActive.Name = "gcActive";
            this.gcActive.Size = new System.Drawing.Size(1268, 340);
            this.gcActive.TabIndex = 85;
            this.gcActive.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvActive});
            // 
            // gvActive
            // 
            this.gvActive.Appearance.Empty.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gvActive.Appearance.Empty.Options.UseBackColor = true;
            this.gvActive.Appearance.Row.BackColor = System.Drawing.Color.Plum;
            this.gvActive.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gvActive.Appearance.Row.Options.UseBackColor = true;
            this.gvActive.Appearance.Row.Options.UseFont = true;
            this.gvActive.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gvActive.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gvActive.GridControl = this.gcActive;
            this.gvActive.Name = "gvActive";
            this.gvActive.OptionsBehavior.Editable = false;
            this.gvActive.OptionsBehavior.ReadOnly = true;
            this.gvActive.OptionsCustomization.AllowFilter = false;
            this.gvActive.OptionsFind.AllowFindPanel = false;
            this.gvActive.OptionsMenu.EnableColumnMenu = false;
            this.gvActive.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvActive.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvActive.OptionsView.ShowGroupPanel = false;
            this.gvActive.OptionsView.ShowIndicator = false;
            this.gvActive.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.gcActive);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 215);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(1296, 354);
            this.panelGrid.TabIndex = 87;
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.gbVisitor);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1296, 215);
            this.panelTop.TabIndex = 86;
            // 
            // gbVisitor
            // 
            this.gbVisitor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbVisitor.Controls.Add(this.panel2);
            this.gbVisitor.Controls.Add(this.dateStart);
            this.gbVisitor.Controls.Add(this.dateEnd);
            this.gbVisitor.Controls.Add(this.btnService);
            this.gbVisitor.Controls.Add(this.btnGiveUp);
            this.gbVisitor.Controls.Add(this.btnSave);
            this.gbVisitor.Controls.Add(this.txtName);
            this.gbVisitor.Controls.Add(this.txtTC);
            this.gbVisitor.Controls.Add(this.txtSurname);
            this.gbVisitor.Controls.Add(this.label15);
            this.gbVisitor.Controls.Add(this.label11);
            this.gbVisitor.Controls.Add(this.label16);
            this.gbVisitor.Controls.Add(this.label8);
            this.gbVisitor.Controls.Add(this.label14);
            this.gbVisitor.Controls.Add(this.txtTelephone);
            this.gbVisitor.Controls.Add(this.label13);
            this.gbVisitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.gbVisitor.Location = new System.Drawing.Point(12, 17);
            this.gbVisitor.Name = "gbVisitor";
            this.gbVisitor.Size = new System.Drawing.Size(1268, 187);
            this.gbVisitor.TabIndex = 1;
            this.gbVisitor.TabStop = false;
            this.gbVisitor.Text = "Ziyaretçi Kimlik Bilgileri";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblClock);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(920, 155);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(347, 30);
            this.panel2.TabIndex = 86;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(18, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "Aktif Ziyaretçi";
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblClock.Location = new System.Drawing.Point(164, 7);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(0, 18);
            this.lblClock.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.ForeColor = System.Drawing.Color.Plum;
            this.label5.Location = new System.Drawing.Point(3, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 25);
            this.label5.TabIndex = 20;
            this.label5.Text = "•";
            // 
            // dateStart
            // 
            this.dateStart.EditValue = null;
            this.dateStart.Enabled = false;
            this.dateStart.Location = new System.Drawing.Point(709, 94);
            this.dateStart.Name = "dateStart";
            this.dateStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateStart.Properties.Mask.EditMask = "G";
            this.dateStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateStart.Size = new System.Drawing.Size(186, 20);
            this.dateStart.TabIndex = 7;
            // 
            // dateEnd
            // 
            this.dateEnd.EditValue = null;
            this.dateEnd.Enabled = false;
            this.dateEnd.Location = new System.Drawing.Point(709, 139);
            this.dateEnd.Name = "dateEnd";
            this.dateEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEnd.Properties.Mask.EditMask = "G";
            this.dateEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEnd.Size = new System.Drawing.Size(186, 20);
            this.dateEnd.TabIndex = 8;
            // 
            // btnService
            // 
            this.btnService.Appearance.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnService.Appearance.Options.UseBackColor = true;
            this.btnService.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnService.ImageOptions.SvgImage")));
            this.btnService.ImageOptions.SvgImageSize = new System.Drawing.Size(20, 20);
            this.btnService.Location = new System.Drawing.Point(389, 48);
            this.btnService.Name = "btnService";
            this.btnService.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnService.Size = new System.Drawing.Size(28, 26);
            this.btnService.TabIndex = 85;
            this.btnService.Click += new System.EventHandler(this.btnService_Click);
            // 
            // btnGiveUp
            // 
            this.btnGiveUp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGiveUp.ImageOptions.Image")));
            this.btnGiveUp.Location = new System.Drawing.Point(934, 113);
            this.btnGiveUp.Name = "btnGiveUp";
            this.btnGiveUp.Size = new System.Drawing.Size(121, 32);
            this.btnGiveUp.TabIndex = 6;
            this.btnGiveUp.Text = "Vazgeç";
            this.btnGiveUp.Click += new System.EventHandler(this.btnGiveUp_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.ImageOptions.SvgImageSize = new System.Drawing.Size(25, 25);
            this.btnSave.Location = new System.Drawing.Point(934, 64);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(121, 32);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Kaydet";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(197, 95);
            this.txtName.Name = "txtName";
            this.txtName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtName.Size = new System.Drawing.Size(186, 22);
            this.txtName.TabIndex = 2;
            // 
            // txtTC
            // 
            this.txtTC.Location = new System.Drawing.Point(197, 50);
            this.txtTC.Name = "txtTC";
            this.txtTC.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtTC.Properties.Mask.EditMask = "\\d{0,11}";
            this.txtTC.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtTC.Properties.Mask.ShowPlaceHolders = false;
            this.txtTC.Size = new System.Drawing.Size(186, 22);
            this.txtTC.TabIndex = 1;
            this.txtTC.Leave += new System.EventHandler(this.txtTC_Leave);
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(197, 140);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtSurname.Size = new System.Drawing.Size(186, 22);
            this.txtSurname.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(25, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 17);
            this.label15.TabIndex = 81;
            this.label15.Text = "TC Kimlik No:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(507, 142);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "Ziyaret Bitiş Tarihi:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(507, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(136, 17);
            this.label16.TabIndex = 83;
            this.label16.Text = "Ziyaretçi Telefon:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(507, 97);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Ziyaret Tarihi:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(25, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 17);
            this.label14.TabIndex = 80;
            this.label14.Text = "Ziyaretçi Adı:";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(709, 50);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.txtTelephone.Properties.Mask.EditMask = "(999) 000-0000";
            this.txtTelephone.Properties.Mask.IgnoreMaskBlank = false;
            this.txtTelephone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.txtTelephone.Size = new System.Drawing.Size(186, 22);
            this.txtTelephone.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(25, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(130, 17);
            this.label13.TabIndex = 79;
            this.label13.Text = "Ziyaretçi Soyadı:";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // AddVisitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1296, 569);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddVisitor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddVisitor";
            ((System.ComponentModel.ISupportInitialize)(this.gcActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvActive)).EndInit();
            this.panelGrid.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.gbVisitor.ResumeLayout(false);
            this.gbVisitor.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSurname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelephone.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraGrid.GridControl gcActive;
        private DevExpress.XtraGrid.Views.Grid.GridView gvActive;
        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.GroupBox gbVisitor;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.DateEdit dateStart;
        private DevExpress.XtraEditors.DateEdit dateEnd;
        private DevExpress.XtraEditors.SimpleButton btnService;
        private DevExpress.XtraEditors.SimpleButton btnGiveUp;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtTC;
        private DevExpress.XtraEditors.TextEdit txtSurname;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.TextEdit txtTelephone;
        private System.Windows.Forms.Label label13;
    }
}