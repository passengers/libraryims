﻿using System;
using System.Data;
using System.Linq;
using LibraryIMS.ORM;
using System.Windows.Forms;
using LibraryService.Common;
using LibraryIMS.ORM.ORMRepo;
using LibraryIMS.Entity.Model;
using LibraryIMS.Common.Enums;
using System.Collections.Generic;

namespace LibraryIMS.UI
{
    public partial class ActiveList : Form
    {

        #region Field

        // guId.
        private int guID;

        #endregion

        #region Ctor

        public ActiveList()
        {
            InitializeComponent();
            BindActiveList();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Metoda gönderilen listeyi gride bas.
        /// </summary>
        private void BindActiveList(List<object> listMovement = null)
        {
            if (listMovement == null) listMovement = this.GetActiveList();

            gcMovement.DataSource = listMovement;
            gvMovement.Columns[0].Visible = false;
            gvMovement.Columns[4].Visible = false;
            panelCenter.Visible = listMovement.Count() > 0;
            lblMessage.Text = string.Format("{0} adet ziyaret listeleniyor.  ", listMovement.Count());
        }

        /// <summary>
        /// Sistemde aktif ziyaret listesini getirir.
        /// </summary>
        private List<object> GetActiveList()
        {
            var actList =
            MovementsORM.Current.Select().Data.
            Where(x => x.Status == false).
            OrderByDescending(x => x.DateStart).
            Select(x => new { ID = x.ID, TC_No = x.VisitorTC, Ad_Soyad = x.NameSurname, Giriş_Saati = x.DateStart.ToString("dd MMMM HH:mm:ss"), IsVisitor = x.IsVisitor });

            return actList.Cast<object>().ToList();
        }

        /// <summary>
        /// Kimlik kartı numarasına ve isme göre ziyaret ara.
        /// </summary>
        private void SearchVisit()
        {
            string searchText = this.txtSearch.Text;

            if (String.IsNullOrEmpty(searchText))
                this.BindActiveList(null);

            var actList =
            MovementsORM.Current.Select().Data.
            Where(x => x.Status == false && (x.NameSurname.ToLower().Contains(searchText) || x.VisitorTC.Contains(searchText))).
            OrderByDescending(x => x.DateStart).
            Select(x => new { ID = x.ID, TC_No = x.VisitorTC, Ad_Soyad = x.NameSurname, Giriş_Saati = x.DateStart.ToString("dd MMMM HH:mm:ss"), IsVisitor = x.IsVisitor });

            this.BindActiveList(actList.Cast<object>().ToList());
        }

        /// <summary>
        /// Ziyaret bilgilerini güncelle ekranını aç.
        /// </summary>
        private void UpdateVisit(object sender, EventArgs e)
        {
            var get = MovementsORM.Current.Select().Data.First(x => x.ID == guID);
            if (!get.IsVisitor.Value)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Üye bilgileri güncelleme işlemi bu kanaldan yapılamaz!", "Uyarı",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var form = new AddVisitor(guID);
            form.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            form.ControlBox = false;
            var result = form.ShowDialog();
            
            if (result == DialogResult.OK)
            {
                form.Close();
                this.BindActiveList();
            }
        }

        /// <summary>
        /// Ziyareti sonlandır ve durumu guncelle.
        /// </summary>
        private void EndOfVisit(object sender, EventArgs e)
        {
            var result = DevExpress.XtraEditors.XtraMessageBox.Show("Ziyaret sonlandırılacaktır. Emin misiniz?", "Uyarı",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                var movement = MovementsORM.Current.Select().Data.First(x => x.ID == guID);
                if (movement != null)
                {
                    movement.DateEnd = DateTime.Now;
                    movement.Status = true;
                    var model = MovementsORM.Current.Update(movement);

                    if (model.Data)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.BindActiveList();
                    }
                    else
                    {
                        string error = model.Message;
                        DevExpress.XtraEditors.XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!\n\n" + error, "Hata",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        /// <summary>
        /// Ziyaretçiyi yasaklı listesine ekler.
        /// </summary>
        private void ForbiddenVisitor(object sender, EventArgs e)
        {
            DialogResult result = DialogResult.No;
            string error;
            bool Data=false;
            var get = MovementsORM.Current.Select().Data.First(x => x.ID == guID);

            if (get.IsVisitor.Value)
                result = DevExpress.XtraEditors.XtraMessageBox.Show("Ziyaretçi yasaklı listeye eklenecek. Emin misiniz?", "Uyarı",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            else
                result = DevExpress.XtraEditors.XtraMessageBox.Show("Üye yasaklanacak! Emin misiniz?", "Uyarı",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                if (get.IsVisitor.Value)
                {
                    var model = new ForbiddenVisitor
                    {
                        VisitorTC = get.VisitorTC,
                        NameSurname = get.NameSurname.Replace("\r\n", string.Empty),
                        BlockerTC = MembersORM.Current.Select().Data.First(x => x.MemberID == Tools.Identity).TC,
                        DateBlock = DateTime.Now,
                        BanSituation = (int)BanStatus.EntryBan
                    };
                    var forbidden = ForbiddenVisitorORM.Current.Insert(model);
                    error = forbidden.Message;
                    Data = forbidden.Data;
                }
                else
                {
                    var model = MembersORM.Current.Select().Data.First(x => x.TC == get.VisitorTC);
                    model.BanSituation = (int)BanStatus.EntryBan;
                    var member = MembersORM.Current.Update(model);
                    error = member.Message;
                    Data = member.Data;
                }

                if (Data)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("İşleminiz başarılı bir şekilde gerçekleşmiştir.", "Bilgi",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("İşlem gerçekleşirken bir hata oluştu!\n\n" + error, "Hata",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion

        #region Timer

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblClock.Text = DateTime.Now.ToString("dd.MMMM.yyyy HH:mm:ss");
        }

        #endregion

        #region Events

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.SearchVisit();
        }

        #endregion

        #region Grid

        /// <summary>
        /// Grid üzerindeki kullanıcılara mouse butonu ile işlem uygular.
        /// </summary>
        private void gcMovement_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var cm = new ContextMenu();
                cm.MenuItems.Add(new MenuItem("Ziyareti Sonlandır", this.EndOfVisit));
                cm.MenuItems.Add(new MenuItem("Ziyareti Güncelle", this.UpdateVisit));
                cm.MenuItems.Add(new MenuItem("Ziyaretçiyi / Üyeyi Yasakla", this.ForbiddenVisitor));

                int rowIndex = gvMovement.CalcHitInfo(e.X, e.Y).RowHandle;
                if (rowIndex > -1)
                {
                    gvMovement.ClearSelection();
                    gvMovement.SelectRow(rowIndex);
                    guID = Convert.ToInt32(gvMovement.GetRowCellValue(rowIndex, "ID"));
                    cm.Show(gcMovement, new System.Drawing.Point(e.X, e.Y));
                }
            }
        }

        #endregion

        #region LinkLabel

        private void lnkExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "active.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                gcMovement.ExportToXls(sfd.FileName);
            }
        }

        #endregion

    }
}
