﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class ContactDto
    {
        public Contact Contact { get; set; }
    }
}
