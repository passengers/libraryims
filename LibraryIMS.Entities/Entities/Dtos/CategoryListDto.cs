﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CategoryListDto
    {
        public IList<Category> Categories { get; set; }
    }
}
