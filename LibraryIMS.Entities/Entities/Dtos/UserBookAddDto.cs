﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class UserBookAddDto
    {
        public int UserId { get; set; }
        public int BookId { get; set; }
    }
}
