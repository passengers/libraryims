﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class UserListDto
    {
        public IList<User> Users { get; set; }
    }
}
