﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CommentAddDto
    {
        public int UserId { get; set; }
        public string CommentText { get; set; }
        public int BookId { get; set; }
    }
}
