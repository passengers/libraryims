﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class BookCategoryAddDto
    {
        public int BookId { get; set; }
        public int CategoryId { get; set; }
    }
}