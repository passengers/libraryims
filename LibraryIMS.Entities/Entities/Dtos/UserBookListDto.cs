﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class UserBookListDto
    {
        public IList<UserBook> Userbooks { get; set; }
    }
}
