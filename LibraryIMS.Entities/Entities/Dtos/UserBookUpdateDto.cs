﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class UserBookUpdateDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
    }
}
