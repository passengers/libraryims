﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class ContactAddDto
    {
        public int UserId { get; set; }
        public string Content { get; set; }
        public GeneralStatus GeneralStatus { get; set; }
    }
}