﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class UserDto
    {
        public User User { get; set; }
    }
}
