﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CategoryDto
    {
        public Category Category { get; set; }
    }
}
