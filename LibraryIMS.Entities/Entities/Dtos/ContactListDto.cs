﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;


namespace LibraryIMS.Entities.Entities.Dtos
{
    public class ContactListDto
    {
        public IList<Contact> Contacts { get; set; }
    }
}