﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CategoryAddDto
    {
        public int ParentId { get; set; }
        public string Name { get; set; }
        public GeneralStatus GeneralStatus { get; set; }
    }
}
