﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class FavoriteBookDto
    {
        public FavoriteBook FavoriteBook { get; set; }
    }
}
