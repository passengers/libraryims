﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class WriterListDto
    {
        public IList<Writer> Writers { get; set; }
    }
}