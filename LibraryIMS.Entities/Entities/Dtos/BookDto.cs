﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class BookDto
    {
        public Book Book { get; set; }
    }
}
