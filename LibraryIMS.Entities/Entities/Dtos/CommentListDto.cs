﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CommentListDto
    {
        public IList<Comment> Comments { get; set; }
    }
}
