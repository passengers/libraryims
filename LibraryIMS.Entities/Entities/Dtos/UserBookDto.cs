﻿using LibraryIMS.Entities.Entities.Concrete;
namespace LibraryIMS.Entities.Entities.Dtos
{
    public class UserBookDto
    {
        public UserBook Userbook { get; set; }
    }
}
