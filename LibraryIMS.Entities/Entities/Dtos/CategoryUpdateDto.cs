﻿namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CategoryUpdateDto
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public GeneralStatus GeneralStatus { get; set; }
    }
}
