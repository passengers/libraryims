﻿using LibraryIMS.Entities.Entities.Concrete;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class FavoriteBookListDto
    {
        public IList<FavoriteBook> FavoriteBooks { get; set; }
    }
}
