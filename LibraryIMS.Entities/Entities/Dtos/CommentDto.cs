﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class CommentDto
    {
        public Comment Comment { get; set; }
    }
}
