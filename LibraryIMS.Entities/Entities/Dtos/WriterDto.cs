﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class WriterDto
    {
        public Writer Writer { get; set; }
    }
}