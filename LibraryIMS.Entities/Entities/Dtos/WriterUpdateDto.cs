﻿using System;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class WriterUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Biography { get; set; }
        public string Picture { get; set; }
        public DateTime DateOfBirth { get; set; }
        public GeneralStatus GeneralStatus { get; set; }
    }
}