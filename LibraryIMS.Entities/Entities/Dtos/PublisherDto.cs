﻿using LibraryIMS.Entities.Entities.Concrete;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class PublisherDto
    {
        public Publisher Publisher { get; set; }
    }
}
