﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class FavoriteBookAddDto
    {
        public int UserId { get; set; }
        public int BookId { get; set; }
    }
}
