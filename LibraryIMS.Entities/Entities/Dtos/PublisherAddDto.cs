﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryIMS.Entities.Entities.Dtos
{
    public class PublisherAddDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public AccessStatus AccessStatus { get; set; }
        public GeneralStatus GeneralStatus { get; set; }
    }
}
