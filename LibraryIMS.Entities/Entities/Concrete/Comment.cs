﻿using LibraryIMS.Core.Entities.Abstract;

namespace LibraryIMS.Entities.Entities.Concrete
{
    public class Comment : EntityBase
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public string CommentText { get; set; }
    }
}
