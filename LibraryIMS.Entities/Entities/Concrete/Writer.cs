﻿using System;
using LibraryIMS.Core.Entities.Abstract;
using System.Collections.Generic;

namespace LibraryIMS.Entities.Entities.Concrete
{
    public class Writer : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Biography { get; set; }
        public string Picture { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int NumberOfBooks { get; set; } = 0;
        public GeneralStatus GeneralStatus { get; set; } = GeneralStatus.Active;

        public ICollection<Book> Books { get; set; }
    }
}
