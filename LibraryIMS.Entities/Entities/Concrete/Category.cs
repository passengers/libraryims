﻿using System.Collections.Generic;
using LibraryIMS.Core.Entities.Abstract;

namespace LibraryIMS.Entities.Entities.Concrete
{
    public class Category : IEntity
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public GeneralStatus GeneralStatus { get; set; } = GeneralStatus.Active;

        public ICollection<BookCategory> BookCategories { get; set; }
    }
}
