﻿using System;
using LibraryIMS.Core.Entities.Abstract;

namespace LibraryIMS.Entities.Entities.Concrete
{
    public class UserBook : IEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
        public BookStatus BookStatus { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
